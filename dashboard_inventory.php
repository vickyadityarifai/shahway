<?php
include('database_connection.php');
?>
<?php
$active_page = 'inventory';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}

.table_list_box{
    padding: 10px 0;
}
.orders_list button {
  background: #69F;
  border-radius: 5px;
  font-family: Raleway;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  display: inline-block;
  align-items: center;
  text-transform: capitalize;
  color: #FFFFFF;
  padding: 10px 20px;
  margin: 15px 0 5px 0;
  border: 0;
}
.orders_list input {
  border: 1px solid #dfdfdf;
  padding: 3px 8px;
}
.actions .update_inventory {
  background: #69F;
  border-radius: 5px;
  font-family: Raleway;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  color: #FFFFFF;
  padding: 5px 10px;
  margin: 0 5px 0 0;
}
.actions .update_inventory.loading {
  background: #E1C800;
}
.actions .update_inventory.done {
  background: #00971F;
}
.refresh_inventory {
  color: #69F;
  text-decoration: underline;
  font-size: 16px;
  margin: 0 0 10px 0;
  font-weight: 700;
} 
</style>
<?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 30;
                $offset = ($pageno-1) * $no_of_records_per_page; 
                ?>
                
                <div class="recently_view">
                    <div class="orders_list">
                      <!-- <a class="refresh_inventory" href="/?get_inventory">Refresh inventory from Walmart</a> | <a class="refresh_inventory" href="/?get_inventory1">Refresh WFS inventory from Walmart</a> -->
                        <form class="filter_orders">
                            <!-- <div class="input_box">
                                <label>Product Name</label>
                                <input type="text" name="ProductName" placeholder="Product Name" value="">
                            </div> -->
                            <div class="input_box">
                                <label>SKU</label>
                                <?php
                                $EnterSKU = '';
                                if(isset($_GET['EnterSKU'])){
                                    $EnterSKU = $_GET['EnterSKU'];
                                }
                                ?>
                                <input value="<?php echo $EnterSKU; ?>" type="text" name="EnterSKU" placeholder="Enter SKU" value="">
                            </div>
                            <div class="input_box">
                                <label>Product Name</label>
                                <?php
                                $search_product_name = '';
                                if(isset($_GET['product_name'])){
                                    $search_product_name = $_GET['product_name'];
                                }
                                ?>
                                <input type="text" name="product_name" placeholder="Enter Product Name" value="<?php echo $search_product_name; ?>">
                            </div>
                            <div class="input_box"> 
                                <label>Status</label>
                                <select name="status">
                                    <option value="">All</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']=='wfs'){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="wfs" <?php echo $selected; ?>>WFS</option>
                                    
                                </select>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
                        <div class="table_list_outer orders_list">
                            <!-- <form method="post" > -->
                                <!-- <input type="hidden" name="update_shipping"> -->
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    SKU
                                </div>
                                <div class="box">
                                    WFS Avail to Sale Qty
                                </div>
                                <div class="box">
                                    WFS On Hand Qty
                                </div>
                                <?php
                                if(isset($_GET['status']) && $_GET['status']=='wfs'){

                                }else{
                                ?>
                                <div class="box">
                                    inputQty
                                </div>
                                <div class="box">
                                    Action
                                </div>
                                <?php } ?>
                            </div>
                            
                            <?php
                            

                            $filter_code = '';
                            

                            

                            if(isset($_GET['status']) && !empty($_GET['status'])){
                                if(isset($_GET['EnterSKU'])){
                                    $ProductName = $_GET['product_name'];
                                    $EnterSKU = $_GET['EnterSKU'];
                                    if(!empty($EnterSKU)){
                                        $filter_code.= ' WHERE wfs_inventory.sku="'.$EnterSKU.'"';
                                    }else if($ProductName){
                                        $filter_code .= " WHERE walmart_items.product_name LIKE '%".$ProductName."%'";
                                    }
                                }


                                $get_orders_count = "SELECT * FROM wfs_inventory JOIN walmart_items ON wfs_inventory.sku=walmart_items.sku $filter_code $sort_by";
                                $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                                $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);



                                
                                $get_orders = "SELECT * FROM wfs_inventory JOIN walmart_items ON wfs_inventory.sku=walmart_items.sku $filter_code $sort_by LIMIT $offset, $no_of_records_per_page;";
                            }else{
                                if(isset($_GET['EnterSKU'])){
                                    $ProductName = $_GET['product_name'];
                                    $EnterSKU = $_GET['EnterSKU'];
                                    if(!empty($EnterSKU)){
                                        $filter_code.= ' WHERE inventory.sku="'.$EnterSKU.'"';
                                    }else if($ProductName){
                                        $filter_code .= " WHERE walmart_items.product_name LIKE '%".$ProductName."%'";
                                    }
                                }


                                $get_orders_count = "SELECT * FROM inventory JOIN walmart_items ON inventory.sku=walmart_items.sku $filter_code";
                                $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                                $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);



                                
                                $get_orders = "SELECT * FROM inventory JOIN walmart_items ON inventory.sku=walmart_items.sku $filter_code LIMIT $offset, $no_of_records_per_page;";
                            }


                            
                            // $get_orders = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id WHERE orders.shipNode = 'SellerFulfilled' AND NULLIF(trackingNumber, '') IS NOT NULL  ORDER BY id desc LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);
                            

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;

                                    $sku = $order['sku'];
                                    $get_wfs = "SELECT * FROM wfs_inventory WHERE sku='".$sku."'"; 
                                    $get_wfs = mysqli_query($conn, $get_wfs);
                                    $row_get_wfs = $get_wfs->fetch_assoc();

                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <?php
                                    $sku = $order['sku'];
                                    $walmart_item_link = 'link_not_found';
                                    $link_found = '';
                                    $get_walmart_items = "SELECT * FROM walmart_items WHERE sku='".$sku."'"; 
                                    $walmart_items = mysqli_query($conn, $get_walmart_items);
                                    $row_walmart_items = $walmart_items->fetch_assoc();
                                    if(!empty($row_walmart_items)){
                                        $walmart_item_link = $row_walmart_items['wpid'];
                                        $link_found = 'link_found';
                                    }
                                    
                                    ?>
                                    <a class="<?php echo $link_found; ?>" href="https://www.walmart.com/ip/<?php echo $walmart_item_link; ?>" target="_blank">
                                        <?php echo $order['product_name']; ?>
                                    <br>
                                    <?php echo $order['sku']; ?>
                                    </a>

                                    
                                    
                                </div>
                                <div class="box">
                                    <?php
                                    
                                    if(!empty($row_get_wfs)){
                                        echo $row_get_wfs['availToSellQty'];
                                    }
                                    ?>
                                </div>
                                <div class="box">
                                    <?php
                                    if(!empty($row_get_wfs)){
                                        echo $row_get_wfs['onHandQty'];
                                    }
                                    ?>
                                </div>
                                <?php
                                if(isset($_GET['status']) && $_GET['status']=='wfs'){
                                    
                                }else{
                                ?>
                                <div class="box">
                                    
                                        <input type="text" id="inputQty_<?php echo $k+($no_of_records_per_page*($pageno-1)); ?>" name="inputQty" placeholder="Amount" value="<?php echo $order['inputQty'] ?>">
                                      
                                    
                                </div>
                                
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" data-shipNode="<?php echo $order['shipNode']; ?>" data-sku="<?php echo $order['sku']; ?>" class="update_inventory update_inventory_<?php echo $k+($no_of_records_per_page*($pageno-1)); ?>" data-id="<?php echo $k+($no_of_records_per_page*($pageno-1)); ?>">Update</a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <?php  
                        // }
                        } } ?>
                            <!-- </form> -->
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                

                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_inventory.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_inventory.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_inventory.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <script>
    $(window).scroll(function() {
       if($(this).scrollTop() > 240) {
         $(".table_list_box.table_list_heading").addClass("fixed");
       }else{
        $(".table_list_box.table_list_heading").removeClass("fixed");
       }
    });
</script>
<?php
include('dashboard_footer.php');
?>