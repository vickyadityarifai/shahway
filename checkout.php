<?php include 'header.php';?>

            
            
            <div class="page_title checkout_pages">
                <div class="big_container">
                    <div class="page_title_inner">
                        <h2>Enter your shipping address</h2>
                        <p><a class="all">Checkout</a> - <a class="current">Enter your shipping address</a></p>
                        <div class="checkout_process">
                            <div class="bg_line"></div>
                            <ul>
                                <li class="active">
                                    <span class="number">1</span>
                                    <span class="text">Delivery</span>
                                </li>
                                <li>
                                    <span class="number">2</span>
                                    <span class="text">Payment</span>
                                </li>
                                <li>
                                    <span class="number">3</span>
                                    <span class="text">Review</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="checkout_form">
                <div class="container">
                    <div class="checkout_form_inner">
                        <h2>Enter your shipping address</h2>
                        <form>
                            <div class="form_box">
                                <label>Full name </label>
                                <input type="text">
                            </div>
                            <div class="form_box">
                                <label>Flat / Other (optional)</label>
                                <input type="text">
                            </div>
                            <div class="form_box">
                                <label>Street address </label>
                                <input type="text">
                            </div>
                            <div class="form_box">
                                <label>City / Suburb </label>
                                <input type="text">
                            </div>
                            <div class="2_col">
                                <div class="form_box">
                                    <label>State </label>
                                    <select>
                                        <option>Select State</option>
                                    </select>
                                </div>
                                <div class="form_box">
                                    <label>Postal code</label>
                                    <input type="text">
                                </div>
                            </div>
                            <div class="form_box">
                                <label>Country</label>
                                <select>
                                    <option>Select Country</option>
                                </select>
                            </div>
                            <a class="continue" href="/payment.php">
                                Continue to payment
                            </a>
                        </form>
                    </div>
                </div>
            </div>
            

            

        
            
            

<?php include 'footer.php';?>