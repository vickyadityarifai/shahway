            <div class="footer_top">
                <div class="container">
                    <div class="footer_top_inner">
                        <div class="box_1">
                            <h3>Customer Service</h3>
                            <ul>
                                <li><a href="/help.php">Help</a></li>
                                <li><a href="#">Track My Order</a></li>
                                <li><a href="#">Returns & Refunds</a></li>
                                <li><a href="#">Cancellations</a></li>
                                <li><a href="#">FAQs</a></li>
                                <li><a href="/contact-us.php">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="box_1">
                            <h3>Shop</h3>
                            <ul>
                                <li><a href="/help.php">Help</a></li>
                                <li><a href="#">Track My Order</a></li>
                                <li><a href="#">Returns & Refunds</a></li>
                                <li><a href="#">Cancellations</a></li>
                                <li><a href="#">FAQs</a></li>
                                <li><a href="/register_seller.php">Become a Seller</a></li>
                                <li><a href="/contact-us.php">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="box_1">
                            <h3>About</h3>
                            <ul>
                                <li><a href="/about-us.php">About Us</a></li>
                                <li><a href="#">Policies</a></li>
                                <li><a href="#">Investors</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Press</a></li>
                                <li><a href="#">Impact</a></li>
                                <li><a href="#">Legal imprint</a></li>
                            </ul>
                        </div>
                        <div class="box_1">
                            <h3>Help</h3>
                            <ul>
                                <li><a href="/help.php">Help Centre</a></li>
                                <li><a href="#">Trust and safety</a></li>
                                <li><a href="#">Privacy settings</a></li>
                            </ul>
                            <ul class="social_account">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyrights">
                <div class="container">
                    <div class="copyrights_inner">
                        <div class="left">
                            © 2022 imboo. All rights reserved.
                        </div>
                        <div class="right">
                            <img src="/assets/img/image 21.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
          <!-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> -->
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/custom.js?<?php echo time(); ?>"></script>
          <script>
  $( function() {
    $( "#slider_price_range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 20, 400 ],
      slide: function( event, ui ) {
        var val0 = ui.values[ 0 ];
        var val1 = ui.values[ 1 ];
        var number0 = val0;
        var number1 = val1;
        // $( "#indoorsize" ).html(  number0 + " m<span class='zee_min'>2</span> <span class='text_right'>" + number1 +" m<span class='zee_min'>2</span></span>" );
        $( ".price_range_values .left span" ).html(  val0 );
        $( ".price_range_values .right span" ).html(  val1 );
      }
    });
  } );

  </script>
    </body>
</html>