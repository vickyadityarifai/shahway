<?php include 'header.php';?>

<div class="page_title">
    <div class="big_container">
        <div class="page_title_inner">
            <h2>Thank You</h2>
            <p><a class="all">Checkout</a> - <a class="current">Thank You</a></p>
        </div>
    </div>
</div>     

            <div class="contact_support thankyou_page">
                <div class="container">
                    <div class="about_page_section_inner">
                        <div class="about_page_section_inner_inner">
                            <div class="image">
                                <img src="/assets/img/thank-you(1) 1.png">
                            </div>
                            <h2>Thanks For Shopping</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue sodales arcu vel interdum. Proin nibh nisl, blandit ac sem vitae.</p>
                            <a href="/">Go Back to home page</a>
                        </div>
                    </div>
                </div>
            </div>
            

            

        
            
            

<?php include 'footer.php';?>