<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard.php"  ><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/wishlist.php" ><i class="fas fa-heart"></i> Wishlists</a></li>
                        <li><a href="/dashboard_orders.php"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_messages.php" class=""><i class="fas fa-comment-dots"></i> Messages</a></li>
                        <li><a href="/dashboard_disputes.php"><i class="fas fa-people-carry"></i> Disputes</a></li>
                        <li><a href="/support.php" class="active"><i class="fas fa-headset"></i> Support</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <input type="text" placeholder="Search Here">
                        <button><i class="fas fa-search"></i></button>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                
                <div class="recently_view">
                    <h2>
                        Support
                    </h2>
                    <div class="contact_form">
                        <div class="col_2">
                            <div class="form_row">
                                <input type="text" placeholder="First Name">
                            </div>
                            <div class="form_row">
                                <input type="text" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="col_2">
                            <div class="form_row">
                                <input type="text" placeholder="Enter email address">
                            </div>
                            <div class="form_row">
                                <input type="text" placeholder="Phone number">
                            </div>
                        </div>
                        <div class="col_1">
                            <div class="form_row">
                                <textarea placeholder="Enter Message"></textarea>
                            </div>
                        </div>
                        <button>Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>