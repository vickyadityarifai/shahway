<?php include 'header.php';?>

            
            
            <div class="page_title">
                <div class="big_container">
                    <div class="page_title_inner">
                        <h2>Jewellery & Accessories</h2>
                        <p><a class="all">All</a> - <a class="current">Jewellery & Accessories</a></p>
                        <p class="total_results">( 1,977,173 results,)</p>
                    </div>
                </div>
            </div>
            

            <div class="product_image_details">
                <div class="big_container">
                    <div class="product_image_details_inner">
                        <div class="left">
                            <div class="image_gallery">
                                <div class="top_images">
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                </div>
                                <div class="for_images">
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                </div>
                                <div class="slider_arrows">
                                    <span class="for_images_arrow left"><i class="fas fa-angle-left"></i></span>
                                    <span class="for_images_arrow right"><i class="fas fa-angle-right"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="right">
                            <div class="product_details">
                                <div class="rating_box">
                                    <h2><a href="/seller.php">DinaJewelryShop</a></h2>
                                    <div class="sales">
                                        <span>57 sales</span>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="product_name">3 Natural Diamonds • 14/18k Gold Three Stone Ring • 
        Anniversary Gift</h2>
                                <div class="price_box">
                                    <span>In stock</span>
                                    <h2>USD 24.61</h2>
                                    <h3>You save USD 6.16 (20%)</h3>
                                    <p>Local taxes included (where applicable)</p>
                                </div>
                                <div class="input_box">
                                    <h2>Ring size</h2>
                                    <select>
                                        <option>21/3 US</option>
                                    </select>
                                </div>
                                <div class="input_box">
                                    <h2>Material</h2>
                                    <select>
                                        <option>14k Solid Gold</option>
                                    </select>
                                </div>
                                <button onclick="location.href='http://imboo.sialkotians.com/cart.php'">Add to basket</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="reviews_and_details">
                <div class="big_container">
                    <div class="reviews_and_details_inner">
                        <div class="left">
                            <div class="toggle_heading active">
                                <h2>Details<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            <div class="toggle_heading">
                                <h2>Description<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            <div class="toggle_heading">
                                <h2>Delivery and return policies<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            <div class="toggle_heading">
                                <h2>FAQs<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            <div class="toggle_heading">
                                <h2>Meet your sellers<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                        </div>
                        <div class="right">
                            <h2 class="total_reviews">18 shop reviews</h2>
                            <div class="reviews_list">
                                <div class="reviews_list_box">
                                    <div class="user_image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="details">
                                        <h2>Rachael 20 Dec, 2020</h2>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sodales sagittis viverra. Vivamus tristique metus sit amet suscipit feugiat. Nunc faucibus ligula vitae porta commodo. Mauris vitae velit mauris. Etiam tincidunt nulla eu diam egestas ultrices.
                                        </div>
                                        <div class="product">
                                            <a href="#">
                                                <span class="image">
                                                    
                                                </span>
                                                <span class="name">
                                                    Diamonds Rings• Solitaire Rings • Anniversary Gift
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews_list_box">
                                    <div class="user_image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="details">
                                        <h2>Rachael 20 Dec, 2020</h2>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sodales sagittis viverra. Vivamus tristique metus sit amet suscipit feugiat. Nunc faucibus ligula vitae porta commodo. Mauris vitae velit mauris. Etiam tincidunt nulla eu diam egestas ultrices.
                                        </div>
                                        <div class="product">
                                            <a href="#">
                                                <span class="image">
                                                    
                                                </span>
                                                <span class="name">
                                                    Diamonds Rings• Solitaire Rings • Anniversary Gift
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews_list_box">
                                    <div class="user_image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="details">
                                        <h2>Rachael 20 Dec, 2020</h2>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sodales sagittis viverra. Vivamus tristique metus sit amet suscipit feugiat. Nunc faucibus ligula vitae porta commodo. Mauris vitae velit mauris. Etiam tincidunt nulla eu diam egestas ultrices.
                                        </div>
                                        <div class="product">
                                            <a href="#">
                                                <span class="image">
                                                    
                                                </span>
                                                <span class="name">
                                                    Diamonds Rings• Solitaire Rings • Anniversary Gift
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pagination">
                                <ul>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#"><i class="fas fa-ellipsis-h"></i></a></li>
                                    <li><a href="#">20</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            

<?php include 'footer.php';?>