<?php include 'header.php';?>

            
            
            <div class="page_title">
                <div class="big_container">
                    <div class="page_title_inner">
                        <h2>Mother of Pearl title</h2>
                        <p><a class="all">All - </a> <a class="all"> shop</a> - <a class="current">Mother of Pearl title</a></p>
                    </div>
                </div>
            </div>
            <div class="seller_profile_info">
                <div class="big_container">
                    <div class="seller_profile_info_inner">
                        <div class="cover_photo" style="background-image: url('/assets/img/Rectangle 1513.png')">
                    
                        </div>
                        <div class="seller_profile_info_box">
                            <div class="left">
                                <div class="image_outer">
                                    <div class="image">
                                        <img src="/assets/img/Rectangle 1514.png">
                                    </div>
                                </div>
                                <div class="seller_profile_info_content">
                                    <h2>Avita Jewellery</h2>
                                    <p>Mother of Pearl Tile Expert</p>
                                    <div class="total_sales">
                                        <div class="total_sales_count">
                                            1,043 Sales
                                        </div>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <a href="#">Contact shop owner</a>
                                </div>
                            </div>
                            <div class="right">
                                <div class="bio">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae tempor velit, eu porttitor metus. Nulla facilisi. Curabitur iaculis quam non nulla ornare condimentum. Quisque ante felis, rutrum sit amet tincidunt at, volutpat nec mi. Vivamus non viverra felis. Sed accumsan interdum est eget euismod. Morbi fermentum feugiat libero non viverra.
                                </div>
                            </div>
                        </div>
                        <div class="announcement">
                            <h2>Announcement</h2>
                            <p>Last updated on Dec 28, 2018</p>
                        </div>
                    </div>
                </div>
                
            </div>
            

            <div class="shop_loop_section seller_page">
                <div class="big_container">
                    <div class="shop_loop_section_inner">
                        <div class="filters">
                            <div class="filters_inner">
                                <div class="close_filter"><i class="fas fa-times"></i></div>
                                <div class="f_categories">
                                    <h2>Shop Categories</h2>
                                    <ul class="categories_list">
                                        <li>
                                            <a href="/shop.php">Jewellery & Accessories</a>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Rings</a>
                                            <ul>
                                                <li>
                                                    <a href="/shop.php">Wedding</a>
                                                </li>
                                                <li>
                                                    <a href="/shop.php">Statement</a>
                                                </li>
                                                <li>
                                                    <a href="/shop.php">Solitaire</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Necklace</a>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Bracelet</a>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Earings</a>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Nose pin</a>
                                        </li>
                                    </ul>
                                </div>

                                
                            </div>

                        </div>
                        <div class="products_loop">
                            <div class="products">
                    <div class="products_inner">
                        <div class="heading">
                            <h2>Featured Products</h2>
                            <div class="right">
                                <div class="slider_arrows">
                                    <span class="slider2 left"><i class="fas fa-angle-left"></i></span>
                                    <span class="slider2 right"><i class="fas fa-angle-right"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="products_slider slider2">
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>

                            <div class="heading">
                                <h2>All Items</h2>
                                <div class="right">
                                    <a class="view_all" href="/shop.php">View All</a>
                                </div>
                            </div>
                            <div class="products_loop_inner">
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            </div>
                        </div>
                    </div>


                    <div class="reviews_and_details">
                <div class="big_container">
                    <div class="reviews_and_details_inner">
                        <div class="left">
                            
                            
                            <div class="toggle_heading active">
                                <h2>Delivery and return policies<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            <div class="toggle_heading">
                                <h2>FAQs<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            
                        </div>
                        <div class="right">
                            <h2 class="total_reviews">18 shop reviews</h2>
                            <div class="reviews_list">
                                <div class="reviews_list_box">
                                    <div class="user_image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="details">
                                        <h2>Rachael 20 Dec, 2020</h2>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sodales sagittis viverra. Vivamus tristique metus sit amet suscipit feugiat. Nunc faucibus ligula vitae porta commodo. Mauris vitae velit mauris. Etiam tincidunt nulla eu diam egestas ultrices.
                                        </div>
                                        <div class="product">
                                            <a href="#">
                                                <span class="image">
                                                    
                                                </span>
                                                <span class="name">
                                                    Diamonds Rings• Solitaire Rings • Anniversary Gift
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews_list_box">
                                    <div class="user_image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="details">
                                        <h2>Rachael 20 Dec, 2020</h2>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sodales sagittis viverra. Vivamus tristique metus sit amet suscipit feugiat. Nunc faucibus ligula vitae porta commodo. Mauris vitae velit mauris. Etiam tincidunt nulla eu diam egestas ultrices.
                                        </div>
                                        <div class="product">
                                            <a href="#">
                                                <span class="image">
                                                    
                                                </span>
                                                <span class="name">
                                                    Diamonds Rings• Solitaire Rings • Anniversary Gift
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews_list_box">
                                    <div class="user_image">
                                        <img src="/assets/img/Rectangle 307.png">
                                    </div>
                                    <div class="details">
                                        <h2>Rachael 20 Dec, 2020</h2>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sodales sagittis viverra. Vivamus tristique metus sit amet suscipit feugiat. Nunc faucibus ligula vitae porta commodo. Mauris vitae velit mauris. Etiam tincidunt nulla eu diam egestas ultrices.
                                        </div>
                                        <div class="product">
                                            <a href="#">
                                                <span class="image">
                                                    
                                                </span>
                                                <span class="name">
                                                    Diamonds Rings• Solitaire Rings • Anniversary Gift
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pagination">
                                <ul>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#"><i class="fas fa-ellipsis-h"></i></a></li>
                                    <li><a href="#">20</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                </div>
            </div>
            
            
            
            

<?php include 'footer.php';?>