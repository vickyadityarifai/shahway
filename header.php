<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/style.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="mobile_header">
                <div class="header">
                    <div class="container">
                        <div class="header_inner">
                            <div class="box1">
                                <a href="/"><img src="/assets/img/imboo (1) 1.png" alt="logo"></a>
                            </div>
                            <div class="box4">
                                <a href="/login.php" class="my_account"><i class="far fa-user"></i> My Account</a>
                                <div class="cart">
                                    <a href="/cart.php">
                                        <span class="cart_icon"><i class="fas fa-shopping-cart"></i></span>
                                        <span class="cart_text">My Bag<span class="cart_item_bount">(0)</span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header">
                    <div class="container">
                        <div class="header_inner">
                            <div class="box2">
                                <div class="shop_by_categories1">
                                    <span class="s_b_c_1"><i class="fas fa-bars"></i></span>
                                </div>
                            </div>
                            <div class="box3">
                                <form>
                                    <input type="text" placeholder="I'm Shopping For">
                                    <button type="submit"><i class="fas fa-search"></i></button>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile_menu">
                <div class="mobile_menu_inner">
                    <div class="mobile_main_categories">
                        <div class="mobile_menu_header">
                            <div class="left"></div>
                            <div class="center">
                                <h2>Browse Categories</h2>
                                <p></p>
                            </div>
                            <div class="right"><span class="close_menu"><i class="fas fa-times"></i></span></div>
                        </div>
                        <div class="mobile_categories">
                            <ul>
                                <li data-id="1"><a href="/shop.php">Jewellery & Accessories <i class="fas fa-angle-right"></i></a></li>
                                <li data-id="2"><a href="/shop.php">Clothing & Shoes <i class="fas fa-angle-right"></i></a></li>
                                <li data-id="3"><a href="/shop.php">Home & Living <i class="fas fa-angle-right"></i></a></li>
                                <li data-id="4"><a href="/shop.php">Wedding & Party <i class="fas fa-angle-right"></i></a></li>
                                <li data-id="5"><a href="/shop.php">Craft Supplies <i class="fas fa-angle-right"></i></a></li>
                                <li data-id="6"><a href="/shop.php">Vintage <i class="fas fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile_sub_categories">
                        <div class="mobile_sub_categories_inner parent_id_1">
                            <div class="mobile_menu_header">
                                <div class="left"><span class="go_back"><i class="fas fa-chevron-left"></i></span></div>
                                <div class="center">
                                    <h2>Jewellery & Accessories</h2>
                                    <p><a href="/shop.php">View All</a></p>
                                </div>
                                <div class="right"><span class="close_menu"><i class="fas fa-times"></i></span></div>
                            </div>
                            <div class="mobile_categories">
                                <ul>
                                    <li data-id="1"><a href="/shop.php">Jewellery & Accessories 1<i class="fas fa-angle-right"></i></a></li>
                                    <li data-id="2"><a href="/shop.php">Clothing & Shoes <i class="fas fa-angle-right"></i></a></li>
                                    <li data-id="3"><a href="/shop.php">Home & Living <i class="fas fa-angle-right"></i></a></li>
                                    <li data-id="4"><a href="/shop.php">Wedding & Party <i class="fas fa-angle-right"></i></a></li>
                                    <li data-id="5"><a href="/shop.php">Craft Supplies <i class="fas fa-angle-right"></i></a></li>
                                    <li data-id="6"><a href="/shop.php">Vintage <i class="fas fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="mobile_inner_sub_categories">
                        <div class="mobile_inner_sub_categories_inner parent_id_1">
                            <div class="mobile_menu_header">
                                <div class="left"><span class="go_back"><i class="fas fa-chevron-left"></i></span></div>
                                <div class="center">
                                    <h2>Jewellery & Accessories 1</h2>
                                    <p><a href="/shop.php">View All</a></p>
                                </div>
                                <div class="right"><span class="close_menu"><i class="fas fa-times"></i></span></div>
                            </div>
                            <div class="mobile_categories">
                                <ul>
                                    <li data-id="1"><a href="/shop.php">Jewellery & Accessories</a>
                                        <ul>
                                            <li data-id="1"><a href="/shop.php">Jewellery & Accessories</a></li>
                                            <li data-id="2"><a href="/shop.php">Clothing & Shoes</a></li>
                                            <li data-id="3"><a href="/shop.php">Home & Living</a></li>
                                            <li data-id="4"><a href="/shop.php">Wedding & Party</a></li>
                                            <li data-id="5"><a href="/shop.php">Craft Supplies</a></li>
                                            <li data-id="6"><a href="/shop.php">Vintage</a></li>
                                        </ul>
                                    </li>
                                    <li data-id="2"><a href="/shop.php">Clothing & Shoes</a></li>
                                    <li data-id="3"><a href="/shop.php">Home & Living</a></li>
                                    <li data-id="4"><a href="/shop.php">Wedding & Party</a></li>
                                    <li data-id="5"><a href="/shop.php">Craft Supplies</a></li>
                                    <li data-id="6"><a href="/shop.php">Vintage</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top_bar">
                <div class="container">
                    <div class="top_bar_inner">
                        <div class="left">
                            <ul>
                                <li><a href="/shop.php">Membership</a></li>
                                <li><a href="/shop.php">Coupons & Deals</a></li>
                                <li><a href="/shop.php">Bestsellers</a></li>
                            </ul>
                        </div>
                        <div class="right">
                            <ul>
                                <li><a href="/login.php"><i class="far fa-user"></i> My Account</a></li>
                                <li><a href="/shop.php"><i class="far fa-heart"></i> Wishlist</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header">
                <div class="container">
                    <div class="header_inner">
                        <div class="box1">
                            <a href="/"><img src="/assets/img/imboo (1) 1.png" alt="logo"></a>
                        </div>
                        <div class="box2">
                            <div class="shop_by_categories">
                                <span class="s_b_c_1"><i class="fas fa-bars"></i></span>
                                <span class="s_b_c_2">Shop By Categories</span>
                                <span class="s_b_c_3"><i class="fas fa-angle-down"></i></span>
                            </div>
                            <div class="categories_dropdown">
                                <div class="left">
                                    <ul>
                                        <li class="active" data-id="1"><a href="/shop.php">Jewellery & Accessories <i class="fas fa-angle-right"></i></a></li>
                                        <li data-id="2"><a href="/shop.php">Clothing & Shoes <i class="fas fa-angle-right"></i></a></li>
                                        <li data-id="3"><a href="/shop.php">Home & Living <i class="fas fa-angle-right"></i></a></li>
                                        <li data-id="4"><a href="/shop.php">Wedding & Party <i class="fas fa-angle-right"></i></a></li>
                                        <li data-id="5"><a href="/shop.php">Craft Supplies <i class="fas fa-angle-right"></i></a></li>
                                        <li data-id="6"><a href="/shop.php">Vintage <i class="fas fa-angle-right"></i></a></li>
                                    </ul>
                                </div>
                                <div class="right">
                                    <ul class="active drop_main_cat_id_1 inner_sub">
                                        <li>
                                            <a href="/shop.php">Jewellery & Accessories</a>
                                            <ul>
                                                <li><a href="/shop.php">Clothing & Shoes</a></li>
                                                <li><a href="/shop.php">Home & Living</a></li>
                                                <li><a href="/shop.php">Wedding & Party</a></li>
                                                <li><a href="/shop.php">Craft Supplies</a></li>
                                                <li><a href="/shop.php">Vintage</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="/shop.php">Clothing & Shoes</a></li>
                                        <li><a href="/shop.php">Home & Living</a></li>
                                        <li><a href="/shop.php">Wedding & Party</a></li>
                                        <li><a href="/shop.php">Craft Supplies</a></li>
                                        <li><a href="/shop.php">Vintage</a></li>
                                    </ul>
                                    <ul class="drop_main_cat_id_2 inner_sub">
                                        <li><a href="/shop.php">Jewellery & Accessories</a></li>
                                        <li>
                                            <a href="/shop.php">Clothing & Shoes</a>
                                            <ul>
                                                <li><a href="/shop.php">Clothing & Shoes</a></li>
                                                <li><a href="/shop.php">Home & Living</a></li>
                                                <li><a href="/shop.php">Wedding & Party</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="/shop.php">Home & Living</a></li>
                                        <li><a href="/shop.php">Wedding & Party</a></li>
                                    </ul>
                                    <ul class="drop_main_cat_id_3 inner_sub">
                                        <li><a href="/shop.php">Wedding & Party</a></li>
                                        <li><a href="/shop.php">Craft Supplies</a></li>
                                        <li><a href="/shop.php">Vintage</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="box3">
                            <form>
                                <input type="text" placeholder="I'm Shopping For">
                                <button type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                        <div class="box4">
                            <div class="cart">
                                <a href="/cart.php">
                                    <span class="cart_icon"><i class="fas fa-shopping-cart"></i></span>
                                    <span class="cart_text">My Bag<span class="cart_item_bount">(0)</span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header_menu">
                <div class="container">
                    <div class="header_menu_inner">
                        <ul>
                            <li>
                                <a href="/shop.php">January Sales Event</a>
                                <div class="sub_menu">
                                    <div class="left">
                                        <a class="view_all_link" href="/shop.php">All January Sales Event <i class="fas fa-long-arrow-alt-right"></i></a>
                                        <ul>
                                            <li class="active" data-id="1"><a href="/shop.php">Jewellery & Accessories <i class="fas fa-angle-right"></i></a></li>
                                            <li data-id="2"><a href="/shop.php">Clothing & Shoes <i class="fas fa-angle-right"></i></a></li>
                                            <li data-id="3"><a href="/shop.php">Home & Living <i class="fas fa-angle-right"></i></a></li>
                                            <li data-id="4"><a href="/shop.php">Wedding & Party <i class="fas fa-angle-right"></i></a></li>
                                            <li data-id="5"><a href="/shop.php">Craft Supplies <i class="fas fa-angle-right"></i></a></li>
                                            <li data-id="6"><a href="/shop.php">Vintage <i class="fas fa-angle-right"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="right">
                                        <ul class="active main_cat_id_1 inner_sub">
                                            <li><a href="/shop.php">Jewellery & Accessories</a></li>
                                            <li><a href="/shop.php">Clothing & Shoes</a></li>
                                            <li><a href="/shop.php">Home & Living</a></li>
                                            <li><a href="/shop.php">Wedding & Party</a></li>
                                            <li><a href="/shop.php">Craft Supplies</a></li>
                                            <li><a href="/shop.php">Vintage</a></li>
                                        </ul>
                                        <ul class="main_cat_id_2 inner_sub">
                                            <li><a href="/shop.php">Jewellery & Accessories</a></li>
                                            <li><a href="/shop.php">Clothing & Shoes</a></li>
                                            <li><a href="/shop.php">Home & Living</a></li>
                                            <li><a href="/shop.php">Wedding & Party</a></li>
                                        </ul>
                                        <ul class="main_cat_id_3 inner_sub">
                                            <li><a href="/shop.php">Wedding & Party</a></li>
                                            <li><a href="/shop.php">Craft Supplies</a></li>
                                            <li><a href="/shop.php">Vintage</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li><a href="/shop.php">Jewellery & Accessories</a></li>
                            <li><a href="/shop.php">Clothing & Shoes</a></li>
                            <li><a href="/shop.php">Home & Living</a></li>
                            <li><a href="/shop.php">Wedding & Party</a></li>
                            <li><a href="/shop.php">Craft Supplies</a></li>
                            <li><a href="/shop.php">Vintage</a></li>
                        </ul>
                    </div>
                </div>
            </div>