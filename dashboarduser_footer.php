</div>
        </div>
        
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>

<!-- 
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>

<script>
	(function( $ ) {
	    $(document).ready(function(){

			setTimeout(function() {
				$(".notification_messages").html('');
			}, 5000);

			$(".close_notification_message").on('click', function(){
	        	$(".notification_messages").html('');
	        });
		});
	})( jQuery );
	
</script>


<script type="text/javascript">
$(function() {

    $(".tab_products").on('click', function(){
        $(".orders_list").removeClass('active');
        $(".tab_button").removeClass('active');
        $(this).addClass('active');
        $(".box_products").addClass('active');
    });

    $(".tab_orders").on('click', function(){
        $(".orders_list").removeClass('active');
        $(".tab_button").removeClass('active');
        $(this).addClass('active');
        $(".box_orders").addClass('active');
    });

    <?php
    if(isset($_GET['date_range'])){
    	$date_range = explode ("_", $_GET['date_range']); 
        ?>
		var start = moment(new Date('<?php echo $date_range[0]; ?>'));
        // var start = moment();//moment().subtract(29, 'days');
        var end = moment(new Date('<?php echo $date_range[1]; ?>'));
    	// var end = moment();
        <?php
    }else{
    	?>
    	var start = moment();//moment().subtract(29, 'days');
    	var end = moment();
    	<?php
    }
    ?>


    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $("#date_range").val(start.format('YYYY-M-D') + '_' + end.format('YYYY-M-D'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);


    $(".add_shipping_daily").on('click', function(e){
        e.preventDefault();
        $(".add_shipping_daily_box_wrapper").append('<div class="add_shipping_daily_box">\
                                            <div class="input_box">\
                                            </div>\
                                            <div class="input_box">\
                                                <input type="text" name="recipient[]" placeholder="Recipient">\
                                            </div>\
                                            <div class="input_box">\
                                                <input type="text" name="order[]" placeholder="Order #">\
                                            </div>\
                                            <div class="input_box">\
                                                <input type="text" name="tracking_number[]" placeholder="Tracking Number">\
                                            </div>\
                                            <div class="input_box">\
                                                <input type="text" name="shipping_cost[]" placeholder="Shipping Cost">\
                                            </div>\
                                            <div class="input_box">\
                                                <input type="text" name="status[]" placeholder="Status">\
                                            </div>\
                                            <span class="remove_row"><i class="fas fa-times"></i></span>\
                                        </div>');
    });

    $(document).on('click', '.remove_row', function(){
        $(this).parent().remove();
    });

});
</script>

    </body>
</html>