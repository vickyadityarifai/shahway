<?php
include('database_connection.php');
?>
<?php
$active_page = 'product_prices';
?>
<?php
include('dashboard_header.php');

$filter_code = '';
if(isset($_GET['EnterSKU'])){
    // $ProductName = $_GET['ProductName'];
    $EnterSKU = $_GET['EnterSKU'];
    if(!empty($EnterSKU)){
        $filter_code .= " WHERE sku = '".$EnterSKU."'";
    }
    
}


$get_orders = "SELECT * FROM product_prices $filter_code"; 
$product = mysqli_query($conn, $get_orders);
$row_product = $product->fetch_assoc();
if(!empty($row_product)){
?>

<div class="user_page_wrapper">
                        <div class="recently_view category_section add_product_page">
                            <h2>Edit Product Information</h2>
                            <p>Edit product name and cost for <?php echo $_GET['EnterSKU']; ?></p>
                            <div class="category_section_inner">
                                <form method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="edit_product_information">
                                    <input type="hidden" name="sku" value="<?php echo $_GET['EnterSKU']; ?>">
                                    <div class="input_box">
                                        <label>Product Name</label>
                                        <input type="text" name="name" value="<?php echo $row_product['product_name']; ?>">
                                    </div>
                                    <div class="input_box">
                                        <label>Cost</label>
                                        <input type="text" name="cost" value="<?php echo $row_product['cost']; ?>">
                                    </div>
                                    <div class="input_box">
                                        <button class="submit_buttons">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    
                </div>                
                
<?php
}
include('dashboard_footer.php');
?>