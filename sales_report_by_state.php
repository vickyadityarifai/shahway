<?php
include('database_connection.php');

?>
<?php
$active_page = 'sales_report_by_state';
?>
<?php
include('dashboard_header.php');
?>
<style>
</style>
                <?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }
                ?>
                <div class="recently_view">
                    <div class="orders_list">
                        <form class="filter_orders" method="get">
                            <input type="hidden" name="pageno" value="<?php echo $pageno; ?>">
                            
                            <div class="input_box"> 
                                <label>Status</label>
                                <select name="status">
                                    <option value="">Select Fullfilled</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']=="SellerFulfilled"){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="SellerFulfilled" <?php echo $selected; ?>>SellerFulfilled</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']=="WFSFulfilled"){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="WFSFulfilled" <?php echo $selected; ?>>WFSFulfilled</option>
                                </select>
                            </div>
                            <div class="input_box"> 
                                <label>Date</label>
                                <?php
                                $date_range = date('Y-m-d').'_'.date('Y-m-d');
                                if(isset($_GET['date_range'])){
                                    $date_range = $_GET['date_range'];
                                }
                                ?>
                                <input type="hidden" name="date_range" id="date_range" value="<?php echo $date_range; ?>">
                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span><?php echo $date_range; ?></span> <i class="fa fa-caret-down"></i>
                                </div>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                            <!-- <div class="export_to_csv">
                                <a href="/dashboard_orders_admin.php?export_to_csv">Export</a>

                            </div> -->
                        </form>
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Shipping State
                                </div>
                                <div class="box">
                                    Orders Count
                                </div>
                                <div class="box">
                                    Products Count
                                </div>
                                <div class="box">
                                    Total
                                </div>
                            </div>


                            


                            
                            <?php


                            

                            $no_of_records_per_page = 30;
                            $offset = ($pageno-1) * $no_of_records_per_page; 


                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0])* 1000;
                                $date_end = strtotime($date_range[1])* 1000;
                                if($date_range[0]==$date_range[1]){
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;
                                    $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . "";
                                }else{
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;
                                    $filter_code .= " WHERE orderDate >= " . $date_start . " AND orderDate <= " . $date_end . "";
                                }
                                
                            }
                            if(isset($_GET['status']) && !empty($_GET['status'])){
                                $filter_code .= " AND shipNode = '".$_GET['status']."'";
                            }
                            
                            

                            $get_orders_count = "SELECT * FROM orders $filter_code GROUP BY shipping_state ORDER BY id desc";   
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);
                            

                            $get_orders = "SELECT * FROM orders $filter_code GROUP BY shipping_state ORDER BY id desc LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);

                            




                            $final_quantity = 0;
                            $final_chargeAmount_amount = 0;
                            $final_product_price = 0;
                            $final_walmart_fees = 0;
                            $final_total_shipping_cost = 0;
                            $final_customer_shipping_cost =0;
                            $final_customer_shipping_cost_tax = 0;
                            $final_taxAmount_amount = 0;


                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_id = $order['id'];
                                    $refund = 0;
                                    $product_price = 0;
                                    $walmart_fees = 0;
                                    $total_shipping_cost = 0;
                                    $customer_shipping_cost =0;
                                    $customer_shipping_cost_tax = 0;



                                    $CommissionRate = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Commission on Product'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $CommissionRate = $row_reconciliationreport['Commission_Rate'];
                                    }

                                    $Fee_Reimbursement = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Fee/Reimbursement'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $Fee_Reimbursement = $row_reconciliationreport['Amount'];
                                    }

                                    $Fee_Reimbursement = preg_replace("/[^0-9.]/","",$Fee_Reimbursement);


                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <?php echo $order['shipping_state']; ?>
                                </div>
                                <div class="box">
                                    <?php
                                    $new_filter_code = '';
                                    if(empty($filter_code)){
                                        $new_filter_code .=$filter_code;
                                        $new_filter_code .= " WHERE shipping_state = '".$order['shipping_state']."'";
                                    }else{
                                        $new_filter_code .=$filter_code;
                                        $new_filter_code .= " AND shipping_state = '".$order['shipping_state']."'";
                                    }
                                        $get_orders1 = "SELECT * FROM orders $new_filter_code ORDER BY id desc";
                                        $get_orders_query1 = mysqli_query($conn, $get_orders1);
                                        echo mysqli_num_rows($get_orders_query1);
                                    ?>
                                </div>
                                <div class="box">
                                    <?php 
                                    $k1=0;
                                    $chargeAmount_amount = 0;
                                    if(mysqli_num_rows($get_orders_query1) > 0){
                                        while($order1 = $get_orders_query1->fetch_assoc()) {
                                            $get_orderlines1 = "SELECT * FROM orderlines WHERE order_id=".$order_id;
                                            $get_orderlines_query1 = mysqli_query($conn, $get_orderlines1);
                                            if(mysqli_num_rows($get_orderlines_query1) > 0){
                                                $k1 = $k1+mysqli_num_rows($get_orderlines_query1);
                                                while($orderline1 = $get_orderlines_query1->fetch_assoc()) {
                                                    $chargeAmount_amount = $chargeAmount_amount+floatval($orderline1['chargeAmount_amount']);
                                                }
                                            }
                                        }
                                    }
                                    echo $k1;
                                     ?>
                                </div>
                                <div class="box">
                                    <?php echo $chargeAmount_amount; ?>
                                </div>
                                
                            </div>
                            <?php  



                        } } 


                        ?>
                        
                            
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                if(isset($_GET['date_range'])){
                                    $other_link = '&status='.$_GET['status'].'&date_range='.$_GET['date_range'];
                                }
                                if(isset($_GET['order_id'])){
                                    $other_link.= '&order_id='.$_GET['order_id'];
                                }




                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/sales_report_by_state.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/sales_report_by_state.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/sales_report_by_state.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>

<script>
    $(window).scroll(function() {
       if($(this).scrollTop() > 240) {
         $(".table_list_box.table_list_heading").addClass("fixed");
       }else{
        $(".table_list_box.table_list_heading").removeClass("fixed");
       }
    });
</script>
<?php
include('dashboard_footer.php');

?>