<?php
include('database_connection.php');
?>
<?php
$active_page = 'reconciliationreport_monthly';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}

.table_list_box{
    padding: 10px 0;
}
.table_list_box.table_last_column {
  padding: 5px 0;
  background: #e2e2e2;
  font-weight: 900;
}
</style>
<!-- <div class="recently_view_t_bg">
    <a href="/reconciliationreport.php"><i class="fas fa-plus"></i> Add Reconciliation Report</a>
    <a href="/recon_WFS_StorageFee.php"><i class="fas fa-edit"></i> WFS StorageFee</a>
    <a href="/recon_WFS_LostInventory.php"><i class="fas fa-edit"></i> WFS LostInventory</a>
    <a href="/recon_WFS_FoundInventory.php"><i class="fas fa-edit"></i> WFS FoundInventory</a>
    <a href="/recon_WFS_InboundTransportationFee.php"><i class="fas fa-edit"></i> WFS InboundTransportationFee</a>
    <a href="/recon_WFS_RC_InventoryDisposalFee.php"><i class="fas fa-edit"></i> WFS RC_InventoryDisposalFee</a>
    <a href="/recon_Deposited_in_HYPERWALLET_account.php"><i class="fas fa-edit"></i> Deposited in HYPERWALLET account</a>
    <a class="highlight" href="/recon_WFS_Refund.php"><i class="fas fa-edit"></i> WFS Refund</a>
</div> -->
<?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 30;
                $offset = ($pageno-1) * $no_of_records_per_page; 
                ?>
                
                <div class="recently_view">
                    <div class="orders_list">
                        <form class="filter_orders" method="get">
                            <input type="hidden" name="pageno" value="<?php echo $pageno; ?>">
                            <div class="input_box"> 
                                <label>Date</label>
                                <?php
                                $date_range = date('Y-m-d').'_'.date('Y-m-d');
                                if(isset($_GET['date_range'])){
                                    $date_range = $_GET['date_range'];
                                }
                                ?>
                                <input type="hidden" name="date_range" id="date_range" value="<?php echo $date_range; ?>">
                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span><?php echo $date_range; ?></span> <i class="fa fa-caret-down"></i>
                                </div>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                            <!-- <div class="export_to_csv">
                                <a href="/dashboard_orders_admin.php?export_to_csv">Export</a>

                            </div> -->
                        </form>
                        <!-- <form class="filter_orders">
                            <div class="input_box"> 
                                <label>Status</label>
                                <select>
                                    <option>All</option>
                                </select>
                            </div>
                            <div class="input_box"> 
                                <label>Date</label>
                                <select>
                                    <option>All Date</option>
                                </select>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form> -->
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Period Start Date
                                </div>
                                <div class="box">
                                    Period End Date
                                </div>
                                <?php /*
                                <div class="box">
                                    Transaction Type
                                </div> */ ?>
                                <div class="box">
                                    Transaction Description
                                </div>
                                <div class="box">
                                    Amount
                                </div>
                                <?php /*
                                <div class="box">
                                    Amount Type
                                </div>
                                <div class="box">
                                    Total Payable
                                </div>
                                <div class="box">
                                    Currency
                                </div> */ ?>
                            </div>
                            
                            <?php
                           
                            $filter_code = '';
                            if(isset($_GET['date_range'])){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0]);
                                $date_end = strtotime($date_range[1]);
                                if($date_range[0]==$date_range[1]){
                                    $date_start = date("m/d/Y", strtotime("".$date_range[0]." 12:00 AM"));
                                    $date_end = date("m/d/Y", strtotime("".$date_range[1]." 11:59 PM"));
                                    
                                    $filter_code .= " AND Period_Start_Date='".$date_start."'";
                                }else{
                                    
                                    $date_start = date("m/d/Y", $date_start);

                                    $date_end = date("m/d/Y", $date_end);
                                    // echo $date_start;
                                    // die();
                                    // $filter_code .= " AND Period_Start_Date >= '" . $date_start . "'' AND Period_Start_Date <= '" . $date_end . "'";
                                    // $date_start='08/01/2022';
                                    // $date_end='12/31/2022';
                                    $filter_code .= " AND Period_Start_Date >= '" . $date_start . "' AND Period_Start_Date <= '" . $date_end . "'";
                                }
                                
                            }

                            $get_orders_count = "SELECT * FROM reconciliationreport_monthly WHERE Transaction_Description='WFS Refund' $filter_code ORDER BY Period_Start_Date desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);
                            

                            $get_orders = "SELECT * FROM reconciliationreport_monthly WHERE Transaction_Description='WFS Refund' $filter_code ORDER BY Period_Start_Date desc LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);



                            $total_amount = 0;
                            $Total_Payable = 0;


                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    
                                    
                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <?php echo date("m-d-Y", strtotime($order['Period_Start_Date'])); ?>
                                </div>
                                <div class="box">
                                    <?php echo date("m-d-Y", strtotime($order['Period_End_Date'])); ?>
                                </div>
                                <?php /*
                                <div class="box">
                                    <?php echo $order['Transaction_Type']; ?>
                                </div> */ ?>
                                <div class="box">
                                    <?php echo $order['Transaction_Description']; ?>
                                </div>
                                <div class="box">
                                    $<?php 
                                    $Amount = preg_replace("/[^0-9.]/","",$order['Amount']);
                                    echo number_format($Amount, 2);
                                        $total_amount = $total_amount+$Amount;
                                     ?>
                                </div>
                                <div class="box">
                                    <?php echo $order['Amount_Type']; ?>
                                </div>
                                <?php /*
                                <div class="box">
                                    <?php
                                    $total_payable = 0;
                                    if(!empty($order['Total_Payable'])){
                                        $total_payable = $order['Total_Payable'];    
                                    }
                                    ?>
                                    $<?php echo number_format($total_payable, 2);
                                    $Total_Payable = $Total_Payable+$total_payable;
                                     ?>
                                </div>
                                <div class="box">
                                    <?php echo $order['Currency']; ?>
                                </div> */ ?>
                            </div>
                            <?php  } } ?>
                            <div class="table_list_box table_last_column">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                </div>
                                <div class="box">
                                </div>
                          <!--       <div class="box">
                                </div> -->
                                <div class="box">
                                    WFS Refund
                                </div>
                                <div class="box">
                                    $<?php echo $total_amount; ?>
                                </div>
                                <?php /*
                                <div class="box">
                                    
                                </div>
                                <div class="box">
                                    $<?php echo $Total_Payable; ?>
                                </div>
                                <div class="box">

                                </div> */ ?>
                            </div>
                            
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                


                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/recon_WFS_Refund.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/recon_WFS_Refund.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/recon_WFS_Refund.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }

                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
<?php
include('dashboard_footer.php');
?>