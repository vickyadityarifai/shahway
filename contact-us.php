<?php include 'header.php';?>
<div class="page_title">
    <div class="big_container">
        <div class="page_title_inner">
            <h2>Contact us</h2>
            <p><a class="all">All</a> - <a class="current">Contact us</a></p>
        </div>
    </div>
</div>
<div class="about_page_section">
	<div class="big_container">
		<div class="about_page_section_inner bg_white">
			<div class="about_page_section_inner_inner">
				<h2>Contact us</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue sodales arcu vel interdum. Proin nibh nisl, blandit ac sem vitae, eleifend tristique nisi. Maecenas vitae nisi neque.</p>
			</div>
		</div>
	</div>
</div>

<div class="about_page_section_with_image">
	<div class="big_container">
		<div class="about_page_section_with_image_inner">
			<div class="left">
				<div class="contact_form">
					<h2>Send Message</h2>
					<form>
						<div class="input_box">
							<input placeholder="Enter full name">
						</div>
						<div class="input_box">
							<input placeholder="Enter Email address">
						</div>
						<div class="input_box">
							<textarea placeholder="Message"></textarea>
						</div>
						<div class="input_box">
							<button>Send Message</button>
						</div>
					</form>
				</div>
			</div>
			<div class="right">
				<div class="office_address">
					<h2>Office Address</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue sodales arcu vel interdum. Proin nibh nisl, blandit ac sem vitae.</p>
					<p class="address_line"><span><i class="fas fa-map-marker-alt"></i></span> Address : No 40 Baria Sreet 133/2 NewYork City</p>
					<p class="address_line"><span><i class="fas fa-envelope"></i></span> info@yourdomain.com</p>
					<p class="address_line"><span><i class="fas fa-phone-alt"></i></span> +88013245657</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contact_support">
	<div class="big_container">
		<div class="about_page_section_inner">
			<div class="about_page_section_inner_inner">
				<h2>Didn't find what you needed? Try these.</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue sodales arcu vel interdum. Proin nibh nisl, blandit ac sem vitae.</p>
				<a href="#">Contact imboo Support</a>
			</div>
		</div>
	</div>
</div>
<?php include 'footer.php';?>