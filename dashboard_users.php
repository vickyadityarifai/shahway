<?php 
include('database_connection.php');
?>
<?php
$active_page = 'user_manager';


?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}
.table_list_box{
    padding: 10px 0;
}
.view_details.delete {
  background: red;
}
.view_details.restore {
  background: green;
}
</style>
<div class="recently_view_t_bg">
    <a href="/dashboard_user_create.php"><i class="fas fa-plus"></i> Add User</a>
</div>
<form class="filter_orders">
                           <div class="input_box"> 
                                <label>Status</label>
                                <select name="status">
                                    <option value="">All</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']==1){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="1" <?php echo $selected; ?>>Active</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']==-1){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="-1" <?php echo $selected; ?>>Deleted</option>
                                </select>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
<?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 30;
                $offset = ($pageno-1) * $no_of_records_per_page; 
                ?>
                
                <div class="recently_view">
                    <div class="orders_list">
                        
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Name
                                </div>
                                <div class="box">
                                    Username
                                </div>
                                <div class="box">
                                    Email
                                </div>
                                <div class="box">
                                    Type
                                </div>
                                <div class="box">
                                    Action
                                </div>
                            </div>

                            
                            <?php


                            $filter_code = '';
                            if(isset($_GET['status'])){
                                // $ProductName = $_GET['ProductName'];
                                $status = $_GET['status'];
                                if(!empty($status)){
                                    $filter_code .= " WHERE status = ".$status;
                                }
                                
                            }

                            $get_orders_count = "SELECT * FROM users $filter_code ORDER BY id desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);


                            

                            $get_orders = "SELECT * FROM users $filter_code ORDER BY id desc LIMIT $offset, $no_of_records_per_page;"; 

                            $get_orders_query = mysqli_query($conn, $get_orders);

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_id = $order['id'];
                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <?php echo $order['fullname']; ?>
                                    
                                </div>
                                <div class="box">
                                    <?php echo $order['username']; ?>
                                    
                                </div>
                                <div class="box">
                                    <?php echo $order['email']; ?>
                                    
                                </div>
                                <div class="box">
                                    <?php
                                    if($order['type']==1){
                                        echo 'Admin';
                                    }else{
                                        echo 'Employee';
                                    }
                                    ?>
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_user_create.php?id=<?php echo $order['id']; ?>" class="view_details">Edit Profile</a>
                                        <?php
                                        if($order['status']==-1){
                                            ?>
                                            <a href="/dashboard_users.php?restore_user=<?php echo $order['id']; ?>" class="view_details restore">Restore</a>
                                            <?php
                                        }else{
                                            ?>
                                            <a href="/dashboard_users.php?delete_user=<?php echo $order['id']; ?>" class="view_details delete">Delete</a>
                                            <?php
                                        }
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
                            <?php  } } ?>
                            
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                

                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_users.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_users.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_users.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <script>
    $(window).scroll(function() {
       if($(this).scrollTop() > 240) {
         $(".table_list_box.table_list_heading").addClass("fixed");
       }else{
        $(".table_list_box.table_list_heading").removeClass("fixed");
       }
    });
</script>
<?php
include('dashboard_footer.php');
?>