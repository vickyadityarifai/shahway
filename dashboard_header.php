<?php
include('database_connection.php');
?>
<?php
//session_start();
if(!isset($_SESSION['logedin'])){
  if(isset($_COOKIE['logedin'])) {
    setcookie('logedin', $_COOKIE['logedin'], time() + 2419200, '/');
    $_SESSION['logedin'] = $_COOKIE['logedin'];


    $user_id = $_SESSION['user_id'];

    $check_users  = "SELECT * FROM users WHERE id=".$user_id."";
    $users = mysqli_query($conn, $check_users);
    $row_users = $users->fetch_assoc();
    if($row_users){
    
        if($row_users['type']==1){
            header("Location: ".$domainLink."/dashboard_admin.php");
        }else{

            $allowed_pages = $row_users['allowed_pages'];
            $allowed_pages = explode (",", $allowed_pages); 
            if(isset($allowed_pages[0])){
                header("Location: ".$domainLink."/".$allowed_pages[0].".php");
            }else{
                header("Location: ".$domainLink."?logout");
            }

            
        }

    }else{
      header("Location: ".$domainLink."");
    }

    

    // header("Location: ".$domainLink."/dashboard_admin.php");
  }else{
    header("Location: ".$domainLink."/");
  } 
}else{
    $user_id = $_SESSION['user_id'];
    if(!empty($user_id)){
        $check_users  = "SELECT * FROM users WHERE id=".$user_id."";
        $users = mysqli_query($conn, $check_users);
        $row_users = $users->fetch_assoc();
        if($row_users){
        //this is dashboard
          // header("Location: ".$domainLink."/dashboard_admin.php");
        }else{
          header("Location: ".$domainLink."");
        }
    }else{
        header("Location: ".$domainLink."");
    }

    
}
$allowed_pages = $row_users['allowed_pages'];
$allowed_pages = explode (",", $allowed_pages); 

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Shahway</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="./assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="./assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="./assets/dashboard.css?<?php echo time(); ?>"/>
        <link rel="stylesheet" type="text/css" href="./assets/dashboard_responsive.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>
        <style>

.menu.menu_desktop li a i {
  display: none;
}
.sidebar .menu li a i {
  display: none;
}



    .notification_messages {
  position: absolute;
  top: 10px;
  right: 10px;
  z-index: 123123123;
}
.error_message {
  background-color: #ff6d6d;
  padding: 10px 20px;
  width: 300px;
  min-height: 100px;
  border-radius: 4px;
  color: #fff;
  position: relative;
}
.success_message {
  background-color: #61ce61;
  padding: 10px 20px;
  width: 300px;
  min-height: 100px;
  border-radius: 4px;
  color: #fff;
  position: relative;
}
.dropdown-item.active_dashboard {
  background-color: #f2f2f2;
}

.check_require.error {
  border-color: red;
}
.close_notification_message{
  position: absolute;
  top: 10px;
  right: 10px;
  cursor: pointer;
  z-index: 123;
}
.page_wrapper .header .notification a {
  color: #000;
}
</style>
        <div class="notification_messages">
            <?php
            if(isset($_SESSION['error_message'])){
                if(!isset($_SESSION['success_message'])){

                
              ?>
              <div class="error_message">
                <span class="close_notification_message"><i class="fas fa-times"></i></span>
                <?php echo $_SESSION['error_message']; ?>
              </div>
              <?php
              }
              unset($_SESSION['error_message']);
            }
            if(isset($_SESSION['success_message'])){

              ?>
              <div class="success_message">
                <span class="close_notification_message"><i class="fas fa-times"></i></span>
                <?php echo $_SESSION['success_message'];
                 ?>
              </div>
              <?php
              unset($_SESSION['success_message']);
            }
            ?>
          </div>

<?php
$settings_id = 1;
$check_if_setting_already = "SELECT * FROM settings WHERE id=".$settings_id;
$run_check_if = mysqli_query($conn, $check_if_setting_already);
$if_settings = $run_check_if->fetch_assoc();
$is_api_active = 'no';
if(!empty($if_settings)){
    if($if_settings['status']==1){
        $is_api_active = 'yes';
    }
}

if($is_api_active=='no'){
    ?>
<div class="walmart_api_error">
    Your walmart API is not connected!
</div>
    <?php
}
?>


        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="./dashboard_admin.php">Shahway</a>
                </div>
                <div class="menu">
                    <ul>
                        
                        <?php
                        if(in_array("dashboard_admin",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='dashboard'){
                                $active_class = 'active';
                            }
                            ?>
                            <li><a href="./dashboard_admin.php"   class="<?php echo $active_class; ?>"><i class="fas fa-home"></i> Home</a></li>
                            <?php
                        }
                        if(in_array("dashboard_orders_admin",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='orders'){
                                $active_class = 'active';
                            }
                            ?>
                            <li><a href="./dashboard_orders_admin.php"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Orders</a></li>
                            <?php
                        }
                        if(in_array("dashboard_order_products_admin",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='orders_products'){
                                $active_class = 'active';
                            }
                            ?>
                            <li><a href="./dashboard_order_products_admin.php"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Products</a></li>
                            <?php 
                        }
                        if(in_array("reconciliationreport_monthly",$allowed_pages)
                                || in_array("reconciliationreport",$allowed_pages)
                                || in_array("recon_WFS_StorageFee",$allowed_pages)
                                || in_array("recon_WFS_LostInventory",$allowed_pages)
                                || in_array("recon_WFS_FoundInventory",$allowed_pages)
                                || in_array("recon_WFS_InboundTransportationFee",$allowed_pages)
                                || in_array("recon_WFS_RC_InventoryDisposalFee",$allowed_pages)
                                || in_array("recon_Deposited_in_HYPERWALLET_account",$allowed_pages)
                                || in_array("recon_WFS_Refund",$allowed_pages)
                                || in_array("sales_report_by_state",$allowed_pages)
                                || in_array("dashboard_monthly_reports",$allowed_pages)
                        ){
                            $active_class = '';
                            if($active_page=='reconciliationreport_monthly'){
                                $active_class = 'active';
                            }
                            if($active_page=='sales_report_by_state'){
                                $active_class = 'active';
                            }
                            if($active_page=='monthly_reports'){
                                $active_class = 'active';
                            }
                            ?>
                            <li><a href="#"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Reports</a>
                                <ul class="sub_menu">
                                    <?php
                                    if(in_array("reconciliationreport_monthly",$allowed_pages)
                                            || in_array("reconciliationreport",$allowed_pages)
                                            || in_array("recon_WFS_StorageFee",$allowed_pages)
                                            || in_array("recon_WFS_LostInventory",$allowed_pages)
                                            || in_array("recon_WFS_FoundInventory",$allowed_pages)
                                            || in_array("recon_WFS_InboundTransportationFee",$allowed_pages)
                                            || in_array("recon_WFS_RC_InventoryDisposalFee",$allowed_pages)
                                            || in_array("recon_Deposited_in_HYPERWALLET_account",$allowed_pages)
                                            || in_array("recon_WFS_Refund",$allowed_pages)
                                    ){
                                            $active_class = '';
                                            if($active_page=='reconciliationreport'){
                                                $active_class = 'active';
                                            }


                                            $page_link = '#';
                                            if(in_array("reconciliationreport",$allowed_pages)){
                                                $page_link = './reconciliationreport.php';
                                            }

                                            ?>
                                            
                                            <li><a href="<?php echo $page_link; ?>"   class="<?php echo $active_class; ?>"><i class="fas fa-calendar"></i> Reconciliation Report</a>

                                                <ul>
                                                    <?php
                                                    if(in_array("reconciliationreport",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./reconciliationreport.php"><i class="fas fa-plus"></i> Add Reconciliation Report</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_StorageFee",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_StorageFee.php"><i class="fas fa-edit"></i> WFS StorageFee</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_LostInventory",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_LostInventory.php"><i class="fas fa-edit"></i> WFS LostInventory</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_FoundInventory",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_FoundInventory.php"><i class="fas fa-edit"></i> WFS FoundInventory</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_InboundTransportationFee",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_InboundTransportationFee.php"><i class="fas fa-edit"></i> WFS InboundTransportationFee</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_RC_InventoryDisposalFee",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_RC_InventoryDisposalFee.php"><i class="fas fa-edit"></i> WFS RC_InventoryDisposalFee</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_Deposited_in_HYPERWALLET_account",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_Deposited_in_HYPERWALLET_account.php"><i class="fas fa-edit"></i> Deposited in HYPERWALLET account</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_Refund",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_Refund.php"><i class="fas fa-edit"></i> WFS Refund</a></li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                        <?php
                                        if(in_array("sales_report_by_state",$allowed_pages)){
                                            $active_class = '';
                                            if($active_page=='sales_report_by_state'){
                                                $active_class = 'active';
                                            }
                                            ?>
                                            
                                            <li><a href="./sales_report_by_state.php"   class="<?php echo $active_class; ?>"><i class="fas fa-users"></i> SR by state</a></li>
                                        <?php }
                                        if(in_array("dashboard_monthly_reports",$allowed_pages)){
                                            $active_class = '';
                                            if($active_page=='monthly_reports'){
                                                $active_class = 'active';
                                            }
                                            ?>
                                            
                                            <li><a href="./dashboard_monthly_reports.php"   class="<?php echo $active_class; ?>"><i class="fas fa-file"></i> Monthly Reports</a></li>
                                        <?php }
                                        ?>
                                </ul>
                            </li>
                            <?php 
                        }
                        if(in_array("dashboard_shipping_cost",$allowed_pages)
                            || in_array("dashboard_add_shipping_cost",$allowed_pages)
                            || in_array("dashboard_add_shipping_cost_daily",$allowed_pages)
                            || in_array("dashboard_update_shipping_cost",$allowed_pages)
                    ){
                            $active_class = '';
                            if($active_page=='shipping_cost'){
                                $active_class = 'active';
                            }

                            $page_link = '#';
                            if(in_array("dashboard_shipping_cost",$allowed_pages)){
                                $page_link = './dashboard_shipping_cost.php';
                            }


                            ?>
                            
                            <li><a href="<?php echo $page_link ?>" class="<?php echo $active_class; ?>"><i class="fas fa-hand-holding-usd"></i> Shipping Cost</a>
                                <ul class="sub_menu">
                                    <?php
                                    if(in_array("dashboard_add_shipping_cost",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_add_shipping_cost.php"><i class="fas fa-plus"></i> Add Shipping Cost</a></li>
                                    <?php } ?>
                                    <?php
                                    if(in_array("dashboard_add_shipping_cost_daily",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_add_shipping_cost_daily.php"><i class="fas fa-edit"></i> Daily Shipping Cost</a></li>
                                    <?php } ?>
                                    <?php
                                    if(in_array("dashboard_update_shipping_cost",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_update_shipping_cost.php"><i class="fas fa-edit"></i> Missing Shipping Cost</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php 
                        }
                        if(in_array("dashboard_product_price",$allowed_pages)
                            || in_array("dashboard_add_product_price",$allowed_pages)
                            || in_array("dashboard_update_product_price",$allowed_pages)
                    ){
                            $active_class = '';
                            if($active_page=='product_prices'){
                                $active_class = 'active';
                            }

                            $page_link = '#';
                            if(in_array("dashboard_product_price",$allowed_pages)){
                                $page_link = './dashboard_product_price.php';
                            }

                            ?>
                            
                            <li><a href="<?php echo $page_link; ?>"   class="<?php echo $active_class; ?>"><i class="fas fa-dollar-sign"></i> Product Price</a>
                                <ul class="sub_menu">
                                    <?php
                                    if(in_array("dashboard_add_product_price",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_add_product_price.php"><i class="fas fa-plus"></i> Add Product Price</a></li>
                                    <?php } ?>
                                    <?php
                                    if(in_array("dashboard_update_product_price",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_update_product_price.php"><i class="fas fa-edit"></i> Missing Product Price</a></li>
                                    <?php } ?>
                                </ul>
                            </li>

                            <?php
                        }
                        
                        if(in_array("dashboard_users",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='user_manager'){
                                $active_class = 'active';
                            }
                            ?>
                            
                            <li><a href="./dashboard_users.php"   class="<?php echo $active_class; ?>"><i class="fas fa-users"></i> Users</a></li>
                        <?php }
                        


                        if(in_array("dashboard_inventory",$allowed_pages)
                            || in_array("dashboard_wfs_inventory",$allowed_pages)
                    ){
                            $active_class = '';
                            if($active_page=='inventory'){
                                $active_class = 'active';
                            }

                            $page_link = '#';
                            if(in_array("dashboard_inventory",$allowed_pages)){
                                $page_link = './dashboard_inventory.php';
                            }

                            ?>
                            
                            <li><a href="<?php echo $page_link; ?>"   class="<?php echo $active_class; ?>"><i class="fas fa-file"></i> Inventory</a>
                                <?php /*
                                <ul>
                                    <?php
                                    if(in_array("dashboard_wfs_inventory",$allowed_pages)){
                                    
                                    <li><a href="/dashboard_wfs_inventory.php"><i class="fas fa-edit"></i> WFS Inventory</a></li>
                                    <?php } 
                                </ul> */ ?>
                            </li>
                        <?php } /*
                        if(in_array("dashboard_walmart_items",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='walmart_items'){
                                $active_class = 'active';
                            }


                        
                            
                            <li><a href="/dashboard_walmart_items.php"   class="<?php echo $active_class; "><i class="fas fa-file"></i> Walmart Items</a></li>
                        <?php } */ 
                        

                        if(in_array("add_product_to_walmart",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='add_product_to_walmart'){
                                $active_class = 'active';
                            }
                            ?>
                            
                            <li><a href="./add_product_to_walmart.php"   class="<?php echo $active_class; ?>"><i class="fas fa-users"></i> Add Walmart Item</a></li>
                        <?php }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="./assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <div class="show_side_bar">
                        <i class="fas fa-list-alt"></i>
                    </div>
                    <form>
                        <h1>Hi, <?php echo $row_users['fullname']; ?> 👋</h1>
                        <p>Good Morning, Have a nice day.</p>
                    </form>
                    <?php
                    if($row_users['type']==1){
                        ?>
                        <div class="notification">
                            <a href="./notifications.php">
                                <i class="fas fa-bell"></i>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                    
                    <div class="user user_section_header">
                        <a href="#" class="profile_button">
                            <img src="./assets/img/Ellipse 63.png">
                        </a>
                        <ul>
                            <?php
                            if($row_users['type']==1){
                            ?>
                            <li><a href="./settings.php">Walmart settings</a></li>
                            <?php } ?>
                            <li><a href="./dashboard_edit_profile.php">Edit Profile</a></li>
                            <li><a href="./?logout=yes">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="menu menu_desktop">
                    <ul>
                        
                        <?php
                        if(in_array("dashboard_admin",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='dashboard'){
                                $active_class = 'active';
                            }
                            ?>
                            <li><a href="./dashboard_admin.php"   class="<?php echo $active_class; ?>"><i class="fas fa-home"></i> Home</a></li>
                            <?php
                        }
                        if(in_array("dashboard_orders_admin",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='orders'){
                                $active_class = 'active';
                            }
                            ?>
                            <li><a href="./dashboard_orders_admin.php"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Orders</a></li>
                            <?php
                        }
                        if(in_array("dashboard_order_products_admin",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='orders_products'){
                                $active_class = 'active';
                            }
                            ?>
                            <li><a href="./dashboard_order_products_admin.php"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Products</a></li>
                            <?php 
                        }
                        if(in_array("reconciliationreport_monthly",$allowed_pages)
                                || in_array("reconciliationreport",$allowed_pages)
                                || in_array("recon_WFS_StorageFee",$allowed_pages)
                                || in_array("recon_WFS_LostInventory",$allowed_pages)
                                || in_array("recon_WFS_FoundInventory",$allowed_pages)
                                || in_array("recon_WFS_InboundTransportationFee",$allowed_pages)
                                || in_array("recon_WFS_RC_InventoryDisposalFee",$allowed_pages)
                                || in_array("recon_Deposited_in_HYPERWALLET_account",$allowed_pages)
                                || in_array("recon_WFS_Refund",$allowed_pages)
                                || in_array("sales_report_by_state",$allowed_pages)
                                || in_array("dashboard_monthly_reports",$allowed_pages)
                        ){
                            $active_class = '';
                            if($active_page=='reconciliationreport_monthly'){
                                $active_class = 'active';
                            }
                            if($active_page=='sales_report_by_state'){
                                $active_class = 'active';
                            }
                            if($active_page=='monthly_reports'){
                                $active_class = 'active';
                            }
                            ?>
                            <li><a href="#"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Reports</a>

                                <ul class="sub_menu">
                                    <?php
                                    if(in_array("reconciliationreport_monthly",$allowed_pages)
                                            || in_array("reconciliationreport",$allowed_pages)
                                            || in_array("recon_WFS_StorageFee",$allowed_pages)
                                            || in_array("recon_WFS_LostInventory",$allowed_pages)
                                            || in_array("recon_WFS_FoundInventory",$allowed_pages)
                                            || in_array("recon_WFS_InboundTransportationFee",$allowed_pages)
                                            || in_array("recon_WFS_RC_InventoryDisposalFee",$allowed_pages)
                                            || in_array("recon_Deposited_in_HYPERWALLET_account",$allowed_pages)
                                            || in_array("recon_WFS_Refund",$allowed_pages)

                                    ){
                                            $active_class = '';
                                            if($active_page=='reconciliationreport'){
                                                $active_class = 'active';
                                            }


                                            $page_link = '#';
                                            if(in_array("reconciliationreport",$allowed_pages)){
                                                $page_link = './reconciliationreport.php';
                                            }

                                            ?>
                                            
                                            <li><a href="<?php echo $page_link; ?>"   class="<?php echo $active_class; ?>"><i class="fas fa-calendar"></i> Reconciliation Report</a>

                                                <ul>
                                                    <?php
                                                    if(in_array("reconciliationreport",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./reconciliationreport.php"><i class="fas fa-plus"></i> Add Reconciliation Report</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_StorageFee",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_StorageFee.php"><i class="fas fa-edit"></i> WFS StorageFee</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_LostInventory",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_LostInventory.php"><i class="fas fa-edit"></i> WFS LostInventory</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_FoundInventory",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_FoundInventory.php"><i class="fas fa-edit"></i> WFS FoundInventory</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_InboundTransportationFee",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_InboundTransportationFee.php"><i class="fas fa-edit"></i> WFS InboundTransportationFee</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_RC_InventoryDisposalFee",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_RC_InventoryDisposalFee.php"><i class="fas fa-edit"></i> WFS RC_InventoryDisposalFee</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_Deposited_in_HYPERWALLET_account",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_Deposited_in_HYPERWALLET_account.php"><i class="fas fa-edit"></i> Deposited in HYPERWALLET account</a></li>
                                                    <?php } ?>
                                                    <?php
                                                    if(in_array("recon_WFS_Refund",$allowed_pages)){
                                                    ?>
                                                    <li><a href="./recon_WFS_Refund.php"><i class="fas fa-edit"></i> WFS Refund</a></li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                        <?php
                                        if(in_array("sales_report_by_state",$allowed_pages)){
                                            $active_class = '';
                                            if($active_page=='sales_report_by_state'){
                                                $active_class = 'active';
                                            }
                                            ?>
                                            
                                            <li><a href="./sales_report_by_state.php"   class="<?php echo $active_class; ?>"><i class="fas fa-users"></i> SR by state</a></li>
                                        <?php }
                                        if(in_array("dashboard_monthly_reports",$allowed_pages)){
                                            $active_class = '';
                                            if($active_page=='monthly_reports'){
                                                $active_class = 'active';
                                            }
                                            ?>
                                            
                                            <li><a href="./dashboard_monthly_reports.php"   class="<?php echo $active_class; ?>"><i class="fas fa-file"></i> Monthly Reports</a></li>
                                        <?php }
                                        ?>
                                </ul>
                            </li>
                            <?php 
                        }
                        if(in_array("dashboard_shipping_cost",$allowed_pages)
                            || in_array("dashboard_add_shipping_cost",$allowed_pages)
                            || in_array("dashboard_add_shipping_cost_daily",$allowed_pages)
                            || in_array("dashboard_update_shipping_cost",$allowed_pages)
                    ){
                            $active_class = '';
                            if($active_page=='shipping_cost'){
                                $active_class = 'active';
                            }

                            $page_link = '#';
                            if(in_array("dashboard_shipping_cost",$allowed_pages)){
                                $page_link = './dashboard_shipping_cost.php';
                            }


                            ?>
                            
                            <li><a href="<?php echo $page_link ?>" class="<?php echo $active_class; ?>"><i class="fas fa-hand-holding-usd"></i> Shipping Cost</a>
                                <ul class="sub_menu">
                                    <?php
                                    if(in_array("dashboard_add_shipping_cost",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_add_shipping_cost.php"><i class="fas fa-plus"></i> Add Shipping Cost</a></li>
                                    <?php } ?>
                                    <?php
                                    if(in_array("dashboard_add_shipping_cost_daily",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_add_shipping_cost_daily.php"><i class="fas fa-edit"></i> Daily Shipping Cost</a></li>
                                    <?php } ?>
                                    <?php
                                    if(in_array("dashboard_update_shipping_cost",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_update_shipping_cost.php"><i class="fas fa-edit"></i> Missing Shipping Cost</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php 
                        }
                        if(in_array("dashboard_product_price",$allowed_pages)
                            || in_array("dashboard_add_product_price",$allowed_pages)
                            || in_array("dashboard_update_product_price",$allowed_pages)
                    ){
                            $active_class = '';
                            if($active_page=='product_prices'){
                                $active_class = 'active';
                            }

                            $page_link = '#';
                            if(in_array("dashboard_product_price",$allowed_pages)){
                                $page_link = './dashboard_product_price.php';
                            }

                            ?>
                            
                            <li><a href="<?php echo $page_link; ?>"   class="<?php echo $active_class; ?>"><i class="fas fa-dollar-sign"></i> Product Price</a>
                                <ul class="sub_menu">
                                    <?php
                                    if(in_array("dashboard_add_product_price",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_add_product_price.php"><i class="fas fa-plus"></i> Add Product Price</a></li>
                                    <?php } ?>
                                    <?php
                                    if(in_array("dashboard_update_product_price",$allowed_pages)){
                                    ?>
                                    <li><a href="./dashboard_update_product_price.php"><i class="fas fa-edit"></i> Missing Product Price</a></li>
                                    <?php } ?>
                                </ul>
                            </li>

                            <?php
                        }
                        
                        if(in_array("dashboard_users",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='user_manager'){
                                $active_class = 'active';
                            }
                            ?>
                            
                            <li><a href="./dashboard_users.php"   class="<?php echo $active_class; ?>"><i class="fas fa-users"></i> Users</a></li>
                        <?php }
                        


                        if(in_array("dashboard_inventory",$allowed_pages)
                            || in_array("dashboard_wfs_inventory",$allowed_pages)
                    ){
                            $active_class = '';
                            if($active_page=='inventory'){
                                $active_class = 'active';
                            }

                            $page_link = '#';
                            if(in_array("dashboard_inventory",$allowed_pages)){
                                $page_link = './dashboard_inventory.php';
                            }

                            ?>
                            
                            <li><a href="<?php echo $page_link; ?>"   class="<?php echo $active_class; ?>"><i class="fas fa-file"></i> Inventory</a>
                                <?php /*
                                <ul>
                                    <?php
                                    if(in_array("dashboard_wfs_inventory",$allowed_pages)){
                                    
                                    <li><a href="/dashboard_wfs_inventory.php"><i class="fas fa-edit"></i> WFS Inventory</a></li>
                                    <?php } 
                                </ul> */ ?>
                            </li>
                        <?php } /*
                        if(in_array("dashboard_walmart_items",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='walmart_items'){
                                $active_class = 'active';
                            }


                        
                            
                            <li><a href="/dashboard_walmart_items.php"   class="<?php echo $active_class; "><i class="fas fa-file"></i> Walmart Items</a></li>
                        <?php } */ 
                        

                        if(in_array("add_product_to_walmart",$allowed_pages)){
                            $active_class = '';
                            if($active_page=='add_product_to_walmart'){
                                $active_class = 'active';
                            }
                            ?>
                            
                            <li><a href="./add_product_to_walmart.php"   class="<?php echo $active_class; ?>"><i class="fas fa-users"></i> Add Walmart Item</a></li>
                        <?php }
                        ?>
                    </ul>
                </div>