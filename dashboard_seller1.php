<!DOCTYPE html>
<html> 
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard_seller1.php"  class="active"><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/dashboard_products.php" ><i class="fas fa-boxes"></i> Products</a></li>
                        <li><a href="/dashboard_orders_seller.php"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_withdraw.php" ><i class="fas fa-hand-holding-usd"></i> Withdraw</a></li>
                        <li><a href="/dashboard_messages_seller.php" class=""><i class="fas fa-comment-dots"></i> Messages</a></li>
                        <li><a href="/dashboard_disputes_seller.php"><i class="fas fa-people-carry"></i> Disputes</a></li>
                        <li><a href="/support_seller.php" class=""><i class="fas fa-headset"></i> Support</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <h1>Hi, John Doe 👋</h1>
                        <p>Good Morning, Have a nice day.</p>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                <div class="recently_view seller_profile_box saved_view active">
                    <a class="edit_profile_button go_to_edit" href="#"><span><i class="fas fa-pencil-alt"></i></span> Edit Profile</a>
                    <div class="cover_photo" style="background-image: url('/assets/img/Rectangle 1513.png');">
                        
                    </div>
                    <div class="user_info">
                        <div class="user_profile_image">
                            <div class="image">
                                <img src="/assets/img/Rectangle 1514.png">
                                <span>Avita Jewellery</span>
                            </div>
                        </div>
                        <div class="user_info_content">
                            <div class="user_info_content_box">
                                <div class="user_info_content_box_inner">
                                    <p><strong>user name:</strong>Dummy name</p>
                                </div>
                                <div class="user_info_content_box_inner">
                                    <p><strong>Phone Num:</strong>+0033-xxxxxxxxxxxx</p>
                                </div>
                            </div>
                            <div class="user_info_content_box">
                                <div class="user_info_content_box_inner">
                                    <p><strong>Email:</strong>info@company.com</p>
                                </div>
                                <div class="user_info_content_box_inner">
                                    <p><strong>address:</strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="shop_bio">
                            <ul>
                                <li data-id="1" class="active shop_bio_toggle_button">Shop Bio</li>
                                <li data-id="2" class="shop_bio_toggle_button">return policy</li>
                                <li data-id="3" class="shop_bio_toggle_button">FAQs</li>
                            </ul>
                            <div class="shop_bio_content shop_bio_tab_1 active">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae tempor velit, eu porttitor metus. Nulla facilisi. Curabitur iaculis quam non nulla ornare condimentum. Quisque ante felis, rutrum sit amet tincidunt at, volutpat nec mi. Vivamus non viverra felis. Sed accumsan interdum est eget euismod. Morbi fermentum feugiat libero non viverra.
                            </div>
                            <div class="shop_bio_content shop_bio_tab_2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae tempor velit, eu porttitor metus. Nulla facilisi. Curabitur iaculis quam non nulla ornare condimentum. Quisque ante felis, rutrum sit amet tincidunt at, volutpat nec mi. Vivamus non viverra felis. Sed accumsan interdum est eget euismod. Morbi fermentum feugiat libero non viverra.
                            </div>
                            <div class="shop_bio_content shop_bio_tab_3">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae tempor velit, eu porttitor metus. Nulla facilisi. Curabitur iaculis quam non nulla ornare condimentum. Quisque ante felis, rutrum sit amet tincidunt at, volutpat nec mi. Vivamus non viverra felis. Sed accumsan interdum est eget euismod. Morbi fermentum feugiat libero non viverra.
                            </div>
                        </div>
                </div>
                <div class="recently_view seller_profile_box edit_view">
                    <a class="edit_profile_button go_to_saved" href="#">Save Profile</a>
                    <div class="cover_photo" style="background-image: url('/assets/img/Rectangle 1513.png');">
                        
                    </div>
                    <div class="user_info">
                        <div class="user_profile_image">
                            <div class="image">
                                <img src="/assets/img/Rectangle 1514.png">
                                <span><input type="text" value="Avita Jewellery"></span>
                            </div>
                        </div>
                        <div class="user_info_content">
                            <div class="user_info_content_box">
                                <div class="user_info_content_box_inner">
                                    <p><strong>user name:</strong><input type="text" value="Dummy name"></p>
                                </div>
                                <div class="user_info_content_box_inner">
                                    <p><strong>Phone Num:</strong><input type="text" value="+0033-xxxxxxxxxxxx"></p>
                                </div>
                            </div>
                            <div class="user_info_content_box">
                                <div class="user_info_content_box_inner">
                                    <p><strong>Email:</strong><input type="text" value="info@company.com"></p>
                                </div>
                                <div class="user_info_content_box_inner">
                                    <p><strong>address:</strong><input type="text" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit."></p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="shop_bio">
                            <ul>
                                <li data-id="1" class="active shop_bio_toggle_button">Shop Bio</li>
                                <li data-id="2" class="shop_bio_toggle_button">return policy</li>
                                <li data-id="3" class="shop_bio_toggle_button">FAQs</li>
                            </ul>
                            <div class="shop_bio_content shop_bio_tab_1 active">
                                <textarea>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae tempor velit, eu porttitor metus. Nulla facilisi. Curabitur iaculis quam non nulla ornare condimentum. Quisque ante felis, rutrum sit amet tincidunt at, volutpat nec mi. Vivamus non viverra felis. Sed accumsan interdum est eget euismod. Morbi fermentum feugiat libero non viverra.</textarea>
                            </div>
                            <div class="shop_bio_content shop_bio_tab_2">
                                <textarea>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae tempor velit, eu porttitor metus. Nulla facilisi. Curabitur iaculis quam non nulla ornare condimentum. Quisque ante felis, rutrum sit amet tincidunt at, volutpat nec mi. Vivamus non viverra felis. Sed accumsan interdum est eget euismod. Morbi fermentum feugiat libero non viverra.</textarea>
                            </div>
                            <div class="shop_bio_content shop_bio_tab_3">
                                <textarea>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae tempor velit, eu porttitor metus. Nulla facilisi. Curabitur iaculis quam non nulla ornare condimentum. Quisque ante felis, rutrum sit amet tincidunt at, volutpat nec mi. Vivamus non viverra felis. Sed accumsan interdum est eget euismod. Morbi fermentum feugiat libero non viverra.</textarea>
                            </div>
                        </div>
                </div>
                <div class="recently_view">
                    <h2>
                        Recent Orders
                        <div class="slider_arrows">
                            <span class="slider5_arrow left"><i class="fas fa-angle-left"></i></span>
                            <span class="slider5_arrow right"><i class="fas fa-angle-right"></i></span>
                        </div>
                    </h2>
                    <div class="products_slider slider5">
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Completed">(Completed)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="/dashboard_orders_details1.php">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Processing">(Processing)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="/dashboard_orders_details1.php">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Canceled">(Canceled)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="/dashboard_orders_details1.php">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Completed">(Completed)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="/dashboard_orders_details1.php">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Completed">(Completed)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="/dashboard_orders_details1.php">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Completed">(Completed)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="/dashboard_orders_details1.php">View Detail</a>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>