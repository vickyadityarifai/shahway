<?php include 'header.php';?>

            
            
            <div class="page_title">
                <div class="big_container">
                    <div class="page_title_inner single_heading">
                        <h2>Cart</h2>
                    </div>
                </div>
            </div>

            <div class="cart_items">
                <div class="container">
                    <div class="cart_items_inner">
                        <div class="cart_items_labels">
                            <div class="box1">
                                <h2>Product</h2>
                            </div>
                            <div class="box2">
                                <h2>Price</h2>
                            </div>
                            <div class="box3">
                                <h2>Quantity</h2>
                            </div>
                            <div class="box4">
                                <h2>Total</h2>
                            </div>
                        </div>
                        <div class="cart_items_items">
                            <div class="cart_items_labels">
                                <div class="box1">
                                    <div class="cart_products">
                                        <div class="image">
                                            <a href="#" style="background-image: url('/assets/img/Rectangle 307.png');"></a>
                                        </div>
                                        <div class="details">
                                            <h3>Skin Gloss Leather Bag</h3>
                                            <p>Color: <span></span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="box2">
                                    <p class="price">$69,77</p>
                                </div>
                                <div class="box3">
                                    <div class="qty">
                                        <span class="minus" data-id="1"><i class="fas fa-minus"></i></span>
                                        <span class="input"><input id="product_1" type="text" value="1"></span>
                                        <span class="plus" data-id="1"><i class="fas fa-plus"></i></span>
                                    </div>
                                </div>
                                <div class="box4">
                                    <p class="price_total">$69,77 <span><i class="fas fa-times"></i></span></p>
                                </div>
                            </div>
                            <div class="cart_items_labels">
                                <div class="box1">
                                    <div class="cart_products">
                                        <div class="image">
                                            <a href="#" style="background-image: url('/assets/img/Rectangle 307.png');"></a>
                                        </div>
                                        <div class="details">
                                            <h3>Skin Gloss Leather Bag</h3>
                                            <p>Color: <span></span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="box2">
                                    <p class="price">$69,77</p>
                                </div>
                                <div class="box3">
                                    <div class="qty">
                                        <span class="minus" data-id="2"><i class="fas fa-minus"></i></span>
                                        <span class="input"><input id="product_2" type="text" value="1"></span>
                                        <span class="plus" data-id="2"><i class="fas fa-plus"></i></span>
                                    </div>
                                </div>
                                <div class="box4">
                                    <p class="price_total">$69,77 <span><i class="fas fa-times"></i></span></p>
                                </div>
                            </div>
                        </div>
                        <div class="checkout_section">
                            <div class="checkout">
                                <h2>Subtotal<span>$100.00</span></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <a href="/checkout.php">Check Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            

        
            
            

<?php include 'footer.php';?>