<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/style.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/responsive.css"/>
    </head>
    <body>
        <div class="account_wrapper">
            <div class="account_slider">
                <div class="account_slide_box" style="background-image: url('/assets/img/Rectangle 1524.png');">
                    <div class="account_slide_content">
                        <h1>A unique approach in <span>shopping</span> </h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum 
has been the industry's standard dummy text ever since the 1500s,</p>
                    </div>
                </div>
                <div class="account_slide_box" style="background-image: url('/assets/img/Rectangle 1524.png');">
                    <div class="account_slide_content">
                        <h1>A unique approach in <span>shopping</span> </h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum 
has been the industry's standard dummy text ever since the 1500s,</p>
                    </div>
                </div>
                <div class="account_slide_box" style="background-image: url('/assets/img/Rectangle 1524.png');">
                    <div class="account_slide_content">
                        <h1>A unique approach in <span>shopping</span> </h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum 
has been the industry's standard dummy text ever since the 1500s,</p>
                    </div>
                </div>
            </div>
            <div class="account_form login">
                <a href="/" class="logo"><img src="/assets/img/imboo (1) 1.png" alt="logo"></a>
                <h2>Welcome Back</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <form>
                    <div class="input_box">
                        <label>Email</label>
                        <input type="text" placeholder="Enter Your Email Here">
                    </div>
                    <div class="input_box">
                        <label>Password</label>
                        <input type="text" placeholder="Enter Your Password Here">
                    </div>
                    <div class="input_box">
                        <button>Login</button>
                    </div>
                </form>
                <p class="bottom_link">Dont have an Account? <a href="/register.php">Create Account</a></p>
            </div>
        </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> -->
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<script type="text/javascript" src="/assets/slick/slick.min.js"></script>
<script src="/assets/custom.js?<?php echo time(); ?>"></script>
          <script>
  $( function() {
    $( "#slider_price_range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 20, 400 ],
      slide: function( event, ui ) {
        var val0 = ui.values[ 0 ];
        var val1 = ui.values[ 1 ];
        var number0 = val0;
        var number1 = val1;
        // $( "#indoorsize" ).html(  number0 + " m<span class='zee_min'>2</span> <span class='text_right'>" + number1 +" m<span class='zee_min'>2</span></span>" );
        $( ".price_range_values .left span" ).html(  val0 );
        $( ".price_range_values .right span" ).html(  val1 );
      }
    });
  } );

  </script>
    </body>
</html>