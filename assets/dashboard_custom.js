$(document).ready(function(){

	$(".slider1").slick({
		slidesToShow: 4,
	    slidesToScroll: 1,
	    dots: false,
	    arrows: false,
	    autoplay: false,
	    responsive: [
		    
		    {
		      breakpoint: 901,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 2,
		      }
		    }
		  ]
	});
	$('.slider1_arrow.left').click(function(){
	    $(".slider1").slick('slickPrev');
	});
	$('.slider1_arrow.right').click(function(){
	    $(".slider1").slick('slickNext');
	});

	$(".slider5").slick({
		slidesToShow: 5,
	    slidesToScroll: 1,
	    dots: false,
	    arrows: false,
	    autoplay: false,
	    responsive: [
		    
		    {
		      breakpoint: 901,
		      settings: {
		        slidesToShow: 4,
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 2,
		      }
		    }
		  ]
	});
	$('.slider5_arrow.left').click(function(){
	    $(".slider5").slick('slickPrev');
	});
	$('.slider5_arrow.right').click(function(){
	    $(".slider5").slick('slickNext');
	});


	$(".show_filters").on('click', function(){
		$(".filters").addClass('active');
	});
	$(".close_filter").on('click', function(){
		$(".filters").removeClass('active');
	});

	$(".sort_by_dropdown_active").on("click", function(){
		$(".sort_by_dropdown_options").toggleClass('active');
	});
	$(".sort_by_dropdown_options").on("click", function(){
		$(".sort_by_dropdown_options").toggleClass('active');
	});

	$(".view_by_dropdown_active").on("click", function(){
		$(".view_by_dropdown_options").toggleClass('active');
	});
	$(".view_by_dropdown_options").on("click", function(){
		$(".view_by_dropdown_options").toggleClass('active');
	});


	$(".show_side_bar").on('click', function(){
		$(".sidebar").addClass('active');
	});

	$(".close_side_bar").on('click', function(){
		$(".sidebar").removeClass('active');
	});

	$(".backtochats").on('click', function(){
		$(".chat_box").addClass('hide_it');
	});

	$(".chat_list_box").on('click', function(){
		$(".chat_box").removeClass('hide_it');
		$(".chat_list_box").removeClass('active');
		$(this).addClass('active');
	});

	$(".shop_bio_toggle_button").on('click', function(){
		var id = $(this).attr('data-id');
		$(".shop_bio_toggle_button").removeClass('active');
		$(this).addClass('active');
		$(".shop_bio_content").removeClass('active');
		$(".shop_bio_tab_"+id).addClass('active');
	});

	$(".go_to_edit").on('click', function(e){
		e.preventDefault();
		$(".recently_view.seller_profile_box.edit_view").addClass('active');
		$(".recently_view.seller_profile_box.saved_view").removeClass('active');
	});

	$(".go_to_saved").on('click', function(e){
		e.preventDefault();
		$(".recently_view.seller_profile_box.edit_view").removeClass('active');
		$(".recently_view.seller_profile_box.saved_view").addClass('active');
	});

	$(".sub_category_selector ul .select_more").on('click', function(e){
		e.preventDefault();
		$(".sub_category_selector ul .select_more ul").toggleClass('active');
	});

	$(".sub_category_selector ul .select_more ul li").on('click', function(e){
		e.preventDefault();
		if($(this).hasClass('selected')){

		}else{
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			$(".sub_category_selector ul .previous_selected").append('<li data-id="'+id+'" data-name="'+name+'">'+name+' <span class="remove"><i class="fas fa-times"></i></span></li>');
			$(this).addClass('selected');
		}
	});

	$(document).on('click', ".sub_category_selector ul .previous_selected li .remove", function(e){
		e.preventDefault();
		var id = $(this).parent().attr('data-id');
		var name = $(this).parent().attr('data-name');
		$(this).parent().remove();
		$(".sub_category_selector ul .select_more ul li.sub_cat_"+id).removeClass('selected');
	});

	$(document).on("click", function(event){
	    if(!$(event.target).closest(".sub_category_selector ul .select_more").length){
	    	$(".sub_category_selector ul .select_more ul").removeClass('active');
	    }
	});



	$(".hide_dispute_popup").on('click', function(){
		$(".dispute_popup").removeClass('active');
		$(".wrapper").removeClass('blur');
	});

	$(".dispute").on('click', function(){
		$(".dispute_popup").addClass('active');
		$(".wrapper").addClass('blur');
	});

	$(".buyer_seller_toggle a.fields_for_buyer").on('click', function(){
		$(".buyer_seller_toggle a.fields_for_seller").removeClass('active');
		$(".buyer_seller_toggle a.fields_for_buyer").addClass('active');
		$(".fields_for_sellers").removeClass('active');
	});
	$(".buyer_seller_toggle a.fields_for_seller").on('click', function(){
		$(".buyer_seller_toggle a.fields_for_seller").addClass('active');
		$(".buyer_seller_toggle a.fields_for_buyer").removeClass('active');
		$(".fields_for_sellers").addClass('active');
	});

	$(".buyer_edit_profile").on('click', function(e){
		e.preventDefault();
		$(".profile_box.buyer_dashboard.saved_view").removeClass('active');
		$(".profile_box.buyer_dashboard.edit_view").addClass('active');
	});

	$(".buyer_save_profile").on('click', function(e){
		e.preventDefault();
		$(".profile_box.buyer_dashboard.saved_view").addClass('active');
		$(".profile_box.buyer_dashboard.edit_view").removeClass('active');
	});

	

	

	$(".profile_button").on('click', function(e){
		e.preventDefault();
		$(".user_section_header ul").toggleClass('active');
	});

	
});