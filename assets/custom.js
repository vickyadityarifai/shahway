$(document).ready(function(){
	

	$(".account_slider").slick({
		slidesToShow: 1,
	    slidesToScroll: 1,
	    dots: true,
	    arrows: false,
	    autoplay: false,
	});

	$('.top_images').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  dots: false,
	  asNavFor: '.for_images'
	});
	$('.for_images').slick({
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  asNavFor: '.top_images',
	  dots: false,
	  arrows: false,
	  focusOnSelect: true,
	  responsive: [
		    
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 4,
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 3,
		      }
		    }
		  ]
	});
	$('.for_images_arrow.left').click(function(){
	    $(".for_images").slick('slickPrev');
	});
	$('.for_images_arrow.right').click(function(){
	    $(".for_images").slick('slickNext');
	});


	$(".slider1").slick({
		slidesToShow: 4,
	    slidesToScroll: 1,
	    dots: false,
	    arrows: false,
	    autoplay: false,
	    responsive: [
		    
		    {
		      breakpoint: 901,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 2,
		      }
		    }
		  ]
	});
	$('.slider1.left').click(function(){
	    $(".slider1").slick('slickPrev');
	});
	$('.slider1.right').click(function(){
	    $(".slider1").slick('slickNext');
	});

	$(".slider2").slick({
		slidesToShow: 4,
	    slidesToScroll: 1,
	    dots: false,
	    arrows: false,
	    autoplay: false,
	    responsive: [
		    
		    {
		      breakpoint: 901,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 2,
		      }
		    }
		  ]
	});
	$('.slider2.left').click(function(){
	    $(".slider2").slick('slickPrev');
	});
	$('.slider2.right').click(function(){
	    $(".slider2").slick('slickNext');
	});

	$(".slider3").slick({
		slidesToShow: 4,
	    slidesToScroll: 1,
	    dots: false,
	    arrows: false,
	    autoplay: false,
	    responsive: [
		    
		    {
		      breakpoint: 901,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 2,
		      }
		    }
		  ]
	});
	$('.slider3.left').click(function(){
	    $(".slider3").slick('slickPrev');
	});
	$('.slider3.right').click(function(){
	    $(".slider3").slick('slickNext');
	});

	$(".slider4").slick({
		slidesToShow: 5,
	    slidesToScroll: 1,
	    dots: false,
	    arrows: false,
	    autoplay: false,
	    responsive: [
	    	{
		      breakpoint: 901,
		      settings: {
		        slidesToShow: 4,
		      }
		    },
		    
		    {
		      breakpoint: 801,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        slidesToShow: 2,
		      }
		    }
		  ]
	});
	$('.slider4.left').click(function(){
	    $(".slider4").slick('slickPrev');
	});
	$('.slider4.right').click(function(){
	    $(".slider4").slick('slickNext');
	});

	setTimeout(function(){
		$("#slider_price_range .ui-slider-handle.ui-corner-all.ui-state-default:nth-child(2)").html('<i class="fas fa-angle-left"></i>');
		$("#slider_price_range span.ui-slider-handle.ui-corner-all.ui-state-default:last-child").html('<i class="fas fa-angle-right"></i>');

	}, 2000);

	$(".sub_menu .left li").on("mouseover", function(){
		$(".sub_menu .left li").removeClass('active');
		$(this).addClass('active');
		var id = $(this).attr('data-id');
		$(".sub_menu .right .inner_sub").removeClass('active');
		$(".sub_menu .right .inner_sub.main_cat_id_"+id).addClass('active');
	});

	$(".categories_dropdown .left li").on("mouseover", function(){
		$(".categories_dropdown .left li").removeClass('active');
		$(this).addClass('active');
		var id = $(this).attr('data-id');
		$(".categories_dropdown .right .inner_sub").removeClass('active');
		$(".categories_dropdown .right .inner_sub.drop_main_cat_id_"+id).addClass('active');
	});

	$(".shop_by_categories").on('click', function(){
		$(".categories_dropdown").addClass('active');
	});

	$(".categories_dropdown").on("mouseleave", function(){
		$(this).removeClass('active');
	});
	$(".filters .filter_box ul li .checkmark").on('click', function(){
		$(this).parent().find('label').trigger( "click" );
	});
	$(".filters .filter_box ul.colors_filter li").on('click', function(){
		$(this).toggleClass('selected');
	});

	

	$(".filters .filter_box ul li input[type='checkbox']").on('change', function(){
		$(this).parent().toggleClass('checked');
	});



	$(".mobile_main_categories li").on('click', function(e){
		e.preventDefault();
		var id = $(this).attr("data-id");
		$(".mobile_sub_categories").addClass('active');
		$(".mobile_sub_categories_inner").removeClass('active');
		$(".mobile_sub_categories_inner.parent_id_"+id).addClass('active');
	});

	$(".mobile_sub_categories li").on('click', function(e){
		e.preventDefault();
		var id = $(this).attr("data-id");
		$(".mobile_inner_sub_categories").addClass('active');
		$(".mobile_inner_sub_categories_inner").removeClass('active');
		$(".mobile_inner_sub_categories_inner.parent_id_"+id).addClass('active');
	});


	$(".go_back").on('click', function(){
		$(this).parent().parent().parent().parent().removeClass('active');
	});

	$(".close_menu").on('click', function(){
		$(".mobile_menu").removeClass('active');
	});

	$(".shop_by_categories1").on('click', function(){
		$(".mobile_menu").addClass('active');
		$(".mobile_sub_categories").removeClass('active');
		$(".mobile_inner_sub_categories").removeClass('active');
	});

	if(window.innerWidth<481){
		$(".mobile_menu_inner").css({'height': window.innerHeight-123+'px'});
		$(".mobile_categories").css({'height': window.innerHeight-201+'px'});
	}else{
		$(".mobile_menu_inner").css({'height': window.innerHeight+'px'});
		$(".mobile_categories").css({'height': window.innerHeight-78+'px'});
	}
	
	
	$(".account_wrapper .account_slider .account_slide_box").css({'height': window.innerHeight+'px'});
	$(".account_wrapper").css({'height': window.innerHeight+'px'});


	$(".show_filters").on('click', function(){
		$(".filters").addClass('active');
	});
	$(".close_filter").on('click', function(){
		$(".filters").removeClass('active');
	});

	$(".sort_by_dropdown_active").on("click", function(){
		$(".sort_by_dropdown_options").toggleClass('active');
	});
	$(".sort_by_dropdown_options").on("click", function(){
		$(".sort_by_dropdown_options").toggleClass('active');
	});

	$(".view_by_dropdown_active").on("click", function(){
		$(".view_by_dropdown_options").toggleClass('active');
	});
	$(".view_by_dropdown_options").on("click", function(){
		$(".view_by_dropdown_options").toggleClass('active');
	});
	


	$(".toggle_heading").on('click', function(){
		$(".toggle_heading").removeClass('active');
		$(this).addClass('active');
	});


	$(".qty .minus").on('click', function(){
		var id = $(this).attr('data-id');
		var qty = $("#product_"+id).val();
		var new_qty = qty-1;
		if(new_qty<1){
			new_qty = 1;
		}
		$("#product_"+id).val(new_qty);
	});

	$(".qty .plus").on('click', function(){
		var id = $(this).attr('data-id');
		var qty = $("#product_"+id).val();
		var new_qty = parseInt(qty)+parseInt(1);
		
		$("#product_"+id).val(new_qty);
	});

	

	$(".payment_redios_option .heading_option").on('click', function(){
		$(".payment_redios_option").removeClass('active');
		$(this).parent().addClass('active');
	});

	$(".show_step_2").on('click', function(){
		$(".step_1").removeClass('active');
		$(".step_2").addClass('active');
	});

	$(".account_form .top_back_link").on('click', function(){
		$(".step_1").addClass('active');
		$(".step_2").removeClass('active');
	});



	

	
	
	

	
});