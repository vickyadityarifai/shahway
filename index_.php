<?php include 'header.php';?>

            <div class="banner_section">
                <div class="banner" style="background-image: url('/assets/img/Rectangle 245.png');">
                    <div class="container">
                        <div class="banner_inner">
                            <div class="left">
                                <h2>New Winter</h2>
                                <h1>Collections 2022</h1>
                                <p>There's nothing like trend</p>
                                <a href="/shop.php">Shop Now</a>
                            </div>
                            <div class="right">
                                <div class="image">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="categories">
                <div class="container">
                    <div class="categories_inner">
                        <div class="heading">
                            <h2>Shop by Categories</h2>
                            <div class="right">
                                <a class="view_all" href="/shop.php">View All</a>

                            </div>
                        </div>
                        <div class="top_2_categories">
                            <div class="category_box" style="background-image: url('/assets/img/Rectangle 243.png')">
                                <div class="category_name_link"><h2>Women Collection<a href="/shop.php">Shop Now</a></h2></div>
                            </div>
                            <div class="category_box" style="background-image: url('/assets/img/Rectangle 243.png')">
                                <div class="category_name_link"><h2>All watches<a href="/shop.php">Shop Now</a></h2></div>
                            </div>
                        </div>
                        <div class="3_categories">
                            <a href="/shop.php" class="category_box" style="background-image: url('/assets/img/Rectangle 214.png')">
                                <div class="category_name_link"><h2>Jewelry</h2></div>
                            </a>
                            <a href="/shop.php" class="category_box" style="background-image: url('/assets/img/Rectangle 177.png')">
                                <div class="category_name_link"><h2>Kids Clothing</h2></div>
                            </a>
                            <a href="/shop.php" class="category_box" style="background-image: url('/assets/img/Rectangle 176.png')">
                                <div class="category_name_link"><h2>Ceramic Mugs</h2></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="products">
                <div class="container">
                    <div class="products_inner">
                        <div class="heading">
                            <h2>Top Rated</h2>
                            <div class="right">
                                <a class="view_all" href="/shop.php">View All</a>
                                <div class="slider_arrows">
                                    <span class="slider1 left"><i class="fas fa-angle-left"></i></span>
                                    <span class="slider1 right"><i class="fas fa-angle-right"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="products_slider slider1">
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="products">
                <div class="container">
                    <div class="products_inner">
                        <div class="heading">
                            <h2>Hot Deals</h2>
                            <div class="right">
                                <a class="view_all" href="/shop.php">View All</a>
                                <div class="slider_arrows">
                                    <span class="slider2 left"><i class="fas fa-angle-left"></i></span>
                                    <span class="slider2 right"><i class="fas fa-angle-right"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="products_slider slider2">
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="products">
                <div class="container">
                    <div class="products_inner">
                        <div class="heading">
                            <h2>Recent Products</h2>
                            <div class="right">
                                <a class="view_all" href="/shop.php">View All</a>
                                <div class="slider_arrows">
                                    <span class="slider3 left"><i class="fas fa-angle-left"></i></span>
                                    <span class="slider3 right"><i class="fas fa-angle-right"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="products_slider slider3">
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="/single.php">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="/single.php">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="big_sale" style="background-image: url('/assets/img/Rectangle 253.png')">
                <h1>Big Sale Up To 70% Off</h1>
                <p>Exclussive Offers For Limited Time</p>
                <a href="/shop.php">Shop Now</a>
            </div>
            <div class="newsletter">
                <div class="newsletter_inner">
                    <h2>Join our newsletter and get $20  for your first order</h2>
                    <div class="newsletter_form">
                        <form>
                            <input placeholder="Enter Email Address">
                            <button type="submit">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
<?php include 'footer.php';?>