<?php
include('database_connection.php');

/*
if(isset($_GET['export_to_csv'])){
                                $todays_date = date("j-F-Y-g-i");
                                $filename = 'Shahway-'.$todays_date.".csv"; $delimiter=";";
                                header('Content-Type: application/csv');
                                header('Content-Disposition: attachment; filename="'.$filename.'";');
                                $final_export = fopen('php://output', 'w');
                                $this_line = array(
                                    '#',
                                    'Customer Order ID',
                                    'Order Items',
                                    'Quantity',
                                    'Charge Amount',
                                    'Shipping customer',
                                    'Product Price',
                                    'Tax',
                                    'Walmart Fees',
                                    'Shipping Cost',
                                    'Profit'
                                );
                                fputcsv($final_export, $this_line);

if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $date_range = date('Y-m-d').'_'.date('Y-m-d');
                                if(isset($_GET['date_range'])){
                                    $date_range = $_GET['date_range'];
                                }

                            

                            $no_of_records_per_page = 200;
                            $offset = ($pageno-1) * $no_of_records_per_page; 


                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0])* 1000;
                                $date_end = strtotime($date_range[1])* 1000;
                                if($date_range[0]==$date_range[1]){
                                    $date_start = strtotime('-1 day', strtotime($date_range[0]))* 1000;
                                    $date_end = strtotime('+1 day', strtotime($date_range[1]))* 1000;

                                    $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . "";
                                }else{
                                    $filter_code .= " WHERE orderDate >= " . $date_start . " AND orderDate <= " . $date_end . "";
                                }
                                
                            }



                            if(isset($_GET['status']) && !empty($_GET['status'])){
                                $filter_code .= " AND shipNode = '".$_GET['status']."'";
                            }
                            

                            $get_orders_count = "SELECT * FROM orders $filter_code ORDER BY id desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);
                            

                            $get_orders = "SELECT * FROM orders $filter_code ORDER BY id desc LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);

                            


                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_id = $order['id'];
                                    $refund = 0;
                                    $product_price = 0;
                                    $walmart_fees = 0;
                                    $total_shipping_cost = 0;
                                    $customer_shipping_cost =0;




                                    $total_qty = 0;
                                    $chargeAmount_currency = '';
                                    $taxAmount_currency = '';
                                    $chargeAmount_amount = 0;
                                    $taxAmount_amount = 0;
                                    $ssssku = '';
                                    $get_orderlines = "SELECT * FROM orderlines WHERE order_id=".$order_id;
                                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                                    if(mysqli_num_rows($get_orderlines_query) > 0){
                                    $kk=0;
                                    $o_items_aku = array();
                                    while($orderline = $get_orderlines_query->fetch_assoc()) {
                                        $kk++;
                                        $total_qty = $total_qty+$orderline['orderLineQuantity_amount'];
                                        $chargeAmount_currency = $orderline['chargeAmount_currency'];
                                        $taxAmount_currency = $orderline['taxAmount_currency'];
                                        $chargeAmount_amount = $chargeAmount_amount+floatval($orderline['chargeAmount_amount']);
                                        $taxAmount_amount = $taxAmount_amount+floatval($orderline['taxAmount_amount']);




                                        $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
        
                                        $product_prices = mysqli_query($conn, $product_prices);
                                        $row_product_prices = $product_prices->fetch_assoc();
                                        if(!empty($row_product_prices)){
                                            $product_price = $product_price+floatval($row_product_prices['cost']);
                                        }


                                        


                                        if(!empty($orderline['shipping_cost'])){
                                            $this_item_shipping_cost1 = floatval($orderline['shipping_cost'])+floatval($orderline['shipping_tax']);

                                            $customer_shipping_cost = $customer_shipping_cost+floatval($orderline['shipping_cost']);
                                            $total_shipping_cost = $total_shipping_cost+floatval($orderline['shipping_tax']);
                                        }else{
                                            $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
        
                                            $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                            $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                            if(!empty($row_shipping_cost)){
                                                $total_shipping_cost = $total_shipping_cost+floatval($row_shipping_cost['cost']);
                                            }
                                        }
                                        
                                        
                                        


                                        if (in_array($orderline['sku'], $o_items_aku)){
                                        }else{
                                            $ssssku= $orderline['sku'];
                                        }
                                        $o_items_aku[] = $orderline['sku'];

                                    } }

                            $wl_mart_fees = $chargeAmount_amount;//+$taxAmount_amount+$customer_shipping_cost;
                            $walmart_fees = $wl_mart_fees/100*15;

                            $profit = $chargeAmount_amount-$total_shipping_cost-$walmart_fees-$product_price-$refund;

                            $this_line = array(
                                $k,
                                $order_id,
                                $ssssku,
                                $total_qty,
                                number_format($chargeAmount_amount, 2),
                                number_format($customer_shipping_cost, 2),
                                number_format($product_price, 2),
                                number_format($taxAmount_amount, 2),
                                number_format($walmart_fees,2),
                                number_format($total_shipping_cost, 2),
                                number_format($profit,2)
                            );
                           
                            fputcsv($final_export, $this_line);



                        } } 



                        fclose($final_export);
                        die();

                            }

*/
?>
<?php
$active_page = 'orders';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}

/*.table_list_box{
    display: inline-block;
}*/
.table_list_outer.orders_list .table_list_box .box:nth-child(1) {
  text-align: left;
  padding: 0 0 0 0;
  width: 3%;
}
.table_list_box .box {
  width: 8%;
  display: inline-block;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(2) {
  width: 8%;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(3) {
  width: 28%;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(4) {
  width: 5%;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(5) {
  width: 5%;
}

.table_list_box.table_list_heading{
    font-size: 11px;
}
.table_list_box{
    font-size: 11px;
}
.order_detail_page_link{
    text-decoration: underline;
}
.table_list_box.table_last_column {
  padding: 5px 0;
  background: #e2e2e2;
  font-weight: 900;
}
</style>
                <?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }
                ?>
                <div class="recently_view">
                    <div class="orders_list">
                        <form class="filter_orders" method="get">
                            <input type="hidden" name="pageno" value="<?php echo $pageno; ?>">
                            <div class="input_box">
                                <label>Order ID</label>
                                <?php
                                $order_id = '';
                                if(isset($_GET['order_id'])){
                                    $order_id = $_GET['order_id'];
                                }
                                ?>
                                <input type="text" name="order_id" placeholder="Enter Order ID" value="<?php echo $order_id; ?>">
                            </div>
                            <div class="input_box"> 
                                <label>Status</label>
                                <select name="status">
                                    <option value="">Select Fullfilled</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']=="SellerFulfilled"){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="SellerFulfilled" <?php echo $selected; ?>>SellerFulfilled</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']=="WFSFulfilled"){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="WFSFulfilled" <?php echo $selected; ?>>WFSFulfilled</option>
                                </select>
                            </div>
                            <div class="input_box"> 
                                <label>Date</label>
                                <?php
                                $date_range = date('Y-m-d').'_'.date('Y-m-d');
                                if(isset($_GET['date_range'])){
                                    $date_range = $_GET['date_range'];
                                }
                                ?>
                                <input type="hidden" name="date_range" id="date_range" value="<?php echo $date_range; ?>">
                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span><?php echo $date_range; ?></span> <i class="fa fa-caret-down"></i>
                                </div>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                            <!-- <div class="export_to_csv">
                                <a href="/dashboard_orders_admin.php?export_to_csv">Export</a>

                            </div> -->
                        </form>
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Customer Order ID
                                </div>
                                <div class="box">
                                    Order Items
                                </div>
                                <div class="box">
                                    Quantity
                                </div>
                                <!-- <div class="box">
                                    Refund
                                </div> -->
                                <div class="box">
                                    Charge Amount
                                </div>
                                <div class="box">
                                    Shipping customer
                                </div>
                                <div class="box">
                                    Shipping charges
                                </div>
                                <div class="box">
                                    Product Price
                                </div>
                                <div class="box">
                                    Tax
                                </div>
                                <div class="box">
                                    Walmart Fees
                                </div>
                                <div class="box">
                                    Shipping Cost
                                </div>
                                
                                <div class="box">
                                    Profit
                                </div>
                            </div>


                            


                            
                            <?php


                            

                            $no_of_records_per_page = 30;
                            $offset = ($pageno-1) * $no_of_records_per_page; 


                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0])* 1000;
                                $date_end = strtotime($date_range[1])* 1000;
                                if($date_range[0]==$date_range[1]){
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;
                                    $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . "";
                                }else{
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;
                                    $filter_code .= " WHERE orderDate >= " . $date_start . " AND orderDate <= " . $date_end . "";
                                }
                                
                            }
                            if(isset($_GET['status']) && !empty($_GET['status'])){
                                $filter_code .= " AND shipNode = '".$_GET['status']."'";
                            }
                            if(isset($_GET['order_id']) && !empty($_GET['order_id'])){
                                $filter_code .= " AND customerOrderId = '".$_GET['order_id']."'";
                            }
                            

                            $get_orders_count = "SELECT * FROM orders $filter_code ORDER BY id desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);
                            

                            $get_orders = "SELECT * FROM orders $filter_code ORDER BY id desc LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);

                            




                            $final_quantity = 0;
                            $final_chargeAmount_amount = 0;
                            $final_product_price = 0;
                            $final_walmart_fees = 0;
                            $final_total_shipping_cost = 0;
                            $final_customer_shipping_cost =0;
                            $final_customer_shipping_cost_tax = 0;
                            $final_taxAmount_amount = 0;


                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_id = $order['id'];
                                    $refund = 0;
                                    $product_price = 0;
                                    $walmart_fees = 0;
                                    $total_shipping_cost = 0;
                                    $customer_shipping_cost =0;
                                    $customer_shipping_cost_tax = 0;



                                    $CommissionRate = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Commission on Product'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $CommissionRate = $row_reconciliationreport['Commission_Rate'];
                                    }

                                    $Fee_Reimbursement = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Fee/Reimbursement'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $Fee_Reimbursement = $row_reconciliationreport['Amount'];
                                    }

                                    $Fee_Reimbursement = preg_replace("/[^0-9.]/","",$Fee_Reimbursement);


                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <a class="order_detail_page_link" href="/dashboard_add_product_admin.php?order_id=<?php echo $order_id; ?>"><?php echo $order['customerOrderId']; ?></a>
                                    <div class="fulfilled_by <?php echo $order['shipNode']; ?>"><span><?php echo $order['shipNode']; ?></span></div>
                                    <?php 
                                    $mil = $order['orderDate'];
                                    $seconds = ceil($mil / 1000);
                                    echo date("m-d-Y H:i", $seconds);
                                     ?>
                                </div>
                                <div class="box">
                                    <?php
                                    $total_qty = 0;
                                    $chargeAmount_currency = '';
                                    $taxAmount_currency = '';
                                    $chargeAmount_amount = 0;
                                    $taxAmount_amount = 0;
                                    $get_orderlines = "SELECT * FROM orderlines WHERE order_id=".$order_id;
                                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                                    if(mysqli_num_rows($get_orderlines_query) > 0){
                                    $kk=0;
                                    $o_items_aku = array();
                                    while($orderline = $get_orderlines_query->fetch_assoc()) {


                                        $kk++;
                                        $total_qty = $total_qty+$orderline['orderLineQuantity_amount'];
                                        $chargeAmount_currency = $orderline['chargeAmount_currency'];
                                        $taxAmount_currency = $orderline['taxAmount_currency'];
                                        $chargeAmount_amount = $chargeAmount_amount+floatval($orderline['chargeAmount_amount']);
                                        $taxAmount_amount = $taxAmount_amount+floatval($orderline['taxAmount_amount']);




                                        $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
        
                                        $product_prices = mysqli_query($conn, $product_prices);
                                        $row_product_prices = $product_prices->fetch_assoc();
                                        if(!empty($row_product_prices)){
                                            $product_price = $product_price+floatval($row_product_prices['cost']);
                                        }


                                        


                                        if(!empty($orderline['shipping_cost'])){
                                            $this_item_shipping_cost1 = floatval($orderline['shipping_cost'])+floatval($orderline['shipping_tax']);

                                            $customer_shipping_cost = $customer_shipping_cost+floatval($orderline['shipping_cost']);
                                            $customer_shipping_cost_tax = $customer_shipping_cost_tax+floatval($orderline['shipping_tax']);
                                        }else{
                                            $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
        
                                            $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                            $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                            if(!empty($row_shipping_cost)){
                                                $total_shipping_cost = $total_shipping_cost+floatval($row_shipping_cost['cost']);
                                            }
                                        }
                                        
                                        
                                        


                                        if (in_array($orderline['sku'], $o_items_aku)){
                                        }else{
                                            ?>
                                            <div class="order_item_box">
                                                <?php
                                                $sku = $orderline['sku'];
                                                $walmart_item_link = 'link_not_found';
                                                $link_found = '';
                                                $get_walmart_items = "SELECT * FROM walmart_items WHERE sku='".$sku."'"; 
                                                $walmart_items = mysqli_query($conn, $get_walmart_items);
                                                $row_walmart_items = $walmart_items->fetch_assoc();
                                                if(!empty($row_walmart_items)){
                                                    $walmart_item_link = $row_walmart_items['wpid'];
                                                    $link_found = 'link_found';
                                                }
                                                
                                                ?>
                                                <a class="<?php echo $link_found; ?>" href="https://www.walmart.com/ip/<?php echo $walmart_item_link; ?>" target="_blank">
                                                    <p>#<?php echo $kk.'. '.$orderline['productName']; ?></p>
                                                    <p>Sku. <?php echo $orderline['sku']; ?></p>
                                                </a>
                                                <p>Tracking#. <?php echo $orderline['trackingNumber']; ?></p>
                                            </div>
                                            <?php
                                        }
                                        $o_items_aku[] = $orderline['sku'];







                                    } }
                                    ?>
                                </div>
                                <div class="box">
                                    <?php echo $total_qty;
                                    $final_quantity = $final_quantity+$total_qty;
                                     ?>
                                </div>
                                <!-- <div class="box"> -->
                                    <?php //echo number_format($refund, 2); ?>
                                <!-- </div> -->
                                <div class="box">
                                    $<?php echo number_format($chargeAmount_amount, 2);
                                    $final_chargeAmount_amount = $final_chargeAmount_amount+$chargeAmount_amount;
                                     ?>
                                </div>
                                <div class="box">
                                    $<?php echo number_format($customer_shipping_cost, 2);
                                    $final_customer_shipping_cost = $final_customer_shipping_cost+$customer_shipping_cost;
                                     ?>
                                </div>
                                <div class="box">
                                    $<?php echo number_format($customer_shipping_cost_tax, 2);
                                    $final_customer_shipping_cost_tax = $final_customer_shipping_cost_tax+$customer_shipping_cost_tax;
                                     ?>
                                </div>
                                <div class="box negative">
                                    $<?php echo number_format($product_price, 2);
                                    $final_product_price = $final_product_price+$product_price;
                                     ?>
                                </div>
                                <div class="box">
                                    $<?php echo number_format($taxAmount_amount, 2); 
                                    $final_taxAmount_amount = $final_taxAmount_amount+$taxAmount_amount;
                                    ?>
                                </div>
                                <div class="box negative">
                                    $<?php 



                                    $wl_mart_fees = $chargeAmount_amount;//+$taxAmount_amount+$customer_shipping_cost;
                                    $walmart_fees = $wl_mart_fees/100*$CommissionRate;
                                    $final_walmart_fees = $final_walmart_fees+$walmart_fees;
                                    echo number_format($walmart_fees,2); ?>
                                </div>
                                <div class="box negative">
                                    $<?php 
                                    $total_shipping_cost = number_format($Fee_Reimbursement*$total_qty, 2);
                                    echo $total_shipping_cost;
                                    $final_total_shipping_cost = $final_total_shipping_cost+$total_shipping_cost;
                                     ?>
                                </div>
                                
                                <?php
                                $profit_class = 'positive';
                                $profit = $chargeAmount_amount-$total_shipping_cost-$walmart_fees-$product_price-$refund;
                                if($profit<0){
                                    $profit_class = 'negative';
                                }
                                ?>
                                <div class="box <?php echo $profit_class; ?>">
                                    $<?php
                                    
                                    echo number_format($profit,2);
                                    ?>
                                </div>
                            </div>
                            <?php  



                        } } 


                        ?>
                        <div class="table_list_box table_last_column">
                            <div class="box">
                                <?php echo mysqli_num_rows($get_orders_query); ?>
                            </div>
                            <div class="box">
                                <!-- Customer Order ID -->
                            </div>
                            <div class="box">
                                <!-- Order Items -->
                            </div>
                            <div class="box">
                                <?php echo $final_quantity; ?>
                            </div>
                            <!-- <div class="box">
                                Refund
                            </div> -->
                            <div class="box">
                                $<?php echo number_format($final_chargeAmount_amount, 2); ?>
                            </div>
                            <div class="box">
                                $<?php echo number_format($final_customer_shipping_cost, 2); ?>
                            </div>
                            <div class="box">
                                $<?php echo number_format($final_customer_shipping_cost_tax, 2); ?>
                            </div>
                            <div class="box negative">
                                $<?php echo number_format($final_product_price, 2); ?>
                            </div>
                            <div class="box">
                                $<?php echo number_format($final_taxAmount_amount, 2); ?>
                            </div>
                            <div class="box negative">
                                $<?php 
                                    echo number_format($final_walmart_fees,2); ?>
                            </div>
                            <div class="box negative">
                                $<?php echo number_format($final_total_shipping_cost, 2); ?>
                            </div>
                            
                            <?php
                                $profit_class = 'positive';
                                $refund = 0;
                                $profit = $final_chargeAmount_amount-$final_total_shipping_cost-$final_walmart_fees-$final_product_price-$refund;
                                if($profit<0){
                                    $profit_class = 'negative';
                                }
                                ?>
                                <div class="box <?php echo $profit_class; ?>">
                                $<?php
                                
                                    echo number_format($profit,2);
                                ?>
                            </div>
                        </div>
                            
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                if(isset($_GET['date_range'])){
                                    $other_link = '&status='.$_GET['status'].'&date_range='.$_GET['date_range'];
                                }
                                if(isset($_GET['order_id'])){
                                    $other_link.= '&order_id='.$_GET['order_id'];
                                }




                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_orders_admin.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_orders_admin.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_orders_admin.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>

<script>
    $(window).scroll(function() {
       if($(this).scrollTop() > 240) {
         $(".table_list_box.table_list_heading").addClass("fixed");
       }else{
        $(".table_list_box.table_list_heading").removeClass("fixed");
       }
    });
</script>
<?php
include('dashboard_footer.php');

?>