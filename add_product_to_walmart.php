<?php
include('database_connection.php');
?>
<?php
$active_page = 'add_product_to_walmart';
?>
<?php
include('dashboard_header.php');
?>
<div class="user_page_wrapper">
                        <div class="recently_view category_section add_product_page">
                            <h2>Add New Walmart Item</h2>
                            <div class="category_section_inner">
                                <form method="post" enctype="multipart/form-data">

                                    
                                        <input type="hidden" name="upload_walmart_item">
                                        
                                        <div class="input_box">
                                            <label>Product Name</label>
                                            <input type="text" name="productName" id="productName">
                                        </div>
                                        <div class="input_box">
                                            <label>price</label>
                                            <input type="text" name="price" id="price">
                                        </div>
                                        <div class="input_box">
                                            <label>ShippingWeight</label>
                                            <input type="text" name="ShippingWeight" id="ShippingWeight">
                                        </div>
                                        <div class="input_box">
                                            <label>shortDescription</label>
                                            <input type="text" name="shortDescription" id="shortDescription">
                                        </div>
                                        <div class="input_box">
                                            <label>keyFeatures</label>
                                            <input type="text" name="keyFeatures" id="keyFeatures">
                                        </div>
                                        <div class="input_box">
                                            <label>shortDescription</label>
                                            <input type="text" name="shortDescription" id="shortDescription">
                                        </div>
                                        <div class="input_box">
                                            <label>mainImageUrl</label>
                                            <input type="file" name="mainImageUrl" id="mainImageUrl" accept=".png, .jpg, .jpeg">
                                        </div>
                                        <div class="input_box">
                                            <label>productSecondaryImageURL</label>
                                            <input type="file" name="productSecondaryImageURL[]" id="productSecondaryImageURL" accept=".png, .jpg, .jpeg" multiple>
                                        </div>
                                        <div class="input_box">
                                            <button class="submit_buttons">Submit</button>
                                        </div>
                                </form>
                            </div>
                        </div>
                    
                </div>                
                
<?php
include('dashboard_footer.php');
?>