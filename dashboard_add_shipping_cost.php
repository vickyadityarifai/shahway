<?php
include('database_connection.php');
?>
<?php
$active_page = 'shipping_cost';
?>
<?php
include('dashboard_header.php');
?>
<div class="recently_view_t_bg">
    <a href="/dashboard_add_shipping_cost.php"><i class="fas fa-plus"></i> Add Shipping Cost</a>
    <a href="/dashboard_add_shipping_cost_daily.php"><i class="fas fa-edit"></i> Daily Shipping Cost</a>
    <a href="/dashboard_update_shipping_cost.php"><i class="fas fa-edit"></i> Missing Shipping Cost</a>
</div>
<div class="user_page_wrapper">
                        <div class="recently_view category_section add_product_page">
                            <h2>Add New Shipping Cost</h2>
                            <p>Export from pirate ship</p>
                            <div class="category_section_inner">
                                <form method="post" enctype="multipart/form-data">
                                    
                                    
                                    <?php
                                    if(isset($_GET['step_2'])){
                                        $selection = $_SESSION['selection'];
                                        $drop_downs = explode (",", $selection); 
                                        ?>
                                        <input type="hidden" name="save_shipping1">
                                        <div class="input_box">
                                            <label>Tracking Number</label>
                                            <select name="tracking_number" id="tracking_number">
                                                <?php 
                                                foreach ($drop_downs as $key => $drop_down) {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $drop_down; ?></option>
                                                    <?php
                                                }
                                                 ?>
                                            </select>
                                        </div>
                                        <div class="input_box">
                                            <label>Cost</label>
                                            <select name="cost" id="cost">
                                                <?php 
                                                foreach ($drop_downs as $key => $drop_down) {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $drop_down; ?></option>
                                                    <?php
                                                }
                                                 ?>
                                            </select>
                                        </div>
                                        <div class="input_box">
                                            <button class="submit_buttons">Submit</button>
                                        </div>
                                        <?php
                                    }else{
                                        ?>
                                        <input type="hidden" name="save_shipping">
                                        <div class="input_box">
                                            <label>Upload File (.csv)</label>
                                            <input type="file" name="shipping_cost" id="shipping_cost" accept=".csv">
                                        </div>
                                        <div class="input_box">
                                            <button class="submit_buttons">Next</button>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    
                                    
                                </form>
                            </div>
                        </div>
                    
                </div>                
                
<?php
include('dashboard_footer.php');
?>