<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard_seller1.php"  ><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/dashboard_products.php" class="active"><i class="fas fa-boxes"></i> Products</a></li>
                        <li><a href="/dashboard_orders_seller.php"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_withdraw.php" ><i class="fas fa-hand-holding-usd"></i> Withdraw</a></li>
                        <li><a href="/dashboard_messages_seller.php" class=""><i class="fas fa-comment-dots"></i> Messages</a></li>
                        <li><a href="/dashboard_disputes_seller.php"><i class="fas fa-people-carry"></i> Disputes</a></li>
                        <li><a href="/support_seller.php" class=""><i class="fas fa-headset"></i> Support</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <h1>Hi, John Doe 👋</h1>
                        <p>Good Morning, Have a nice day.</p>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                
                <div class="recently_view category_section add_product_page add_product_form">
                    <h2>Category</h2>
                    <div class="category_section_inner">
                        <div class="category_section_inner_box">
                            <div class="input_box">
                                <label>parent category</label>
                                <select>
                                    <option>Apparel</option>
                                </select>
                            </div>
                        </div>
                        <div class="category_section_inner_box">
                            <div class="input_box">
                                <label>Sub category</label>
                                <div class="sub_category_selector">
                                    <ul>
                                        <div class="previous_selected">
                                            <li data-id="1" data-name="T-Shirts">T-Shirts <span class="remove"><i class="fas fa-times"></i></span></li>
                                            <li data-id="2" data-name="Hoodies">Hoodies <span class="remove"><i class="fas fa-times"></i></span></li>
                                        </div>
                                        <li class="select_more">
                                            Select More
                                            <ul>
                                                <li class="sub_cat_1 selected" data-id="1" data-name="T-Shirts">T-Shirts</li>
                                                <li class="sub_cat_2 selected" data-id="2" data-name="Hoodies">Hoodies</li>
                                                <li class="sub_cat_3" data-id="3" data-name="Jackets">Jackets</li>
                                                <li class="sub_cat_4" data-id="4" data-name="Shoes">Shoes</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="recently_view product_info add_product_page">
                    <h2>Product Info</h2>
                    <div class="product_info_top">
                        <div class="product_info_top_left">
                            <div class="input_box">
                                <label>Product Name</label>
                                <input type="text" placeholder="Enter Title Here">
                            </div>
                            <div class="input_box p_description">
                                <label>description</label>
                                <textarea placeholder="Write Discription Here"></textarea>
                            </div>
                            <div class="input_box">
                                <label>Price</label>
                                <span class="price_sign">$</span>
                                <input type="text" placeholder="Enter Price Here">
                            </div>
                        </div>
                        <div class="product_info_top_right">
                            <div class="input_box">
                                <label>Featured Image</label>
                                <div class="featured_image_upload">
                                    <div class="featured_image_upload_inner">
                                        <div class="image">
                                            <img src="/assets/img/upload(1) 1.png">
                                        </div>
                                        <p>
                                            Drag & Drop Image or <span>Choose Image</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="input_box">
                                <label>Image Gallery</label>
                                <div class="product_gallery">
                                    <div class="gallery_box upload_gallery_button">
                                        <div class="image">
                                            <img src="/assets/img/upload(1) 1.png">
                                        </div>
                                        <p>
                                            Upload Photo
                                        </p>
                                    </div>
                                    <div class="gallery_box">
                                        
                                    </div>
                                    <div class="gallery_box">
                                        
                                    </div>
                                    <div class="gallery_box">
                                        
                                    </div>
                                    <div class="gallery_box">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="product_info_bottom">
                        <div class="input_box">
                            <label>Additional Detail</label>
                            <textarea placeholder="Write Additional Detail Here"></textarea>
                        </div>
                        <h3><span><i class="far fa-check-square"></i></span> Stock Management</h3>
                        <div class="input_box qty_input">
                            <label>Enter Quantity</label>
                            <span class="pcs_sign">PCS</span>
                            <input placeholder="Enter Quantity Here">
                        </div>
                    </div>
                </div>
                <div class="recently_view product_variations add_product_page">
                    <h2>Variations</h2>
                    <div class="product_variations_box">
                        <div class="left">
                            <h3>Attributes</h3>
                            <div class="attributes_box heading col_3">
                                <div class="box">
                                    Name
                                </div>
                                <div class="box">
                                    Label
                                </div>
                                <div class="box">
                                    Action
                                </div>
                            </div>
                            <div class="attributes_box col_3">
                                <div class="box">
                                    Size
                                </div>
                                <div class="box">
                                    L, XL
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" class="edit">Edit</a>
                                        <a class="remove" href="#"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="attributes_box col_3">
                                <div class="box">
                                    Size
                                </div>
                                <div class="box">
                                    L, XL
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" class="edit">Edit</a>
                                        <a class="remove" href="#"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="attributes_box col_3">
                                <div class="box">
                                    Size
                                </div>
                                <div class="box">
                                    L, XL
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" class="edit">Edit</a>
                                        <a class="remove" href="#"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right">
                            <h3>
                                Add New Attributes
                            </h3>
                            <div class="input_box">
                                <label>Name</label>
                                <input placeholder="Enter Quantity Here">
                            </div>
                            <div class="input_box">
                                <label>Values</label>
                                <input placeholder="L, XL, XXL">
                            </div>
                            <div class="input_box">
                                <button>Add</button>
                            </div>
                        </div>
                    </div>
                    <div class="product_variations_box veriations">
                        <div class="left">
                            <h3>Product Variations</h3>
                            <div class="attributes_box heading col_4">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Attributes
                                </div>
                                <div class="box">
                                    Price
                                </div>
                                <div class="box">
                                    Action
                                </div>
                            </div>
                            <div class="attributes_box col_4">
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    <p>Size: XL</p>
                                    <p>Material: Silver</p>
                                </div>
                                <div class="box">
                                    $5962
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" class="edit">Edit</a>
                                        <a class="remove" href="#"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="attributes_box col_4">
                                <div class="box">
                                    02
                                </div>
                                <div class="box">
                                    <p>Size: XL</p>
                                    <p>Material: Silver</p>
                                </div>
                                <div class="box">
                                    $5962
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" class="edit">Edit</a>
                                        <a class="remove" href="#"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="attributes_box col_4">
                                <div class="box">
                                    03
                                </div>
                                <div class="box">
                                    <p>Size: XL</p>
                                    <p>Material: Silver</p>
                                </div>
                                <div class="box">
                                    $5962
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" class="edit">Edit</a>
                                        <a class="remove" href="#"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="attributes_box col_4">
                                <div class="box">
                                    04
                                </div>
                                <div class="box">
                                    <p>Size: XL</p>
                                    <p>Material: Silver</p>
                                </div>
                                <div class="box">
                                    $5962
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" class="edit">Edit</a>
                                        <a class="remove" href="#"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="attributes_box col_4">
                                <div class="box">
                                    05
                                </div>
                                <div class="box">
                                    <p>Size: XL</p>
                                    <p>Material: Silver</p>
                                </div>
                                <div class="box">
                                    $5962
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" class="edit">Edit</a>
                                        <a class="remove" href="#"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="attributes_box col_4">
                                <div class="box">
                                    06
                                </div>
                                <div class="box">
                                    <p>Size: XL</p>
                                    <p>Material: Silver</p>
                                </div>
                                <div class="box">
                                    $5962
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="#" class="edit">Edit</a>
                                        <a class="remove" href="#"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right">
                            <h3>
                                Add New Variations
                            </h3>
                            <div class="input_box">
                                <label>Size </label>
                                <select>
                                    <option>Any</option>
                                </select>
                            </div>
                            <div class="input_box">
                                <label>Color </label>
                                <select>
                                    <option>Any</option>
                                </select>
                            </div>
                            <div class="input_box">
                                <label>Material </label>
                                <select>
                                    <option>Any</option>
                                </select>
                            </div>
                            <div class="input_box">
                                <label>Price</label>
                                <span class="price_sign">$</span>
                                <input type="text" placeholder="Enter Price Here">
                            </div>
                            <div class="input_box">
                                <button>Add</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="recently_view submit_button">
                    <button>Submit</button>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>