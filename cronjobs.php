<?php 
if(isset($_POST['upload_walmart_item'])){

    if(empty($_FILES['mainImageUrl'])){

        $_SESSION['success_message'] = 'Add Images';
        header("Location: ".$domainLink."/add_product_to_walmart.php");
        die();
    }

    $target_dir = "./images/";
    $file = $_FILES['mainImageUrl']['name'];
    $path = pathinfo($file);
    $filename = $path['filename'].rand(9999999,99999999);
    $ext = $path['extension'];
    $temp_name = $_FILES['mainImageUrl']['tmp_name'];
    $path_filename_ext = $_SERVER['DOCUMENT_ROOT'].$target_dir.$filename.".".$ext;
    move_uploaded_file($temp_name,$path_filename_ext);


    $mainImageUrl = $domainLink."/images/".$filename.".".$ext;
    $productSecondaryImageURL = array();

    $file1 = $_FILES['productSecondaryImageURL'];
    $total_additional_images = count($_FILES['productSecondaryImageURL']['name']);

    for($i=0; $i<$total_additional_images; $i++){
        $target_dir = "./images/";
        $file = $_FILES['productSecondaryImageURL']['name'][$i];
        $path = pathinfo($file);
        $filename = $path['filename'].rand(9999999,99999999);
        $ext = $path['extension'];
        $temp_name = $_FILES['productSecondaryImageURL']['tmp_name'][$i];
        $path_filename_ext = $_SERVER['DOCUMENT_ROOT'].$target_dir.$filename.".".$ext;
        move_uploaded_file($temp_name,$path_filename_ext);

        $productSecondaryImageURL[] = $domainLink."/images/".$filename.".".$ext;
    }





    die();
}
if(isset($_GET['get_items'])){


    $client_id = "8369470f-234b-4489-ae51-5abe1dd9d6d9";
    $client_secret = "AOJCwTpaaGkBuQ_rwWzeb319PlnfDRySgYgPzPZHEeJ7wcYpvemFBo4EUnQuuDsIbbazoag7iW781-1Y0FVgi2k";


    $settings_id = 1;
    $check_if_setting_already = "SELECT * FROM settings WHERE id=".$settings_id;
    $run_check_if = mysqli_query($conn, $check_if_setting_already);
    $if_settings = $run_check_if->fetch_assoc();
    $is_api_active = 'no';
    if(!empty($if_settings)){
        if($if_settings['status']==1){
            $is_api_active = 'yes';
        }
    }

    if($is_api_active=='yes'){
        $client_id = $if_settings['client_id'];
        $client_secret = $if_settings['client_secret'];
    }




    
    $bytes = random_bytes(16);
    
    $curl = curl_init();

    $url = 'https://marketplace.walmartapis.com/v3/token';

    $options = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Digital Cloud Commerce',
        CURLOPT_HEADER => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLINFO_HEADER_OUT => true
    );

    $httpHeaders = array();

    $options[CURLOPT_POST] = 1;
    $options[CURLOPT_POSTFIELDS] = 'grant_type=client_credentials';
    $httpHeaders[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
    $httpHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
    $httpHeaders[] = 'Accept: application/json';
    $httpHeaders[] = 'WM_SVC.NAME: Walmart Marketplace';
    $httpHeaders[] = 'WM_QOS.CORRELATION_ID: '.base64_encode($bytes);
    $httpHeaders[] = 'WM_SVC.VERSION: 1.0.0';
    
    //var_dump($config);

    $options[CURLOPT_HTTPHEADER] = $httpHeaders;

    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);
    

    $token = '';
    if($response !== false) {
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        $header = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);

        $accessToken = json_decode($body, true);

        if(isset($accessToken['access_token'])) {
            
        $token =  $accessToken['access_token'];

        
        } else {
            
            if($flag==0)
            throw new \Exception('OAuth Failed');
            else
            $token = "OAuth Failed";
        }
    } else {
        
        
        if($flag==0)
        throw new \Exception('OAuth Failed');
        else
        $token = "OAuth Failed";
    }
    
    curl_close($curl);


    

    
    $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));
    $auth =base64_encode($client_id.':'.$client_secret);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic '.$auth,    
          'Content-Type: application/json',
          'Accept: application/json',
          'WM_SEC.ACCESS_TOKEN: '.$token,
          'WM_QOS.CORRELATION_ID: '.$uuid ,
          'WM_SVC.NAME: Walmart Marketplace'  
    ));
    

    $data11 = array(
        "inventories" => array(
            "nodes"=> array(
                array(
                    "shipNode"=>"".$shipNode."",
                    "inputQty"=>array("unit"=>"EACH", "amount"=>$inputQty)
                )
            )
        )
    );

    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/items?nextCursor=*&limit=50');

    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_exec($ch);
    $result_decode = json_decode($result);
    echo '<pre>';
    print_r($result_decode);
    die();

    foreach ($result_decode->ItemResponse as $key => $item) {
        $sku = $item->sku;
        $wpid = $item->wpid;
        $product_name = $item->productName;
        

        $table_inventory  = "SELECT * FROM walmart_items WHERE sku='".$sku."'";
    
        $table_inventory = mysqli_query($conn, $table_inventory);
        if($table_inventory){
            $row_table_inventory = $table_inventory->fetch_assoc();
            if(empty($row_table_inventory)){


                $insert_by = 0;
                $insert_date = time();



                $insert_inventory = "INSERT INTO walmart_items (
                    sku,
                    wpid,
                    product_name
                ) VALUES (
                    '".$sku."',
                    '".$wpid."',
                    '".$product_name."'
                )";
                $insert_inventory = mysqli_query($conn, $insert_inventory);   
            }else{
                $updated_by = 0;
            $updated_date = time();
                $update_inventory = "UPDATE walmart_items SET wpid='".$wpid."', product_name='".$product_name."' WHERE sku='".$sku."'";
                $update_inventory = mysqli_query($conn, $update_inventory);
            }
        }
        

    }


    $totalCount = $result_decode->totalItems;
    $nextCursor = $result_decode->nextCursor;
    $total_pages = ceil($totalCount/50);
    for($i=1; $i<$total_pages; $i++){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Authorization: Basic '.$auth,    
              'Content-Type: application/json',
              'Accept: application/json',
              'WM_SEC.ACCESS_TOKEN: '.$token,
              'WM_QOS.CORRELATION_ID: '.$uuid ,
              'WM_SVC.NAME: Walmart Marketplace'  
        ));
        

        $data11 = array(
            "inventories" => array(
                "nodes"=> array(
                    array(
                        "shipNode"=>"".$shipNode."",
                        "inputQty"=>array("unit"=>"EACH", "amount"=>$inputQty)
                    )
                )
            )
        );

        curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/items?nextCursor='.$nextCursor.'&limit=50');

        // curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result=curl_exec($ch);
        $result_decode = json_decode($result);
        

        foreach ($result_decode->ItemResponse as $key => $item) {
            $sku = $item->sku;
            $wpid = $item->wpid;
            $product_name = $item->productName;
            

            $table_inventory  = "SELECT * FROM walmart_items WHERE sku='".$sku."'";
        
            $table_inventory = mysqli_query($conn, $table_inventory);
            if($table_inventory){
                $row_table_inventory = $table_inventory->fetch_assoc();
                if(empty($row_table_inventory)){


                    $insert_by = 0;
                    $insert_date = time();



                    $insert_inventory = "INSERT INTO walmart_items (
                        sku,
                        wpid,
                        product_name
                    ) VALUES (
                        '".$sku."',
                        '".$wpid."',
                        '".$product_name."'
                    )";
                    $insert_inventory = mysqli_query($conn, $insert_inventory);   
                }else{
                    $updated_by = 0;
                $updated_date = time();
                    $update_inventory = "UPDATE walmart_items SET wpid='".$wpid."', product_name='".$product_name."' WHERE sku='".$sku."'";
                    $update_inventory = mysqli_query($conn, $update_inventory);
                }
            }
            

        }


        $nextCursor = $result_decode->nextCursor;

    }

    echo 'Done'; 

    // $_SESSION['success_message'] = 'Walmart Items Refreshed!';

    // header("Location: ".$domainLink."/dashboard_walmart_items.php");


    die();
    
    
    die();
}



if(isset($_GET['update_inventory'])){
    $sku = $_GET['sku'];
    $shipNode = $_GET['shipNode'];
    $inputQty = $_GET['inputQty'];


    $client_id = "8369470f-234b-4489-ae51-5abe1dd9d6d9";
    $client_secret = "AOJCwTpaaGkBuQ_rwWzeb319PlnfDRySgYgPzPZHEeJ7wcYpvemFBo4EUnQuuDsIbbazoag7iW781-1Y0FVgi2k";


    $settings_id = 1;
    $check_if_setting_already = "SELECT * FROM settings WHERE id=".$settings_id;
    $run_check_if = mysqli_query($conn, $check_if_setting_already);
    $if_settings = $run_check_if->fetch_assoc();
    $is_api_active = 'no';
    if(!empty($if_settings)){
        if($if_settings['status']==1){
            $is_api_active = 'yes';
        }
    }

    if($is_api_active=='yes'){
        $client_id = $if_settings['client_id'];
        $client_secret = $if_settings['client_secret'];
    }




    
    $bytes = random_bytes(16);
    
    $curl = curl_init();

    $url = 'https://marketplace.walmartapis.com/v3/token';

    $options = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Digital Cloud Commerce',
        CURLOPT_HEADER => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLINFO_HEADER_OUT => true
    );

    $httpHeaders = array();

    $options[CURLOPT_POST] = 1;
    $options[CURLOPT_POSTFIELDS] = 'grant_type=client_credentials';
    $httpHeaders[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
    $httpHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
    $httpHeaders[] = 'Accept: application/json';
    $httpHeaders[] = 'WM_SVC.NAME: Walmart Marketplace';
    $httpHeaders[] = 'WM_QOS.CORRELATION_ID: '.base64_encode($bytes);
    $httpHeaders[] = 'WM_SVC.VERSION: 1.0.0';
    
    //var_dump($config);

    $options[CURLOPT_HTTPHEADER] = $httpHeaders;

    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);
    

    $token = '';
    if($response !== false) {
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        $header = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);

        $accessToken = json_decode($body, true);

        if(isset($accessToken['access_token'])) {
            
        $token =  $accessToken['access_token'];

        
        } else {
            
            if($flag==0)
            throw new \Exception('OAuth Failed');
            else
            $token = "OAuth Failed";
        }
    } else {
        
        
        if($flag==0)
        throw new \Exception('OAuth Failed');
        else
        $token = "OAuth Failed";
    }
    
    curl_close($curl);


    

    
    $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));
    $auth =base64_encode($client_id.':'.$client_secret);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic '.$auth,    
          'Content-Type: application/json',
          'Accept: application/json',
          'WM_SEC.ACCESS_TOKEN: '.$token,
          'WM_QOS.CORRELATION_ID: '.$uuid ,
          'WM_SVC.NAME: Walmart Marketplace'  
    ));
    

    $data11 = array(
        "inventories" => array(
            "nodes"=> array(
                array(
                    "shipNode"=>"".$shipNode."",
                    "inputQty"=>array("unit"=>"EACH", "amount"=>$inputQty)
                )
            )
        )
    );

    // $body = '{"inventories":{"nodes":[{"shipNode":"10001042081","inputQty":{"unit":"EACH","amount":40},"availToSellQty":{"unit":"EACH","amount":40},"reservedQty":{"unit":"EACH","amount":40}}]}}';
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/getReport?type=buybox');
    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/inventories/'.$sku);
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/report/reconreport/availableReconFiles?reportVersion=v1');
    
    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data11));
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_exec($ch);
    $result_decode = json_decode($result);
    // echo '<pre>';
    // print_r($result_decode);


    $table_inventory  = "SELECT * FROM inventory WHERE sku='".$sku."'";
    $table_inventory = mysqli_query($conn, $table_inventory);
    if($table_inventory){
        $row_table_inventory = $table_inventory->fetch_assoc();
        if(!empty($row_table_inventory)){

            $updated_by = $_SESSION['user_id'];
            $updated_date = time();
            $update_inventory = "UPDATE inventory SET inputQty=".$inputQty.", updated_by=".$updated_by.", updated_date=".$updated_date." WHERE sku='".$sku."'";
            $update_inventory = mysqli_query($conn, $update_inventory); 
        }
    }




    echo json_encode(array(
        'status'=>200
    ));
    exit();
    die();
    
    
    die();
}
if(isset($_GET['get_inventory'])){


    $client_id = "8369470f-234b-4489-ae51-5abe1dd9d6d9";
    $client_secret = "AOJCwTpaaGkBuQ_rwWzeb319PlnfDRySgYgPzPZHEeJ7wcYpvemFBo4EUnQuuDsIbbazoag7iW781-1Y0FVgi2k";


    $settings_id = 1;
    $check_if_setting_already = "SELECT * FROM settings WHERE id=".$settings_id;
    $run_check_if = mysqli_query($conn, $check_if_setting_already);
    $if_settings = $run_check_if->fetch_assoc();
    $is_api_active = 'no';
    if(!empty($if_settings)){
        if($if_settings['status']==1){
            $is_api_active = 'yes';
        }
    }

    if($is_api_active=='yes'){
        $client_id = $if_settings['client_id'];
        $client_secret = $if_settings['client_secret'];
    }
    $bytes = random_bytes(16);
    
    $curl = curl_init();

    $url = 'https://marketplace.walmartapis.com/v3/token';

    $options = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Digital Cloud Commerce',
        CURLOPT_HEADER => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLINFO_HEADER_OUT => true
    );

    $httpHeaders = array();

    $options[CURLOPT_POST] = 1;
    $options[CURLOPT_POSTFIELDS] = 'grant_type=client_credentials';
    $httpHeaders[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
    $httpHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
    $httpHeaders[] = 'Accept: application/json';
    $httpHeaders[] = 'WM_SVC.NAME: Walmart Marketplace';
    $httpHeaders[] = 'WM_QOS.CORRELATION_ID: '.base64_encode($bytes);
    $httpHeaders[] = 'WM_SVC.VERSION: 1.0.0';
    
    //var_dump($config);

    $options[CURLOPT_HTTPHEADER] = $httpHeaders;

    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);

    $token = '';
    if($response !== false) {
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        $header = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);

        $accessToken = json_decode($body, true);

        if(isset($accessToken['access_token'])) {
            
        $token =  $accessToken['access_token'];

        
        } else {
            
            if($flag==0)
            throw new \Exception('OAuth Failed');
            else
            $token = "OAuth Failed";
        }
    } else {
        
        
        if($flag==0)
        throw new \Exception('OAuth Failed');
        else
        $token = "OAuth Failed";
    }
    
    curl_close($curl);

    

    $body = '{
       "RetireItemHeader": {  
        "feedDate": "2020-01-22T00:59:14.000Z",
         "version": "1.0"
        },
       "RetireItem": [
         
    ';
    $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));
    $auth =base64_encode($client_id.':'.$client_secret);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic '.$auth,    
          'Accept: application/json',
          'WM_SEC.ACCESS_TOKEN: '.$token,
          'WM_QOS.CORRELATION_ID: '.$uuid ,
          'WM_SVC.NAME: Walmart Marketplace'  
    ));
    
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/getReport?type=buybox');
    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/inventories?limit=50');
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/report/reconreport/availableReconFiles?reportVersion=v1');
    
    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_exec($ch);
    $result_decode = json_decode($result);
    // echo '<pre>';
    // print_r($result_decode);
    // die();
    
    $nextCursor=$result_decode->meta->nextCursor;
    $totalCount=$result_decode->meta->totalCount;


    $inventories = $result_decode->elements->inventories;
    foreach ($inventories as $key => $inventory) {
        $sku = $inventory->sku;
        $shipNode = $inventory->nodes[0]->shipNode;
        $inputQty = $inventory->nodes[0]->inputQty->amount;
        

        $table_inventory  = "SELECT * FROM inventory WHERE sku='".$sku."'";
    
        $table_inventory = mysqli_query($conn, $table_inventory);
        if($table_inventory){
            $row_table_inventory = $table_inventory->fetch_assoc();
            if(empty($row_table_inventory)){


                $insert_by = 0;
                $insert_date = time();



                $insert_inventory = "INSERT INTO inventory (
                    sku,
                    shipNode,
                    inputQty,
                    insert_by,
                    insert_date
                ) VALUES (
                    '".$sku."',
                    '".$shipNode."',
                    ".$inputQty.",
                    ".$insert_by.",
                    ".$insert_date."
                )";
                $insert_inventory = mysqli_query($conn, $insert_inventory);   
            }else{
                $updated_by = 0;
            $updated_date = time();
                $update_inventory = "UPDATE inventory SET inputQty=".$inputQty.", updated_by=".$updated_by.", updated_date=".$updated_date." WHERE sku='".$sku."'";
                $update_inventory = mysqli_query($conn, $update_inventory);
            }
        }
        

    }

    $total_pages = ceil($totalCount/50);
    for($i=1; $i<$total_pages; $i++){

        // echo '<h1>Page# '.$i.'</h1>';
        // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/getReport?type=buybox');
        curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/inventories?limit=50&nextCursor='.$nextCursor);
        // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/report/reconreport/availableReconFiles?reportVersion=v1');
        
        // curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result=curl_exec($ch);
        $result_decode = json_decode($result);
        
        $nextCursor=$result_decode->meta->nextCursor;

            $inventories = $result_decode->elements->inventories;
            foreach ($inventories as $key => $inventory) {
                $sku = $inventory->sku;
                $shipNode = $inventory->nodes[0]->shipNode;
                $inputQty = $inventory->nodes[0]->inputQty->amount;
                

                $table_inventory  = "SELECT * FROM inventory WHERE sku='".$sku."'";
            
                $table_inventory = mysqli_query($conn, $table_inventory);
                if($table_inventory){
                    $row_table_inventory = $table_inventory->fetch_assoc();
                    if(empty($row_table_inventory)){
                        $insert_by = 0;
                        $insert_date = time();
                        $insert_inventory = "INSERT INTO inventory (
                            sku,
                            shipNode,
                            inputQty,
                            insert_by,
                            insert_date
                        ) VALUES (
                            '".$sku."',
                            '".$shipNode."',
                            ".$inputQty.",
                            ".$insert_by.",
                            ".$insert_date."
                        )";
                        $insert_inventory = mysqli_query($conn, $insert_inventory);   
                    }else{
                        $updated_by = 0;
            $updated_date = time();
                        $update_inventory = "UPDATE inventory SET inputQty=".$inputQty.", updated_by=".$updated_by.", updated_date=".$updated_date." WHERE sku='".$sku."'";
                        $update_inventory = mysqli_query($conn, $update_inventory);
                    }
                }
                

            }

        
        
    }
    echo 'Done!';

    // $_SESSION['success_message'] = 'Inventory Refreshed!';

    // header("Location: ".$domainLink."/dashboard_inventory.php");


    // print_r($result_decode);
    die();
    // print_r($result);
    // echo mb_convert_encoding($result, 'UTF-8', 'ISO-8859-1');
    // echo mb_convert_encoding($result, 'UTF-16LE', 'UTF-8');
    // echo mb_convert_encoding($result, 'UTF-8', 'SJIS');
    // echo "<table>\n";
    echo '<pre>';
    print_r($result);
    die();
}


if(isset($_GET['get_inventory1'])){
    $client_id = "8369470f-234b-4489-ae51-5abe1dd9d6d9";
    $client_secret = "AOJCwTpaaGkBuQ_rwWzeb319PlnfDRySgYgPzPZHEeJ7wcYpvemFBo4EUnQuuDsIbbazoag7iW781-1Y0FVgi2k";


    $settings_id = 1;
    $check_if_setting_already = "SELECT * FROM settings WHERE id=".$settings_id;
    $run_check_if = mysqli_query($conn, $check_if_setting_already);
    $if_settings = $run_check_if->fetch_assoc();
    $is_api_active = 'no';
    if(!empty($if_settings)){
        if($if_settings['status']==1){
            $is_api_active = 'yes';
        }
    }

    if($is_api_active=='yes'){
        $client_id = $if_settings['client_id'];
        $client_secret = $if_settings['client_secret'];
    }
    $bytes = random_bytes(16);
    
    $curl = curl_init();

    $url = 'https://marketplace.walmartapis.com/v3/token';

    $options = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Digital Cloud Commerce',
        CURLOPT_HEADER => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLINFO_HEADER_OUT => true
    );

    $httpHeaders = array();

    $options[CURLOPT_POST] = 1;
    $options[CURLOPT_POSTFIELDS] = 'grant_type=client_credentials';
    $httpHeaders[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
    $httpHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
    $httpHeaders[] = 'Accept: application/json';
    $httpHeaders[] = 'WM_SVC.NAME: Walmart Marketplace';
    $httpHeaders[] = 'WM_QOS.CORRELATION_ID: '.base64_encode($bytes);
    $httpHeaders[] = 'WM_SVC.VERSION: 1.0.0';
    
    //var_dump($config);

    $options[CURLOPT_HTTPHEADER] = $httpHeaders;

    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);
    

    $token = '';
    if($response !== false) {
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        $header = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);

        $accessToken = json_decode($body, true);

        if(isset($accessToken['access_token'])) {
            
        $token =  $accessToken['access_token'];

        
        } else {
            
            if($flag==0)
            throw new \Exception('OAuth Failed');
            else
            $token = "OAuth Failed";
        }
    } else {
        
        
        if($flag==0)
        throw new \Exception('OAuth Failed');
        else
        $token = "OAuth Failed";
    }
    
    curl_close($curl);

    

    $body = '{
       "RetireItemHeader": {  
        "feedDate": "2020-01-22T00:59:14.000Z",
         "version": "1.0"
        },
       "RetireItem": [
         
    ';
    $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));
    $auth =base64_encode($client_id.':'.$client_secret);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic '.$auth,    
          'Accept: application/json',
          'WM_SEC.ACCESS_TOKEN: '.$token,
          'WM_QOS.CORRELATION_ID: '.$uuid ,
          'WM_SVC.NAME: Walmart Marketplace'  
    ));
    
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/getReport?type=buybox');
    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/fulfillment/inventory?limit=300');
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/report/reconreport/availableReconFiles?reportVersion=v1');
    
    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_exec($ch);
    $result_decode = json_decode($result);


    $inventories = $result_decode->payload->inventory;
    foreach ($inventories as $key => $inventory) {
        $sku = $inventory->sku;
        $offerId = $inventory->offerId;
        $modifiedDate = $inventory->shipNodes[0]->modifiedDate;
        $availToSellQty = $inventory->shipNodes[0]->availToSellQty;
        $onHandQty = $inventory->shipNodes[0]->onHandQty;
        $shipNodeType = $inventory->shipNodes[0]->shipNodeType;


        $table_inventory  = "SELECT * FROM wfs_inventory WHERE sku='".$sku."'";
            
        $table_inventory = mysqli_query($conn, $table_inventory);
        if($table_inventory){
            $row_table_inventory = $table_inventory->fetch_assoc();
            if(empty($row_table_inventory)){
                $insert_by = 0;
                $insert_date = time();
                $insert_inventory = "INSERT INTO wfs_inventory (
                    sku,
                    offerId,
                    modifiedDate,
                    availToSellQty,
                    onHandQty,
                    shipNodeType,
                    insert_by,
                    insert_date
                ) VALUES (
                    '".$sku."',
                    '".$offerId."',
                    '".$modifiedDate."',
                    '".$availToSellQty."',
                    '".$onHandQty."',
                    '".$shipNodeType."',
                    ".$insert_by.",
                    ".$insert_date."
                )";
                $insert_inventory = mysqli_query($conn, $insert_inventory);   
            }else{
                $updated_by = 0;
                $updated_date = time();
                $update_inventory = "UPDATE wfs_inventory SET offerId='".$offerId."', modifiedDate='".$modifiedDate."', availToSellQty='".$availToSellQty."', onHandQty='".$onHandQty."', shipNodeType='".$shipNodeType."', updated_by=".$updated_by.", updated_date=".$updated_date." WHERE sku='".$sku."'";
                $update_inventory = mysqli_query($conn, $update_inventory);
            }
        }


    }

    // echo '<pre>';
    // print_r($result_decode);
    // die();
    
    
    echo 'Done!';

    // $_SESSION['success_message'] = 'Inventory Refreshed!';

    // header("Location: ".$domainLink."/dashboard_inventory.php");


    // print_r($result_decode);
    die();
    // print_r($result);
    // echo mb_convert_encoding($result, 'UTF-8', 'ISO-8859-1');
    // echo mb_convert_encoding($result, 'UTF-16LE', 'UTF-8');
    // echo mb_convert_encoding($result, 'UTF-8', 'SJIS');
    // echo "<table>\n";
    echo '<pre>';
    print_r($result);
    die();
}




if(isset($_POST['save_reconciliationreport'])){
    $shipping_cost = $_FILES['reconciliationreport_file'];
    $fileName = $_FILES["reconciliationreport_file"]["tmp_name"];


    $target_dir = "/files/";
    $file = $_FILES['reconciliationreport_file']['name'];
    $path = pathinfo($file);
    $filename = $path['filename'].'_zzz_'.rand(9999999,99999999);
    $ext = $path['extension'];
    $temp_name = $_FILES['reconciliationreport_file']['tmp_name'];
    $path_filename_ext = $_SERVER['DOCUMENT_ROOT'].$target_dir.$filename.".".$ext;
    $_SESSION['reconciliationreport_file'] = $filename.'.'.$ext;
    $_SESSION['reconciliationreport_filename'] = $path['filename'];
    move_uploaded_file($temp_name,$path_filename_ext);

    

    header("Location: ".$domainLink."/reconciliationreport.php?step_2");
    
    die();

}




if(isset($_POST['reconciliationreport'])){
    $file = fopen($_SERVER['DOCUMENT_ROOT']."/files/".$_SESSION['reconciliationreport_file'], "r");

    
    $csv_date = array();

    while (($data = fgetcsv($file, 1000, ",")) !== FALSE) 
    {                       
      // Each individual array is being pushed into the nested array
      $csv_date[] = $data;
    }



    foreach ($csv_date as $key => $csv_dat) {
        if($key>0){

            if(!empty($csv_dat[0]) && !empty($csv_dat[1])){

                $Period_Start_Date = $csv_dat[0];
                $Period_End_Date = $csv_dat[1];
                $Transaction_Type = $csv_dat[6];
                $Transaction_Description = $csv_dat[7];
                $Amount = $csv_dat[12];
                $Amount_Type = $csv_dat[13];
                $Total_Payable = $csv_dat[2];
                $Currency = $csv_dat[3];



                $reconciliationreport_monthly  = "SELECT * FROM reconciliationreport_monthly WHERE Transaction_Description='".$Transaction_Description."' AND Period_Start_Date='".$Period_Start_Date."'";
    
                $reconciliationreport_monthly = mysqli_query($conn, $reconciliationreport_monthly);
                $row_reconciliationreport_monthly = $reconciliationreport_monthly->fetch_assoc();
                if(empty($row_reconciliationreport_monthly)){
                    $insert_by = $_SESSION['user_id'];
                    $insert_date = time();
                    $insert_orders = "INSERT INTO reconciliationreport_monthly (
                        Period_Start_Date,
                        Period_End_Date,
                        Transaction_Type,
                        Transaction_Description,
                        Amount,
                        Amount_Type,
                        Total_Payable,
                        Currency,
                        insert_by,
                        insert_date
                    ) VALUES (
                        '".$Period_Start_Date."',
                        '".$Period_End_Date."',
                        '".$Transaction_Type."',
                        '".$Transaction_Description."',
                        '".$Amount."',
                        '".$Amount_Type."',
                        '".$Total_Payable."',
                        '".$Currency."',
                        ".$insert_by.",
                        ".$insert_date."
                    )";
                    $insert_orders = mysqli_query($conn, $insert_orders);   
                }


            }else if(!empty($csv_dat[8])){

                $Transaction_Key = $csv_dat[4];
                $Transaction_Posted_Timestamp = $csv_dat[5];
                $Transaction_Type = $csv_dat[6];
                $Transaction_Description = $csv_dat[7];
                $Customer_Order = $csv_dat[8];
                $Customer_Order_line = $csv_dat[9];
                $Purchase_Order = $csv_dat[10];
                $Purchase_Order_line = $csv_dat[11];
                $Amount = $csv_dat[12];
                $Amount_Type = $csv_dat[13];
                $Ship_Qty = $csv_dat[14];
                $Commission_Rate = $csv_dat[15];
                $Transaction_Reason_Description = $csv_dat[16];
                $Partner_Item_Id = $csv_dat[17];
                $Partner_GTIN = $csv_dat[18];
                $Partner_Item_Name = $csv_dat[19];
                $Product_Tax_Code = $csv_dat[20];
                $Ship_to_State = $csv_dat[21];
                $Ship_to_City = $csv_dat[22];
                $Ship_to_Zipcode = $csv_dat[23];
                $Contract_Category = $csv_dat[24];
                $Product_Type = $csv_dat[25];
                $Commission_Rule = $csv_dat[26];
                $Shipping_Method = $csv_dat[27];
                $Fulfillment_Type = $csv_dat[28];
                $Fulfillment_Details = $csv_dat[29];
                $Original_Commission = $csv_dat[30];
                $Commission_Incentive_Program = $csv_dat[31];
                $Commission_Saving = $csv_dat[32];
                $Customer_Promo_Type = $csv_dat[33];
                $Campaign_Id = $csv_dat[34];

                $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE 
                    Transaction_Key='".$Transaction_Key."' AND 
                    Transaction_Posted_Timestamp='".$Transaction_Posted_Timestamp."' AND 
                    Transaction_Type='".$Transaction_Type."' AND 
                    Transaction_Description='".$Transaction_Description."' AND 
                    Customer_Order='".$Customer_Order."' AND 
                    Customer_Order_line='".$Customer_Order_line."' AND 
                    Purchase_Order='".$Purchase_Order."' AND 
                    Purchase_Order_line='".$Purchase_Order_line."' AND 
                    Amount='".$Amount."' AND 
                    Amount_Type='".$Amount_Type."' AND 
                    Ship_Qty='".$Ship_Qty."' AND 
                    Commission_Rate='".$Commission_Rate."' AND 
                    Transaction_Reason_Description='".$Transaction_Reason_Description."' AND 
                    Partner_Item_Id='".$Partner_Item_Id."' AND 
                    Partner_GTIN='".$Partner_GTIN."' AND 
                    Partner_Item_Name='".$Partner_Item_Name."' AND 
                    Product_Tax_Code='".$Product_Tax_Code."' AND 
                    Ship_to_State='".$Ship_to_State."' AND 
                    Ship_to_City='".$Ship_to_City."' AND 
                    Ship_to_Zipcode='".$Ship_to_Zipcode."' AND 
                    Contract_Category='".$Contract_Category."' AND 
                    Product_Type='".$Product_Type."' AND 
                    Commission_Rule='".$Commission_Rule."' AND 
                    Shipping_Method='".$Shipping_Method."' AND 
                    Fulfillment_Type='".$Fulfillment_Type."' AND 
                    Fulfillment_Details='".$Fulfillment_Details."' AND 
                    Original_Commission='".$Original_Commission."' AND 
                    Commission_Incentive_Program='".$Commission_Incentive_Program."' AND 
                    Commission_Saving='".$Commission_Saving."' AND 
                    Customer_Promo_Type='".$Customer_Promo_Type."' AND 
                    Campaign_Id='".$Campaign_Id."'
                    ";
    
                $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                if(empty($row_reconciliationreport)){
                    $insert_by = $_SESSION['user_id'];
                    $insert_date = time();
                    $insert_orders = "INSERT INTO reconciliationreport (
                        Transaction_Key,
                        Transaction_Posted_Timestamp,
                        Transaction_Type,
                        Transaction_Description,
                        Customer_Order,
                        Customer_Order_line,
                        Purchase_Order,
                        Purchase_Order_line,
                        Amount,
                        Amount_Type,
                        Ship_Qty,
                        Commission_Rate,
                        Transaction_Reason_Description,
                        Partner_Item_Id,
                        Partner_GTIN,
                        Partner_Item_Name,
                        Product_Tax_Code,
                        Ship_to_State,
                        Ship_to_City,
                        Ship_to_Zipcode,
                        Contract_Category,
                        Product_Type,
                        Commission_Rule,
                        Shipping_Method,
                        Fulfillment_Type,
                        Fulfillment_Details,
                        Original_Commission,
                        Commission_Incentive_Program,
                        Commission_Saving,
                        Customer_Promo_Type,
                        Campaign_Id,
                        insert_by,
                        insert_date
                    ) VALUES (
                        '".$Transaction_Key."',
                        '".$Transaction_Posted_Timestamp."',
                        '".$Transaction_Type."',
                        '".$Transaction_Description."',
                        '".$Customer_Order."',
                        '".$Customer_Order_line."',
                        '".$Purchase_Order."',
                        '".$Purchase_Order_line."',
                        '".$Amount."',
                        '".$Amount_Type."',
                        '".$Ship_Qty."',
                        '".$Commission_Rate."',
                        '".$Transaction_Reason_Description."',
                        '".$Partner_Item_Id."',
                        '".$Partner_GTIN."',
                        '".$Partner_Item_Name."',
                        '".$Product_Tax_Code."',
                        '".$Ship_to_State."',
                        '".$Ship_to_City."',
                        '".$Ship_to_Zipcode."',
                        '".$Contract_Category."',
                        '".$Product_Type."',
                        '".$Commission_Rule."',
                        '".$Shipping_Method."',
                        '".$Fulfillment_Type."',
                        '".$Fulfillment_Details."',
                        '".$Original_Commission."',
                        '".$Commission_Incentive_Program."',
                        '".$Commission_Saving."',
                        '".$Customer_Promo_Type."',
                        '".$Campaign_Id."',
                        ".$insert_by.",
                        ".$insert_date."
                    )";
                    $insert_orders = mysqli_query($conn, $insert_orders);
                    // echo mysqli_error($conn);
                    $order_id = mysqli_insert_id($conn);
                }

            }
        }
    }


    $message= 'Reconciliation Report Added!';
    $user_id = $_SESSION['user_id'];
    $status = 0;
    $date_created = time();
    $insert_notification = "INSERT INTO notifications (
        user_id,
        message,
        status,
        date_created
    ) VALUES (
        ".$user_id.",
        '".$message."',
        ".$status.",
        ".$date_created."
    )";
    $insert_notification = mysqli_query($conn, $insert_notification);



    $insert_date = time();
    $filename = $_SESSION['reconciliationreport_filename'];
    $reconciliationreport_files = "INSERT INTO reconciliationreport_files (
        insert_date,
        filename
    ) VALUES (
        ".$insert_date.",
        '".$filename."'
    )";
    $reconciliationreport_files = mysqli_query($conn, $reconciliationreport_files);

    
    

    $_SESSION['success_message'] = 'Reconciliation Report added successfully!';

    header("Location: ".$domainLink."/reconciliationreport.php");

    die();
}
if(isset($_GET['recon_report'])){
	$reportDate = '20221105';//date('yyyyMMdd');
	// echo $reportDate;
	// die();
	$flag = 1;
    $client_id = "8369470f-234b-4489-ae51-5abe1dd9d6d9";
    $client_secret = "AOJCwTpaaGkBuQ_rwWzeb319PlnfDRySgYgPzPZHEeJ7wcYpvemFBo4EUnQuuDsIbbazoag7iW781-1Y0FVgi2k";


    $settings_id = 1;
    $check_if_setting_already = "SELECT * FROM settings WHERE id=".$settings_id;
    $run_check_if = mysqli_query($conn, $check_if_setting_already);
    $if_settings = $run_check_if->fetch_assoc();
    $is_api_active = 'no';
    if(!empty($if_settings)){
        if($if_settings['status']==1){
            $is_api_active = 'yes';
        }
    }

    if($is_api_active=='yes'){
        $client_id = $if_settings['client_id'];
        $client_secret = $if_settings['client_secret'];
    }
    $bytes = random_bytes(16);
    
    $curl = curl_init();

    $url = 'https://marketplace.walmartapis.com/v3/token';

    $options = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Digital Cloud Commerce',
        CURLOPT_HEADER => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLINFO_HEADER_OUT => true
    );

    $httpHeaders = array();

    $options[CURLOPT_POST] = 1;
    $options[CURLOPT_POSTFIELDS] = 'grant_type=client_credentials';
    $httpHeaders[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
    $httpHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
    $httpHeaders[] = 'Accept: application/json';
    $httpHeaders[] = 'WM_SVC.NAME: Walmart Marketplace';
    $httpHeaders[] = 'WM_QOS.CORRELATION_ID: '.base64_encode($bytes);
    $httpHeaders[] = 'WM_SVC.VERSION: 1.0.0';
    
    //var_dump($config);

    $options[CURLOPT_HTTPHEADER] = $httpHeaders;

    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);
    

    $token = '';
    if($response !== false) {
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        $header = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);

        $accessToken = json_decode($body, true);

        if(isset($accessToken['access_token'])) {
            
        $token =  $accessToken['access_token'];

        
        } else {
            
            if($flag==0)
            throw new \Exception('OAuth Failed');
            else
            $token = "OAuth Failed";
        }
    } else {
        
        
        if($flag==0)
        throw new \Exception('OAuth Failed');
        else
        $token = "OAuth Failed";
    }
    
    curl_close($curl);

    

    $body = '{
       "RetireItemHeader": {  
        "feedDate": "2020-01-22T00:59:14.000Z",
         "version": "1.0"
        },
       "RetireItem": [
         
    ';
    $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));
    $auth =base64_encode($client_id.':'.$client_secret);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic '.$auth,    
          'Accept: application/json',
          'WM_SEC.ACCESS_TOKEN: '.$token,
          'WM_QOS.CORRELATION_ID: '.$uuid ,
          'WM_SVC.NAME: Walmart Marketplace'  
    )); 
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/getReport?type=buybox');
    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/report/reconreport/reconFile?partnerId=10001042081&reportVersion=v1&reportDate=05242022');
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/report/reconreport/availableReconFiles?reportVersion=v1');
    
    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_exec($ch);
    // echo mb_convert_encoding($result, 'UTF-8', 'ISO-8859-1');
    // echo mb_convert_encoding($result, 'UTF-16LE', 'UTF-8');
    // echo mb_convert_encoding($result, 'UTF-8', 'SJIS');
    // echo "<table>\n";
    echo $result;
    // echo "</table>\n";
    die();
    echo '<pre>';
    print_r($result);
    die();
    // $orders = json_decode($result);
    // echo '<pre>';
    // print_r($orders);

    die();
}
if(isset($_GET['save_orders'])){

	// for SellerFulfilled

	$_shipNodeType = "SellerFulfilled";


	$scrap_date_start = date('Y-m-d');
	$scrap_date_end = date('Y-m-d',strtotime("+1 days"));

    // $scrap_date_start = '2022-10-01';
    // $scrap_date_end = '2022-10-02';


	$flag = 1;
    $client_id = "8369470f-234b-4489-ae51-5abe1dd9d6d9";
    $client_secret = "AOJCwTpaaGkBuQ_rwWzeb319PlnfDRySgYgPzPZHEeJ7wcYpvemFBo4EUnQuuDsIbbazoag7iW781-1Y0FVgi2k";


    $settings_id = 1;
    $check_if_setting_already = "SELECT * FROM settings WHERE id=".$settings_id;
    $run_check_if = mysqli_query($conn, $check_if_setting_already);
    $if_settings = $run_check_if->fetch_assoc();
    $is_api_active = 'no';
    if(!empty($if_settings)){
        if($if_settings['status']==1){
            $is_api_active = 'yes';
        }
    }

    if($is_api_active=='yes'){
        $client_id = $if_settings['client_id'];
        $client_secret = $if_settings['client_secret'];
    }
    $bytes = random_bytes(16);
    
    $curl = curl_init();

    $url = 'https://marketplace.walmartapis.com/v3/token';

    $options = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Digital Cloud Commerce',
        CURLOPT_HEADER => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLINFO_HEADER_OUT => true
    );

    $httpHeaders = array();

    $options[CURLOPT_POST] = 1;
    $options[CURLOPT_POSTFIELDS] = 'grant_type=client_credentials';
    $httpHeaders[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
    $httpHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
    $httpHeaders[] = 'Accept: application/json';
    $httpHeaders[] = 'WM_SVC.NAME: Walmart Marketplace';
    $httpHeaders[] = 'WM_QOS.CORRELATION_ID: '.base64_encode($bytes);
    $httpHeaders[] = 'WM_SVC.VERSION: 1.0.0';
    
    //var_dump($config);

    $options[CURLOPT_HTTPHEADER] = $httpHeaders;

    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);
    

    $token = '';
    if($response !== false) {
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        $header = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);

        $accessToken = json_decode($body, true);

        if(isset($accessToken['access_token'])) {
            
        $token =  $accessToken['access_token'];

        
        } else {
            
            if($flag==0)
            throw new \Exception('OAuth Failed');
            else
            $token = "OAuth Failed";
        }
    } else {
        
        
        if($flag==0)
        throw new \Exception('OAuth Failed');
        else
        $token = "OAuth Failed";
    }
    
    curl_close($curl);

    

    $body = '{
       "RetireItemHeader": {  
        "feedDate": "2020-01-22T00:59:14.000Z",
         "version": "1.0"
        },
       "RetireItem": [
         
    ';
    $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));
    $auth =base64_encode($client_id.':'.$client_secret);
    

    



    $_shipNodeType = "SellerFulfilled";



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic '.$auth,    
          'Accept: application/json',
          'WM_SEC.ACCESS_TOKEN: '.$token,
          'WM_QOS.CORRELATION_ID: '.$uuid ,
          'WM_SVC.NAME: Walmart Marketplace'  
    ));
    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/orders?limit=1000&createdStartDate='.$scrap_date_start.'&createdEndDate='.$scrap_date_end.'&shipNodeType='.$_shipNodeType);
    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_exec($ch);
    $orders = json_decode($result);

    

    // echo '<pre>';
    // print_r($orders);
    // die();
    echo 'SellerFulfilled '.$scrap_date_start.' '.$scrap_date_end;
    echo $orders->list->meta->totalCount;
    echo '<br>';


    foreach ($orders->list->elements->order as $key => $order) {



    	

	    	




	    	$purchaseOrderId = 0;
	    	$customerOrderId = 0;
	    	$customerEmailId = '';
	    	$orderDate = 0;
	    	$shipNode = '';
	    	$shipping_phone = '';
	    	$shipping_estimatedDeliveryDate = 0;
	    	$shipping_estimatedShipDate = 0;
	    	$shipping_methodCode = '';
	    	$shipping_address1 = '';
	    	$shipping_address2 = '';
	    	$shipping_city = '';
	    	$shipping_state = '';
	    	$shipping_postalCode = '';
	    	$shipping_country = '';
	    	$shipping_addressType = '';

	    	








	    	$purchaseOrderId = $order->purchaseOrderId;
	    	$customerOrderId = $order->customerOrderId;
	    	$customerEmailId = $order->customerEmailId;
	    	$orderDate = $order->orderDate;
	    	if(!empty($order->shippingInfo)){
	    		$shipping_phone = $order->shippingInfo->phone;
		    	$shipping_estimatedDeliveryDate = $order->shippingInfo->estimatedDeliveryDate;
		    	$shipping_estimatedShipDate = $order->shippingInfo->estimatedShipDate;
		    	$shipping_methodCode = $order->shippingInfo->methodCode;
		    	if(!empty($order->shippingInfo->postalAddress)){
		    		$shipping_address1 = $order->shippingInfo->postalAddress->address1;
			    	$shipping_address2 = $order->shippingInfo->postalAddress->address2;
			    	$shipping_city = $order->shippingInfo->postalAddress->city;
			    	$shipping_state = $order->shippingInfo->postalAddress->state;
			    	$shipping_postalCode = $order->shippingInfo->postalAddress->postalCode;
			    	$shipping_country = $order->shippingInfo->postalAddress->country;
			    	$shipping_addressType = $order->shippingInfo->postalAddress->addressType;
		    	}
	    	}
	    	if(!empty($order->shipNode)){
	    		$shipNode = $order->shipNode->type;
	    	}

	    	$check_if_order_already = "SELECT * FROM orders WHERE purchaseOrderId='".$purchaseOrderId."'";
			$run_check_if = mysqli_query($conn, $check_if_order_already);
	    	$if_order = $run_check_if->fetch_assoc();
	    	if(empty($if_order)){


                $insert_by = 0;
                $insert_date = time();
	    	$insert_orders = "INSERT INTO orders (
		    	purchaseOrderId,
		    	customerOrderId,
		    	customerEmailId,
		    	orderDate,
		    	shipNode,
		    	shipping_phone,
		    	shipping_estimatedDeliveryDate,
		    	shipping_estimatedShipDate,
		    	shipping_methodCode,
		    	shipping_address1,
		    	shipping_address2,
		    	shipping_city,
		    	shipping_state,
		    	shipping_postalCode,
		    	shipping_country,
		    	shipping_addressType,
                insert_by,
                    insert_date
	    	) VALUES (
	    		'".$purchaseOrderId."',
		    	'".$customerOrderId."',
		    	'".$customerEmailId."',
		    	'".$orderDate."',
		    	'".$shipNode."',
		    	'".$shipping_phone."',
		    	'".$shipping_estimatedDeliveryDate."',
		    	'".$shipping_estimatedShipDate."',
		    	'".$shipping_methodCode."',
		    	'".$shipping_address1."',
		    	'".$shipping_address2."',
		    	'".$shipping_city."',
		    	'".$shipping_state."',
		    	'".$shipping_postalCode."',
		    	'".$shipping_country."',
		    	'".$shipping_addressType."',
                ".$insert_by.",
                    ".$insert_date."
	    	)";
			$insert_orders = mysqli_query($conn, $insert_orders);
			$order_id = mysqli_insert_id($conn);


	    	$orderLines = $order->orderLines->orderLine;
	    	foreach ($orderLines as $k => $orderLine) {






	    		$productName = '';
	    		$sku = '';
	    		$orderLineQuantity_unitOfMeasurement = '';
	    		$orderLineQuantity_amount = 0;
	    		$statusDate = 0;
	    		$orderLineStatus_status = '';
	    		$trackingNumber = '';
	    		$fulfillmentOption = '';
	    		$pickUpDateTime = 0;
	    		$chargeType = '';
	    		$chargeAmount_currency = '';
	    		$chargeAmount_amount = 0;
	    		$taxAmount_currency = '';
	    		$taxAmount_amount = 0;
	    		$shipping_cost = 0;
	    		$shipping_tax = 0;








	    		$productName = $orderLine->item->productName;
	    		$sku = $orderLine->item->sku;
	    		$orderLineQuantity_unitOfMeasurement = $orderLine->orderLineQuantity->unitOfMeasurement;
	    		$orderLineQuantity_amount = $orderLine->orderLineQuantity->amount;
	    		$statusDate = $orderLine->statusDate;
	    		if(!empty($orderLine->orderLineStatuses)){
	    			if(isset($orderLine->orderLineStatuses->orderLineStatus[0])){
	    				$orderLineStatus_status = $orderLine->orderLineStatuses->orderLineStatus[0]->status;
			    		if(!empty($orderLine->orderLineStatuses->orderLineStatus[0]->trackingInfo)){
				    		$trackingNumber = $orderLine->orderLineStatuses->orderLineStatus[0]->trackingInfo->trackingNumber;
				    	}
	    			}
		    		
		    	}
		    	if(!empty($orderLine->fulfillment)){
		    		$fulfillmentOption = $orderLine->fulfillment->fulfillmentOption;
		    		$pickUpDateTime = $orderLine->fulfillment->pickUpDateTime;
		    	}

		    	if(!empty($orderLine->charges)){
		    		if(isset($orderLine->charges->charge[0])){
		    			$chargeType = $orderLine->charges->charge[0]->chargeType;
		    			$chargeAmount_currency = $orderLine->charges->charge[0]->chargeAmount->currency;
		    			$chargeAmount_amount = $orderLine->charges->charge[0]->chargeAmount->amount;
		    			if(!empty($orderLine->charges->charge[0]->tax)){
		    				$taxAmount_currency = $orderLine->charges->charge[0]->tax->taxAmount->currency;
		    				$taxAmount_amount = $orderLine->charges->charge[0]->tax->taxAmount->amount;
		    			}
		    		}
		    		if(isset($orderLine->charges->charge[1])){
		    			$chargeType = $orderLine->charges->charge[1]->chargeType;
		    			if($chargeType=='SHIPPING'){
			    			$shipping_cost = $orderLine->charges->charge[1]->chargeAmount->amount;
			    			if(!empty($orderLine->charges->charge[1]->tax)){
			    				$shipping_tax = $orderLine->charges->charge[1]->tax->taxAmount->amount;
			    			}
		    			}
		    			
		    		}
		    	}





                $insert_by = 1;
                $insert_date = time();
		    	$insert_orderlines = "INSERT INTO orderlines (
			    	order_id,
			    	productName,
			    	sku,
			    	orderLineQuantity_unitOfMeasurement,
			    	orderLineQuantity_amount,
			    	statusDate,
			    	orderLineStatus_status,
			    	trackingNumber,
			    	fulfillmentOption,
			    	pickUpDateTime,
			    	chargeType,
			    	chargeAmount_currency,
			    	chargeAmount_amount,
			    	taxAmount_currency,
			    	taxAmount_amount,
			    	shipping_cost,
			    	shipping_tax,
                    insert_by,
                    insert_date
		    	) VALUES (
		    		".$order_id.",
			    	'".$productName."',
			    	'".$sku."',
			    	'".$orderLineQuantity_unitOfMeasurement."',
			    	'".$orderLineQuantity_amount."',
			    	'".$statusDate."',
			    	'".$orderLineStatus_status."',
			    	'".$trackingNumber."',
			    	'".$fulfillmentOption."',
			    	'".$pickUpDateTime."',
			    	'".$chargeType."',
			    	'".$chargeAmount_currency."',
			    	'".$chargeAmount_amount."',
			    	'".$taxAmount_currency."',
			    	'".$taxAmount_amount."',
			    	'".$shipping_cost."',
			    	'".$shipping_tax."',
                    ".$insert_by.",
                    ".$insert_date."
		    	)";
				$insert_orderlines = mysqli_query($conn, $insert_orderlines);

	    	}
    	
    	}	
    }




    // check if second page 
    if(!empty($orders->list->meta->nextCursor)){
    	
    	$ch = curl_init();
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	          'Authorization: Basic '.$auth,    
	          'Accept: application/json',
	          'WM_SEC.ACCESS_TOKEN: '.$token,
	          'WM_QOS.CORRELATION_ID: '.$uuid ,
	          'WM_SVC.NAME: Walmart Marketplace'  
	    ));
	    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/orders'.$orders->list->meta->nextCursor);
	    // curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $result=curl_exec($ch);
	    $orders = json_decode($result);





	    foreach ($orders->list->elements->order as $key => $order) {



	    	

		    	




		    	$purchaseOrderId = 0;
		    	$customerOrderId = 0;
		    	$customerEmailId = '';
		    	$orderDate = 0;
		    	$shipNode = '';
		    	$shipping_phone = '';
		    	$shipping_estimatedDeliveryDate = 0;
		    	$shipping_estimatedShipDate = 0;
		    	$shipping_methodCode = '';
		    	$shipping_address1 = '';
		    	$shipping_address2 = '';
		    	$shipping_city = '';
		    	$shipping_state = '';
		    	$shipping_postalCode = '';
		    	$shipping_country = '';
		    	$shipping_addressType = '';

		    	








		    	$purchaseOrderId = $order->purchaseOrderId;
		    	$customerOrderId = $order->customerOrderId;
		    	$customerEmailId = $order->customerEmailId;
		    	$orderDate = $order->orderDate;
		    	if(!empty($order->shippingInfo)){
		    		$shipping_phone = $order->shippingInfo->phone;
			    	$shipping_estimatedDeliveryDate = $order->shippingInfo->estimatedDeliveryDate;
			    	$shipping_estimatedShipDate = $order->shippingInfo->estimatedShipDate;
			    	$shipping_methodCode = $order->shippingInfo->methodCode;
			    	if(!empty($order->shippingInfo->postalAddress)){
			    		$shipping_address1 = $order->shippingInfo->postalAddress->address1;
				    	$shipping_address2 = $order->shippingInfo->postalAddress->address2;
				    	$shipping_city = $order->shippingInfo->postalAddress->city;
				    	$shipping_state = $order->shippingInfo->postalAddress->state;
				    	$shipping_postalCode = $order->shippingInfo->postalAddress->postalCode;
				    	$shipping_country = $order->shippingInfo->postalAddress->country;
				    	$shipping_addressType = $order->shippingInfo->postalAddress->addressType;
			    	}
		    	}
		    	if(!empty($order->shipNode)){
		    		$shipNode = $order->shipNode->type;
		    	}

		    	$check_if_order_already = "SELECT * FROM orders WHERE purchaseOrderId='".$purchaseOrderId."'";
				$run_check_if = mysqli_query($conn, $check_if_order_already);
		    	$if_order = $run_check_if->fetch_assoc();
		    	if(empty($if_order)){


                    $insert_by = 0;
                $insert_date = time();
		    	$insert_orders = "INSERT INTO orders (
			    	purchaseOrderId,
			    	customerOrderId,
			    	customerEmailId,
			    	orderDate,
			    	shipNode,
			    	shipping_phone,
			    	shipping_estimatedDeliveryDate,
			    	shipping_estimatedShipDate,
			    	shipping_methodCode,
			    	shipping_address1,
			    	shipping_address2,
			    	shipping_city,
			    	shipping_state,
			    	shipping_postalCode,
			    	shipping_country,
			    	shipping_addressType,
                    insert_by,
                    insert_date
		    	) VALUES (
		    		'".$purchaseOrderId."',
			    	'".$customerOrderId."',
			    	'".$customerEmailId."',
			    	'".$orderDate."',
			    	'".$shipNode."',
			    	'".$shipping_phone."',
			    	'".$shipping_estimatedDeliveryDate."',
			    	'".$shipping_estimatedShipDate."',
			    	'".$shipping_methodCode."',
			    	'".$shipping_address1."',
			    	'".$shipping_address2."',
			    	'".$shipping_city."',
			    	'".$shipping_state."',
			    	'".$shipping_postalCode."',
			    	'".$shipping_country."',
			    	'".$shipping_addressType."',
                    ".$insert_by.",
                    ".$insert_date."
		    	)";
				$insert_orders = mysqli_query($conn, $insert_orders);
				$order_id = mysqli_insert_id($conn);


		    	$orderLines = $order->orderLines->orderLine;
		    	foreach ($orderLines as $k => $orderLine) {






		    		$productName = '';
		    		$sku = '';
		    		$orderLineQuantity_unitOfMeasurement = '';
		    		$orderLineQuantity_amount = 0;
		    		$statusDate = 0;
		    		$orderLineStatus_status = '';
		    		$trackingNumber = '';
		    		$fulfillmentOption = '';
		    		$pickUpDateTime = 0;
		    		$chargeType = '';
		    		$chargeAmount_currency = '';
		    		$chargeAmount_amount = 0;
		    		$taxAmount_currency = '';
		    		$taxAmount_amount = 0;
		    		$shipping_cost = 0;
		    		$shipping_tax = 0;








		    		$productName = $orderLine->item->productName;
		    		$sku = $orderLine->item->sku;
		    		$orderLineQuantity_unitOfMeasurement = $orderLine->orderLineQuantity->unitOfMeasurement;
		    		$orderLineQuantity_amount = $orderLine->orderLineQuantity->amount;
		    		$statusDate = $orderLine->statusDate;
		    		if(!empty($orderLine->orderLineStatuses)){
		    			if(isset($orderLine->orderLineStatuses->orderLineStatus[0])){
		    				$orderLineStatus_status = $orderLine->orderLineStatuses->orderLineStatus[0]->status;
				    		if(!empty($orderLine->orderLineStatuses->orderLineStatus[0]->trackingInfo)){
					    		$trackingNumber = $orderLine->orderLineStatuses->orderLineStatus[0]->trackingInfo->trackingNumber;
					    	}
		    			}
			    		
			    	}
			    	if(!empty($orderLine->fulfillment)){
			    		$fulfillmentOption = $orderLine->fulfillment->fulfillmentOption;
			    		$pickUpDateTime = $orderLine->fulfillment->pickUpDateTime;
			    	}

			    	if(!empty($orderLine->charges)){
			    		if(isset($orderLine->charges->charge[0])){
			    			$chargeType = $orderLine->charges->charge[0]->chargeType;
			    			$chargeAmount_currency = $orderLine->charges->charge[0]->chargeAmount->currency;
			    			$chargeAmount_amount = $orderLine->charges->charge[0]->chargeAmount->amount;
			    			if(!empty($orderLine->charges->charge[0]->tax)){
			    				$taxAmount_currency = $orderLine->charges->charge[0]->tax->taxAmount->currency;
			    				$taxAmount_amount = $orderLine->charges->charge[0]->tax->taxAmount->amount;
			    			}
			    		}
			    		if(isset($orderLine->charges->charge[1])){
			    			$chargeType = $orderLine->charges->charge[1]->chargeType;
			    			if($chargeType=='SHIPPING'){
				    			$shipping_cost = $orderLine->charges->charge[1]->chargeAmount->amount;
				    			if(!empty($orderLine->charges->charge[1]->tax)){
				    				$shipping_tax = $orderLine->charges->charge[1]->tax->taxAmount->amount;
				    			}
			    			}
			    			
			    		}
			    	}





                    $insert_by = 0;
                $insert_date = time();
			    	$insert_orderlines = "INSERT INTO orderlines (
				    	order_id,
				    	productName,
				    	sku,
				    	orderLineQuantity_unitOfMeasurement,
				    	orderLineQuantity_amount,
				    	statusDate,
				    	orderLineStatus_status,
				    	trackingNumber,
				    	fulfillmentOption,
				    	pickUpDateTime,
				    	chargeType,
				    	chargeAmount_currency,
				    	chargeAmount_amount,
				    	taxAmount_currency,
				    	taxAmount_amount,
				    	shipping_cost,
				    	shipping_tax,
                        insert_by,
                    insert_date
			    	) VALUES (
			    		".$order_id.",
				    	'".$productName."',
				    	'".$sku."',
				    	'".$orderLineQuantity_unitOfMeasurement."',
				    	'".$orderLineQuantity_amount."',
				    	'".$statusDate."',
				    	'".$orderLineStatus_status."',
				    	'".$trackingNumber."',
				    	'".$fulfillmentOption."',
				    	'".$pickUpDateTime."',
				    	'".$chargeType."',
				    	'".$chargeAmount_currency."',
				    	'".$chargeAmount_amount."',
				    	'".$taxAmount_currency."',
				    	'".$taxAmount_amount."',
				    	'".$shipping_cost."',
				    	'".$shipping_tax."',
                        ".$insert_by.",
                    ".$insert_date."
			    	)";
					$insert_orderlines = mysqli_query($conn, $insert_orderlines);

		    	}
	    	
	    	}	
	    }



    }







    // for WFS


    $_shipNodeType = "WFSFulfilled";



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic '.$auth,    
          'Accept: application/json',
          'WM_SEC.ACCESS_TOKEN: '.$token,
          'WM_QOS.CORRELATION_ID: '.$uuid ,
          'WM_SVC.NAME: Walmart Marketplace'  
    ));
    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/orders?limit=1000&createdStartDate='.$scrap_date_start.'&createdEndDate='.$scrap_date_end.'&shipNodeType='.$_shipNodeType);
    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_exec($ch);
    $orders = json_decode($result);

    

    // echo '<pre>';
    // print_r($orders);
    // die();
    echo 'WFSFulfilled ';
    echo $orders->list->meta->totalCount;
    echo '<br>';


    foreach ($orders->list->elements->order as $key => $order) {



    	

	    	




	    	$purchaseOrderId = 0;
	    	$customerOrderId = 0;
	    	$customerEmailId = '';
	    	$orderDate = 0;
	    	$shipNode = '';
	    	$shipping_phone = '';
	    	$shipping_estimatedDeliveryDate = 0;
	    	$shipping_estimatedShipDate = 0;
	    	$shipping_methodCode = '';
	    	$shipping_address1 = '';
	    	$shipping_address2 = '';
	    	$shipping_city = '';
	    	$shipping_state = '';
	    	$shipping_postalCode = '';
	    	$shipping_country = '';
	    	$shipping_addressType = '';

	    	








	    	$purchaseOrderId = $order->purchaseOrderId;
	    	$customerOrderId = $order->customerOrderId;
	    	$customerEmailId = $order->customerEmailId;
	    	$orderDate = $order->orderDate;
	    	if(!empty($order->shippingInfo)){
	    		$shipping_phone = $order->shippingInfo->phone;
		    	$shipping_estimatedDeliveryDate = $order->shippingInfo->estimatedDeliveryDate;
		    	$shipping_estimatedShipDate = $order->shippingInfo->estimatedShipDate;
		    	$shipping_methodCode = $order->shippingInfo->methodCode;
		    	if(!empty($order->shippingInfo->postalAddress)){
		    		$shipping_address1 = $order->shippingInfo->postalAddress->address1;
			    	$shipping_address2 = $order->shippingInfo->postalAddress->address2;
			    	$shipping_city = $order->shippingInfo->postalAddress->city;
			    	$shipping_state = $order->shippingInfo->postalAddress->state;
			    	$shipping_postalCode = $order->shippingInfo->postalAddress->postalCode;
			    	$shipping_country = $order->shippingInfo->postalAddress->country;
			    	$shipping_addressType = $order->shippingInfo->postalAddress->addressType;
		    	}
	    	}
	    	if(!empty($order->shipNode)){
	    		$shipNode = $order->shipNode->type;
	    	}

	    	$check_if_order_already = "SELECT * FROM orders WHERE purchaseOrderId='".$purchaseOrderId."'";
			$run_check_if = mysqli_query($conn, $check_if_order_already);
	    	$if_order = $run_check_if->fetch_assoc();
	    	if(empty($if_order)){


                $insert_by = 0;
                $insert_date = time();
	    	$insert_orders = "INSERT INTO orders (
		    	purchaseOrderId,
		    	customerOrderId,
		    	customerEmailId,
		    	orderDate,
		    	shipNode,
		    	shipping_phone,
		    	shipping_estimatedDeliveryDate,
		    	shipping_estimatedShipDate,
		    	shipping_methodCode,
		    	shipping_address1,
		    	shipping_address2,
		    	shipping_city,
		    	shipping_state,
		    	shipping_postalCode,
		    	shipping_country,
		    	shipping_addressType,
                insert_by,
                    insert_date
	    	) VALUES (
	    		'".$purchaseOrderId."',
		    	'".$customerOrderId."',
		    	'".$customerEmailId."',
		    	'".$orderDate."',
		    	'".$shipNode."',
		    	'".$shipping_phone."',
		    	'".$shipping_estimatedDeliveryDate."',
		    	'".$shipping_estimatedShipDate."',
		    	'".$shipping_methodCode."',
		    	'".$shipping_address1."',
		    	'".$shipping_address2."',
		    	'".$shipping_city."',
		    	'".$shipping_state."',
		    	'".$shipping_postalCode."',
		    	'".$shipping_country."',
		    	'".$shipping_addressType."',
                ".$insert_by.",
                    ".$insert_date."
	    	)";
			$insert_orders = mysqli_query($conn, $insert_orders);
			$order_id = mysqli_insert_id($conn);


	    	$orderLines = $order->orderLines->orderLine;
	    	foreach ($orderLines as $k => $orderLine) {






	    		$productName = '';
	    		$sku = '';
	    		$orderLineQuantity_unitOfMeasurement = '';
	    		$orderLineQuantity_amount = 0;
	    		$statusDate = 0;
	    		$orderLineStatus_status = '';
	    		$trackingNumber = '';
	    		$fulfillmentOption = '';
	    		$pickUpDateTime = 0;
	    		$chargeType = '';
	    		$chargeAmount_currency = '';
	    		$chargeAmount_amount = 0;
	    		$taxAmount_currency = '';
	    		$taxAmount_amount = 0;
	    		$shipping_cost = 0;
	    		$shipping_tax = 0;








	    		$productName = $orderLine->item->productName;
	    		$sku = $orderLine->item->sku;
	    		$orderLineQuantity_unitOfMeasurement = $orderLine->orderLineQuantity->unitOfMeasurement;
	    		$orderLineQuantity_amount = $orderLine->orderLineQuantity->amount;
	    		$statusDate = $orderLine->statusDate;
	    		if(!empty($orderLine->orderLineStatuses)){
	    			if(isset($orderLine->orderLineStatuses->orderLineStatus[0])){
	    				$orderLineStatus_status = $orderLine->orderLineStatuses->orderLineStatus[0]->status;
			    		if(!empty($orderLine->orderLineStatuses->orderLineStatus[0]->trackingInfo)){
				    		$trackingNumber = $orderLine->orderLineStatuses->orderLineStatus[0]->trackingInfo->trackingNumber;
				    	}
	    			}
		    		
		    	}
		    	if(!empty($orderLine->fulfillment)){
		    		$fulfillmentOption = $orderLine->fulfillment->fulfillmentOption;
		    		$pickUpDateTime = $orderLine->fulfillment->pickUpDateTime;
		    	}

		    	if(!empty($orderLine->charges)){
		    		if(isset($orderLine->charges->charge[0])){
		    			$chargeType = $orderLine->charges->charge[0]->chargeType;
		    			$chargeAmount_currency = $orderLine->charges->charge[0]->chargeAmount->currency;
		    			$chargeAmount_amount = $orderLine->charges->charge[0]->chargeAmount->amount;
		    			if(!empty($orderLine->charges->charge[0]->tax)){
		    				$taxAmount_currency = $orderLine->charges->charge[0]->tax->taxAmount->currency;
		    				$taxAmount_amount = $orderLine->charges->charge[0]->tax->taxAmount->amount;
		    			}
		    		}
		    		if(isset($orderLine->charges->charge[1])){
		    			$chargeType = $orderLine->charges->charge[1]->chargeType;
		    			if($chargeType=='SHIPPING'){
			    			$shipping_cost = $orderLine->charges->charge[1]->chargeAmount->amount;
			    			if(!empty($orderLine->charges->charge[1]->tax)){
			    				$shipping_tax = $orderLine->charges->charge[1]->tax->taxAmount->amount;
			    			}
		    			}
		    			
		    		}
		    	}





                $insert_by = 0;
                $insert_date = time();
		    	$insert_orderlines = "INSERT INTO orderlines (
			    	order_id,
			    	productName,
			    	sku,
			    	orderLineQuantity_unitOfMeasurement,
			    	orderLineQuantity_amount,
			    	statusDate,
			    	orderLineStatus_status,
			    	trackingNumber,
			    	fulfillmentOption,
			    	pickUpDateTime,
			    	chargeType,
			    	chargeAmount_currency,
			    	chargeAmount_amount,
			    	taxAmount_currency,
			    	taxAmount_amount,
			    	shipping_cost,
			    	shipping_tax,
                    insert_by,
                    insert_date
		    	) VALUES (
		    		".$order_id.",
			    	'".$productName."',
			    	'".$sku."',
			    	'".$orderLineQuantity_unitOfMeasurement."',
			    	'".$orderLineQuantity_amount."',
			    	'".$statusDate."',
			    	'".$orderLineStatus_status."',
			    	'".$trackingNumber."',
			    	'".$fulfillmentOption."',
			    	'".$pickUpDateTime."',
			    	'".$chargeType."',
			    	'".$chargeAmount_currency."',
			    	'".$chargeAmount_amount."',
			    	'".$taxAmount_currency."',
			    	'".$taxAmount_amount."',
			    	'".$shipping_cost."',
			    	'".$shipping_tax."',
                    ".$insert_by.",
                    ".$insert_date."
		    	)";
				$insert_orderlines = mysqli_query($conn, $insert_orderlines);

	    	}
    	
    	}	
    }




    // check if second page 
    if(!empty($orders->list->meta->nextCursor)){
    	
    	$ch = curl_init();
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	          'Authorization: Basic '.$auth,    
	          'Accept: application/json',
	          'WM_SEC.ACCESS_TOKEN: '.$token,
	          'WM_QOS.CORRELATION_ID: '.$uuid ,
	          'WM_SVC.NAME: Walmart Marketplace'  
	    ));
	    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/orders'.$orders->list->meta->nextCursor);
	    // curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $result=curl_exec($ch);
	    $orders = json_decode($result);


	    foreach ($orders->list->elements->order as $key => $order) {



	    	

		    	




		    	$purchaseOrderId = 0;
		    	$customerOrderId = 0;
		    	$customerEmailId = '';
		    	$orderDate = 0;
		    	$shipNode = '';
		    	$shipping_phone = '';
		    	$shipping_estimatedDeliveryDate = 0;
		    	$shipping_estimatedShipDate = 0;
		    	$shipping_methodCode = '';
		    	$shipping_address1 = '';
		    	$shipping_address2 = '';
		    	$shipping_city = '';
		    	$shipping_state = '';
		    	$shipping_postalCode = '';
		    	$shipping_country = '';
		    	$shipping_addressType = '';

		    	








		    	$purchaseOrderId = $order->purchaseOrderId;
		    	$customerOrderId = $order->customerOrderId;
		    	$customerEmailId = $order->customerEmailId;
		    	$orderDate = $order->orderDate;
		    	if(!empty($order->shippingInfo)){
		    		$shipping_phone = $order->shippingInfo->phone;
			    	$shipping_estimatedDeliveryDate = $order->shippingInfo->estimatedDeliveryDate;
			    	$shipping_estimatedShipDate = $order->shippingInfo->estimatedShipDate;
			    	$shipping_methodCode = $order->shippingInfo->methodCode;
			    	if(!empty($order->shippingInfo->postalAddress)){
			    		$shipping_address1 = $order->shippingInfo->postalAddress->address1;
				    	$shipping_address2 = $order->shippingInfo->postalAddress->address2;
				    	$shipping_city = $order->shippingInfo->postalAddress->city;
				    	$shipping_state = $order->shippingInfo->postalAddress->state;
				    	$shipping_postalCode = $order->shippingInfo->postalAddress->postalCode;
				    	$shipping_country = $order->shippingInfo->postalAddress->country;
				    	$shipping_addressType = $order->shippingInfo->postalAddress->addressType;
			    	}
		    	}
		    	if(!empty($order->shipNode)){
		    		$shipNode = $order->shipNode->type;
		    	}

		    	$check_if_order_already = "SELECT * FROM orders WHERE purchaseOrderId='".$purchaseOrderId."'";
				$run_check_if = mysqli_query($conn, $check_if_order_already);
		    	$if_order = $run_check_if->fetch_assoc();
		    	if(empty($if_order)){


                    $insert_by = 0;
                $insert_date = time();
		    	$insert_orders = "INSERT INTO orders (
			    	purchaseOrderId,
			    	customerOrderId,
			    	customerEmailId,
			    	orderDate,
			    	shipNode,
			    	shipping_phone,
			    	shipping_estimatedDeliveryDate,
			    	shipping_estimatedShipDate,
			    	shipping_methodCode,
			    	shipping_address1,
			    	shipping_address2,
			    	shipping_city,
			    	shipping_state,
			    	shipping_postalCode,
			    	shipping_country,
			    	shipping_addressType,
                    insert_by,
                    insert_date
		    	) VALUES (
		    		'".$purchaseOrderId."',
			    	'".$customerOrderId."',
			    	'".$customerEmailId."',
			    	'".$orderDate."',
			    	'".$shipNode."',
			    	'".$shipping_phone."',
			    	'".$shipping_estimatedDeliveryDate."',
			    	'".$shipping_estimatedShipDate."',
			    	'".$shipping_methodCode."',
			    	'".$shipping_address1."',
			    	'".$shipping_address2."',
			    	'".$shipping_city."',
			    	'".$shipping_state."',
			    	'".$shipping_postalCode."',
			    	'".$shipping_country."',
			    	'".$shipping_addressType."',
                    ".$insert_by.",
                    ".$insert_date."
		    	)";
				$insert_orders = mysqli_query($conn, $insert_orders);
				$order_id = mysqli_insert_id($conn);


		    	$orderLines = $order->orderLines->orderLine;
		    	foreach ($orderLines as $k => $orderLine) {






		    		$productName = '';
		    		$sku = '';
		    		$orderLineQuantity_unitOfMeasurement = '';
		    		$orderLineQuantity_amount = 0;
		    		$statusDate = 0;
		    		$orderLineStatus_status = '';
		    		$trackingNumber = '';
		    		$fulfillmentOption = '';
		    		$pickUpDateTime = 0;
		    		$chargeType = '';
		    		$chargeAmount_currency = '';
		    		$chargeAmount_amount = 0;
		    		$taxAmount_currency = '';
		    		$taxAmount_amount = 0;
		    		$shipping_cost = 0;
		    		$shipping_tax = 0;








		    		$productName = $orderLine->item->productName;
		    		$sku = $orderLine->item->sku;
		    		$orderLineQuantity_unitOfMeasurement = $orderLine->orderLineQuantity->unitOfMeasurement;
		    		$orderLineQuantity_amount = $orderLine->orderLineQuantity->amount;
		    		$statusDate = $orderLine->statusDate;
		    		if(!empty($orderLine->orderLineStatuses)){
		    			if(isset($orderLine->orderLineStatuses->orderLineStatus[0])){
		    				$orderLineStatus_status = $orderLine->orderLineStatuses->orderLineStatus[0]->status;
				    		if(!empty($orderLine->orderLineStatuses->orderLineStatus[0]->trackingInfo)){
					    		$trackingNumber = $orderLine->orderLineStatuses->orderLineStatus[0]->trackingInfo->trackingNumber;
					    	}
		    			}
			    		
			    	}
			    	if(!empty($orderLine->fulfillment)){
			    		$fulfillmentOption = $orderLine->fulfillment->fulfillmentOption;
			    		$pickUpDateTime = $orderLine->fulfillment->pickUpDateTime;
			    	}

			    	if(!empty($orderLine->charges)){
			    		if(isset($orderLine->charges->charge[0])){
			    			$chargeType = $orderLine->charges->charge[0]->chargeType;
			    			$chargeAmount_currency = $orderLine->charges->charge[0]->chargeAmount->currency;
			    			$chargeAmount_amount = $orderLine->charges->charge[0]->chargeAmount->amount;
			    			if(!empty($orderLine->charges->charge[0]->tax)){
			    				$taxAmount_currency = $orderLine->charges->charge[0]->tax->taxAmount->currency;
			    				$taxAmount_amount = $orderLine->charges->charge[0]->tax->taxAmount->amount;
			    			}
			    		}
			    		if(isset($orderLine->charges->charge[1])){
			    			$chargeType = $orderLine->charges->charge[1]->chargeType;
			    			if($chargeType=='SHIPPING'){
				    			$shipping_cost = $orderLine->charges->charge[1]->chargeAmount->amount;
				    			if(!empty($orderLine->charges->charge[1]->tax)){
				    				$shipping_tax = $orderLine->charges->charge[1]->tax->taxAmount->amount;
				    			}
			    			}
			    			
			    		}
			    	}





                    $insert_by = $_SESSION['user_id'];
                $insert_date = time();
			    	$insert_orderlines = "INSERT INTO orderlines (
				    	order_id,
				    	productName,
				    	sku,
				    	orderLineQuantity_unitOfMeasurement,
				    	orderLineQuantity_amount,
				    	statusDate,
				    	orderLineStatus_status,
				    	trackingNumber,
				    	fulfillmentOption,
				    	pickUpDateTime,
				    	chargeType,
				    	chargeAmount_currency,
				    	chargeAmount_amount,
				    	taxAmount_currency,
				    	taxAmount_amount,
				    	shipping_cost,
				    	shipping_tax,
                        insert_by,
                    insert_date
			    	) VALUES (
			    		".$order_id.",
				    	'".$productName."',
				    	'".$sku."',
				    	'".$orderLineQuantity_unitOfMeasurement."',
				    	'".$orderLineQuantity_amount."',
				    	'".$statusDate."',
				    	'".$orderLineStatus_status."',
				    	'".$trackingNumber."',
				    	'".$fulfillmentOption."',
				    	'".$pickUpDateTime."',
				    	'".$chargeType."',
				    	'".$chargeAmount_currency."',
				    	'".$chargeAmount_amount."',
				    	'".$taxAmount_currency."',
				    	'".$taxAmount_amount."',
				    	'".$shipping_cost."',
				    	'".$shipping_tax."',
                        ".$insert_by.",
                    ".$insert_date."
			    	)";
					$insert_orderlines = mysqli_query($conn, $insert_orderlines);

		    	}
	    	
	    	}	
	    }



    }


    die();

    
}




// shipping cost
if(isset($_POST['save_shipping'])){
	$shipping_cost = $_FILES['shipping_cost'];
	$fileName = $_FILES["shipping_cost"]["tmp_name"];


	$target_dir = "/files/";
    $file = $_FILES['shipping_cost']['name'];
    $path = pathinfo($file);
    $filename = $path['filename'].rand(9999999,99999999);
    $ext = $path['extension'];
    $temp_name = $_FILES['shipping_cost']['tmp_name'];
    $path_filename_ext = $_SERVER['DOCUMENT_ROOT'].$target_dir.$filename.".".$ext;
    $_SESSION['source_file'] = $filename.'.'.$ext;
    move_uploaded_file($temp_name,$path_filename_ext);

    $file = fopen($_SERVER['DOCUMENT_ROOT']."/files/".$filename.".".$ext, "r");
    $csv_date = array();

    while (($data = fgetcsv($file, 1000, ",")) !== FALSE) 
    {
      // Each individual array is being pushed into the nested array
      $csv_date[] = $data;    


    }
    $csv_date_string = implode(",",$csv_date[0]);
    $_SESSION['selection'] = $csv_date_string;

    header("Location: ".$domainLink."/dashboard_add_shipping_cost.php?step_2");
    
    die();

}
if(isset($_POST['save_shipping1'])){
	$file = fopen($_SERVER['DOCUMENT_ROOT']."/files/".$_SESSION['source_file'], "r");
	$csv_date = array();
	while (($data = fgetcsv($file, 1000, ",")) !== FALSE) 
	  {
	    // Each individual array is being pushed into the nested array
	    $csv_date[] = $data;		
	  }

	  $trackingNumber_col = $_POST['tracking_number'];
	  $cost_col = $_POST['cost'];
	  


	  foreach ($csv_date as $key => $csv_dat) {

	  	if($key>0){
	  		$tracking_number_val = $csv_dat[$trackingNumber_col];
	  		$cost_val = preg_replace("/[^0-9.]/","",$csv_dat[$cost_col]);
	  		
	  		$check_if_shipping_cost_already = "SELECT * FROM shipping_cost WHERE tracking_number='".$tracking_number_val."'";
			$run_check_if = mysqli_query($conn, $check_if_shipping_cost_already);
	    	$if_shipping_cost = $run_check_if->fetch_assoc();
	    	if(empty($if_shipping_cost)){

                $insert_by = $_SESSION['user_id'];
                $insert_date = time();

		    	$insert_shipping_cost = "INSERT INTO shipping_cost (
			    	tracking_number,
			    	cost,
                    insert_by,
                    insert_date
		    	) VALUES (
		    		'".$tracking_number_val."',
			    	'".$cost_val."',
                    ".$insert_by.",
                    ".$insert_date."
		    	)";
				$insert_shipping_cost = mysqli_query($conn, $insert_shipping_cost);
				

			}
	  	}
	  }



      $message= 'Shipping cost added! Export from pirate ship';
      $user_id = $_SESSION['user_id'];
      $status = 0;
      $date_created = time();
        $insert_notification = "INSERT INTO notifications (
            user_id,
            message,
            status,
            date_created
        ) VALUES (
            ".$user_id.",
            '".$message."',
            ".$status.",
            ".$date_created."
        )";
        $insert_notification = mysqli_query($conn, $insert_notification);

	  $_SESSION['success_message'] = 'Shipping cost added successfully!';

	  header("Location: ".$domainLink."/dashboard_add_shipping_cost.php");
    
      die();

}


// product price
if(isset($_POST['save_product_price'])){
	$shipping_cost = $_FILES['product_price'];
	$fileName = $_FILES["product_price"]["tmp_name"];


	$target_dir = "/files/";
    $file = $_FILES['product_price']['name'];
    $path = pathinfo($file);
    $filename = $path['filename'].rand(9999999,99999999);
    $ext = $path['extension'];
    $temp_name = $_FILES['product_price']['tmp_name'];
    $path_filename_ext = $_SERVER['DOCUMENT_ROOT'].$target_dir.$filename.".".$ext;
    $_SESSION['source_file'] = $filename.'.'.$ext;
    move_uploaded_file($temp_name,$path_filename_ext);

    $file = fopen($_SERVER['DOCUMENT_ROOT']."/files/".$filename.".".$ext, "r");
    $csv_date = array();

    while (($data = fgetcsv($file, 1000, ",")) !== FALSE) 
    {
      // Each individual array is being pushed into the nested array
      $csv_date[] = $data;    


    }
    $csv_date_string = implode(",",$csv_date[0]);
    $_SESSION['selection'] = $csv_date_string;

    header("Location: ".$domainLink."/dashboard_add_product_price.php?step_2");
    
    die();

}
if(isset($_POST['save_product_price1'])){
	$file = fopen($_SERVER['DOCUMENT_ROOT']."/files/".$_SESSION['source_file'], "r");
	$csv_date = array();
	while (($data = fgetcsv($file, 1000, ",")) !== FALSE) 
	  {
	    // Each individual array is being pushed into the nested array
	    $csv_date[] = $data;		
	  }

	  $MSKU_col = $_POST['MSKU'];
	  $cost_col = $_POST['cost'];
	  $Title_col = $_POST['title'];
	  
	  foreach ($csv_date as $key => $csv_dat) {

	  	if($key>0){
	  		$sku = $csv_dat[$MSKU_col];
	  		$cost_val = preg_replace("/[^0-9.]/","",$csv_dat[$cost_col]);
	  		$product_name = $csv_dat[$Title_col];
	  		
	  		$check_if_product_prices_already = "SELECT * FROM product_prices WHERE sku='".$sku."'";
			$run_check_if = mysqli_query($conn, $check_if_product_prices_already);
	    	$if_product_prices = $run_check_if->fetch_assoc();
	    	if(empty($if_product_prices)){

                $insert_by = $_SESSION['user_id'];
                $insert_date = time();
		    	$insert_product_prices = "INSERT INTO product_prices (
			    	sku,
			    	cost,
			    	product_name,
                    insert_by,
                    insert_date
		    	) VALUES (
		    		'".$sku."',
			    	'".$cost_val."',
			    	'".$product_name."',
                    ".$insert_by.",
                    ".$insert_date."
		    	)";
				$insert_product_prices = mysqli_query($conn, $insert_product_prices);
				
			}
	  	}
	  }




      $message= 'Product prices added! Export from Inventorylab';
        $user_id = $_SESSION['user_id'];
        $status = 0;
        $date_created = time();
        $insert_notification = "INSERT INTO notifications (
            user_id,
            message,
            status,
            date_created
        ) VALUES (
            ".$user_id.",
            '".$message."',
            ".$status.",
            ".$date_created."
        )";
        $insert_notification = mysqli_query($conn, $insert_notification);



	  $_SESSION['success_message'] = 'product prices added successfully!';
	   header("Location: ".$domainLink."/dashboard_add_product_price.php");
    
    die();

}

if(isset($_POST['update_order'])){
	$order_id = $_POST['update_order'];
	foreach ($_POST['product_name'] as $key => $product) {
		$product_name = $_POST['product_name'][$key];
		$sku = $_POST['sku'][$key];
		$product_price = $_POST['product_price'][$key];
		if(!empty($product_price) && $product_price!=0){
			$check_if_product_prices_already = "SELECT * FROM product_prices WHERE sku='".$sku."'";
			$run_check_if = mysqli_query($conn, $check_if_product_prices_already);
	    	$if_product_prices = $run_check_if->fetch_assoc();
	    	if(empty($if_product_prices)){


                $insert_by = $_SESSION['user_id'];
                $insert_date = time();
		    	$insert_product_prices = "INSERT INTO product_prices (
			    	sku,
			    	cost,
			    	product_name,
                    insert_by,
                    insert_date
		    	) VALUES (
		    		'".$sku."',
			    	'".$product_price."',
			    	'".$product_name."',
                    ".$insert_by.",
                    ".$insert_date."
		    	)";
				$insert_product_prices = mysqli_query($conn, $insert_product_prices);
				

			}else{
                $updated_by = $_SESSION['user_id'];
            $updated_date = time();
				$update_product_price = "UPDATE product_prices SET cost='".$product_price."', updated_by=".$updated_by.", updated_date=".$updated_date." WHERE sku='".$sku."'";
		    	$run_update_product_price = mysqli_query($conn, $update_product_price);
			}
		}

		$tracking_number = $_POST['tracking_number'][$key];
		$shipping_cost = $_POST['shipping_cost'][$key];
		if(!empty($tracking_number)){
			if(!empty($shipping_cost) && $shipping_cost!=0){

				$check_if_shipping_cost_already = "SELECT * FROM shipping_cost WHERE tracking_number='".$tracking_number."'";
				$run_check_if = mysqli_query($conn, $check_if_shipping_cost_already);
		    	$if_shipping_cost = $run_check_if->fetch_assoc();
		    	if(empty($if_shipping_cost)){

                    $insert_by = $_SESSION['user_id'];
                $insert_date = time();

			    	$insert_shipping_cost = "INSERT INTO shipping_cost (
				    	tracking_number,
				    	cost,
                        insert_by,
                    insert_date
			    	) VALUES (
			    		'".$tracking_number."',
				    	'".$shipping_cost."',
                        ".$insert_by.",
                    ".$insert_date."
			    	)";
					$insert_shipping_cost = mysqli_query($conn, $insert_shipping_cost);
					

				}else{
                    $updated_by = $_SESSION['user_id'];
            $updated_date = time();
					$update_tracking_number = "UPDATE shipping_cost SET cost='".$shipping_cost."', updated_by=".$updated_by.", updated_date=".$updated_date." WHERE tracking_number='".$tracking_number."'";
		    		$run_update_tracking_number = mysqli_query($conn, $update_tracking_number);
				}


				
			}
		}
		

	}
	$_SESSION['success_message'] = 'Order updated successfully!';
}


if(isset($_POST['update_shipping'])){
	$tracking_number = $_POST['tracking_number'];
	$costs = $_POST['cost'];
	foreach ($tracking_number as $key => $tracking_numbe) {
		if(!empty($costs[$key])){
			$shipping_cost = preg_replace("/[^0-9.]/","",$costs[$key]);


            $insert_by = $_SESSION['user_id'];
                $insert_date = time();
			$insert_shipping_cost = "INSERT INTO shipping_cost (
		    	tracking_number,
		    	cost,
                insert_by,
                    insert_date
	    	) VALUES (
	    		'".$tracking_numbe."',
		    	'".$shipping_cost."',
                ".$insert_by.",
                    ".$insert_date."
	    	)";
			$insert_shipping_cost = mysqli_query($conn, $insert_shipping_cost);


			
		}
	}


    $message= 'Updated Shipping cost sumitted!';
      $user_id = $_SESSION['user_id'];
      $status = 0;
      $date_created = time();
        $insert_notification = "INSERT INTO notifications (
            user_id,
            message,
            status,
            date_created
        ) VALUES (
            ".$user_id.",
            '".$message."',
            ".$status.",
            ".$date_created."
        )";
        $insert_notification = mysqli_query($conn, $insert_notification);


	$_SESSION['success_message'] = 'Shippings updated successfully!';
}


if(isset($_POST['update_product_price'])){
	$product_sku = $_POST['product_sku'];
	$product_names = $_POST['product_name'];
	$costs = $_POST['cost'];
	foreach ($product_sku as $key => $sku) {
		if(!empty($costs[$key])){
			$product_price = preg_replace("/[^0-9.]/","",$costs[$key]);
			$product_name = $product_names[$key];


            $insert_by = $_SESSION['user_id'];
                $insert_date = time();
			$insert_product_prices = "INSERT INTO product_prices (
		    	sku,
		    	cost,
		    	product_name,
                insert_by,
                    insert_date
	    	) VALUES (
	    		'".$sku."',
		    	'".$product_price."',
		    	'".$product_name."',
                ".$insert_by.",
                    ".$insert_date."
	    	)";
			$insert_product_prices = mysqli_query($conn, $insert_product_prices);
		}
	}

    $message= 'Missing Product Price submitted';
    $user_id = $_SESSION['user_id'];
    $status = 0;
    $date_created = time();
    $insert_notification = "INSERT INTO notifications (
        user_id,
        message,
        status,
        date_created
    ) VALUES (
        ".$user_id.",
        '".$message."',
        ".$status.",
        ".$date_created."
    )";
    $insert_notification = mysqli_query($conn, $insert_notification);
	$_SESSION['success_message'] = 'Product price updated successfully!';
}

if(isset($_POST['edit_product_information'])){
    $sku = $_POST['sku'];
    $product_name = $_POST['name'];
    $cost = $_POST['cost'];
    $get_orders = "SELECT * FROM product_prices WHERE sku='".$sku."'"; 
    $product = mysqli_query($conn, $get_orders);
    $row_product = $product->fetch_assoc();
    if(!empty($row_product)){
        $updated_by = $_SESSION['user_id'];
            $updated_date = time();
        $update_product = "UPDATE product_prices SET product_name='".$product_name."', cost='".$cost."', updated_by=".$updated_by.", updated_date=".$updated_date." WHERE sku='".$sku."'";
        $run_update_product = mysqli_query($conn, $update_product);
    }


    $message= 'Product updated with the sku '.$sku;
    $user_id = $_SESSION['user_id'];
    $status = 0;
    $date_created = time();
    $insert_notification = "INSERT INTO notifications (
        user_id,
        message,
        status,
        date_created
    ) VALUES (
        ".$user_id.",
        '".$message."',
        ".$status.",
        ".$date_created."
    )";
    $insert_notification = mysqli_query($conn, $insert_notification);


    $_SESSION['success_message'] = 'Product updated successfully!';
}

if(isset($_POST['save_shipping_daily'])){
    foreach ($_POST['date'] as $key => $date) {
        $date_added = $_POST['date'][0];
        $recipient = $_POST['recipient'][$key];
        $order = $_POST['order'][$key];
        $tracking_number = $_POST['tracking_number'][$key];
        $shipping_cost = $_POST['shipping_cost'][$key];
        $status = $_POST['status'][$key];
        
        if(!empty($tracking_number) && !empty($shipping_cost) && !empty($order)){
            $shipping_cost_daily   = "SELECT * FROM shipping_cost_daily WHERE tracking_number='".$tracking_number."'";
        
            $shipping_cost_daily = mysqli_query($conn, $shipping_cost_daily);
            $row_shipping_cost_daily = $shipping_cost_daily->fetch_assoc();
            if(empty($row_shipping_cost_daily)){
                

                $insert_by = $_SESSION['user_id'];
                $insert_date = time();
                $insert_orders = "INSERT INTO shipping_cost_daily (
                    date_added,
                    recipient,
                    order_no,
                    tracking_number,
                    shipping_cost,
                    status,
                    insert_by,
                    insert_date
                ) VALUES (
                    '".$date_added."',
                    '".$recipient."',
                    '".$order_no."',
                    '".$tracking_number."',
                    '".$shipping_cost."',
                    '".$status."',
                    ".$insert_by.",
                    ".$insert_date."
                )";
                $insert_orders = mysqli_query($conn, $insert_orders);   


                $shipping_cost = preg_replace("/[^0-9.]/","",$shipping_cost);




                $shipping_cost_find   = "SELECT * FROM shipping_cost WHERE tracking_number='".$tracking_number."'";
        
                $shipping_cost_find = mysqli_query($conn, $shipping_cost_find);
                $row_shipping_cost_find = $shipping_cost_find->fetch_assoc();
                if(empty($row_shipping_cost_find)){


                    $insert_by = $_SESSION['user_id'];
                $insert_date = time();
                    $insert_shipping_cost = "INSERT INTO shipping_cost (
                        tracking_number,
                        cost,
                        insert_by,
                    insert_date
                    ) VALUES (
                        '".$tracking_number."',
                        '".$shipping_cost."',
                        ".$insert_by.",
                    ".$insert_date."
                    )";
                    $insert_shipping_cost = mysqli_query($conn, $insert_shipping_cost);
                }
            }
        }

        

    }

    $message= 'Daily Shipping cost added!';
      $user_id = $_SESSION['user_id'];
      $status = 0;
      $date_created = time();
        $insert_notification = "INSERT INTO notifications (
            user_id,
            message,
            status,
            date_created
        ) VALUES (
            ".$user_id.",
            '".$message."',
            ".$status.",
            ".$date_created."
        )";
        $insert_notification = mysqli_query($conn, $insert_notification);

    $_SESSION['success_message'] = 'Shipping cost added successfully!';
}


if(isset($_POST['edit_profile_information'])){
    $name = $_POST['name'];
    $password = $_POST['password'];
    $user_id = $_POST['edit_profile_information'];
    $type = $_POST['type'];

    $allowed_pages = $_POST['allowed_pages'];
    $allowed_pages = implode(',', $allowed_pages);

    $get_orders = "SELECT * FROM users WHERE id=".$user_id;
    $product = mysqli_query($conn, $get_orders);
    $row_product = $product->fetch_assoc();
    if(!empty($row_product)){
        if($password!=$row_product['password']){
            $password = md5($password);
        }
        $update_product = "UPDATE users SET fullname='".$name."', password='".$password."', allowed_pages='".$allowed_pages."', type=".$type." WHERE id=".$user_id;
        $run_update_product = mysqli_query($conn, $update_product);
    }
    $_SESSION['success_message'] = 'Profile updated successfully!';
}

if(isset($_POST['create_profile_information'])){
    $name = $_POST['name'];
    $password = $_POST['password'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $type = $_POST['type'];


    $allowed_pages = $_POST['allowed_pages'];
    $allowed_pages = implode(',', $allowed_pages);

    $users  = "SELECT * FROM users WHERE username='".$username."' OR email='".$email."'";
    
    $users = mysqli_query($conn, $users);
    $row_users = $users->fetch_assoc();
    $_SESSION['error_message'] = 'Username & email already registered!!';
    if(empty($row_users)){
        $user_created = 'yes';
        $_SESSION['success_message'] = 'Profile Created successfully!';
        unset($_SESSION['error_message']);
        $password = md5($password);
        $insert_users = "INSERT INTO users (
                            username,
                            email,
                            password,
                            fullname,
                            type,
                            allowed_pages
                        ) VALUES (
                            '".$username."',
                            '".$email."',
                            '".$password."',
                            '".$name."',
                            ".$type.",
                            '".$allowed_pages."'
                        )";

                        $insert_users = mysqli_query($conn, $insert_users);
                        
    }

    
}

if(isset($_POST['edit_profile_information_my'])){
    $name = $_POST['name'];
    $password = $_POST['password'];
    $user_id = $_SESSION['user_id'];

    $get_orders = "SELECT * FROM users WHERE id=".$user_id;
    $product = mysqli_query($conn, $get_orders);
    $row_product = $product->fetch_assoc();
    if(!empty($row_product)){
        if($password!=$row_product['password']){
            $password = md5($password);
        }
        $update_product = "UPDATE users SET fullname='".$name."', password='".$password."' WHERE id=".$user_id;
        $run_update_product = mysqli_query($conn, $update_product);
    }
    $_SESSION['success_message'] = 'Profile updated successfully!';
}

if(isset($_GET['delete_user'])){
    $user_id = $_GET['delete_user'];
    $get_orders = "SELECT * FROM users WHERE id=".$user_id; 
    $product = mysqli_query($conn, $get_orders);
    $row_product = $product->fetch_assoc();
    $status = -1;

    $update_user = "UPDATE users SET status=".$status." WHERE id=".$user_id;
    $update_user = mysqli_query($conn, $update_user);

    $_SESSION['success_message'] = 'User deleted';

    header("Location: ".$domainLink."/dashboard_users.php");
}

if(isset($_GET['restore_user'])){
    $user_id = $_GET['restore_user'];
    $get_orders = "SELECT * FROM users WHERE id=".$user_id; 
    $product = mysqli_query($conn, $get_orders);
    $row_product = $product->fetch_assoc();
    $status = 1;

    $update_user = "UPDATE users SET status=".$status." WHERE id=".$user_id;
    $update_user = mysqli_query($conn, $update_user);

    $_SESSION['success_message'] = 'User Restored';

    header("Location: ".$domainLink."/dashboard_users.php");
}




if(isset($_POST['walmart_settings'])){
    $client_id = $_POST['client_id'];
    $client_secret = $_POST['client_secret'];

    $bytes = random_bytes(16);
    
    $curl = curl_init();

    $url = 'https://marketplace.walmartapis.com/v3/token';

    $options = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Digital Cloud Commerce',
        CURLOPT_HEADER => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLINFO_HEADER_OUT => true
    );

    $httpHeaders = array();

    $options[CURLOPT_POST] = 1;
    $options[CURLOPT_POSTFIELDS] = 'grant_type=client_credentials';
    $httpHeaders[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
    $httpHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
    $httpHeaders[] = 'Accept: application/json';
    $httpHeaders[] = 'WM_SVC.NAME: Walmart Marketplace';
    $httpHeaders[] = 'WM_QOS.CORRELATION_ID: '.base64_encode($bytes);
    $httpHeaders[] = 'WM_SVC.VERSION: 1.0.0';
    
    //var_dump($config);

    $options[CURLOPT_HTTPHEADER] = $httpHeaders;

    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);
    $token = '';
    if($response !== false) {
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        $header = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);

        $accessToken = json_decode($body, true);

        if(isset($accessToken['access_token'])) {
            
        $token =  $accessToken['access_token'];

        $status = 1;

        
        } else {
            
            $status = 0;

        }
    } else {
        
        $status = 0;
    }
    
    curl_close($curl);



    $id = 1;
    $user_id = $_SESSION['user_id'];
    
    $check_if_setting_already = "SELECT * FROM settings WHERE id=".$id;
    $run_check_if = mysqli_query($conn, $check_if_setting_already);
    $if_settings = $run_check_if->fetch_assoc();
    if(empty($if_settings)){



        $insert_by = $_SESSION['user_id'];
        $insert_date = time();
        $date_created = time();

        $insert_inventory = "INSERT INTO settings (
            client_id,
            client_secret,
            user_id,
            status,
            date_created,
            insert_by,
            insert_date
        ) VALUES (
            '".$client_id."',
            '".$client_secret."',
            ".$user_id.",
            ".$status.",
            ".$date_created.",
            ".$insert_by.",
            ".$insert_date."
        )";
        $insert_inventory = mysqli_query($conn, $insert_inventory);  
        

    }else{


        $updated_by = $_SESSION['user_id'];
        $updated_date = time();
        $update_setting = "UPDATE settings SET client_id='".$client_id."', client_secret='".$client_secret."', status=".$status.", updated_by=".$updated_by.", updated_date=".$updated_date." WHERE id=".$id;
        $run_update_tracking_number = mysqli_query($conn, $update_setting);

    }

}



if(isset($_GET['load_monthly_reports_on_dashboard'])){

    if(isset($_GET['fullfilled_by_filter'])){

      if($_GET['fullfilled_by_filter']=='SellerFulfilled'){
        if(isset($_SESSION['fullfilled_by_filter_seller'])){
          unset($_SESSION['fullfilled_by_filter_seller']);
        }else{
          $_SESSION['fullfilled_by_filter_seller'] = $_GET['fullfilled_by_filter'];
        }
        
      }
      if($_GET['fullfilled_by_filter']=='WFSFulfilled'){
        if(isset($_SESSION['fullfilled_by_filter_wfs'])){
          unset($_SESSION['fullfilled_by_filter_wfs']);
        }else{
          $_SESSION['fullfilled_by_filter_wfs'] = $_GET['fullfilled_by_filter'];
        }
        
      }

    }
    $fullfilled_by_filter = array();
    if(isset($_SESSION['fullfilled_by_filter_seller'])){
        $fullfilled_by_filter[] = $_SESSION['fullfilled_by_filter_seller'];
    }
    if(isset($_SESSION['fullfilled_by_filter_wfs'])){
        $fullfilled_by_filter[] = $_SESSION['fullfilled_by_filter_wfs'];
    }
    if(empty($fullfilled_by_filter)){
      $fullfilled_by_filter[]='SellerFulfilled';
      $fullfilled_by_filter[]='WFSFulfilled';
      $_SESSION['fullfilled_by_filter_seller'] = 'SellerFulfilled';
      $_SESSION['fullfilled_by_filter_wfs'] = 'WFSFulfilled';
    }

    $date_range = date('Y-m-d'); 

    $html_to_load = '';
    $html_to_load .= '<a class="report_box" href="/dashboard_admin.php?set_date='.$date_range.'_'.$date_range.'">
                        <div class="report_box_header">
                            <h2>Today</h2>
                            <h3>'.date("m-d-Y", strtotime($date_range)).'</h3>
                        </div>';
                            $total_orders = 0;
                            $total_qty = 0;
                            $chargeAmount_amount = 0;
                            $product_price = 0;
                            $total_seller = 0;
                            $total_wfs = 0;
                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_start = strtotime("".$date_range." 12:00 AM")* 1000;
                                $date_end = strtotime("".$date_range." 11:59 PM")* 1000;

                                $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . ""; 
                                $filter_code .= " AND shipNode IN('".implode("','",$fullfilled_by_filter)."')";
                                
                            }
                            

                            

                            $get_orders = "SELECT * FROM orders $filter_code ORDER BY id desc";
                            $get_orders_query = mysqli_query($conn, $get_orders);

                            
                            $total_orders = mysqli_num_rows($get_orders_query);

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {

                                    if($order['shipNode']=='SellerFulfilled'){
                                        $total_seller = $total_seller+1;
                                    }else if($order['shipNode']=='WFSFulfilled'){
                                        $total_wfs = $total_wfs+1;
                                    }

                                    $order_id = $order['id'];
                                    $chargeAmount_currency = '';
                                    $taxAmount_currency = '';
                                    
                                    $taxAmount_amount = 0;
                                    $get_orderlines = "SELECT * FROM orderlines WHERE order_id=".$order_id;
                                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                                    if(mysqli_num_rows($get_orderlines_query) > 0){
                                    $kk=0;
                                    while($orderline = $get_orderlines_query->fetch_assoc()) {


                                        $kk++;
                                        $total_qty = $total_qty+$orderline['orderLineQuantity_amount'];
                                        $chargeAmount_currency = $orderline['chargeAmount_currency'];
                                        $taxAmount_currency = $orderline['taxAmount_currency'];
                                        $chargeAmount_amount = $chargeAmount_amount+floatval($orderline['chargeAmount_amount']);
                                        $taxAmount_amount = $taxAmount_amount+floatval($orderline['taxAmount_amount']);




                                        $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
        
                                        $product_prices = mysqli_query($conn, $product_prices);
                                        $row_product_prices = $product_prices->fetch_assoc();
                                        if(!empty($row_product_prices)){
                                            $product_price = $product_price+floatval($row_product_prices['cost']);
                                        }


                                        


                                        if(!empty($orderline['shipping_cost'])){
                                            $this_item_shipping_cost1 = floatval($orderline['shipping_cost'])+floatval($orderline['shipping_tax']);

                                            $customer_shipping_cost = $customer_shipping_cost+floatval($orderline['shipping_cost']);
                                            $total_shipping_cost = $total_shipping_cost+floatval($orderline['shipping_tax']);
                                        }else{
                                            $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
        
                                            $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                            $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                            if(!empty($row_shipping_cost)){
                                                $total_shipping_cost = $total_shipping_cost+floatval($row_shipping_cost['cost']);
                                            }
                                        }
                                        
                                        
                                    
                                    } }
                                    

                                }
                            }


                        $html_to_load .= '<div class="reports_bottom">
                            <p>
                                <span class="left">Orders</span>
                                <span class="right">'.$total_orders.'</span>
                            </p>
                            <p>
                                <span class="left">Unit</span>
                                <span class="right">'.$total_qty.'</span>
                            </p>
                            <p>
                                <span class="left">Refund</span>
                                <span class="right">$0</span>
                            </p>
                            <p>
                                <span class="left">Sales</span>
                                <span class="right">$'.number_format($chargeAmount_amount, 2).'</span>
                            </p>
                            <p>
                                <span class="left">Seller Fulfilled</span>
                                <span class="right">'.$total_seller.'</span>
                            </p>
                            <p>
                                <span class="left">WFS Fulfilled</span>
                                <span class="right">'.$total_wfs.'</span>
                            </p>
                            <p>
                                <span class="left">Gross Profit</span>
                                <span class="right">$'.number_format($chargeAmount_amount-$product_price, 2).'</span>
                            </p>
                            <p>
                                <span class="left">Net Profit</span>
                                <span class="right">$'.number_format($chargeAmount_amount-$product_price, 2).'</span>
                            </p>
                        </div>
                    </a>';
                    
                    $date_range = date('Y-m-d',strtotime("-1 days"));
                    
                    $html_to_load .= '<a class="report_box" href="/dashboard_admin.php?set_date='.$date_range.'_'.$date_range.'">
                        <div class="report_box_header">
                            <h2>Yesterday</h2>
                            <h3>'.date("m-d-Y", strtotime($date_range)).'</h3>
                        </div>';
                        

                            $total_orders = 0;
                            $total_qty = 0;
                            $chargeAmount_amount = 0;
                            $product_price = 0;
                            $total_seller = 0;
                            $total_wfs = 0;

                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_start = strtotime("".$date_range." 12:00 AM")* 1000;
                                $date_end = strtotime("".$date_range." 11:59 PM")* 1000;

                                $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . "";
                                $filter_code .= " AND shipNode IN('".implode("','",$fullfilled_by_filter)."')";
                                
                            }
                            

                            

                            $get_orders = "SELECT * FROM orders $filter_code ORDER BY id desc";
                            $get_orders_query = mysqli_query($conn, $get_orders);

                            
                            $total_orders = mysqli_num_rows($get_orders_query);

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {

                                    if($order['shipNode']=='SellerFulfilled'){
                                        $total_seller = $total_seller+1;
                                    }else if($order['shipNode']=='WFSFulfilled'){
                                        $total_wfs = $total_wfs+1;
                                    }

                                    $order_id = $order['id'];
                                    $chargeAmount_currency = '';
                                    $taxAmount_currency = '';
                                    
                                    $taxAmount_amount = 0;
                                    $get_orderlines = "SELECT * FROM orderlines WHERE order_id=".$order_id;
                                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                                    if(mysqli_num_rows($get_orderlines_query) > 0){
                                    $kk=0;
                                    while($orderline = $get_orderlines_query->fetch_assoc()) {
                                        $kk++;
                                        $total_qty = $total_qty+$orderline['orderLineQuantity_amount'];
                                        $chargeAmount_currency = $orderline['chargeAmount_currency'];
                                        $taxAmount_currency = $orderline['taxAmount_currency'];
                                        $chargeAmount_amount = $chargeAmount_amount+floatval($orderline['chargeAmount_amount']);
                                        $taxAmount_amount = $taxAmount_amount+floatval($orderline['taxAmount_amount']);




                                        $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
        
                                        $product_prices = mysqli_query($conn, $product_prices);
                                        $row_product_prices = $product_prices->fetch_assoc();
                                        if(!empty($row_product_prices)){
                                            $product_price = $product_price+floatval($row_product_prices['cost']);
                                        }


                                        


                                        if(!empty($orderline['shipping_cost'])){
                                            $this_item_shipping_cost1 = floatval($orderline['shipping_cost'])+floatval($orderline['shipping_tax']);

                                            $customer_shipping_cost = $customer_shipping_cost+floatval($orderline['shipping_cost']);
                                            $total_shipping_cost = $total_shipping_cost+floatval($orderline['shipping_tax']);
                                        }else{
                                            $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
        
                                            $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                            $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                            if(!empty($row_shipping_cost)){
                                                $total_shipping_cost = $total_shipping_cost+floatval($row_shipping_cost['cost']);
                                            }
                                        }
                                        
                                        
                                    
                                    } }
                                    

                                }
                            }


                           
                        $html_to_load .= '<div class="reports_bottom">
                            <p>
                                <span class="left">Orders</span>
                                <span class="right">'.$total_orders.'</span>
                            </p>
                            <p>
                                <span class="left">Unit</span>
                                <span class="right">'.$total_qty.'</span>
                            </p>
                            <p>
                                <span class="left">Refund</span>
                                <span class="right">$0</span>
                            </p>
                            <p>
                                <span class="left">Sales</span>
                                <span class="right">$'.number_format($chargeAmount_amount, 2).'</span>
                            </p>
                            <p>
                                <span class="left">Seller Fulfilled</span>
                                <span class="right">'.$total_seller.'</span>
                            </p>
                            <p>
                                <span class="left">WFS Fulfilled</span>
                                <span class="right">'.$total_wfs.'</span>
                            </p>
                            <p>
                                <span class="left">Gross Profit</span>
                                <span class="right">$'.number_format($chargeAmount_amount-$product_price, 2).'</span>
                            </p>
                            <p>
                                <span class="left">Net Profit</span>
                                <span class="right">$'.number_format($chargeAmount_amount-$product_price, 2).'</span>
                            </p>
                        </div>
                    </a>';
                  
                    $date_range = date('Y-m-01').'_'.date('Y-m-t');
                    
                    $html_to_load .= '<a class="report_box" href="/dashboard_admin.php?set_date=<?php echo $date_range; ?>">
                        <div class="report_box_header">
                            <h2>This Month</h2>
                            <h3>'.date('m-01-Y').' to '.date('m-t-Y').'</h3>
                        </div>';
                        


                            $total_orders = 0;
                            $total_qty = 0;
                            $chargeAmount_amount = 0;
                            $product_price = 0;
                            $total_seller = 0;
                            $total_wfs = 0;

                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0])* 1000;
                                $date_end = strtotime($date_range[1])* 1000;
                                if($date_range[0]==$date_range[1]){
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;

                                    $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . "";
                                }else{

                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;


                                    $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . "";
                                }

                                $filter_code .= " AND shipNode IN('".implode("','",$fullfilled_by_filter)."')";
                                
                            }
                            

                            

                            $get_orders = "SELECT * FROM orders $filter_code ORDER BY id desc";
                            $get_orders_query = mysqli_query($conn, $get_orders);

                            
                            $total_orders = mysqli_num_rows($get_orders_query);

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {

                                    if($order['shipNode']=='SellerFulfilled'){
                                        $total_seller = $total_seller+1;
                                    }else if($order['shipNode']=='WFSFulfilled'){
                                        $total_wfs = $total_wfs+1;
                                    }

                                    $order_id = $order['id'];
                                    $chargeAmount_currency = '';
                                    $taxAmount_currency = '';
                                    
                                    $taxAmount_amount = 0;
                                    $get_orderlines = "SELECT * FROM orderlines WHERE order_id=".$order_id;
                                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                                    if(mysqli_num_rows($get_orderlines_query) > 0){
                                    $kk=0;
                                    while($orderline = $get_orderlines_query->fetch_assoc()) {
                                        $kk++;
                                        $total_qty = $total_qty+$orderline['orderLineQuantity_amount'];
                                        $chargeAmount_currency = $orderline['chargeAmount_currency'];
                                        $taxAmount_currency = $orderline['taxAmount_currency'];
                                        $chargeAmount_amount = $chargeAmount_amount+floatval($orderline['chargeAmount_amount']);
                                        $taxAmount_amount = $taxAmount_amount+floatval($orderline['taxAmount_amount']);




                                        $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
        
                                        $product_prices = mysqli_query($conn, $product_prices);
                                        $row_product_prices = $product_prices->fetch_assoc();
                                        if(!empty($row_product_prices)){
                                            $product_price = $product_price+floatval($row_product_prices['cost']);
                                        }


                                        


                                        if(!empty($orderline['shipping_cost'])){
                                            $this_item_shipping_cost1 = floatval($orderline['shipping_cost'])+floatval($orderline['shipping_tax']);

                                            $customer_shipping_cost = $customer_shipping_cost+floatval($orderline['shipping_cost']);
                                            $total_shipping_cost = $total_shipping_cost+floatval($orderline['shipping_tax']);
                                        }else{
                                            $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
        
                                            $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                            $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                            if(!empty($row_shipping_cost)){
                                                $total_shipping_cost = $total_shipping_cost+floatval($row_shipping_cost['cost']);
                                            }
                                        }
                                        
                                        
                                    
                                    } }
                                    

                                }
                            }


                         
                        $html_to_load .= '<div class="reports_bottom">
                            <p>
                                <span class="left">Orders</span>
                                <span class="right">'.$total_orders.'</span>
                            </p>
                            <p>
                                <span class="left">Unit</span>
                                <span class="right">'.$total_qty.'</span>
                            </p>
                            <p>
                                <span class="left">Refund</span>
                                <span class="right">$0</span>
                            </p>
                            <p>
                                <span class="left">Sales</span>
                                <span class="right">$'.number_format($chargeAmount_amount, 2).'</span>
                            </p>
                            <p>
                                <span class="left">Seller Fulfilled</span>
                                <span class="right">'.$total_seller.'</span>
                            </p>
                            <p>
                                <span class="left">WFS Fulfilled</span>
                                <span class="right">'.$total_wfs.'</span>
                            </p>
                            <p>
                                <span class="left">Gross Profit</span>
                                <span class="right">$'.number_format($chargeAmount_amount-$product_price, 2).'</span>
                            </p>
                            <p>
                                <span class="left">Net Profit</span>
                                <span class="right">$'.number_format($chargeAmount_amount-$product_price, 2).'</span>
                            </p>
                        </div>
                    </a>';
                    $date_range = date("Y-n-j", strtotime("first day of previous month")).'_'.date("Y-n-j", strtotime("last day of previous month"));
                    
                    $html_to_load .= '<a class="report_box" href="/dashboard_admin.php?set_date='.$date_range.'">
                        <div class="report_box_header">
                            <h2>Last Month</h2>
                            <h3>'.date("n-j-Y", strtotime("first day of previous month")).' to '.date("n-j-Y", strtotime("last day of previous month")).'</h3>
                        </div>';


                            $total_orders = 0;
                            $total_qty = 0;
                            $chargeAmount_amount = 0;
                            $product_price = 0;
                            $total_seller = 0;
                            $total_wfs = 0;

                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0])* 1000;
                                $date_end = strtotime($date_range[1])* 1000;
                                if($date_range[0]==$date_range[1]){
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;

                                    $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . "";
                                }else{

                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;


                                    $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . "";
                                }

                                $filter_code .= " AND shipNode IN('".implode("','",$fullfilled_by_filter)."')";
                            }
                            

                            

                            $get_orders = "SELECT * FROM orders $filter_code ORDER BY id desc";
                            $get_orders_query = mysqli_query($conn, $get_orders);

                            
                            $total_orders = mysqli_num_rows($get_orders_query);

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {

                                    if($order['shipNode']=='SellerFulfilled'){
                                        $total_seller = $total_seller+1;
                                    }else if($order['shipNode']=='WFSFulfilled'){
                                        $total_wfs = $total_wfs+1;
                                    }

                                    $order_id = $order['id'];
                                    $chargeAmount_currency = '';
                                    $taxAmount_currency = '';
                                    
                                    $taxAmount_amount = 0;
                                    $get_orderlines = "SELECT * FROM orderlines WHERE order_id=".$order_id;
                                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                                    if(mysqli_num_rows($get_orderlines_query) > 0){
                                    $kk=0;
                                    while($orderline = $get_orderlines_query->fetch_assoc()) {
                                        $kk++;
                                        $total_qty = $total_qty+$orderline['orderLineQuantity_amount'];
                                        $chargeAmount_currency = $orderline['chargeAmount_currency'];
                                        $taxAmount_currency = $orderline['taxAmount_currency'];
                                        $chargeAmount_amount = $chargeAmount_amount+floatval($orderline['chargeAmount_amount']);
                                        $taxAmount_amount = $taxAmount_amount+floatval($orderline['taxAmount_amount']);




                                        $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
        
                                        $product_prices = mysqli_query($conn, $product_prices);
                                        $row_product_prices = $product_prices->fetch_assoc();
                                        if(!empty($row_product_prices)){
                                            $product_price = $product_price+floatval($row_product_prices['cost']);
                                        }


                                        


                                        if(!empty($orderline['shipping_cost'])){
                                            $this_item_shipping_cost1 = floatval($orderline['shipping_cost'])+floatval($orderline['shipping_tax']);

                                            $customer_shipping_cost = $customer_shipping_cost+floatval($orderline['shipping_cost']);
                                            $total_shipping_cost = $total_shipping_cost+floatval($orderline['shipping_tax']);
                                        }else{
                                            $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
        
                                            $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                            $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                            if(!empty($row_shipping_cost)){
                                                $total_shipping_cost = $total_shipping_cost+floatval($row_shipping_cost['cost']);
                                            }
                                        }
                                        
                                        
                                    
                                    } }
                                    

                                }
                            }


                         
                        $html_to_load .= '<div class="reports_bottom">
                            <p>
                                <span class="left">Orders</span>
                                <span class="right">'.$total_orders.'</span>
                            </p>
                            <p>
                                <span class="left">Unit</span>
                                <span class="right">'.$total_qty.'</span>
                            </p>
                            <p>
                                <span class="left">Refund</span>
                                <span class="right">$0</span>
                            </p>
                            <p>
                                <span class="left">Sales</span>
                                <span class="right">$'.number_format($chargeAmount_amount, 2).'</span>
                            </p>
                            <p>
                                <span class="left">Seller Fulfilled</span>
                                <span class="right">'.$total_seller.'</span>
                            </p>
                            <p>
                                <span class="left">WFS Fulfilled</span>
                                <span class="right">'.$total_wfs.'</span>
                            </p>
                            <p>
                                <span class="left">Gross Profit</span>
                                <span class="right">$'.number_format($chargeAmount_amount-$product_price, 2).'</span>
                            </p>
                            <p>
                                <span class="left">Net Profit</span>
                                <span class="right">$'.number_format($chargeAmount_amount-$product_price, 2).'</span>
                            </p>
                        </div>
                    </a>';
    echo json_encode(array(
        'html' => $html_to_load,
        'status'=>200
    ));
    exit();
}




if(isset($_GET['walmart_add_item'])){
    


    $client_id = "8369470f-234b-4489-ae51-5abe1dd9d6d9";
    $client_secret = "AOJCwTpaaGkBuQ_rwWzeb319PlnfDRySgYgPzPZHEeJ7wcYpvemFBo4EUnQuuDsIbbazoag7iW781-1Y0FVgi2k";


    $settings_id = 1;
    $check_if_setting_already = "SELECT * FROM settings WHERE id=".$settings_id;
    $run_check_if = mysqli_query($conn, $check_if_setting_already);
    $if_settings = $run_check_if->fetch_assoc();
    $is_api_active = 'no';
    if(!empty($if_settings)){
        if($if_settings['status']==1){
            $is_api_active = 'yes';
        }
    }

    if($is_api_active=='yes'){
        $client_id = $if_settings['client_id'];
        $client_secret = $if_settings['client_secret'];
    }




    
    $bytes = random_bytes(16);
    
    $curl = curl_init();

    $url = 'https://marketplace.walmartapis.com/v3/token';

    $options = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Digital Cloud Commerce',
        CURLOPT_HEADER => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLINFO_HEADER_OUT => true
    );

    $httpHeaders = array();

    $options[CURLOPT_POST] = 1;
    $options[CURLOPT_POSTFIELDS] = 'grant_type=client_credentials';
    $httpHeaders[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
    $httpHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
    $httpHeaders[] = 'Accept: application/json';
    $httpHeaders[] = 'WM_SVC.NAME: Walmart Marketplace';
    $httpHeaders[] = 'WM_QOS.CORRELATION_ID: '.base64_encode($bytes);
    $httpHeaders[] = 'WM_SVC.VERSION: 1.0.0';
    
    //var_dump($config);

    $options[CURLOPT_HTTPHEADER] = $httpHeaders;

    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);
    

    $token = '';
    if($response !== false) {

        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);
        $accessToken = json_decode($body, true);
        if(isset($accessToken['access_token'])) {
            $token =  $accessToken['access_token'];
        } else {
            if($flag==0)
            throw new \Exception('OAuth Failed');
            else
            $token = "OAuth Failed";
        }

    } else {
        
        if($flag==0)
        throw new \Exception('OAuth Failed');
        else
        $token = "OAuth Failed";

    }
    
    curl_close($curl);


    

    
    $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));
    $auth =base64_encode($client_id.':'.$client_secret);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic '.$auth,    
          'Content-Type: application/json',
          'Accept: application/json',
          'WM_SEC.ACCESS_TOKEN: '.$token,
          'WM_QOS.CORRELATION_ID: '.$uuid ,
          'WM_SVC.NAME: Walmart Marketplace'  
    ));
    

    // $data11 = array(
    //     "inventories" => array(
    //         "nodes"=> array(
    //             array(
    //                 "shipNode"=>"".$shipNode."",
    //                 "inputQty"=>array("unit"=>"EACH", "amount"=>$inputQty)
    //             )
    //         )
    //     )
    // );

    /*
 
    $data11 = array(
        "MPItemFeedHeader" => array(
            "sellingChannel"=>"marketplace",
            // "processMode"=>"CREATE",
            "subset"=>"EXTERNAL",
            "locale"=>"en",
            "version"=>"1.5",
            "subCategory"=>"office_other"
        ),
        "MPItem"=> array(
            array(
                "Orderable"=>array(
                    "sku"=>"00078742044557",
                    "productIdentifiers"=>array(
                        "productIdType"=>"GTIN",
                        "productId"=>"00649081365763"
                    ),
                    "productName"=>"SmileMart 5-Tier Adjustable Metal Garage Storage Rack",
                    "brand"=>"SmileMart",
                    "price"=>140,
                    "ShippingWeight"=>31,
                    "electronicsIndicator"=>"No",
                    "batteryTechnologyType"=>"Does Not Contain a Battery",
                    "chemicalAerosolPesticide"=>"No",
                    "shipsInOriginalPackaging"=>"No",
                    "startDate"=>"2019-01-01T08:00:00Z",
                    "endDate"=>"2060-01-01T08:00:00Z",
                    "MustShipAlone"=>"No"
                ),
                "Visible"=>array(
                    "Office"=>array(
                        "shortDescription"=> "Storage",
                          "mainImageUrl"=> "https://i5-qa.walmartimages.com/asr/e267b894-bce0-4603-b0b9-b70da3a6c65b.b3d16cea4de59e10dbf8c181d148d8a3.jpeg",
                          "productSecondaryImageURL"=>array(
                            "https://i5-qa.walmartimages.com/asr/c053a126-34f2-4a26-bb14-c0b40af56685.ea3111241d98269f7486422e21e8f9fd.jpeg",
                            "https://i5-qa.walmartimages.com/asr/51abfcc8-1903-4113-bebf-09695dc89532.a2952b0bd51326dc0308186f3e04e70a.jpeg"
                          ),
                          "prop65WarningText"=> "None",
                          "smallPartsWarnings"=> array(
                            "0 - No warning applicable"
                          ),
                          "compositeWoodCertificationCode"=> "1 - Does not contain composite wood",
                          "keyFeatures"=> array(
                            "<ul><li><h1>17\\\" LCD Optiplex PC 2 Duo 4GB Mem 160GB HD Windows 10</h1></li><li><h3> Optiplex for Sale</h3></li><li>The Optiplex line of computers includes some of the most versatile PCs on the market. A great choice for business applications, video gaming, and other personal computer.</li></ul>"
                          ),
                          "manufacturer"=> "SmileMart",
                          "manufacturerPartNumber"=> "optiplexrf"
                    )
                )
            )
        )
    ); */

    $data11 = '{
  "MPItemFeedHeader": {
    "sellingChannel": "marketplace",
    "processMode": "REPLACE",
    "subset": "EXTERNAL",
    "locale": "en",
    "version": "1.5",
    "subCategory": "office_other"
  },
  "MPItem": [
    {
      "Orderable": {
        "sku": "2002-ZEBRA-M",
        "productIdentifiers": {
          "productIdType": "UPC",
          "productId": "263462769809"
        },
        "productName": "SmileMart 5-Tier Adjustable Metal Garage Storage Rack",
        "itemName": "ABC SmileMart 5-Tier Adjustable Metal Garage Storage Rack",
        "brand": "SmileMart",
        "price": 140,
        "ShippingWeight": 31,
        "electronicsIndicator": "No",
        "batteryTechnologyType": "Does Not Contain a Battery",
        "chemicalAerosolPesticide": "No",
        "shipsInOriginalPackaging": "No",
        "startDate": "2019-01-01T08:00:00Z",
        "endDate": "2060-01-01T08:00:00Z",
        "MustShipAlone": "No"
      },
      "Visible": {
        "Office": {
          "shortDescription": "Storage",
          "mainImageUrl": "https://trex.com.pk/uploads/trex/L9X018AEveHCEUN493XWciYaMFBacvxcsPFuAuZO.jpg",
          "productSecondaryImageURL": [
            "https://i5-qa.walmartimages.com/asr/c053a126-34f2-4a26-bb14-c0b40af56685.ea3111241d98269f7486422e21e8f9fd.jpeg",
            "https://i5-qa.walmartimages.com/asr/51abfcc8-1903-4113-bebf-09695dc89532.a2952b0bd51326dc0308186f3e04e70a.jpeg"
          ],
          "prop65WarningText": "None",
          "smallPartsWarnings": [
            "0 - No warning applicable"
          ],
          "compositeWoodCertificationCode": "1 - Does not contain composite wood",
          "keyFeatures": [
            "<ul><li><h1>17 LCD Optiplex PC 2 Duo 4GB Mem 160GB HD Windows 10</h1></li><li><h3> Optiplex for Sale</h3></li><li>The Optiplex line of computers includes some of the most versatile PCs on the market. A great choice for business applications, video gaming, and other personal computer.</li></ul>"
          ],
          "manufacturer": "SmileMart",
          "manufacturerPartNumber": "optiplexrf"
        }
      }
    }
  ]
}';





    // $body = '{"inventories":{"nodes":[{"shipNode":"10001042081","inputQty":{"unit":"EACH","amount":40},"availToSellQty":{"unit":"EACH","amount":40},"reservedQty":{"unit":"EACH","amount":40}}]}}';
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/getReport?type=buybox');
    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/feeds?feedType=MP_ITEM');  
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/report/reconreport/availableReconFiles?reportVersion=v1');
    
    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    // curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data11));
    curl_setopt($ch, CURLOPT_POSTFIELDS,$data11);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_exec($ch);
    $result_decode = json_decode($result);
    echo '<pre>';
    print_r($result_decode);

    die();






    echo json_encode(array(
        'status'=>200
    ));
    exit();
    die();
    
    
    die();
}





if(isset($_GET['feed_item_status'])){
    


    $client_id = "8369470f-234b-4489-ae51-5abe1dd9d6d9";
    $client_secret = "AOJCwTpaaGkBuQ_rwWzeb319PlnfDRySgYgPzPZHEeJ7wcYpvemFBo4EUnQuuDsIbbazoag7iW781-1Y0FVgi2k";


    $settings_id = 1;
    $check_if_setting_already = "SELECT * FROM settings WHERE id=".$settings_id;
    $run_check_if = mysqli_query($conn, $check_if_setting_already);
    $if_settings = $run_check_if->fetch_assoc();
    $is_api_active = 'no';
    if(!empty($if_settings)){
        if($if_settings['status']==1){
            $is_api_active = 'yes';
        }
    }

    if($is_api_active=='yes'){
        $client_id = $if_settings['client_id'];
        $client_secret = $if_settings['client_secret'];
    }




    
    $bytes = random_bytes(16);
    
    $curl = curl_init();

    $url = 'https://marketplace.walmartapis.com/v3/token';

    $options = array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Digital Cloud Commerce',
        CURLOPT_HEADER => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLINFO_HEADER_OUT => true
    );

    $httpHeaders = array();

    $options[CURLOPT_POST] = 1;
    $options[CURLOPT_POSTFIELDS] = 'grant_type=client_credentials';
    $httpHeaders[] = 'Authorization: Basic '.base64_encode($client_id.':'.$client_secret);
    $httpHeaders[] = 'Content-Type: application/x-www-form-urlencoded';
    $httpHeaders[] = 'Accept: application/json';
    $httpHeaders[] = 'WM_SVC.NAME: Walmart Marketplace';
    $httpHeaders[] = 'WM_QOS.CORRELATION_ID: '.base64_encode($bytes);
    $httpHeaders[] = 'WM_SVC.VERSION: 1.0.0';
    
    //var_dump($config);

    $options[CURLOPT_HTTPHEADER] = $httpHeaders;

    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);
    

    $token = '';
    if($response !== false) {
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        $header = substr($response, 0, $headerSize);
        $body = substr($response, $headerSize);

        $accessToken = json_decode($body, true);

        if(isset($accessToken['access_token'])) {
            
        $token =  $accessToken['access_token'];

        
        } else {
            
            if($flag==0)
            throw new \Exception('OAuth Failed');
            else
            $token = "OAuth Failed";
        }
    } else {
        
        
        if($flag==0)
        throw new \Exception('OAuth Failed');
        else
        $token = "OAuth Failed";
    }
    
    curl_close($curl);


    

    
    $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));
    $auth =base64_encode($client_id.':'.$client_secret);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic '.$auth,    
          'Content-Type: application/json',
          'Accept: application/json',
          'WM_SEC.ACCESS_TOKEN: '.$token,
          'WM_QOS.CORRELATION_ID: '.$uuid ,
          'WM_SVC.NAME: Walmart Marketplace'  
    ));
    

    // $data11 = array(
    //     "inventories" => array(
    //         "nodes"=> array(
    //             array(
    //                 "shipNode"=>"".$shipNode."",
    //                 "inputQty"=>array("unit"=>"EACH", "amount"=>$inputQty)
    //             )
    //         )
    //     )
    // );
 
    


    $feedId = 'A78555A0349045A29F2B724DA10D526D@AXkBCgA';
    // $body = '{"inventories":{"nodes":[{"shipNode":"10001042081","inputQty":{"unit":"EACH","amount":40},"availToSellQty":{"unit":"EACH","amount":40},"reservedQty":{"unit":"EACH","amount":40}}]}}';
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/getReport?type=buybox');
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/feeds/'.$feedId.'/errorReport?feedType=FITMENT_PIES');  
    curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/feeds/'.$feedId);  
    // curl_setopt($ch, CURLOPT_URL, 'https://marketplace.walmartapis.com/v3/report/reconreport/availableReconFiles?reportVersion=v1');
    
    // curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,     $body );
    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    // curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data11));
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_exec($ch);
    $result_decode = json_decode($result);
    echo '<pre>';
    print_r($result_decode);

    die();






    echo json_encode(array(
        'status'=>200
    ));
    exit();
    die();
    
    
    die();
}




?>