<?php
include('database_connection.php');
?>
<?php
$active_page = 'product_prices';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}
.table_list_box{
    padding: 10px 0;
}
form button {
  background: #69F;
  border-radius: 5px;
  font-family: Raleway;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  display: inline-block;
  align-items: center;
  text-transform: capitalize;
  color: #FFFFFF;
  padding: 10px 20px;
  margin: 15px 0 5px 0;
  border: 0;
}
form input {
  border: 1px solid #dfdfdf;
  padding: 3px 8px;
}
.box.product_name input {
  width: 100%;
}
</style>
<div class="recently_view_t_bg">
    <a href="/dashboard_add_product_price.php"><i class="fas fa-plus"></i> Add Product Price</a>
    <a href="/dashboard_update_product_price.php"><i class="fas fa-edit"></i> Missing Product Price</a>
</div>
<?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 30;
                $offset = ($pageno-1) * $no_of_records_per_page; 
                ?>
                
                <div class="recently_view">
                    <div class="orders_list">
                        <form class="filter_orders">
                            <!-- <div class="input_box">
                                <label>Product Name</label>
                                <input type="text" name="ProductName" placeholder="Product Name" value="">
                            </div> -->
                            <div class="input_box">
                                <label>Product Name or SKU</label>
                                <?php
                                $EnterSKU = '';
                                if(isset($_GET['EnterSKU'])){
                                    $EnterSKU = $_GET['EnterSKU'];
                                }
                                ?>
                                <input value="<?php echo $EnterSKU; ?>" type="text" name="EnterSKU" placeholder="Product Name or SKU" value="">
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
                        <div class="table_list_outer orders_list">
                            <form method="post" >
                                <input type="hidden" name="update_product_price">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Product Name
                                </div>
                                <div class="box">
                                    SKU
                                </div>
                                <div class="box">
                                    Cost
                                </div>
                                <!-- <div class="box">
                                    Action
                                </div> -->
                            </div>
                            
                            <?php

                            $filter_code = '';
                            if(isset($_GET['EnterSKU'])){
                                // $ProductName = $_GET['ProductName'];
                                $EnterSKU = $_GET['EnterSKU'];
                                if(!empty($EnterSKU)){
                                    $filter_code .= " AND productName LIKE '%".$EnterSKU."%' OR sku LIKE '%".$EnterSKU."%'";
                                    // $filter_code .= " AND sku = '".$EnterSKU."'";
                                }
                                
                            }
                            
                            $get_orders_count = "SELECT *
                            FROM orderlines
                            WHERE sku NOT IN
                                (SELECT sku 
                                 FROM product_prices) $filter_code GROUP BY sku";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);
                            

                            $get_orders = "SELECT *
                            FROM orderlines
                            WHERE sku NOT IN
                                (SELECT sku 
                                 FROM product_prices) $filter_code GROUP BY sku LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_id = $order['id'];
                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box product_name">
                                    <input type="text" name="product_name[]" value="<?php echo $order['productName']; ?>">
                                </div>
                                <div class="box">
                                    <?php echo $order['sku']; ?>
                                    
                                </div>
                                <div class="box">
                                    <input type="hidden" name="product_sku[]" value="<?php echo $order['sku']; ?>">
                                    
                                    <input type="text" name="cost[]" placeholder="cost">
                                </div>
                                
                                <!-- <div class="box">
                                    <div class="actions">
                                        <a href="#" class="view_details">View Details</a>
                                    </div>
                                </div> -->
                            </div>
                            <?php  } } ?>
                            <button type="submit">Update</button>
                            </form>
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                

                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_update_product_price.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_update_product_price.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_update_product_price.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <script>
    $(window).scroll(function() {
       if($(this).scrollTop() > 240) {
         $(".table_list_box.table_list_heading").addClass("fixed");
       }else{
        $(".table_list_box.table_list_heading").removeClass("fixed");
       }
    });
</script>
<?php
include('dashboard_footer.php');
?>