<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard_seller1.php"  ><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/dashboard_products.php"><i class="fas fa-boxes"></i> Products</a></li>
                        <li><a href="/dashboard_orders_seller.php"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_withdraw.php" class="active"><i class="fas fa-hand-holding-usd"></i> Withdraw</a></li>
                        <li><a href="/dashboard_messages_seller.php" class=""><i class="fas fa-comment-dots"></i> Messages</a></li>
                        <li><a href="/dashboard_disputes_seller.php"><i class="fas fa-people-carry"></i> Disputes</a></li>
                        <li><a href="/support_seller.php" class=""><i class="fas fa-headset"></i> Support</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <input type="text" placeholder="Search Here">
                        <button><i class="fas fa-search"></i></button>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>

                <div class="recently_view balance_details">
                    <div class="balance_details_inner">
                        <div class="left">
                            <div class="image">
                                <img src="/assets/img/Ellipse 63.png">
                            </div>
                            <h2>John Doe</h2>
                        </div>
                        <div class="right">
                            <h3>personal balance</h3>
                            <h2>$1500</h2>
                            <a href="#">Withdraw</a>
                        </div>
                    </div>
                </div>
                
                <div class="recently_view">
                    <h2>Recent Transactions</h2>
                    <div class="orders_list">
                        <div class="table_list_outer transactions_list recent_transactions">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    # Invoice
                                </div>
                                <div class="box">
                                    Reason
                                </div>
                                <div class="box">
                                    Date
                                </div>
                                <div class="box">
                                    Debit/Credit
                                </div>
                                <div class="box">
                                    Amount
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    Lorem Ipsum is simply dummy text of the printing
                                </div>
                                <div class="box">
                                    01/24/2022
                                </div>
                                <div class="box">
                                    <span class="Received">Received</span>
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                            
                            <div class="table_list_box">
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    Lorem Ipsum is simply dummy text of the printing
                                </div>
                                <div class="box">
                                    01/24/2022
                                </div>
                                <div class="box">
                                    <span class="Received">Received</span>
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    Lorem Ipsum is simply dummy text of the printing
                                </div>
                                <div class="box">
                                    01/24/2022
                                </div>
                                <div class="box">
                                    <span class="Received">Received</span>
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    Lorem Ipsum is simply dummy text of the printing
                                </div>
                                <div class="box">
                                    01/24/2022
                                </div>
                                <div class="box">
                                    <span class="Received">Received</span>
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    Lorem Ipsum is simply dummy text of the printing
                                </div>
                                <div class="box">
                                    01/24/2022
                                </div>
                                <div class="box">
                                    <span class="Received">Received</span>
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    Lorem Ipsum is simply dummy text of the printing
                                </div>
                                <div class="box">
                                    01/24/2022
                                </div>
                                <div class="box">
                                    <span class="Received">Received</span>
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>