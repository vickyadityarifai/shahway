<?php 
include('database_connection.php');
?>
<?php
$active_page = 'shipping_cost';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}

.table_list_box{
    padding: 10px 0;
}
</style>
<!-- <div class="recently_view_t_bg">
    <a href="/dashboard_add_shipping_cost.php"><i class="fas fa-plus"></i> Add Shipping Cost</a>
    <a href="/dashboard_add_shipping_cost_daily.php"><i class="fas fa-edit"></i> Daily Shipping Cost</a>
    <a href="/dashboard_update_shipping_cost.php"><i class="fas fa-edit"></i> Missing Shipping Cost</a>
</div> -->
<?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 30;
                $offset = ($pageno-1) * $no_of_records_per_page; 
                ?>
                
                <div class="recently_view">
                    <div class="orders_list">
                        <form class="filter_orders" method="get">
                            <div class="input_box">
                                <label>Tracking Number</label>
                                <?php
                                $TrackingNumber = '';
                                if(isset($_GET['TrackingNumber'])){
                                    $TrackingNumber = $_GET['TrackingNumber'];
                                }
                                ?>
                                <input value="<?php echo $TrackingNumber; ?>" type="text" name="TrackingNumber" placeholder="Tracking Number" value="">
                            </div>
                            <!-- <div class="input_box">
                                <label>Customer Order Id</label>
                                <input type="text" name="CustomerOrderId" placeholder="Customer Order Id" value="">
                            </div> -->
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Tracking Number
                                </div>
                                <div class="box">
                                    Customer Order Id
                                </div>
                                <div class="box">
                                    Cost
                                </div>
                                <!-- <div class="box">
                                    Action
                                </div> -->
                            </div>
                            
                            <?php

                            $filter_code = '';
                            if(isset($_GET['TrackingNumber'])){
                                $TrackingNumber = $_GET['TrackingNumber'];
                                // $CustomerOrderId = $_GET['CustomerOrderId'];
                                if(!empty($TrackingNumber)){
                                    $filter_code .= " WHERE tracking_number = '".$TrackingNumber."'";
                                }
                                // else if(!empty($TrackingNumber)){
                                //     $filter_code .= " WHERE orderlines.customerOrderId = '".$CustomerOrderId."'";
                                // }
                            }

                            $get_orders_count = "SELECT * FROM shipping_cost $filter_code ORDER BY id desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);

                            $get_orders = "SELECT * FROM shipping_cost $filter_code ORDER BY id desc LIMIT $offset, $no_of_records_per_page;";

                            $get_orders_query = mysqli_query($conn, $get_orders);

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $tracking_number = $order['tracking_number'];
                                    $customerOrderId = '';
                                    $orderlines   = "SELECT * FROM orderlines WHERE trackingNumber='".$tracking_number."'";
        
                                    $orderlines  = mysqli_query($conn, $orderlines );
                                    $row_orderlines  = $orderlines->fetch_assoc();
                                    if(!empty($row_orderlines)){
                                        $order_id = $row_orderlines['order_id'];

                                        $order_no   = "SELECT * FROM orders WHERE id=".$order_id;
            
                                        $order_no  = mysqli_query($conn, $order_no );
                                        $row_order_no  = $order_no->fetch_assoc();
                                        if(!empty($row_order_no)){
                                            $customerOrderId = $row_order_no['customerOrderId'];
                                            if($row_order_no['shipNode']=='WFSFulfilled'){
                                                continue;
                                            }
                                        }
                                        
                                    }
                                    
                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <?php echo $order['tracking_number']; ?>
                                    
                                </div>
                                <div class="box">
                                    <?php echo $customerOrderId; ?>
                                </div>
                                <div class="box">
                                    $<?php echo number_format($order['cost'], 2); ?>
                                </div>
                                
                                <!-- <div class="box">
                                    <div class="actions">
                                        <a href="#" class="view_details">View Details</a>
                                    </div>
                                </div> -->
                            </div>
                            <?php  } } ?>

                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                



                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_shipping_cost.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_shipping_cost.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_shipping_cost.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
<?php
include('dashboard_footer.php');
?>