<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard_admin.php"  ><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/dashboard_users.php" class="active"><i class="fas fa-users"></i> Users</a></li>
                        <li><a href="/dashboard_categories.php" ><i class="fas fa-box"></i> Categories</a></li>
                        <li><a href="/dashboard_products_admin.php" ><i class="fas fa-boxes"></i> Products</a></li>
                        <li><a href="/dashboard_orders_admin.php"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_transactions.php" ><i class="fas fa-hand-holding-usd"></i> Transactions</a></li>
                        <li><a href="/dashboard_disputes_admin.php"><i class="fas fa-people-carry"></i> Disputes</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <h1>Hi, John Doe 👋</h1>
                        <p>Good Morning, Have a nice day.</p>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                <div class="user_page_wrapper">
                        <div class="recently_view category_section add_product_page">
                            <h2>Add New User</h2>
                            <div class="buyer_seller_toggle">
                                <a href="#" class="active fields_for_buyer">Buyer</a>
                                <a href="#" class="fields_for_seller">Seller</a>
                            </div>
                            <div class="category_section_inner">
                                <form>
                                    <div class="profile_photos">
                                        <div class="profile_upload_box input_box">
                                            <label>Featured Image</label>
                                            <div class="featured_image_upload">
                                                <div class="featured_image_upload_inner">
                                                    <div class="image">
                                                        <img src="/assets/img/upload(1) 1.png">
                                                    </div>
                                                    <p>
                                                        Drag &amp; Drop Image or <span>Choose Image</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="profile_upload_box input_box fields_for_sellers">
                                            <label>Banner</label>
                                            <div class="featured_image_upload">
                                                <div class="featured_image_upload_inner">
                                                    <div class="image">
                                                        <img src="/assets/img/upload(1) 1.png">
                                                    </div>
                                                    <p>
                                                        Drag &amp; Drop Image or <span>Choose Image</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="input_box">
                                        <label>Email</label>
                                        <input type="text" placeholder="Enter Your Email Here">
                                    </div>
                                    <div class="input_box">
                                        <label>First Name</label>
                                        <input type="text" placeholder="Enter Your First Name Here">
                                    </div>
                                    <div class="input_box">
                                        <label>Last Name</label>
                                        <input type="text" placeholder="Enter Your Last Name Here">
                                    </div>
                                    <div class="input_box">
                                        <label>Password</label>
                                        <input type="text" placeholder="Enter Your Password Here">
                                    </div>

                                    <div class="input_box fields_for_sellers">
                                        <label>Shop Name</label>
                                        <input type="text" placeholder="Enter Your Shop Name Here">
                                    </div>

                                    <div class="input_box fields_for_sellers">
                                        <label>Address</label>
                                        <input type="text" placeholder="Enter Your Address Here">
                                    </div>

                                    <div class="input_box fields_for_sellers">
                                        <label>Contact</label>
                                        <input type="text" placeholder="Enter Your Contact Here">
                                    </div>
                                    
                                    <div class="input_box">
                                        <button class="submit_buttons">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>