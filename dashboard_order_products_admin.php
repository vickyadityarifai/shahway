<?php
include('database_connection.php');
?>
<?php
$active_page = 'orders_products';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}

/*.table_list_box{
    display: inline-block;
}*/
.table_list_outer.orders_list .table_list_box .box:nth-child(1) {
  text-align: left;
  padding: 0 0 0 0;
  width: 3%;
}
.table_list_box .box {
  width: 8%;
  display: inline-block;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(2) {
  width: 30%;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(3) {
  width: 8%;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(4) {
  width: 5%;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(5) {
  width: 5%;
}

.table_list_box.table_list_heading{
    font-size: 11px;
}
.table_list_box{
    font-size: 11px;
    padding: 5px 0;
}
.table_list_box.table_last_column {
  padding: 5px 0;
  background: #e2e2e2;
  font-weight: 900;
}
</style>
                <?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }
                ?>
                <div class="recently_view">
                    <div class="orders_list">
                        <form class="filter_orders" method="get">
                            
                            <input type="hidden" name="pageno" value="<?php echo $pageno; ?>">
                            <div class="input_box"> 
                                <label>Status</label>
                                <select name="status">
                                    <option value="">Select Fullfilled</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']=="SellerFulfilled"){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="SellerFulfilled" <?php echo $selected; ?>>SellerFulfilled</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']=="WFSFulfilled"){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="WFSFulfilled" <?php echo $selected; ?>>WFSFulfilled</option>
                                </select>
                            </div>
                            <div class="input_box">
                                <label>SKU</label>
                                <?php
                                $search_sku = '';
                                if(isset($_GET['sku'])){
                                    $search_sku = $_GET['sku'];
                                }
                                ?>
                                <input type="text" name="sku" placeholder="Enter SKU" value="<?php echo $search_sku; ?>">
                            </div>
                            <div class="input_box">
                                <label>Product Name</label>
                                <?php
                                $search_product_name = '';
                                if(isset($_GET['product_name'])){
                                    $search_product_name = $_GET['product_name'];
                                }
                                ?>
                                <input type="text" name="product_name" placeholder="Enter Product Name" value="<?php echo $search_product_name; ?>">
                            </div>
                            <div class="input_box"> 
                                <label>Date</label>
                                <?php
                                $date_range = date('Y-m-d').'_'.date('Y-m-d');
                                if(isset($_GET['date_range'])){
                                    $date_range = $_GET['date_range'];
                                }
                                ?>
                                <input type="hidden" name="date_range" id="date_range" value="<?php echo $date_range; ?>">
                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span><?php echo $date_range; ?></span> <i class="fa fa-caret-down"></i>
                                </div>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Item SKU
                                </div>
                                <div class="box">
                                    Quantity
                                </div>
                                <div class="box">
                                    Charge Amount
                                </div>
                                <div class="box">
                                    Shipping customer
                                </div>
                                <div class="box">
                                    Shipping charges
                                </div>
                                <div class="box">
                                    Product Price
                                </div>
                                <div class="box">
                                    Tax
                                </div>
                                <div class="box">
                                    Walmart Fees
                                </div>
                                <div class="box">
                                    Shipping Cost
                                </div>
                                <div class="box">
                                    Profit
                                </div>
                            </div>
                            
                            <?php


                            $no_of_records_per_page = 30;
                            $offset = ($pageno-1) * $no_of_records_per_page; 


                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0])* 1000;
                                $date_end = strtotime($date_range[1])* 1000;
                                if($date_range[0]==$date_range[1]){
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;

                                    $filter_code .= " WHERE orders.orderDate > " . $date_start . " AND orders.orderDate < " . $date_end . "";
                                }else{
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;
                                    $filter_code .= " WHERE orders.orderDate >= " . $date_start . " AND orders.orderDate <= " . $date_end . "";
                                }
                                
                            }

                            if(!empty($search_sku)){
                                $filter_code.= ' AND sku="'.$search_sku.'"';
                            }
                            if(!empty($search_product_name)){
                                $filter_code .= " AND productName LIKE '%".$search_product_name."%'";
                            }
                            if(isset($_GET['status']) && !empty($_GET['status'])){
                                $filter_code .= " AND orders.shipNode = '".$_GET['status']."'";
                            }

                            $get_orders_count = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id $filter_code GROUP BY sku ORDER BY orderlines.id desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);
                            

                            $get_orders = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id $filter_code GROUP BY sku ORDER BY orderlines.id desc LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);




                            $final_refund = 0;
                            $final_product_price = 0;
                            $final_walmart_fees = 0;
                            $final_total_shipping_cost = 0;
                            $final_customer_shipping_cost =0;
                            $final_customer_shipping_cost_tax = 0;

                            $final_walmart_fees = 0;





                            $final_total_qty = 0;
                            $final_chargeAmount_amount = 0;
                            $final_taxAmount_amount = 0;



                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_sku = $order['sku'];
                                    $order_sku_name = $order['productName'];



                                    $refund = 0;
                                    $product_price = 0;
                                    $walmart_fees = 0;
                                    $total_shipping_cost = 0;
                                    $customer_shipping_cost =0;
                                    $customer_shipping_cost_tax = 0;





                                    $total_qty = 0;
                                    $chargeAmount_currency = '';
                                    $taxAmount_currency = '';
                                    $chargeAmount_amount = 0;
                                    $taxAmount_amount = 0;
                                    $ssssku = '';



                                    $CommissionRate = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Commission on Product'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $CommissionRate = $row_reconciliationreport['Commission_Rate'];
                                    }

                                    $Fee_Reimbursement = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Fee/Reimbursement'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $Fee_Reimbursement = $row_reconciliationreport['Amount'];
                                    }

                                    $Fee_Reimbursement = preg_replace("/[^0-9.]/","",$Fee_Reimbursement);



                                    $get_orderlines = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id $filter_code AND sku='".$order_sku."'";
                                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                                    if(mysqli_num_rows($get_orderlines_query) > 0){
                                        $kk=0;
                                        while($orderline = $get_orderlines_query->fetch_assoc()) {
                                            $kk++;

                                            $total_qty = $total_qty+$orderline['orderLineQuantity_amount'];
                                            $chargeAmount_currency = $orderline['chargeAmount_currency'];
                                            $taxAmount_currency = $orderline['taxAmount_currency'];
                                            $chargeAmount_amount = $chargeAmount_amount+floatval($orderline['chargeAmount_amount']);
                                            $taxAmount_amount = $taxAmount_amount+floatval($orderline['taxAmount_amount']);

                                            $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
        
                                            $product_prices = mysqli_query($conn, $product_prices);
                                            $row_product_prices = $product_prices->fetch_assoc();
                                            if(!empty($row_product_prices)){
                                                $product_price = $product_price+floatval($row_product_prices['cost']);
                                            }


                                            


                                            if(!empty($orderline['shipping_cost'])){
                                                $this_item_shipping_cost1 = floatval($orderline['shipping_cost'])+floatval($orderline['shipping_tax']);

                                                $customer_shipping_cost = $customer_shipping_cost+floatval($orderline['shipping_cost']);
                                                $customer_shipping_cost_tax = $customer_shipping_cost_tax+floatval($orderline['shipping_tax']);
                                            }else{
                                                $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
            
                                                $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                                $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                                if(!empty($row_shipping_cost)){
                                                    $total_shipping_cost = $total_shipping_cost+floatval($row_shipping_cost['cost']);
                                                }
                                            }
                                            


                                        }
                                    }


                                    $total_shipping_cost = number_format($Fee_Reimbursement*$total_qty, 2);



                                    $wl_mart_fees = $chargeAmount_amount;//+$taxAmount_amount+$customer_shipping_cost;
                                    $walmart_fees = $wl_mart_fees/100*$CommissionRate;
                                    $final_walmart_fees = $final_walmart_fees+$walmart_fees;

                                    $profit = $chargeAmount_amount-$total_shipping_cost-$walmart_fees-$product_price-$refund;

                                    
                                    ?>
                                    <div class="table_list_box">
                                        <div class="box">
                                            <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                        </div>
                                        <div class="box">
                                            <?php
                                                $sku = $order_sku;
                                                $walmart_item_link = 'link_not_found';
                                                $link_found = '';
                                                $get_walmart_items = "SELECT * FROM walmart_items WHERE sku='".$sku."'"; 
                                                $walmart_items = mysqli_query($conn, $get_walmart_items);
                                                $row_walmart_items = $walmart_items->fetch_assoc();
                                                if(!empty($row_walmart_items)){
                                                    $walmart_item_link = $row_walmart_items['wpid'];
                                                    $link_found = 'link_found';
                                                }
                                                
                                                ?>
                                                <a class="<?php echo $link_found; ?>" href="https://www.walmart.com/ip/<?php echo $walmart_item_link; ?>" target="_blank">
                                                    <?php echo $order_sku_name; ?>
                                                    <br>
                                                    <?php echo $order_sku; ?>
                                                </a>
                                        </div>
                                        <div class="box">
                                            <?php echo $total_qty; 

                                            $final_total_qty = $final_total_qty+$total_qty;
                                            ?>
                                        </div>

                                        <div class="box">
                                            $<?php echo number_format($chargeAmount_amount, 2); 

                                            $final_chargeAmount_amount = $final_chargeAmount_amount+$chargeAmount_amount;
                                            ?>
                                        </div>
                                        <div class="box">
                                            $<?php echo number_format($customer_shipping_cost, 2);

                                            $final_customer_shipping_cost = $final_customer_shipping_cost+$customer_shipping_cost;
                                             ?>
                                        </div>
                                        <div class="box">
                                            $<?php echo number_format($customer_shipping_cost_tax, 2);
                                            $final_customer_shipping_cost_tax = $final_customer_shipping_cost_tax+$customer_shipping_cost_tax;
                                             ?>
                                        </div>
                                        <div class="box negative">
                                            $<?php echo number_format($product_price, 2); 
                                            $final_product_price = $final_product_price+$product_price;
                                            ?>
                                        </div>
                                        <div class="box ">
                                            $<?php echo number_format($taxAmount_amount, 2); 
                                            $final_taxAmount_amount = $final_taxAmount_amount+$taxAmount_amount;
                                            ?>
                                        </div>
                                        <div class="box negative">
                                            $<?php 
                                            echo number_format($walmart_fees,2); ?>
                                        </div>
                                        <div class="box negative">
                                            $<?php echo number_format($total_shipping_cost, 2);
                                            $final_total_shipping_cost = $final_total_shipping_cost+$total_shipping_cost;
                                                 ?>
                                        </div>
                                        
                                        <?php
                                        $profit_class = 'positive';
                                        if($profit<0){
                                            $profit_class = 'negative';
                                        }
                                        ?>
                                        <div class="box <?php echo $profit_class; ?>">
                                            $<?php
                                            echo number_format($profit,2);
                                            ?>
                                        </div>
                                    </div>
                                    <?php



                                } } ?>


                                <div class="table_list_box table_last_column">
                                    <div class="box">
                                        <?php echo mysqli_num_rows($get_orders_query); ?>
                                    </div>
                                    <div class="box">
                                        <!-- Item SKU -->
                                    </div>
                                    <div class="box">
                                        <?php echo $final_total_qty; ?>
                                    </div>
                                    <div class="box">
                                        $<?php
                                        echo $final_chargeAmount_amount;
                                        ?>
                                    </div>
                                    <div class="box">
                                        $<?php
                                        echo $final_customer_shipping_cost;
                                        ?>
                                    </div>
                                    <div class="box">
                                        $<?php
                                        echo $final_customer_shipping_cost_tax;
                                        ?>
                                    </div>
                                    <div class="box negative">
                                        $<?php echo number_format($final_product_price, 2); ?>
                                    </div>
                                    <div class="box">
                                        $<?php echo number_format($final_taxAmount_amount, 2); ?>
                                    </div>
                                    <div class="box negative">
                                        $<?php 
                                        echo number_format($final_walmart_fees,2);
                                        ?>
                                    </div>
                                    <div class="box negative">
                                        $<?php echo number_format($final_total_shipping_cost, 2); ?>
                                    </div>
                                    <?php
                                    $profit_class = 'positive';
                                    $refund = 0;
                                    $profit = $final_chargeAmount_amount-$final_total_shipping_cost-$final_walmart_fees-$final_product_price-$refund;
                                    if($profit<0){
                                        $profit_class = 'negative';
                                    }
                                    ?>
                                    <div class="box <?php echo $profit_class; ?>">
                                        $<?php
                                        
                                            echo number_format($profit,2);
                                        ?>
                                    </div>
                                </div>
                            
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                if(isset($_GET['date_range'])){
                                    $other_link = '&date_range='.$_GET['date_range'];
                                }
                                if(isset($_GET['status'])){
                                    $other_link.= '&status='.$_GET['status'];
                                }
                                if(isset($_GET['sku'])){
                                    $other_link.= '&sku='.$_GET['sku'];
                                }


                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_order_products_admin.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_order_products_admin.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_order_products_admin.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }

                                ?>
                            </ul>
                        </div>
                    </div>
                </div>

<script>
    $(window).scroll(function() {
       if($(this).scrollTop() > 240) {
         $(".table_list_box.table_list_heading").addClass("fixed");
       }else{
        $(".table_list_box.table_list_heading").removeClass("fixed");
       }
    });
</script>
<?php
include('dashboard_footer.php');

?>