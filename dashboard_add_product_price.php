<?php
include('database_connection.php');
?>
<?php
$active_page = 'product_prices';
?>
<?php
include('dashboard_header.php');
?>
<div class="recently_view_t_bg">
    <a href="/dashboard_add_product_price.php"><i class="fas fa-plus"></i> Add Product Price</a>
    <a href="/dashboard_update_product_price.php"><i class="fas fa-edit"></i> Missing Product Price</a>
</div>
<div class="user_page_wrapper">
                        <div class="recently_view category_section add_product_page">
                            <h2>Add New Product Price</h2>
                            <p>Export from Inventorylab</p>
                            <div class="category_section_inner">
                                <form method="post" enctype="multipart/form-data">

                                    <?php
                                    if(isset($_GET['step_2'])){
                                        $selection = $_SESSION['selection'];
                                        $drop_downs = explode (",", $selection); 
                                        ?>
                                        <input type="hidden" name="save_product_price1">
                                        <div class="input_box">
                                            <label>SKU</label>
                                            <select name="MSKU" id="MSKU">
                                                <?php 
                                                foreach ($drop_downs as $key => $drop_down) {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $drop_down; ?></option>
                                                    <?php
                                                }
                                                 ?>
                                            </select>
                                        </div>
                                        <div class="input_box">
                                            <label>Active Cost/Unit</label>
                                            <select name="cost" id="cost">
                                                <?php 
                                                foreach ($drop_downs as $key => $drop_down) {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $drop_down; ?></option>
                                                    <?php
                                                }
                                                 ?>
                                            </select>
                                        </div>
                                        <div class="input_box">
                                            <label>Product Title</label>
                                            <select name="title" id="title">
                                                <?php 
                                                foreach ($drop_downs as $key => $drop_down) {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $drop_down; ?></option>
                                                    <?php
                                                }
                                                 ?>
                                            </select>
                                        </div>
                                        <div class="input_box">
                                            <button class="submit_buttons">Submit</button>
                                        </div>
                                        <?php
                                    }else{
                                        ?>
                                        <input type="hidden" name="save_product_price">
                                        <div class="input_box">
                                            <label>Upload File (.csv)</label>
                                            <input type="file" name="product_price" id="product_price" accept=".csv">
                                        </div>
                                        <div class="input_box">
                                            <button class="submit_buttons">Next</button>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </form>
                            </div>
                        </div>
                    
                </div>                
                
<?php
include('dashboard_footer.php');
?>