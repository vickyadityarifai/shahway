<?php
include('database_connection.php');
?>
<?php
$active_page = 'user_manager';


?>

<?php
include('dashboard_header.php');

$name = '';
$password = '';
$username = '';
$email = '';
$type = 2;
$allowed_pages = array();
if(isset($_GET['id'])){
    $user_id = $_GET['id'];
    $get_orders = "SELECT * FROM users WHERE id=".$user_id; 
    $product = mysqli_query($conn, $get_orders);
    $row_product = $product->fetch_assoc();
    $name = $row_product['fullname'];
    $password = $row_product['password'];
    $username = $row_product['username'];
    $email = $row_product['email'];
    $type = $row_product['type'];
    $allowed_pages = $row_product['allowed_pages'];
    $allowed_pages = explode (",", $allowed_pages); 
}
?>
<style>
.allowed_pages li {
  width: 100%;
  margin: 3px 0;
  font-size: 14px;
}
.allowed_pages li input {
  width: 16px;
  height: 16px;
  -webkit-appearance: auto;
  margin: 0 5px -3px 0;
}
.allowed_pages h3 {
  margin: 0 0 5px 0;
  font-size: 18px;
}
</style>


<div class="user_page_wrapper">
                        <div class="recently_view category_section add_product_page">
                            <?php
                            if(isset($_GET['id'])){
                            ?>
                            <h2>Edit Profile Information</h2>
                            <p>Edit profile for <?php echo $row_product['username']; ?></p>
                            <?php }else{
                                ?>
                                <h2>Add Profile Information</h2>
                                <?php
                            } ?>
                            <div class="category_section_inner">
                                <form method="post" enctype="multipart/form-data">
                                    <div class="input_box">
                                        <label>Name</label>
                                        <input type="text" name="name" value="<?php echo $row_product['fullname']; ?>">
                                    </div>
                                    
                                    <?php
                                        if(isset($_GET['id'])){
                                        ?>
                                    <input type="hidden" name="edit_profile_information" value="<?php echo $_GET['id']; ?>">
                                    <div class="input_box">
                                        <label>Username</label>
                                        <input readonly="" type="text" name="username" value="<?php echo $username; ?>">
                                    </div>
                                    <div class="input_box">
                                        <label>Email</label>
                                        <input readonly="" type="text" name="email" value="<?php echo $email; ?>">
                                    </div>
                                    <?php 
                                    }else{
                                        ?>
                                        <div class="input_box">
                                            <label>Username</label>
                                            <input type="text" name="username">
                                        </div>
                                        <div class="input_box">
                                            <label>Email</label>
                                            <input type="text" name="email">
                                        </div>

                                        <input type="hidden" name="create_profile_information">
                                        <?php
                                    }
                                    ?>

                                    <div class="input_box">
                                        <label>Type</label>
                                        <select name="type">
                                            <?php
                                            $selected = '';
                                            if($type==2){
                                                $selected = 'selected';
                                            }
                                            ?>
                                            <option value="2" <?php echo $selected; ?>>Employee</option>
                                            <?php
                                            $selected = '';
                                            if($type==1){
                                                $selected = 'selected';
                                            }
                                            ?>
                                            <option value="1" <?php echo $selected; ?>>Admin</option>
                                        </select>
                                    </div>
                                    
                                    <div class="input_box">
                                        <label>Password</label>
                                        <input type="password" name="password" value="<?php echo $row_product['password']; ?>">
                                        <?php
                                        if(isset($_GET['id'])){
                                        ?>
                                        <p>Leave as it is to unchange password</p>
                                        <?php } ?>
                                    </div>


                                    <div class="allowed_pages">
                                        <h3>Allowed Pages to view</h3>
                                        <ul>

                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_admin', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_admin" type="checkbox" id="page_1"><label for="page_1">Dashboard Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_orders_admin', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?>  name="allowed_pages[]" value="dashboard_orders_admin" type="checkbox" id="page_2"><label for="page_2">Dashboard Orders Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_order_products_admin', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_order_products_admin" type="checkbox" id="page_3"><label for="page_3">Dashboard Products Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_shipping_cost', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_shipping_cost" type="checkbox" id="page_4"><label for="page_4">Dashboard Shipping Cost Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_add_shipping_cost', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_add_shipping_cost" type="checkbox" id="page_5"><label for="page_5">Dashboard Add Shipping Cost Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_add_shipping_cost_daily', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_add_shipping_cost_daily" type="checkbox" id="page_6"><label for="page_6">Dashboard Add Daily Shipping Cost Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_update_shipping_cost', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_update_shipping_cost" type="checkbox" id="page_7"><label for="page_7">Dashboard Update Shipping Cost Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_product_price', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_product_price" type="checkbox" id="page_8"><label for="page_8">Dashboard Product Price Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_add_product_price', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_add_product_price" type="checkbox" id="page_9"><label for="page_9">Dashboard Add Product Price Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_update_product_price', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_update_product_price" type="checkbox" id="page_10"><label for="page_10">Dashboard Update Product Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('reconciliationreport_monthly', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="reconciliationreport_monthly" type="checkbox" id="page_11"><label for="page_11">Dashboard Reconciliation Report Monthly Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('reconciliationreport', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="reconciliationreport" type="checkbox" id="page_12"><label for="page_12">Dashboard Add Reconciliation Report Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('recon_WFS_StorageFee', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="recon_WFS_StorageFee" type="checkbox" id="page_13"><label for="page_13">Dashboard Reconciliation WFS StorageFee Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('recon_WFS_LostInventory', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="recon_WFS_LostInventory" type="checkbox" id="page_14"><label for="page_14">Dashboard Reconciliation WFS LostInventory Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('recon_WFS_FoundInventory', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="recon_WFS_FoundInventory" type="checkbox" id="page_15"><label for="page_15">Dashboard Reconciliation WFS FoundInventory Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('recon_WFS_InboundTransportationFee', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="recon_WFS_InboundTransportationFee" type="checkbox" id="page_16"><label for="page_16">Dashboard Reconciliation WFS Inbound Transportation Fee Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('recon_WFS_RC_InventoryDisposalFee', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="recon_WFS_RC_InventoryDisposalFee" type="checkbox" id="page_17"><label for="page_17">Dashboard Reconciliation WFS RC Inventory Disposal Fee Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('recon_Deposited_in_HYPERWALLET_account', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="recon_Deposited_in_HYPERWALLET_account" type="checkbox" id="page_18"><label for="page_18">Dashboard Reconciliation Deposited in HYPERWALLET account Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('recon_WFS_Refund', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="recon_WFS_Refund" type="checkbox" id="page_19"><label for="page_19">Dashboard Reconciliation WFS Refund Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_monthly_reports', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_monthly_reports" type="checkbox" id="page_20"><label for="page_20">Dashboard Monthly Reports Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_inventory', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_inventory" type="checkbox" id="page_21"><label for="page_21">Dashboard Inventory Page</label></li>
                                            
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_wfs_inventory', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_wfs_inventory" type="checkbox" id="page_22"><label for="page_22">Dashboard WFS Inventory Page</label></li>

                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_users', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_users" type="checkbox" id="page_23"><label for="page_23">Dashboard Users Page</label></li>
                                            <?php
                                            $checked = '';
                                            if(in_array('dashboard_walmart_items', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="dashboard_walmart_items" type="checkbox" id="page_24"><label for="page_24">Dashboard Walmart Items</label></li>

                                            <?php
                                            $checked = '';
                                            if(in_array('sales_report_by_state', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="sales_report_by_state" type="checkbox" id="page_25"><label for="page_25">Sales Report by State</label></li>


                                            <?php
                                            $checked = '';
                                            if(in_array('add_product_to_walmart', $allowed_pages)){
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <li><input <?php echo $checked; ?> name="allowed_pages[]" value="add_product_to_walmart" type="checkbox" id="page_26"><label for="page_26">Add Walmart Item</label></li>



                                            
                                            
                                            
                                        </ul>
                                    </div>

                                    <div class="input_box">
                                        <button class="submit_buttons">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    
                </div>                
                
<?php
include('dashboard_footer.php');
?>