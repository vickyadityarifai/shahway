<?php
include('database_connection.php');
?>
<?php
if(!isset($_SESSION['logedin'])){
  if(isset($_COOKIE['logedin'])) {
    setcookie('logedin', $_COOKIE['logedin'], time() + 2419200, '/');
    $_SESSION['logedin'] = $_COOKIE['logedin'];


    $user_id = $_SESSION['user_id'];

    $check_users  = "SELECT * FROM users WHERE id=".$user_id."";
    $users = mysqli_query($conn, $check_users);
    $row_users = $users->fetch_assoc();
    if($row_users['type'] == 2){
    //this is dashboard
      // header("Location: ".$domainLink."/dashboard_admin.php");
    }else{
      header("Location: ".$domainLink."");
    }

    

    // header("Location: ".$domainLink."/dashboard_admin.php");
  }else{
    header("Location: ".$domainLink."/");
  }
}else{
    $user_id = $_SESSION['user_id'];
    $check_users  = "SELECT * FROM users WHERE id=".$user_id."";
    $users = mysqli_query($conn, $check_users);
    $row_users = $users->fetch_assoc();
    if($row_users['type'] == 2){
    //this is dashboard
      // header("Location: ".$domainLink."/dashboard_admin.php");
    }else{
      header("Location: ".$domainLink."");
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Shahway</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css?<?php echo time(); ?>"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>
        <style>
    .notification_messages {
  position: absolute;
  top: 10px;
  right: 10px;
  z-index: 123123123;
}
.error_message {
  background-color: #ff6d6d;
  padding: 10px 20px;
  width: 300px;
  min-height: 100px;
  border-radius: 4px;
  color: #fff;
  position: relative;
}
.success_message {
  background-color: #61ce61;
  padding: 10px 20px;
  width: 300px;
  min-height: 100px;
  border-radius: 4px;
  color: #fff;
  position: relative;
}
.dropdown-item.active_dashboard {
  background-color: #f2f2f2;
}

.check_require.error {
  border-color: red;
}
.close_notification_message{
  position: absolute;
  top: 10px;
  right: 10px;
  cursor: pointer;
  z-index: 123;
}
</style>
        <div class="notification_messages">
            <?php
            if(isset($_SESSION['error_message'])){
              ?>
              <div class="error_message">
                <span class="close_notification_message"><i class="fas fa-times"></i></span>
                <?php echo $_SESSION['error_message']; ?>
              </div>
              <?php
              unset($_SESSION['error_message']);
            }
            if(isset($_SESSION['success_message'])){
              ?>
              <div class="success_message">
                <span class="close_notification_message"><i class="fas fa-times"></i></span>
                <?php echo $_SESSION['success_message']; ?>
              </div>
              <?php
              unset($_SESSION['success_message']);
            }
            ?>
          </div>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="/dashboard_admin.php">Shahway</a>
                </div>
                <div class="menu">
                    <ul>
                        <?php /*?>
                        <?php
                        $active_class = '';
                        if($active_page=='dashboard'){
                            $active_class = 'active';
                        }
                        ?>
                        <li><a href="/dashboard_admin.php"   class="<?php echo $active_class; ?>"><i class="fas fa-home"></i> Home</a></li>
                        <?php
                        $active_class = '';
                        if($active_page=='orders'){
                            $active_class = 'active';
                        }
                        ?>
                        <li><a href="/dashboard_orders_admin.php"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <?php
                        $active_class = '';
                        if($active_page=='orders_products'){
                            $active_class = 'active';
                        }
                        ?>
                        <li><a href="/dashboard_order_products_admin.php"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Products</a></li>
                        <?php */
                        $active_class = '';
                        if($active_page=='shipping_cost'){
                            $active_class = 'active';
                        }
                        ?>
                        
                        <li><a href="/dashboard_shipping_cost.php"   class="<?php echo $active_class; ?>"><i class="fas fa-hand-holding-usd"></i> Shipping Cost</a></li>
                        <?php

                        /*
                        $active_class = '';
                        if($active_page=='product_prices'){
                            $active_class = 'active';
                        }
                        ?>
                        
                        <li><a href="/dashboard_product_price.php"   class="<?php echo $active_class; ?>"><i class="fas fa-hand-holding-usd"></i> Product Price</a></li>

                        <?php
                        $active_class = '';
                        if($active_page=='reconciliationreport_monthly'){
                            $active_class = 'active';
                        }
                        ?>
                        
                        <li><a href="/reconciliationreport_monthly.php"   class="<?php echo $active_class; ?>"><i class="fas fa-hand-holding-usd"></i> Reconciliation Report</a></li>
                        <!-- <li><a href="/dashboard_users.php" ><i class="fas fa-users"></i> Users</a></li> -->
                        <!-- <li><a href="/dashboard_categories.php" ><i class="fas fa-box"></i> Categories</a></li> -->
                        <!-- <li><a href="/dashboard_products_admin.php" ><i class="fas fa-boxes"></i> Products</a></li> -->
                        
                        <!-- <li><a href="/dashboard_transactions.php" ><i class="fas fa-hand-holding-usd"></i> Transactions</a></li> -->
                        
                        <!-- <li><a href="/dashboard_disputes_admin.php"><i class="fas fa-people-carry"></i> Disputes</a></li> -->
                        */ ?>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <div class="show_side_bar">
                        <i class="fas fa-list-alt"></i>
                    </div>
                    <form>
                        <h1>Hi, <?php echo $row_users['fullname']; ?> 👋</h1>
                        <p>Good Morning, Have a nice day.</p>
                    </form>
                    <!-- <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div> -->
                    <div class="user user_section_header">
                        <a href="#" class="profile_button">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                        <ul>
                            <li><a href="/dashboard_edit_profile.php"><i class="fas fa-edit"></i> Edit Profile</a></li>
                            <li><a href="/?logout=yes"><i class="fas fa-user-lock"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="menu menu_desktop">
                    <ul>
                        
                        <?php /*
                        $active_class = '';
                        if($active_page=='dashboard'){
                            $active_class = 'active';
                        }
                        ?>
                        <li><a href="/dashboard_admin.php"   class="<?php echo $active_class; ?>"><i class="fas fa-home"></i> Home</a></li>
                        <?php
                        $active_class = '';
                        if($active_page=='orders'){
                            $active_class = 'active';
                        }
                        ?>
                        <li><a href="/dashboard_orders_admin.php"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <?php
                        $active_class = '';
                        if($active_page=='orders_products'){
                            $active_class = 'active';
                        }
                        ?>
                        <li><a href="/dashboard_order_products_admin.php"  class="<?php echo $active_class; ?>"><i class="fas fa-list-alt"></i> Products</a></li>
                        <?php */
                        $active_class = '';
                        if($active_page=='shipping_cost'){
                            $active_class = 'active';
                        }
                        ?>
                        
                        <li><a href="/dashboard_shipping_cost.php"   class="<?php echo $active_class; ?>"><i class="fas fa-hand-holding-usd"></i> Shipping Cost</a></li>
                        <?php /*
                        $active_class = '';
                        if($active_page=='product_prices'){
                            $active_class = 'active';
                        }
                        ?>
                        
                        <li><a href="/dashboard_product_price.php"   class="<?php echo $active_class; ?>"><i class="fas fa-hand-holding-usd"></i> Product Price</a></li>

                        <?php
                        $active_class = '';
                        if($active_page=='reconciliationreport_monthly'){
                            $active_class = 'active';
                        }
                        ?>
                        
                        <li><a href="/reconciliationreport_monthly.php"   class="<?php echo $active_class; ?>"><i class="fas fa-hand-holding-usd"></i> Reconciliation Report</a></li>
                        <!-- <li><a href="/dashboard_users.php" ><i class="fas fa-users"></i> Users</a></li> -->
                        <!-- <li><a href="/dashboard_categories.php" ><i class="fas fa-box"></i> Categories</a></li> -->
                        <!-- <li><a href="/dashboard_products_admin.php" ><i class="fas fa-boxes"></i> Products</a></li> -->
                        
                        <!-- <li><a href="/dashboard_transactions.php" ><i class="fas fa-hand-holding-usd"></i> Transactions</a></li> -->
                        
                        <!-- <li><a href="/dashboard_disputes_admin.php"><i class="fas fa-people-carry"></i> Disputes</a></li> --> */ ?>
                    </ul>
                </div>