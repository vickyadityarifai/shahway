<?php
include('database_connection.php');
?>
<?php
$active_page = 'reconciliationreport_monthly';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}

.table_list_box{
    padding: 10px 0;
}
</style>
<!-- <div class="recently_view_t_bg">
    <a href="/reconciliationreport.php"><i class="fas fa-plus"></i> Add Reconciliation Report</a>
    <a href="/recon_WFS_StorageFee.php"><i class="fas fa-edit"></i> WFS StorageFee</a>
    <a href="/recon_WFS_LostInventory.php"><i class="fas fa-edit"></i> WFS LostInventory</a>
    <a href="/recon_WFS_FoundInventory.php"><i class="fas fa-edit"></i> WFS FoundInventory</a>
    <a href="/recon_WFS_InboundTransportationFee.php"><i class="fas fa-edit"></i> WFS InboundTransportationFee</a>
    <a href="/recon_WFS_RC_InventoryDisposalFee.php"><i class="fas fa-edit"></i> WFS RC_InventoryDisposalFee</a>
    <a href="/recon_Deposited_in_HYPERWALLET_account.php"><i class="fas fa-edit"></i> Deposited in HYPERWALLET account</a>
    <a href="/recon_WFS_Refund.php"><i class="fas fa-edit"></i> WFS Refund</a>
</div> -->
<?php
include('dashboard_footer.php');
?>