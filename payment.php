<?php include 'header.php';?>

            
            
            <div class="page_title checkout_pages">
                <div class="big_container">
                    <div class="page_title_inner">
                        <h2>Enter your shipping address</h2>
                        <p><a class="all">Checkout</a> - <a class="current">Enter your shipping address</a></p>
                        <div class="checkout_process">
                            <div class="bg_line">
                                <div class="50_percent"></div>
                            </div>
                            <ul>
                                <li class="active">
                                    <span class="number">1</span>
                                    <span class="text">Delivery</span>
                                </li>
                                <li>
                                    <span class="number">2</span>
                                    <span class="text">Payment</span>
                                </li>
                                <li>
                                    <span class="number">3</span>
                                    <span class="text">Review</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="checkout_form pay_ment_form">
                <div class="container">
                    <div class="checkout_form_inner">
                        <div class="payment_redios">
                            <div class="pay_with_card payment_redios_option active">
                                <div class="heading_option">
                                    <div class="left">
                                        <span></span>Pay with Card
                                    </div>
                                    <div class="right">
                                        <img src="/assets/img/image 41.png">
                                    </div>
                                </div>
                                <div class="payment_form">
                                    <form>
                                        <div class="form_box">
                                            <label>Name on card </label>
                                            <input type="text">
                                        </div>
                                        <div class="form_box">
                                            <label>Card number</label>
                                            <input type="text">
                                        </div>
                                        <div class="3_col">
                                            <div class="form_box">
                                                <label>State </label>
                                                <select>
                                                    <option>01</option>
                                                </select>
                                            </div>
                                            <div class="form_box">
                                                <label> </label>
                                                <select>
                                                    <option>2022</option>
                                                </select>
                                            </div>
                                            <div class="form_box">
                                                <label>Security code</label>
                                                <input type="text">
                                            </div>
                                        </div>
                                        <a class="continue" href="/order.php">
                                            Review your order
                                        </a>
                                    </form>
                                </div>
                            </div>
                            <div class="pay_with_paypal payment_redios_option">
                                <div class="heading_option">
                                    <div class="left">
                                        <span></span>Pay With paypal
                                    </div>
                                    <div class="right">
                                        <img src="/assets/img/image 42.png">
                                    </div>
                                </div>
                                <div class="payment_form">
                                    <form>
                                        <div class="form_box">
                                            <label>Email </label>
                                            <input type="text">
                                        </div>
                                        <a class="continue" href="/order.php">
                                            Review your order
                                        </a>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            

            

        
            
            

<?php include 'footer.php';?>