<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard_seller1.php"  ><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/dashboard_products.php"><i class="fas fa-boxes"></i> Products</a></li>
                        <li><a href="/dashboard_orders_seller.php" class="active"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_withdraw.php" ><i class="fas fa-hand-holding-usd"></i> Withdraw</a></li>
                        <li><a href="/dashboard_messages_seller.php" class=""><i class="fas fa-comment-dots"></i> Messages</a></li>
                        <li><a href="/dashboard_disputes_seller.php"><i class="fas fa-people-carry"></i> Disputes</a></li>
                        <li><a href="/support_seller.php" class=""><i class="fas fa-headset"></i> Support</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <h1>Hi, John Doe 👋</h1>
                        <p>Good Morning, Have a nice day.</p>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                
                <div class="recently_view">
                    <div class="orders_list">
                        <form class="filter_orders">
                            <div class="input_box"> 
                                <label>Status</label>
                                <select>
                                    <option>All</option>
                                </select>
                            </div>
                            <div class="input_box"> 
                                <label>Date</label>
                                <select>
                                    <option>All Date</option>
                                </select>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    Order ID
                                </div>
                                <div class="box">
                                    Ship To
                                </div>
                                <div class="box">
                                    Status
                                </div>
                                <div class="box">
                                    Date
                                </div>
                                <div class="box">
                                    Price
                                </div>
                                <div class="box">
                                    Action
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    #1536
                                </div>
                                <div class="box">
                                    Menlo Park, California,United  States
                                </div>
                                <div class="box">
                                    <span class="Processing">Processing </span>
                                </div>
                                <div class="box">
                                    01/17/2022
                                </div>
                                <div class="box">
                                    $358
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_orders_details1.php" class="view_details">View Details</a>
                                        <a href="#" class="trash"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    #1536
                                </div>
                                <div class="box">
                                    Menlo Park, California,United  States
                                </div>
                                <div class="box">
                                    <span class="Completed">Completed </span>
                                </div>
                                <div class="box">
                                    01/17/2022
                                </div>
                                <div class="box">
                                    $358
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_orders_details1.php" class="view_details">View Details</a>
                                        <a href="#" class="trash"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    #1536
                                </div>
                                <div class="box">
                                    Menlo Park, California,United  States
                                </div>
                                <div class="box">
                                    <span class="Canceled">Canceled </span>
                                </div>
                                <div class="box">
                                    01/17/2022
                                </div>
                                <div class="box">
                                    $358
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_orders_details1.php" class="view_details">View Details</a>
                                        <a href="#" class="trash"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    #1536
                                </div>
                                <div class="box">
                                    Menlo Park, California,United  States
                                </div>
                                <div class="box">
                                    <span class="Processing">Processing </span>
                                </div>
                                <div class="box">
                                    01/17/2022
                                </div>
                                <div class="box">
                                    $358
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_orders_details1.php" class="view_details">View Details</a>
                                        <a href="#" class="trash"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    #1536
                                </div>
                                <div class="box">
                                    Menlo Park, California,United  States
                                </div>
                                <div class="box">
                                    <span class="Completed">Completed </span>
                                </div>
                                <div class="box">
                                    01/17/2022
                                </div>
                                <div class="box">
                                    $358
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_orders_details1.php" class="view_details">View Details</a>
                                        <a href="#" class="trash"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    #1536
                                </div>
                                <div class="box">
                                    Menlo Park, California,United  States
                                </div>
                                <div class="box">
                                    <span class="Canceled">Canceled </span>
                                </div>
                                <div class="box">
                                    01/17/2022
                                </div>
                                <div class="box">
                                    $358
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_orders_details1.php" class="view_details">View Details</a>
                                        <a href="#" class="trash"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    #1536
                                </div>
                                <div class="box">
                                    Menlo Park, California,United  States
                                </div>
                                <div class="box">
                                    <span class="Canceled">Canceled </span>
                                </div>
                                <div class="box">
                                    01/17/2022
                                </div>
                                <div class="box">
                                    $358
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_orders_details1.php" class="view_details">View Details</a>
                                        <a href="#" class="trash"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#"><i class="fas fa-ellipsis-h"></i></a></li>
                                <li><a href="#">20</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>