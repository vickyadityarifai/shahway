<?php include 'header.php';?>

            
            
            <div class="page_title checkout_pages">
                <div class="big_container">
                    <div class="page_title_inner">
                        <h2>Enter your shipping address</h2>
                        <p><a class="all">Checkout</a> - <a class="current">Enter your shipping address</a></p>
                        <div class="checkout_process">
                            <div class="bg_line">
                                <div class="100_percent"></div>
                            </div>
                            <ul>
                                <li class="active">
                                    <span class="number">1</span>
                                    <span class="text">Delivery</span>
                                </li>
                                <li>
                                    <span class="number">2</span>
                                    <span class="text">Payment</span>
                                </li>
                                <li>
                                    <span class="number">3</span>
                                    <span class="text">Review</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="checkout_form order_page">
                <div class="container">
                    <div class="checkout_form_inner">
                        <div class="order_page_text">
                            <div class="row">
                                <div class="left">
                                    <h2>Your Order</h2>
                                </div>
                                <div class="right">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="order_page_text">
                            <div class="row">
                                <div class="left">
                                    <h2>Product</h2>
                                </div>
                                <div class="right">
                                    <h2>Total</h2>
                                </div>
                            </div>
                        </div>
                        <div class="order_page_text">
                            <div class="row">
                                <div class="left">
                                    <p>Skin Gloss Leather Bag</p>
                                </div>
                                <div class="right">
                                    <h2>$50.00</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="left">
                                    <p>Raw denim short with sequins</p>
                                </div>
                                <div class="right">
                                    <h2>$50.00</h2>
                                </div>
                            </div>
                        </div>
                        <div class="order_page_text">
                            <div class="row">
                                <div class="left">
                                    <h2>Subtotal</h2>
                                </div>
                                <div class="right">
                                    <h2>$100,00</h2>
                                </div>
                            </div>
                        </div>
                        <div class="order_page_text">
                            <div class="row">
                                <div class="left">
                                    <h2>Shipping</h2>
                                </div>
                                <div class="right">
                                    <h2>Enter your address to view shipping options.</h2>
                                </div>
                            </div>
                        </div>
                        <div class="order_page_text">
                            <div class="row">
                                <div class="left">
                                    <h2>Total</h2>
                                </div>
                                <div class="right">
                                    <h2>$100,00</h2>
                                </div>
                            </div>
                        </div>
                        <div class="order_page_text">
                            <a href="/thankyou.php" class="continue">
                                Place Order
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            

            

        
            
            

<?php include 'footer.php';?>