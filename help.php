<?php include 'header.php';?>

            
            
            <div class="page_title">
                <div class="big_container">
                    <div class="page_title_inner single_heading">
                        <h2>Help</h2>
                    </div>
                </div>
            </div>

            <div class="search_topic">
                <div class="container">
                    <div class="search_topic_inner">
                        <h2>How can we help you?</h2>
                        <form>
                            <label>Search</label>
                            <input type="text">
                            <button>Search</button>
                        </form>
                        <p>Papular Topic: <a href="#">Lorem Ipsum</a>, <a href="#">Lorem Ipsum</a></p>
                    </div>
                </div>
            </div>

            <div class="search_topic">
                <div class="container">
                    <div class="search_topic_inner">
                        <h2>Explore The interest Base</h2>
                        <div class="interest_links">
                            <ul>
                                <li><a href="#"><img src="/assets/img/Group 25.png"><span>Lorem ipsum dolor sit amet</span> <i class="fas fa-angle-right"></i></a></li>
                                <li><a href="#"><img src="/assets/img/Group 25.png"><span>Lorem ipsum dolor sit amet</span> <i class="fas fa-angle-right"></i></a></li>
                                <li><a href="#"><img src="/assets/img/Group 25.png"><span>Lorem ipsum dolor sit amet</span> <i class="fas fa-angle-right"></i></a></li>
                                <li><a href="#"><img src="/assets/img/Group 25.png"><span>Lorem ipsum dolor sit amet</span> <i class="fas fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="search_topic">
                <div class="container">
                    <div class="search_topic_inner">
                        <h2>A frequently asked questions</h2>
                        <div class="faqs">
                            <div class="toggle_heading active">
                                <h2>Details<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            <div class="toggle_heading">
                                <h2>Description<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            <div class="toggle_heading">
                                <h2>Delivery and return policies<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            <div class="toggle_heading">
                                <h2>FAQs<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                            <div class="toggle_heading">
                                <h2>Meet your sellers<i class="fas fa-angle-down"></i></h2>
                                <div class="content">
                                    Handmade item
                                    Materials: Gold
                                    Gemstone: Diamond
                                    Gem colour: Colourless
                                    Band colour: Gold
                                    Can be personalised
                                    Made to Order
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="search_topic">
                <div class="container">
                    <div class="search_topic_inner">
                        <h2>Returns & Refunds</h2>
                        <div class="return_refund">
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                            <p>
                                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors 
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        
            
            

<?php include 'footer.php';?>