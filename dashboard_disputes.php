<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard.php"><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/wishlist.php"><i class="fas fa-heart"></i> Wishlists</a></li>
                        <li><a href="/dashboard_orders.php" ><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_messages.php"><i class="fas fa-comment-dots"></i> Messages</a></li>
                        <li><a href="/dashboard_disputes.php" class="active"><i class="fas fa-people-carry"></i> Disputes</a></li>
                        <li><a href="/support.php" class=""><i class="fas fa-headset"></i> Support</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <input type="text" placeholder="Search Here">
                        <button><i class="fas fa-search"></i></button>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                
                <div class="recently_view Disputes_page">
                    <h2>Disputes</h2>
                    <div class="orders_list">
                        <form class="filter_orders">
                            <div class="input_box search_input"> 
                                <label>Search</label>
                                <input placeholder="Search">
                            </div>
                            <div class="input_box"> 
                                <label>Status</label>
                                <select>
                                    <option>All</option>
                                </select>
                            </div>
                            <div class="input_box"> 
                                <label>Date</label>
                                <select>
                                    <option>All Date</option>
                                </select>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
                        <div class="classifications">
                            <a href="#" class="active">Needs respnse</a><a href="#">All Disputes</a>
                        </div>
                        <div class="table_list_outer dispute_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    Amount
                                </div>
                                <div class="box">
                                    Status
                                </div>
                                <div class="box">
                                    Reason
                                </div>
                                <div class="box">
                                    Customer
                                </div>
                                <div class="box">
                                    Disputed on
                                </div>
                                <div class="box">
                                    Respond Until
                                </div>
                                <div class="box">
                                    View
                                </div>
                                
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    $80,oo
                                </div>
                                <div class="box">
                                    <span class="Open">Open</span>
                                </div>
                                <div class="box">
                                    Not received
                                </div>
                                <div class="box">
                                    andrew@gmail.com
                                </div>
                                <div class="box">
                                    Jan 12
                                </div>
                                <div class="box">
                                    01/17/2022   -    7:00 pm
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_desputes_detail.php" class="eye_button"><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="table_list_box">
                                <div class="box">
                                    $80,oo
                                </div>
                                <div class="box">
                                    <span class="Resold">Resold</span>
                                </div>
                                <div class="box">
                                    Not received
                                </div>
                                <div class="box">
                                    andrew@gmail.com
                                </div>
                                <div class="box">
                                    Jan 12
                                </div>
                                <div class="box">
                                    01/17/2022   -    7:00 pm
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_desputes_detail.php" class="eye_button"><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    $80,oo
                                </div>
                                <div class="box">
                                    <span class="Resold">Resold</span>
                                </div>
                                <div class="box">
                                    Not received
                                </div>
                                <div class="box">
                                    andrew@gmail.com
                                </div>
                                <div class="box">
                                    Jan 12
                                </div>
                                <div class="box">
                                    01/17/2022   -    7:00 pm
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_desputes_detail.php" class="eye_button"><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    $80,oo
                                </div>
                                <div class="box">
                                    <span class="Resold">Resold</span>
                                </div>
                                <div class="box">
                                    Not received
                                </div>
                                <div class="box">
                                    andrew@gmail.com
                                </div>
                                <div class="box">
                                    Jan 12
                                </div>
                                <div class="box">
                                    01/17/2022   -    7:00 pm
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_desputes_detail.php" class="eye_button"><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    $80,oo
                                </div>
                                <div class="box">
                                    <span class="Resold">Resold</span>
                                </div>
                                <div class="box">
                                    Not received
                                </div>
                                <div class="box">
                                    andrew@gmail.com
                                </div>
                                <div class="box">
                                    Jan 12
                                </div>
                                <div class="box">
                                    01/17/2022   -    7:00 pm
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_desputes_detail.php" class="eye_button"><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    $80,oo
                                </div>
                                <div class="box">
                                    <span class="Resold">Resold</span>
                                </div>
                                <div class="box">
                                    Not received
                                </div>
                                <div class="box">
                                    andrew@gmail.com
                                </div>
                                <div class="box">
                                    Jan 12
                                </div>
                                <div class="box">
                                    01/17/2022   -    7:00 pm
                                </div>
                                <div class="box">
                                    <div class="actions">
                                        <a href="/dashboard_desputes_detail.php" class="eye_button"><i class="far fa-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#"><i class="fas fa-ellipsis-h"></i></a></li>
                                <li><a href="#">20</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>