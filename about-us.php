<?php include 'header.php';?>
<div class="page_title">
    <div class="big_container">
        <div class="page_title_inner">
            <h2>About Us</h2>
            <p><a class="all">All</a> - <a class="current">About Us</a></p>
        </div>
    </div>
</div>
<div class="about_page_section">
	<div class="big_container">
		<div class="about_page_section_inner bg_white">
			<div class="about_page_section_inner_inner">
				<h2>BEHIND THE SCENE</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue sodales arcu vel interdum. Proin nibh nisl, blandit ac sem vitae, eleifend tristique nisi. Maecenas vitae nisi neque. Vestibulum dignissim, mi at elementum laoreet, urna nibh rhoncus erat, </p>
			</div>
		</div>
	</div>
</div>

<div class="about_page_section_with_image">
	<div class="big_container">
		<div class="about_page_section_with_image_inner">
			<div class="left">
				<div class="image" style="background: url('/assets/img/Rectangle 261(1).png');">
					
				</div>
			</div>
			<div class="right">
				<div class="content">
					<div class="about_page_section_inner bg_half_white">
						<div class="about_page_section_inner_inner">
							<h2>Our Story</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue sodales arcu vel interdum. Proin nibh nisl, blandit ac sem vitae, eleifend tristique nisi. Maecenas vitae nisi neque. Vestibulum dignissim, mi at elementum laoreet, urna nibh rhoncus erat, quis pretium enim orci varius mi. Pellentesque pharetra lacinia lobortis. Quisque condimentum risus placerat erat imperdiet tristique. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="about_page_section">
	<div class="big_container">
		<div class="about_page_section_inner bg_half_white">
			<div class="about_page_section_inner_inner">
				<h2>Our Mission</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue sodales arcu vel interdum. Proin nibh nisl, blandit ac sem vitae, eleifend tristique nisi. Maecenas vitae nisi neque. Vestibulum dignissim, mi at elementum laoreet, urna nibh rhoncus erat, quis pretium enim orci varius mi. Pellentesque pharetra lacinia lobortis. Quisque condimentum risus placerat erat imperdiet tristique.  </p>
			</div>
		</div>
	</div>
</div>

<div class="about_page_section_with_image">
	<div class="big_container">
		<div class="about_page_section_with_image_inner">
			<div class="left">
				<div class="content">
					<div class="about_page_section_inner bg_half_white">
						<div class="about_page_section_inner_inner">
							<h2>Hello From CEO</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue sodales arcu vel interdum. Proin nibh nisl, blandit ac sem vitae, eleifend tristique nisi. Maecenas vitae nisi neque. Vestibulum dignissim, mi at elementum laoreet, urna nibh rhoncus erat, quis pretium enim orci varius mi. Pellentesque pharetra lacinia lobortis. Quisque condimentum risus placerat erat imperdiet tristique.  </p>
							<h3>Jane Brown</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="right">
				<div class="image" style="background: url('/assets/img/Rectangle 263.png');">
					
				</div>
				
			</div>
		</div>
	</div>
</div>
<div class="company_partners">
	<div class="big_container">
		<div class="company_partners_inner">
			<div class="image"><img src="/assets/img/Screenshot 2022-01-13 at 12.14 1.png"></div>
		</div>
	</div>
</div>
<?php include 'footer.php';?>