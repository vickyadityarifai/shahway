<?php
include('database_connection.php');
?>
<?php
$active_page = 'shipping_cost';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}

.table_list_box{
    padding: 10px 0;
}
form button {
  background: #69F;
  border-radius: 5px;
  font-family: Raleway;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  display: inline-block;
  align-items: center;
  text-transform: capitalize;
  color: #FFFFFF;
  padding: 10px 20px;
  margin: 15px 0 5px 0;
  border: 0;
}
form input {
  border: 1px solid #dfdfdf;
  padding: 3px 8px;
}
</style>
<div class="recently_view_t_bg">
    <a href="/dashboard_add_shipping_cost.php"><i class="fas fa-plus"></i> Add Shipping Cost</a>
    <a href="/dashboard_add_shipping_cost_daily.php"><i class="fas fa-edit"></i> Daily Shipping Cost</a>
    <a href="/dashboard_update_shipping_cost.php"><i class="fas fa-edit"></i> Missing Shipping Cost</a>
</div>
<?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 30;
                $offset = ($pageno-1) * $no_of_records_per_page; 
                ?>
                
                <div class="recently_view">
                    <div class="orders_list">
                        <form class="filter_orders" method="get">
                            
                            <input type="hidden" name="pageno" value="<?php echo $pageno; ?>">
                            <div class="input_box">
                                <label>Tracking Number</label>
                                <?php
                                $TrackingNumber = '';
                                if(isset($_GET['TrackingNumber'])){
                                    $TrackingNumber = $_GET['TrackingNumber'];
                                }
                                ?>
                                <input type="text" name="TrackingNumber" placeholder="Tracking Number" value="<?php echo $TrackingNumber; ?>">
                            </div>
                            <div class="input_box">
                                <label>Customer Order ID</label>
                                <?php
                                $search_customer_order_id = '';
                                if(isset($_GET['customer_order_id'])){
                                    $search_customer_order_id = $_GET['customer_order_id'];
                                }
                                ?>
                                <input type="text" name="customer_order_id" placeholder="Customer Order ID" value="<?php echo $search_customer_order_id; ?>">
                            </div>
                            
                            <div class="input_box"> 
                                <label>Date</label>
                                <?php
                                $date_range = date('Y-m-d').'_'.date('Y-m-d');
                                if(isset($_GET['date_range'])){
                                    $date_range = $_GET['date_range'];
                                }
                                ?>
                                <input type="hidden" name="date_range" id="date_range" value="<?php echo $date_range; ?>">
                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span><?php echo $date_range; ?></span> <i class="fa fa-caret-down"></i>
                                </div>
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
                        <div class="table_list_outer orders_list">
                            <form method="post" >
                                <input type="hidden" name="update_shipping">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Tracking Number
                                </div>
                                <div class="box">
                                    Customer Order Id
                                </div>
                                <div class="box">
                                    Order Date
                                </div>
                                <div class="box">
                                    Cost
                                </div>
                                <!-- <div class="box">
                                    Action
                                </div> -->
                            </div>
                            
                            <?php
                            $shipping_prices = array();
                            $shipping_cost   = "SELECT * FROM shipping_cost";
        
                            $shipping_cost  = mysqli_query($conn, $shipping_cost );
                            while($shipping = $shipping_cost->fetch_assoc()) {
                              $shipping_prices[]=$shipping['tracking_number'];
                            }



                            $filter_code = '';
                            if(!empty($date_range) && isset($_GET['date_range'])){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0])* 1000;
                                $date_end = strtotime($date_range[1])* 1000;
                                if($date_range[0]==$date_range[1]){
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;

                                    $filter_code .= " AND orders.orderDate > " . $date_start . " AND orders.orderDate < " . $date_end . "";
                                }else{
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;
                                    $filter_code .= " AND orders.orderDate >= " . $date_start . " AND orders.orderDate <= " . $date_end . "";
                                }
                            }

                            if(isset($_GET['customer_order_id']) && !empty($_GET['customer_order_id'])){
                                $filter_code .= " AND orders.customerOrderId = '".$_GET['customer_order_id']."'";
                            }

                            if(isset($_GET['TrackingNumber']) && !empty($_GET['TrackingNumber'])){
                                $filter_code .= " AND orderlines.TrackingNumber = '".$_GET['TrackingNumber']."'";
                            }

                           


                            $get_orders_count = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id WHERE orders.shipNode = 'SellerFulfilled' AND NULLIF(trackingNumber, '') IS NOT NULL AND trackingNumber NOT IN('".implode("','",$shipping_prices)."') $filter_code GROUP BY order_id";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);



                            
                            $get_orders = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id WHERE orders.shipNode = 'SellerFulfilled' AND NULLIF(trackingNumber, '') IS NOT NULL  AND trackingNumber NOT IN('".implode("','",$shipping_prices)."') $filter_code GROUP BY order_id LIMIT $offset, $no_of_records_per_page;";
                            // $get_orders = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id WHERE orders.shipNode = 'SellerFulfilled' AND NULLIF(trackingNumber, '') IS NOT NULL  ORDER BY id desc LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);
                            

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_id = $order['order_id'];

                                    // $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$order['trackingNumber']."'";
        
                                    // $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                    // $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                    // if(empty($row_shipping_cost)){
                                    $customerOrderId = '';
                                    $order_date = '';



                                      $order_no   = "SELECT * FROM orders WHERE id=".$order_id;
        
                                      $order_no  = mysqli_query($conn, $order_no );
                                      $row_order_no  = $order_no->fetch_assoc();

                                      if(!empty($row_order_no)){
                                          $customerOrderId = $row_order_no['customerOrderId'];
                                          $mil = $row_order_no['orderDate'];
                                          $seconds = ceil($mil / 1000);
                                          $order_date = date("m-d-Y H:i", $seconds);
                                      }

                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <?php echo $order['trackingNumber']; ?>
                                    
                                </div>
                                <div class="box">
                                    <?php echo $customerOrderId; ?>
                                </div>
                                <div class="box">
                                    <?php echo $order_date; ?>
                                </div>
                                <div class="box">
                                    <input type="hidden" name="tracking_number[]" value="<?php echo $order['trackingNumber']; ?>">
                                    <input type="text" name="cost[]" placeholder="cost">
                                </div>
                                
                                <!-- <div class="box">
                                    <div class="actions">
                                        <a href="#" class="view_details">View Details</a>
                                    </div>
                                </div> -->
                            </div>
                            <?php  
                        // }
                        } } ?>
                        <button type="submit">Update</button>
                            </form>
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                

                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_update_shipping_cost.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_update_shipping_cost.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/dashboard_update_shipping_cost.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <script>
    $(window).scroll(function() {
       if($(this).scrollTop() > 240) {
         $(".table_list_box.table_list_heading").addClass("fixed");
       }else{
        $(".table_list_box.table_list_heading").removeClass("fixed");
       }
    });
</script>
<?php
include('dashboard_footer.php');
?>