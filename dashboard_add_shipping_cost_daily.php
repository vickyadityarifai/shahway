<?php
include('database_connection.php');
?>
<?php
$active_page = 'shipping_cost';
?>
<?php
include('dashboard_header.php');
?>
<div class="recently_view_t_bg">
    <a href="/dashboard_add_shipping_cost.php"><i class="fas fa-plus"></i> Add Shipping Cost</a>
    <a href="/dashboard_add_shipping_cost_daily.php"><i class="fas fa-edit"></i> Daily Shipping Cost</a>
    <a href="/dashboard_update_shipping_cost.php"><i class="fas fa-edit"></i> Missing Shipping Cost</a>
</div>
<style>
    .user_page_wrapper {
  width: 100%;
  margin: 0 auto;
}
.add_shipping_daily_box {
  display: flex;
  position: relative;
}
.add_shipping_daily_box .input_box {
  margin: 5px 5px;
}
.add_shipping_daily_box:nth-child(2n) {
  background: #f2f2f2;
}
.add_shipping_daily {
  padding: 10px 15px;
  border-radius: 4px;
  background: #69F;
  color: #fff;
  margin: 10px 0 0 0;
}
.add_shipping_daily_box .remove_row {
  cursor: pointer;
  position: absolute;
  top: 5px;
  right: 5px;
} 
</style>
<div class="user_page_wrapper">
                        <div class="recently_view category_section add_product_page">
                            <h2>Add New Shipping Cost</h2>
                            <div class="category_section_inner">
                                <form method="post" enctype="multipart/form-data">
                                    
                                   

                                    <input type="hidden" name="save_shipping_daily">
                                    
                                    <div class="add_shipping_daily_box_wrapper">

                                        <div class="add_shipping_daily_box">
                                            <div class="input_box">
                                                <label>Date</label>
                                                <input type="text" name="date[]" placeholder="Date">
                                            </div>
                                            <div class="input_box">
                                                <label>Recipient</label>
                                                <input type="text" name="recipient[]" placeholder="Recipient">
                                            </div>
                                            <div class="input_box">
                                                <label>Order #</label>
                                                <input type="text" name="order[]" placeholder="Order #">
                                            </div>
                                            <div class="input_box">
                                                <label>Tracking Number</label>
                                                <input type="text" name="tracking_number[]" placeholder="Tracking Number">
                                            </div>
                                            <div class="input_box">
                                                <label>Shipping Cost</label>
                                                <input type="text" name="shipping_cost[]" placeholder="Shipping Cost">
                                            </div>
                                            <div class="input_box">
                                                <label>Status</label>
                                                <input type="text" name="status[]" placeholder="Status">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add_shipping_daily_adder_box">
                                        <a href="#" class="add_shipping_daily"><i class="fas fa-plus"></i></a>
                                    </div>
                                    

                                    <div class="input_box">
                                        <button class="submit_buttons">Submit</button>
                                    </div>
                                    
                                    
                                </form>
                            </div>
                        </div>
                    
                </div>                
                
<?php
include('dashboard_footer.php');
?>