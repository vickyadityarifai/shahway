<?php include 'header.php';?>

            
            
            <div class="page_title">
                <div class="big_container">
                    <div class="page_title_inner">
                        <h2>Jewellery & Accessories</h2>
                        <p><a class="all">All</a> - <a class="current">Jewellery & Accessories</a></p>
                        <p class="total_results">( 1,977,173 results,)</p>
                    </div>
                </div>
            </div>
            <div class="categories_slider">
                <div class="big_container">
                    <div class="categories_slider_inner">
                        <div class="heading">
                            <h2>Choose your <span>Category</span></h2>
                            <div class="right">
                                <div class="slider_arrows">
                                    <span class="slider4 left"><i class="fas fa-angle-left"></i></span>
                                    <span class="slider4 right"><i class="fas fa-angle-right"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="categories_slider_box slider4">
                            <div class="categories_slider_box_inner">
                                <a href="/shop.php" style="background-image: url('/assets/img/Rectangle 284.png')">
                                    <span>Rings</span>
                                </a>
                            </div>
                            <div class="categories_slider_box_inner selected">
                                <a href="/shop.php" style="background-image: url('/assets/img/Rectangle 285.png')">
                                    <span>necklace</span>
                                </a>
                            </div>
                            <div class="categories_slider_box_inner">
                                <a href="/shop.php" style="background-image: url('/assets/img/Rectangle 286.png')">
                                    <span>Bracelet</span>
                                </a>
                            </div>
                            <div class="categories_slider_box_inner">
                                <a href="/shop.php" style="background-image: url('/assets/img/Rectangle 287.png')">
                                    <span>earings</span>
                                </a>
                            </div>
                            <div class="categories_slider_box_inner">
                                <a href="/shop.php" style="background-image: url('/assets/img/Rectangle 288.png')">
                                    <span>nose pin</span>
                                </a>
                            </div>
                            <div class="categories_slider_box_inner">
                                <a href="/shop.php" style="background-image: url('/assets/img/Rectangle 284.png')">
                                    <span>Rings</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="shop_loop_section">
                <div class="big_container">
                    <div class="shop_loop_section_inner">
                        <div class="filters">
                            <div class="filters_inner">
                                <div class="close_filter"><i class="fas fa-times"></i></div>
                                <div class="f_categories">
                                    <h2><i class="fas fa-long-arrow-alt-left"></i> All Categories</h2>
                                    <ul class="categories_list">
                                        <li>
                                            <a href="/shop.php">Jewellery & Accessories</a>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Rings</a>
                                            <ul>
                                                <li>
                                                    <a href="/shop.php">Wedding</a>
                                                </li>
                                                <li>
                                                    <a href="/shop.php">Statement</a>
                                                </li>
                                                <li>
                                                    <a href="/shop.php">Solitaire</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Necklace</a>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Bracelet</a>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Earings</a>
                                        </li>
                                        <li>
                                            <a href="/shop.php">Nose pin</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="filter_box">
                                    <h2>Price ($)</h2>
                                    <div class="price_range_inner">
                                        <div class="price_range_values">
                                            <div class="left">$<span>20</span></div>
                                            <div class="right">$<span>400</span></div>
                                        </div>
                                        <div id="slider_price_range"></div>
                                    </div>
                                </div>

                                <div class="filter_box">
                                    <h2>Colour</h2>
                                    <ul class="colors_filter">
                                        <li style="background-color: #6D6969;"><span></span></li>
                                        <li style="background-color: #000000;"><span></span></li>
                                        <li style="background-color: #2770DD;"><span></span></li>
                                        <li style="background-color: #7B1616;"><span></span></li>
                                        <li style="background-color: #672EE1;"><span></span></li>
                                        <li style="background-color: #28CCA5;"><span></span></li>
                                        <li style="background-color: #FF3F3F;"><span></span></li>
                                        <li style="background-color: #85990C;"><span></span></li>
                                    </ul>
                                    <a class="show_more"><i class="fas fa-plus"></i> Show More</a>
                                </div>

                                <div class="filter_box">
                                    <h2>Size</h2>
                                    <ul>
                                        <li><span class="checkmark"></span><input type="checkbox" id="size_0"><label for="size_0">Extra Small</label></li>
                                        <li><span class="checkmark"></span><input type="checkbox" id="size_1"><label for="size_1">Small</label></li>
                                        <li><span class="checkmark"></span><input type="checkbox" id="size_2"><label for="size_2">Medium</label></li>
                                        <li><span class="checkmark"></span><input type="checkbox" id="size_3"><label for="size_3">Large</label></li>
                                        <li><span class="checkmark"></span><input type="checkbox" id="size_4"><label for="size_4">Extra Large</label></li>
                                    </ul>
                                    <a class="show_more"><i class="fas fa-plus"></i> Show More</a>
                                </div>

                                <div class="filter_box">
                                    <h2>Celebration</h2>
                                    <ul>
                                        <li><span class="checkmark"></span><input type="checkbox" id="1_size_0"><label for="1_size_0">Christmas</label></li>
                                        <li><span class="checkmark"></span><input type="checkbox" id="1_size_1"><label for="1_size_1">Easter</label></li>
                                        <li><span class="checkmark"></span><input type="checkbox" id="1_size_2"><label for="1_size_2">Halloween</label></li>
                                        <li><span class="checkmark"></span><input type="checkbox" id="1_size_3"><label for="1_size_3">Independence Day</label></li>
                                        <li><span class="checkmark"></span><input type="checkbox" id="1_size_4"><label for="1_size_4">Lunar New Year</label></li>
                                    </ul>
                                    <a class="show_more"><i class="fas fa-plus"></i> Show More</a>
                                </div>
                            </div>

                        </div>
                        <div class="products_loop">
                            <div class="shorting_option">
                                <div class="show_filters">All Filters</div>
                                <div class="view_by">View: 
                                    <div class="view_by_dropdown">
                                        <div class="view_by_dropdown_active"><i class="fas fa-th-large"></i> <i class="fas fa-angle-down"></i></div>
                                        <div class="view_by_dropdown_options">
                                            <ul>
                                                <li><i class="fas fa-th-large"></i></li>
                                                <li><i class="fas fa-grip-lines"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="sort_by">Sort: 
                                    <div class="sort_by_dropdown">
                                        <div class="sort_by_dropdown_active">Best Match <i class="fas fa-angle-down"></i></div>
                                        <div class="sort_by_dropdown_options">
                                            <ul>
                                                <li>Best Match</li>
                                                <li>Latest</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="products_loop_inner">
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                                <div class="product_box">
                                    <div class="wishlist_cart">
                                        <span><i class="far fa-heart"></i></span>
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                    </div>
                                    <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                        <a href="/single.php">
                                            
                                        </a>
                                    </div>
                                    <div class="details">
                                        <div class="name">
                                            <a href="/single.php">
                                                Product name
                                            </a>
                                            <div class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                        <p class="price">$66,66</p>
                                    </div>
                                </div>
                            </div>
                            <div class="pagination">
                                <ul>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#"><i class="fas fa-ellipsis-h"></i></a></li>
                                    <li><a href="#">20</a></li>
                                </ul>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            

<?php include 'footer.php';?>