<?php
include('database_connection.php');
?>
<?php
$active_page = 'reconciliationreport_monthly';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .table_list_box{
        padding: 6px 0;
    }
</style>
<div class="user_page_wrapper">
                        <div class="recently_view category_section add_product_page">
                            <h2>Add New Reconciliation Report</h2>
                            <p>Export from Reconciliation report</p>
                            <div class="category_section_inner">
                                <form method="post" enctype="multipart/form-data">
                                    <?php
                                    if(isset($_GET['step_2'])){
                                        ?>
                                        <input type="hidden" name="reconciliationreport">
                                        <p>Save my data</p>
                                        <div class="input_box">
                                            <button class="submit_buttons">Submit</button>
                                        </div>
                                        <?php
                                    }else{
                                        ?>
                                        <input type="hidden" name="save_reconciliationreport">
                                        <div class="input_box">
                                            <label>Upload File (.csv)</label>
                                            <input type="file" name="reconciliationreport_file" id="reconciliationreport_file" accept=".csv">
                                        </div>
                                        <div class="input_box">
                                            <button class="submit_buttons">Next</button>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    
                                </form>
                            </div>
                        </div>





                    
                </div>    


                <?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 30;
                $offset = ($pageno-1) * $no_of_records_per_page; 
                ?>
                
                <div class="recently_view">
                    <div class="orders_list">
                        
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    File Name
                                </div>
                                <div class="box">
                                    Date created
                                </div>
                            </div>

                            
                            <?php


                            $filter_code = '';
                            if(isset($_GET['status'])){
                                // $ProductName = $_GET['ProductName'];
                                $status = $_GET['status'];
                                if(!empty($status)){
                                    $filter_code .= " WHERE status = ".$status;
                                }
                                
                            }

                            $get_orders_count = "SELECT * FROM reconciliationreport_files $filter_code ORDER BY id desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);


                            

                            $get_orders = "SELECT * FROM reconciliationreport_files $filter_code ORDER BY id desc LIMIT $offset, $no_of_records_per_page;"; 

                            $get_orders_query = mysqli_query($conn, $get_orders);

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_id = $order['id'];
                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <?php echo $order['filename']; ?>
                                    
                                </div>
                                <div class="box">
                                    <?php echo date("m/d/Y", $order['insert_date']); ?>
                                    
                                </div>
                            </div>
                            <?php  } } ?>
                            
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                

                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/reconciliationreport.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/reconciliationreport.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/reconciliationreport.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>            
                
<?php
include('dashboard_footer.php');
?>