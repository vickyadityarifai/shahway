<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard_admin.php" ><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/dashboard_users.php" ><i class="fas fa-users"></i> Users</a></li>
                        <li><a href="/dashboard_categories.php" ><i class="fas fa-box"></i> Categories</a></li>
                        <li><a href="/dashboard_products_admin.php" ><i class="fas fa-boxes"></i> Products</a></li>
                        <li><a href="/dashboard_orders_admin.php"  class="active"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_transactions.php" ><i class="fas fa-hand-holding-usd"></i> Transactions</a></li>
                        <li><a href="/dashboard_disputes_admin.php"><i class="fas fa-people-carry"></i> Disputes</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <h1>Hi, John Doe 👋</h1>
                        <p>Good Morning, Have a nice day.</p>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                
                <div class="recently_view">
                    <div class="orders_list_items">
                        <h2>Items</h2>
                        <div class="table_list_outer orders_list_items">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    Name
                                </div>
                                <div class="box">
                                    Shop 
                                </div>
                                <div class="box">
                                    SKU
                                </div>
                                <div class="box">
                                    Color
                                </div>
                                <div class="box">
                                    Size
                                </div>
                                <div class="box">
                                    Price
                                </div>
                                <div class="box">
                                    Quantity
                                </div>
                                <div class="box">
                                    Total Price
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    <div class="order_item">
                                        <span><img src="/assets/img/Rectangle 284.png"></span>
                                        Product Name
                                    </div>
                                </div>
                                <div class="box">
                                    Shop Name
                                </div>
                                <div class="box">
                                    #265596
                                </div>
                                <div class="box">
                                    <div class="color">
                                        <span style="background-color: green;"></span>
                                    </div>
                                    (Green)
                                </div>
                                <div class="box">
                                    24×12
                                </div>
                                <div class="box">
                                    $512
                                </div>
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    <div class="order_item">
                                        <span><img src="/assets/img/Rectangle 284.png"></span>
                                        Product Name
                                    </div>
                                </div>
                                <div class="box">
                                    Shop Name
                                </div>
                                <div class="box">
                                    #265596
                                </div>
                                <div class="box">
                                    <div class="color">
                                        <span style="background-color: green;"></span>
                                    </div>
                                    (Green)
                                </div>
                                <div class="box">
                                    24×12
                                </div>
                                <div class="box">
                                    $512
                                </div>
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    <div class="order_item">
                                        <span><img src="/assets/img/Rectangle 284.png"></span>
                                        Product Name
                                    </div>
                                </div>
                                <div class="box">
                                    Shop Name
                                </div>
                                <div class="box">
                                    #265596
                                </div>
                                <div class="box">
                                    <div class="color">
                                        <span style="background-color: green;"></span>
                                    </div>
                                    (Green)
                                </div>
                                <div class="box">
                                    24×12
                                </div>
                                <div class="box">
                                    $512
                                </div>
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                            <div class="table_list_box">
                                <div class="box">
                                    <div class="order_item">
                                        <span><img src="/assets/img/Rectangle 284.png"></span>
                                        Product Name
                                    </div>
                                </div>
                                <div class="box">
                                    Shop Name
                                </div>
                                <div class="box">
                                    #265596
                                </div>
                                <div class="box">
                                    <div class="color">
                                        <span style="background-color: red;"></span>
                                    </div>
                                    (Red)
                                </div>
                                <div class="box">
                                    24×12
                                </div>
                                <div class="box">
                                    $512
                                </div>
                                <div class="box">
                                    01
                                </div>
                                <div class="box">
                                    $512
                                </div>
                            </div>
                        </div>
                        <div class="invoice_details">
                            <div class="left">
                                <h3>Invoice</h3>
                                <div class="invoice_details_row">
                                    <span class="text_left">Item Sub Total</span>
                                    <span class="text_right">$58556</span>
                                </div>
                                <div class="invoice_details_row">
                                    <span class="text_left">Discount</span>
                                    <span class="text_right">$0.00</span>
                                </div>
                                <div class="invoice_details_row">
                                    <span class="text_left">Shipping Price</span>
                                    <span class="text_right">$158</span>
                                </div>
                                <div class="invoice_details_row">
                                    <span class="text_left">Grand Total</span>
                                    <span class="text_right">$58556</span>
                                </div>
                            </div>
                            <div class="right">
                                <h3>Refunds</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.</p>
                                <div class="report_buttons">
                                    <a class="Repeat" href="#">Refund</a>
                                    <!-- <a class="Request_to_Cancel" href="#">Request to Cancel</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shipping_details_columns">
                    <div class="left">
                        <div class="recently_view shipping_details">
                            <h2>Order #1589 Details</h2>
                            <h3>General </h3>
                            <div class="invoice_details_row">
                                <span class="text_left">Date Created:</span>
                                <span class="text_right">19/02/2022</span>
                            </div>
                            <div class="invoice_details_row">
                                <span class="text_left">Status:</span>
                                <span class="text_right">Shipped</span>
                            </div>
                            <h3>Ship To</h3>
                            <div class="invoice_details_row">
                                <span class="text_left">Address:</span>
                                <span class="text_right">Menlo Park, California, United States</span>
                            </div>
                            <div class="invoice_details_row">
                                <span class="text_left">E-mail:</span>
                                <span class="text_right">example@gmail.com</span>
                            </div>
                            <div class="invoice_details_row">
                                <span class="text_left">Phone:</span>
                                <span class="text_right">+1 123 456 45 78</span>
                            </div>
                        </div>
                        <div class="recently_view shipping_details_shop">
                            <h2>Shop</h2>
                            <div class="shipping_details_shop_box">
                                <div class="image">
                                    <img src="/assets/img/Ellipse 63.png">
                                </div>
                                <div class="content">
                                    <h3>Shop Name</h3>
                                    <p>@shopname</p>
                                </div>
                                <span class="view_more">
                                    <i class="fas fa-ellipsis-v"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="right">
                        <div class="recently_view shipping_details_order_action">
                            <h2>Order Action</h2>
                            <form>
                                <div class="input_box">
                                    <label>Action</label>
                                    <select>
                                        <option>Choose Action</option>
                                    </select>
                                </div>
                                <button>Apply</button>
                            </form>
                        </div>
                        <div class="recently_view shipping_details_order_notes">
                            <h2>Order Notes</h2>
                            <div class="previous_notes">
                                <div class="previous_note_box">
                                    <div class="content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                    </div>
                                    <div class="date">
                                        <span>19/02/2022</span><a href="#">Delete Note</a>
                                    </div>
                                </div>
                                <div class="previous_note_box">
                                    <div class="content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                    </div>
                                    <div class="date">
                                        <span>19/02/2022</span><a href="#">Delete Note</a>
                                    </div>
                                </div>
                            </div>
                            <form>
                                <div class="input_box">
                                    <label>Add Note</label>
                                    <textarea placeholder="Write Note"></textarea>
                                </div>
                                <button>Add</button>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>