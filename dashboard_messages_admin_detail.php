<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard_seller1.php"  ><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/dashboard_products.php"><i class="fas fa-boxes"></i> Products</a></li>
                        <li><a href="/dashboard_orders_seller.php"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_withdraw.php"><i class="fas fa-hand-holding-usd"></i> Withdraw</a></li>
                        <li><a href="/dashboard_messages_seller.php" ><i class="fas fa-comment-dots"></i> Messages</a></li>
                        <li><a href="/dashboard_disputes_seller.php"  class="active"><i class="fas fa-people-carry"></i> Disputes</a></li>
                        <li><a href="/support_seller.php" class=""><i class="fas fa-headset"></i> Support</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <h1>Hi, John Doe 👋</h1>
                        <p>Good Morning, Have a nice day.</p>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                
                <div class="chat_wrapper forum_chat dispute_chat">
                    
                    <div class="chat_wrapper_inner">
                        
                        <div class="chat_box">
                            <div class="chat_with"><span class="backtochats" onclick="location.href='http://imboo.sialkotians.com/dashboard_disputes_admin.php'"><i class="fas fa-angle-left"></i> Back</span>Disputes</div>
                                <h3>Andrew</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam maximus turpis sed nisl sollicitudin, vitae consequat metus placerat. Aenean semper mauris non neque convallis, non imperdiet nisl feugiat. Donec mollis malesuada nibh in aliquam. Vivamus bibendum vitae neque a egestas.</p>
                                <div class="dispute_chat_outer">
                                    <div class="chat_messages">
                                    <div class="message_wrapper">
                                        <div class="message you">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message_wrapper">
                                        <div class="message">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message_wrapper">
                                        <div class="message you">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message_wrapper">
                                        <div class="message">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message_wrapper">
                                        <div class="message you">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message_wrapper">
                                        <div class="message">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message_wrapper">
                                        <div class="message you">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message_wrapper">
                                        <div class="message">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message_wrapper">
                                        <div class="message you">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message_wrapper">
                                        <div class="message">
                                            <div class="image">
                                                <img src="/assets/img/Ellipse 63.png">
                                            </div>
                                            <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor mollis leo proin turpis eu hac. Tortor dolor eu at bibendum suspendisse. Feugiat mi eu, rhoncus diam consectetur libero morbi pharetra. Id tristique mi eget eget tristique orci.
                                                <div class="time">12:15 pm</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat_form">
                                    <textarea placeholder="Write a message..."></textarea>
                                    <div class="form_options">
                                        <span class="attchement"><i class="fas fa-paperclip"></i></span>
                                        <span class="smile"><i class="far fa-smile"></i></span>
                                        <span class="send"><i class="far fa-paper-plane"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>