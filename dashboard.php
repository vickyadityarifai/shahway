<!DOCTYPE html>
<html> 
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="/"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard.php"  class="active"><i class="fas fa-home"></i> Home</a></li>
                        <li><a href="/wishlist.php" ><i class="fas fa-heart"></i> Wishlists</a></li>
                        <li><a href="/dashboard_orders.php"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/dashboard_messages.php" class=""><i class="fas fa-comment-dots"></i> Messages</a></li>
                        <li><a href="/dashboard_disputes.php"><i class="fas fa-people-carry"></i> Disputes</a></li>
                        <li><a href="/support.php" class=""><i class="fas fa-headset"></i> Support</a></li>
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <input type="text" placeholder="Search Here">
                        <button><i class="fas fa-search"></i></button>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                <div class="profile_box buyer_dashboard saved_view active">
                    <a href="#" class="buyer_edit_profile"><span><i class="fas fa-pencil-alt"></i></span> Edit Profile</a>
                    <div class="left">
                        <div class="image">
                            <img src="/assets/img/Ellipse 63.png">
                        </div>
                    </div>
                    <div class="right">
                        <p><strong>User Name: </strong>Dummy name</p>
                        <p><strong>Email: </strong>info@company.com</p>
                        <p><strong>Phone Num: </strong>+0033-xxxxxxxxxxxx</p>
                        <p><strong>address: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </div>
                <div class="profile_box buyer_dashboard edit_view">
                    <a href="#" class="buyer_save_profile">Save Profile</a>
                    <div class="left">
                        <div class="image">
                            <img src="/assets/img/Ellipse 63.png">
                        </div>
                    </div>
                    <div class="right">
                        <p><strong>User Name: </strong><input type="text" value="Dummy name"></p>
                        <p><strong>Email: </strong><input type="text" value="info@company.com"></p>
                        <p><strong>Phone Num: </strong><input type="text" value="+0033-xxxxxxxxxxxx"></p>
                        <p><strong>address: </strong><input type="text" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit."></p>
                    </div>
                </div>
                <div class="recently_view">
                    <h2>
                        Recently Viewed
                        <div class="slider_arrows">
                            <span class="slider1_arrow left"><i class="fas fa-angle-left"></i></span>
                            <span class="slider1_arrow right"><i class="fas fa-angle-right"></i></span>
                        </div>
                    </h2>
                    <div class="products_slider slider1">
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 246.png')">
                                    <a href="#">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="#">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 266.png')">
                                    <a href="#">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="#">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 261.png')">
                                    <a href="#">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="#">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="#">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="#">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                            <div class="product_box">
                                <div class="wishlist_cart">
                                    <span><i class="far fa-heart"></i></span>
                                    <span><i class="fas fa-shopping-cart"></i></span>
                                </div>
                                <div class="image" style="background-image: url('/assets/img/Rectangle 262.png')">
                                    <a href="#">
                                        
                                    </a>
                                </div>
                                <div class="details">
                                    <div class="name">
                                        <a href="#">
                                            Product name
                                        </a>
                                        <div class="rating">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>
                                    <p class="price">$66,66</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>