<!DOCTYPE html>
<html>
    <head>
        <title>Imboo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/dashboard_responsive.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar">
                <span class="close_side_bar"><i class="fas fa-times"></i></span>
                <div class="logo">
                    <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                </div>
                <div class="menu">
                    <ul>
                        <li><a href="/dashboard_seller.php" class="active"><i class="fas fa-home"></i> Home</a></li>
                        
                        <li><a href="/dashboard_products.php" ><i class="fas fa-boxes"></i> Products</a></li>
                        <li><a href="/dashboard_orders.php"><i class="fas fa-list-alt"></i> Orders</a></li>
                        <li><a href="/support.php"><i class="fas fa-headset"></i> Support</a></li>
                        <li><a href="/chat.php" ><i class="fas fa-comment-dots"></i> Messages</a></li>
                        
                    </ul>
                </div>
            </div>
            <div class="page_wrapper">
                <div class="header">
                    <div class="for_mobile">
                        <div class="logo">
                            <a href="#"><img src="/assets/img/imboo (1) 1.png"></a>
                        </div>
                        <div class="show_side_bar">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <div class="show_side_search">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <form>
                        <h1>Hi, John Doe 👋</h1>
                        <p>Good Morning, Have a nice day.</p>
                    </form>
                    <div class="notification">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="user">
                        <a href="#">
                            <img src="/assets/img/Ellipse 63.png">
                        </a>
                    </div>
                </div>
                <div class="profile_box">
                    <a href="#"><span><i class="fas fa-pencil-alt"></i></span> Edit Profile</a>
                    <div class="left">
                        <div class="image">
                            <img src="/assets/img/Ellipse 63.png">
                        </div>
                    </div>
                    <div class="right">
                        <p><strong>User Name: </strong>Dummy name</p>
                        <p><strong>Email: </strong>info@company.com</p>
                        <p><strong>Phone Num: </strong>+0033-xxxxxxxxxxxx</p>
                        <p><strong>address: </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </div>
                <div class="recently_view">
                    <h2>
                        Recent Orders
                        <div class="slider_arrows">
                            <span class="slider5_arrow left"><i class="fas fa-angle-left"></i></span>
                            <span class="slider5_arrow right"><i class="fas fa-angle-right"></i></span>
                        </div>
                    </h2>
                    <div class="products_slider slider5">
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Completed">(Completed)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="#">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Processing">(Processing)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="#">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Canceled">(Canceled)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="#">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Completed">(Completed)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="#">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Completed">(Completed)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="#">View Detail</a>
                        </div>   
                        <div class="order_slider_box">
                            <div class="top_borders">
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                                <span class="b1"></span>
                                <span class="b2"></span>
                            </div>
                            <h2>#15633</h2>
                            <p><span class="Completed">(Completed)</span></p>
                            <div class="line_break"></div>
                            <h3>Customer Name </h3>
                            <h4>$744,55</h4>
                            <div class="line_break"></div>
                            <a href="#">View Detail</a>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script src="/assets/dashboard_custom.js?<?php echo time(); ?>"></script>
    </body>
</html>