<?php
include('database_connection.php');
?>
<?php
$active_page = 'dashboard';

?>
<?php
include('dashboard_header.php');



?>   

                <div class="total_users_products">
                    <style type="text/css">
                        .fullfilled_by_filter {
  padding: 15px 0;
}
.fullfilled_by_filter li {
  margin: 0 20px 0 0;
}
.fullfilled_by_filter li a {
  color: #000;
}
.fullfilled_by_filter li a i {
  display: none;
}
.fullfilled_by_filter li a span {
  width: 20px;
  height: 20px;
  display: inline-block;
  margin: 0 5px -4px 0;
  background-color: #ddd;
  text-align: center;
font-size: 12px;
}
.fullfilled_by_filter li a .show_icon i {
  display: table;
  margin: 4px 0px 0px 4px;
}
                    </style>
                    <div class="fullfilled_by_filter">
                        <ul>
                            <?php

                            if(isset($_GET['fullfilled_by_filter'])){

                              if($_GET['fullfilled_by_filter']=='SellerFulfilled'){
                                if(isset($_SESSION['fullfilled_by_filter_seller'])){
                                  unset($_SESSION['fullfilled_by_filter_seller']);
                                }else{
                                  $_SESSION['fullfilled_by_filter_seller'] = $_GET['fullfilled_by_filter'];
                                }
                                
                              }
                              if($_GET['fullfilled_by_filter']=='WFSFulfilled'){
                                if(isset($_SESSION['fullfilled_by_filter_wfs'])){
                                  unset($_SESSION['fullfilled_by_filter_wfs']);
                                }else{
                                  $_SESSION['fullfilled_by_filter_wfs'] = $_GET['fullfilled_by_filter'];
                                }
                                
                              }

                            }
                            $fullfilled_by_filter = array();
                            if(isset($_SESSION['fullfilled_by_filter_seller'])){
                                $fullfilled_by_filter[] = $_SESSION['fullfilled_by_filter_seller'];
                            }
                            if(isset($_SESSION['fullfilled_by_filter_wfs'])){
                                $fullfilled_by_filter[] = $_SESSION['fullfilled_by_filter_wfs'];
                            }
                            if(empty($fullfilled_by_filter)){
                              $fullfilled_by_filter[]='SellerFulfilled';
                              $fullfilled_by_filter[]='WFSFulfilled';
                              $_SESSION['fullfilled_by_filter_seller'] = 'SellerFulfilled';
                              $_SESSION['fullfilled_by_filter_wfs'] = 'WFSFulfilled';
                            }
                            ?>
                            <?php
                            $icon_class='';
                            if(in_array('SellerFulfilled', $fullfilled_by_filter)){
                                $icon_class = 'show_icon';
                            }
                            ?>
                            <li><a href="./dashboard_admin.php?fullfilled_by_filter=SellerFulfilled"><span class="<?php echo $icon_class; ?>"><i class="fas fa-check"></i></span>Seller Fulfilled</a></li>
                            <?php
                            $icon_class='';
                            if(in_array('WFSFulfilled', $fullfilled_by_filter)){
                                $icon_class = 'show_icon';
                            }
                            ?>
                            <li><a href="./dashboard_admin.php?fullfilled_by_filter=WFSFulfilled"><span class="<?php echo $icon_class; ?>"><i class="fas fa-check"></i></span>WFS Fulfilled</a></li>
                        </ul>
                    </div>
                    <?php /*
                    <div class="recently_view total_users_products">
                        <div class="total_users_products_inner">
                            <div class="icon_box">
                                <i class="fas fa-boxes"></i>
                            </div>
                            <div class="content">
                                <h2>Total Orders</h2>
                                <h1>
                                    <?php
                                    $get_orders_count = "SELECT * FROM orders";
                                    $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                                    echo mysqli_num_rows($get_orders_count_query);
                                    ?>
                                </h1>
                            </div>
                            <a class="Manage" href="/dashboard_orders_admin.php">Manage</a>
                        </div>
                    </div>
                    <div class="recently_view total_users_products">
                        <div class="total_users_products_inner">
                            <div class="icon_box">
                                <i class="fas fa-hand-holding-usd"></i>
                            </div>
                            <div class="content">
                                <h2>Product Prices</h2>
                                <h1>
                                    <?php
                                    $get_orders_count = "SELECT * FROM product_prices";
                                    $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                                    echo mysqli_num_rows($get_orders_count_query);
                                    ?>
                                </h1>
                            </div>
                            <a class="Manage" href="/dashboard_product_price.php">Manage</a>
                        </div>
                    </div> */ ?>
                </div>
                <style>
                    .monthly_reports {
  width: 99%;
  display: flex;
}
.report_box {
  display: inline-block;
  position: relative;
  margin: 10px 5px;
  background: #FFFFFF;
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-sizing: border-box;
  border-radius: 8px;
  width: 100%;
  overflow: hidden;
  color: #fff;
}
.report_box:nth-child(1) {
  background: #00AA9E;
}
.report_box:nth-child(1) .report_box_header {
  display: inline-block;
  width: 90%;
  padding: 15px 5%;
  background: #009E93;
}
.report_box:nth-child(2) {
  background: #0C9;
}
.report_box:nth-child(2) .report_box_header {
  display: inline-block;
  width: 90%;
  padding: 15px 5%;
  background: #00BA8B;
}
.report_box:nth-child(3) {
  background: #69F;
}
.report_box:nth-child(3) .report_box_header {
  display: inline-block;
  width: 90%;
  padding: 15px 5%;
  background: #508BFF;
}
.report_box:nth-child(4) {
  background: #996;
}
.report_box:nth-child(4) .report_box_header {
  display: inline-block;
  width: 90%;
  padding: 15px 5%;
  background: #87875a;
}
.report_box_header h2 {
  display: block;
  font-size: 14px;
}
.report_box_header h3 {
  font-size: 13px;
  font-weight: 400;
  margin: 5px 0 0 0;
}
.reports_bottom {
  width: 90%;
  padding: 10px 5%;
  background-color: #fff;
  color: #000;
}
.reports_bottom p {
  font-size: 14px;
  display: inline-block;
  width: 100%;
  margin: 5px 0;
}
.reports_bottom p .left {
  float: left;
}
.reports_bottom p .right {
  float: right;
}

.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
                </style>
                <?php
                $date_range = date('Y-m-d');
                
                ?>
                <div class="monthly_reports">
                     <div class="loader"></div> 
                </div>
                <style>




    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}

/*.table_list_box{
    display: inline-block;
}*/
.table_list_outer.orders_list .table_list_box .box:nth-child(1) {
  text-align: left;
  padding: 0 0 0 0;
  width: 3%;
}
.table_list_box .box {
  width: 8%;
  display: inline-block;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(2) {
  width: 12%;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(3) {
  width: 23%;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(4) {
  width: 5%;
}
.table_list_outer.orders_list .table_list_box .box:nth-child(5) {
  width: 5%;
}

.table_list_box.table_list_heading{
    font-size: 11px;
}
.table_list_box{
    font-size: 11px;
}
.orders_list.box_products{
    display: none;
}
.orders_list.box_orders{
    display: none;
}
.orders_list.active{
    display: block;
}
.zee_tabs {
  display: flex;
  width: 100%;
  margin: 0 0 20px 0;
}
.zee_tabs .tab_button {
  width: 100%;
  cursor: pointer;
  text-align: center;
  font-weight: 700;
  background-color: #f2f2f2;
  padding: 15px 0;
}
.zee_tabs .tab_button.active {
  background-color: #69F;
  color: #fff;
}






/*for products tab*/
.orders_list.box_products .table_list_outer.orders_list .table_list_box .box:nth-child(2) {
  width: 30%;
  max-height: 30px;
  overflow: hidden;
}
.orders_list.box_products .table_list_outer.orders_list .table_list_box .box:nth-child(3) {
  width: 8%;
}
.orders_list.box_products .table_list_box {
  padding: 5px 0;
}
</style>
                
                <div class="recently_view">
                    <div class="zee_tabs">
                        <?php
                        $active_tab = '';
                        if(isset($_GET['show_tab']) && $_GET['show_tab']=='products'){
                            $active_tab = 'active';
                        }else{
                            if(!isset($_GET['show_tab'])){
                                $active_tab = 'active';
                            }
                        }
                        ?>
                        <div class="tab_button tab_products <?php echo $active_tab; ?>">Products</div>
                        <?php
                        $active_tab = '';
                        if(isset($_GET['show_tab'])){
                            if($_GET['show_tab']=='orders'){
                                $active_tab = 'active';
                            }
                        }
                        ?>
                        <div class="tab_button tab_orders <?php echo $active_tab; ?>">Orders</div>
                        
                    </div>
                    <?php
                    $active_tab = '';
                    if(isset($_GET['show_tab'])){
                        if($_GET['show_tab']=='orders'){
                            $active_tab = 'active';
                        }
                    }

                    if(isset($_GET['set_date'])){
                        $date_range = $_GET['set_date'];
                    }else{
                        $date_range = date('Y-m-d').'_'.date('Y-m-d');
                    }

                    $section_title = 'Todays Orders';
                    if(!empty($date_range)){
                        $date_rangea = explode ("_", $date_range);
                        if($date_rangea[0]==$date_rangea[1]){
                            $section_title = 'Orders '.date("m-d-Y", strtotime($date_rangea[0]));
                        }else{
                            $section_title = 'Orders '.date("m-d-Y", strtotime($date_rangea[0])).' to '.date("m-d-Y", strtotime($date_rangea[1]));
                        }
                    }

                    ?>
                    <div class="orders_list box_orders <?php echo $active_tab; ?>">
                        <h2><?php echo $section_title; ?></h2>
                        <form class="filter_orders" method="get">
                            <input type="hidden" name="set_date" value="<?php echo $date_range; ?>">
                            <input type="hidden" name="pageno" value="<?php echo $pageno; ?>">
                            <div class="input_box">
                                <label>Order ID</label>
                                <?php
                                $order_id = '';
                                if(isset($_GET['order_id'])){
                                    $order_id = $_GET['order_id'];
                                }
                                ?>
                                <input type="text" name="order_id" placeholder="Enter Order ID" value="<?php echo $order_id; ?>">
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                            <!-- <div class="export_to_csv">
                                <a href="/dashboard_orders_admin.php?export_to_csv">Export</a>

                            </div> -->
                        </form>
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Customer Order ID
                                </div>
                                <div class="box">
                                    Order Items
                                </div>
                                <div class="box">
                                    Quantity
                                </div>
                                <!-- <div class="box">
                                    Refund
                                </div> -->
                                <div class="box">
                                    Charge Amount
                                </div>
                                <div class="box">
                                    Shipping customer
                                </div>
                                <div class="box">
                                    Shipping charges
                                </div>
                                <div class="box">
                                    Product Price
                                </div>
                                <div class="box">
                                    Tax
                                </div>
                                <div class="box">
                                    Walmart Fees
                                </div>
                                <div class="box">
                                    Shipping Cost
                                </div>
                                
                                <div class="box">
                                    Profit
                                </div>
                            </div>

                            <?php
                            if (isset($_GET['pageno'])) {
                                $pageno = $_GET['pageno'];
                            } else {
                                $pageno = 1;
                            }
                            ?>
                            
                            <?php


                            

                            $no_of_records_per_page = 30;
                            $offset = ($pageno-1) * $no_of_records_per_page; 
                            
                            
                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0])* 1000;
                                $date_end = strtotime($date_range[1])* 1000;
                                if($date_range[0]==$date_range[1]){
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;
                                    $filter_code .= " WHERE orderDate > " . $date_start . " AND orderDate < " . $date_end . "";
                                }else{
                                  $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;
                                    $filter_code .= " WHERE orderDate >= " . $date_start . " AND orderDate <= " . $date_end . "";
                                }
                                
                            }
                            
                            if(isset($_GET['order_id']) && !empty($_GET['order_id'])){
                                $filter_code .= " AND customerOrderId = '".$_GET['order_id']."'";
                            }

                            $filter_code .= " AND shipNode IN('".implode("','",$fullfilled_by_filter)."')";
                            

                            $get_orders_count = "SELECT * FROM orders $filter_code ORDER BY id desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);
                            

                            $get_orders = "SELECT * FROM orders $filter_code ORDER BY id desc LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);



                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_id = $order['id'];
                                    $refund = 0;
                                    $product_price = 0;
                                    $walmart_fees = 0;
                                    $total_shipping_cost = 0;
                                    $customer_shipping_cost =0;
                                    $customer_shipping_cost_tax = 0;

                                    $CommissionRate = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Commission on Product'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $CommissionRate = $row_reconciliationreport['Commission_Rate'];
                                    }

                                    $Fee_Reimbursement = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Fee/Reimbursement'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $Fee_Reimbursement = $row_reconciliationreport['Amount'];
                                    }

                                    $Fee_Reimbursement = preg_replace("/[^0-9.]/","",$Fee_Reimbursement);



                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <a class="order_detail_page_link" href="./dashboard_add_product_admin.php?order_id=<?php echo $order_id; ?>"><?php echo $order['customerOrderId']; ?></a>
                                    <div class="fulfilled_by <?php echo $order['shipNode']; ?>"><span><?php echo $order['shipNode']; ?></span></div>
                                    <?php 
                                    $mil = $order['orderDate'];
                                    $seconds = ceil($mil / 1000);
                                    echo date("m-d-Y H:i", $seconds);
                                     ?>
                                </div>
                                <div class="box">
                                    <?php
                                    $total_qty = 0;
                                    $chargeAmount_currency = '';
                                    $taxAmount_currency = '';
                                    $chargeAmount_amount = 0;
                                    $taxAmount_amount = 0;
                                    $get_orderlines = "SELECT * FROM orderlines WHERE order_id=".$order_id;
                                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                                    if(mysqli_num_rows($get_orderlines_query) > 0){
                                    $kk=0;
                                    $o_items_aku = array();
                                    while($orderline = $get_orderlines_query->fetch_assoc()) {
                                        $kk++;
                                        $total_qty = $total_qty+$orderline['orderLineQuantity_amount'];
                                        $chargeAmount_currency = $orderline['chargeAmount_currency'];
                                        $taxAmount_currency = $orderline['taxAmount_currency'];
                                        $chargeAmount_amount = $chargeAmount_amount+floatval($orderline['chargeAmount_amount']);
                                        $taxAmount_amount = $taxAmount_amount+floatval($orderline['taxAmount_amount']);




                                        $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
        
                                        $product_prices = mysqli_query($conn, $product_prices);
                                        $row_product_prices = $product_prices->fetch_assoc();
                                        if(!empty($row_product_prices)){
                                            $product_price = $product_price+floatval($row_product_prices['cost']);
                                        }


                                        


                                        if(!empty($orderline['shipping_cost'])){
                                            $this_item_shipping_cost1 = floatval($orderline['shipping_cost'])+floatval($orderline['shipping_tax']);

                                            $customer_shipping_cost = $customer_shipping_cost+floatval($orderline['shipping_cost']);
                                            $customer_shipping_cost_tax = $customer_shipping_cost_tax+floatval($orderline['shipping_tax']);
                                        }else{
                                            $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
        
                                            $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                            $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                            if(!empty($row_shipping_cost)){
                                                $total_shipping_cost = $total_shipping_cost+floatval($row_shipping_cost['cost']);
                                            }
                                        }
                                        
                                        
                                        


                                        if (in_array($orderline['sku'], $o_items_aku)){
                                        }else{
                                            ?>
                                            <div class="order_item_box">


                                                <?php
                                                $sku = $orderline['sku'];
                                                $walmart_item_link = 'link_not_found';
                                                $link_found = '';
                                                $get_walmart_items = "SELECT * FROM walmart_items WHERE sku='".$sku."'"; 
                                                $walmart_items = mysqli_query($conn, $get_walmart_items);
                                                $row_walmart_items = $walmart_items->fetch_assoc();
                                                if(!empty($row_walmart_items)){
                                                    $walmart_item_link = $row_walmart_items['wpid'];
                                                    $link_found = 'link_found';
                                                }
                                                
                                                ?>
                                                <a class="<?php echo $link_found; ?>" href="https://www.walmart.com/ip/<?php echo $walmart_item_link; ?>" target="_blank">
                                                    <p>#<?php echo $kk.'. '.$orderline['productName']; ?></p>
                                                    <p>Sku. <?php echo $orderline['sku']; ?></p>
                                                </a>


                                                
                                                <p>Tracking#. <?php echo $orderline['trackingNumber']; ?></p>
                                            </div>
                                            <?php
                                        }
                                        $o_items_aku[] = $orderline['sku'];

                                    } }
                                    ?>
                                </div>
                                <div class="box">
                                    <?php echo $total_qty;
                                     ?>
                                </div>
                                <!-- <div class="box"> -->
                                    <?php //echo number_format($refund, 2); ?>
                                <!-- </div> -->
                                <div class="box">
                                    $<?php echo number_format($chargeAmount_amount, 2);
                                     ?>
                                </div>
                                <div class="box">
                                    $<?php echo number_format($customer_shipping_cost, 2);
                                     ?>
                                </div>
                                <div class="box">
                                    $<?php echo number_format($customer_shipping_cost_tax, 2);
                                     ?>
                                </div>
                                <div class="box negative">
                                    $<?php echo number_format($product_price, 2);
                                     ?>
                                </div>
                                <div class="box">
                                    $<?php echo number_format($taxAmount_amount, 2); 
                                    ?>
                                </div>
                                <div class="box negative">
                                    $<?php 
                                    $wl_mart_fees = $chargeAmount_amount;//+$taxAmount_amount+$customer_shipping_cost;
                                    $walmart_fees = $wl_mart_fees/100*$CommissionRate;
                                    echo number_format($walmart_fees,2); ?>
                                </div>
                                <div class="box negative">
                                    $<?php 
                                    $total_shipping_cost = number_format($Fee_Reimbursement*$total_qty, 2);
                                    echo $total_shipping_cost;
                                    
                                     ?>
                                </div>
                                <?php
                                $profit_class = 'positive';
                                $profit = $chargeAmount_amount-$total_shipping_cost-$walmart_fees-$product_price-$refund;
                                if($profit<0){
                                    $profit_class = 'negative';
                                }
                                ?>
                                <div class="box <?php echo $profit_class; ?>">
                                    $<?php
                                    
                                    echo number_format($profit,2);
                                    ?>
                                </div>
                            </div>
                            <?php  



                        } } 


                        ?>
                            
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                if(isset($_GET['set_date'])){
                                    $other_link = '&set_date='.$_GET['set_date'];
                                }

                                if(isset($_GET['order_id'])){
                                    $other_link.= '&order_id='.$_GET['order_id'];
                                }


                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="./dashboard_admin.php?pageno=<?php echo $pageno-1; ?>&show_tab=orders&<?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="./dashboard_admin.php?pageno=<?php echo $i; ?>&show_tab=orders&<?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="./dashboard_admin.php?pageno=<?php echo $pageno+1; ?>&show_tab=orders&<?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }


                                ?>
                            </ul>
                        </div>
                    </div>
                    <?php
                    $active_tab = '';
                    if(isset($_GET['show_tab']) && $_GET['show_tab']=='products'){
                        $active_tab = 'active';
                    }else{
                        if(!isset($_GET['show_tab'])){
                            $active_tab = 'active';
                        }
                        
                    }
                    if(isset($_GET['set_date'])){
                        $date_range = $_GET['set_date'];
                    }else{
                        $date_range = date('Y-m-d').'_'.date('Y-m-d');
                    }

                    $section_title = 'Todays Products';
                    if(!empty($date_range)){
                        $date_rangea = explode ("_", $date_range);
                        if($date_rangea[0]==$date_rangea[1]){
                            $section_title = 'Products '.date("m-d-Y", strtotime($date_rangea[0]));
                        }else{
                            $section_title = 'Products '.date("m-d-Y", strtotime($date_rangea[0])).' to '.date("m-d-Y", strtotime($date_rangea[1]));
                        }
                    }

                    ?>
                    <?php
                    if (isset($_GET['pagenopro'])) {
                        $pagenopro = $_GET['pagenopro'];
                    } else {
                        $pagenopro = 1;
                    }
                    ?>
                    <div class="orders_list box_products <?php echo $active_tab; ?>">
                        <h2><?php echo $section_title; ?></h2>
                        <form class="filter_orders" method="get">
                            <input type="hidden" name="set_date" value="<?php echo $date_range; ?>">
                            <input type="hidden" name="pagenopro" value="<?php echo $pagenopro; ?>">
                            <div class="input_box"> 
                                <label>Status</label>
                                <select name="status">
                                    <option value="">Select Fullfilled</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']=="SellerFulfilled"){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="SellerFulfilled" <?php echo $selected; ?>>SellerFulfilled</option>
                                    <?php
                                    $selected = '';
                                    if(isset($_GET['status']) && $_GET['status']=="WFSFulfilled"){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="WFSFulfilled" <?php echo $selected; ?>>WFSFulfilled</option>
                                </select>
                            </div>
                            <div class="input_box">
                                <label>SKU</label>
                                <?php
                                $search_sku = '';
                                if(isset($_GET['sku'])){
                                    $search_sku = $_GET['sku'];
                                }
                                ?>
                                <input type="text" name="sku" placeholder="Enter SKU" value="<?php echo $search_sku; ?>">
                            </div>
                            <div class="input_box">
                                <label>Product Name</label>
                                <?php
                                $search_product_name = '';
                                if(isset($_GET['product_name'])){
                                    $search_product_name = $_GET['product_name'];
                                }
                                ?>
                                <input type="text" name="product_name" placeholder="Enter Product Name" value="<?php echo $search_product_name; ?>">
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Item SKU
                                </div>
                                <div class="box">
                                    Quantity
                                </div>
                                <div class="box">
                                    Charge Amount
                                </div>
                                <div class="box">
                                    Shipping customer
                                </div>
                                <div class="box">
                                    Shipping charges
                                </div>
                                <div class="box">
                                    Product Price
                                </div>
                                <div class="box">
                                    Tax
                                </div>
                                <div class="box">
                                    Walmart Fees
                                </div>
                                <div class="box">
                                    Shipping Cost
                                </div>
                                <div class="box">
                                    Profit
                                </div>
                            </div>

                            
                            
                            <?php


                            $no_of_records_per_page = 30;
                            $offset = ($pagenopro-1) * $no_of_records_per_page; 


                            $filter_code = '';
                            if(!empty($date_range)){
                                $date_range = explode ("_", $date_range);
                                $date_start = strtotime($date_range[0])* 1000;
                                $date_end = strtotime($date_range[1])* 1000;
                                if($date_range[0]==$date_range[1]){
                                    $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;

                                    $filter_code .= " WHERE orders.orderDate > " . $date_start . " AND orders.orderDate < " . $date_end . "";
                                }else{
                                  $date_start = strtotime("".$date_range[0]." 12:00 AM")* 1000;
                                    $date_end = strtotime("".$date_range[1]." 11:59 PM")* 1000;
                                    $filter_code .= " WHERE orders.orderDate >= " . $date_start . " AND orders.orderDate <= " . $date_end . "";
                                }
                                
                            }

                            if(!empty($search_sku)){
                                $filter_code.= ' AND sku="'.$search_sku.'"';
                            }
                            if(!empty($search_product_name)){
                                $filter_code .= " AND productName LIKE '%".$search_product_name."%'";
                            }

                            $filter_code .= " AND orders.shipNode IN('".implode("','",$fullfilled_by_filter)."')";

                            $get_orders_count = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id $filter_code GROUP BY sku ORDER BY orderlines.id desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);
                            

                            $get_orders = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id $filter_code GROUP BY sku ORDER BY orderlines.id desc LIMIT $offset, $no_of_records_per_page;";
                            $get_orders_query = mysqli_query($conn, $get_orders);


                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_sku = $order['sku'];
                                    $order_sku_name = $order['productName'];



                                    $refund = 0;
                                    $product_price = 0;
                                    $walmart_fees = 0;
                                    $total_shipping_cost = 0;
                                    $customer_shipping_cost =0;
                                    $customer_shipping_cost_tax = 0;




                                    $total_qty = 0;
                                    $chargeAmount_currency = '';
                                    $taxAmount_currency = '';
                                    $chargeAmount_amount = 0;
                                    $taxAmount_amount = 0;
                                    $ssssku = '';


                                    $CommissionRate = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Commission on Product'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $CommissionRate = $row_reconciliationreport['Commission_Rate'];
                                    }

                                    $Fee_Reimbursement = 0;
                                    // reconciliationreport
                                    $reconciliationreport  = "SELECT * FROM reconciliationreport WHERE Customer_Order='".$order['customerOrderId']."' AND Amount_Type='Fee/Reimbursement'";
    
                                    $reconciliationreport = mysqli_query($conn, $reconciliationreport);
                                    $row_reconciliationreport = $reconciliationreport->fetch_assoc();
                                    if(!empty($row_reconciliationreport)){
                                        $Fee_Reimbursement = $row_reconciliationreport['Amount'];
                                    }

                                    $Fee_Reimbursement = preg_replace("/[^0-9.]/","",$Fee_Reimbursement);



                                    $get_orderlines = "SELECT * FROM orderlines JOIN orders ON orderlines.order_id=orders.id $filter_code AND sku='".$order_sku."'";
                                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                                    if(mysqli_num_rows($get_orderlines_query) > 0){
                                        $kk=0;
                                        while($orderline = $get_orderlines_query->fetch_assoc()) {
                                            $kk++;

                                            $total_qty = $total_qty+$orderline['orderLineQuantity_amount'];
                                            $chargeAmount_currency = $orderline['chargeAmount_currency'];
                                            $taxAmount_currency = $orderline['taxAmount_currency'];
                                            $chargeAmount_amount = $chargeAmount_amount+floatval($orderline['chargeAmount_amount']);
                                            $taxAmount_amount = $taxAmount_amount+floatval($orderline['taxAmount_amount']);

                                            $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
        
                                            $product_prices = mysqli_query($conn, $product_prices);
                                            $row_product_prices = $product_prices->fetch_assoc();
                                            if(!empty($row_product_prices)){
                                                $product_price = $product_price+floatval($row_product_prices['cost']);
                                            }


                                            


                                            if(!empty($orderline['shipping_cost'])){
                                                $this_item_shipping_cost1 = floatval($orderline['shipping_cost'])+floatval($orderline['shipping_tax']);

                                                $customer_shipping_cost = $customer_shipping_cost+floatval($orderline['shipping_cost']);
                                                $customer_shipping_cost_tax = $customer_shipping_cost_tax+floatval($orderline['shipping_tax']);
                                            }else{
                                                $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
            
                                                $shipping_cost  = mysqli_query($conn, $shipping_cost );
                                                $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                                                if(!empty($row_shipping_cost)){
                                                    $total_shipping_cost = $total_shipping_cost+floatval($row_shipping_cost['cost']);
                                                }
                                            }
                                            


                                        }
                                    }

                                    $total_shipping_cost = number_format($Fee_Reimbursement*$total_qty, 2);


                                    $wl_mart_fees = $chargeAmount_amount;//+$taxAmount_amount+$customer_shipping_cost;
                                    $walmart_fees = $wl_mart_fees/100*$CommissionRate;

                                    $profit = $chargeAmount_amount-$total_shipping_cost-$walmart_fees-$product_price-$refund;

                                    
                                    ?>
                                    <div class="table_list_box">
                                        <div class="box">
                                            <?php echo $k+($no_of_records_per_page*($pagenopro-1)); ?>
                                        </div>
                                        <div class="box">
                                            <?php
                                                $sku = $order_sku;
                                                $walmart_item_link = 'link_not_found';
                                                $link_found = '';
                                                $get_walmart_items = "SELECT * FROM walmart_items WHERE sku='".$sku."'"; 
                                                $walmart_items = mysqli_query($conn, $get_walmart_items);
                                                $row_walmart_items = $walmart_items->fetch_assoc();
                                                if(!empty($row_walmart_items)){
                                                    $walmart_item_link = $row_walmart_items['wpid'];
                                                    $link_found = 'link_found';
                                                }
                                                
                                                ?>
                                                <a class="<?php echo $link_found; ?>" href="https://www.walmart.com/ip/<?php echo $walmart_item_link; ?>" target="_blank">
                                                    <?php echo $order_sku_name; ?>
                                                    <br>
                                                    <?php echo $order_sku; ?>
                                                </a>
                                            
                                        </div>
                                        <div class="box">
                                            <?php echo $total_qty; ?>
                                        </div>
                                        <div class="box">
                                            $<?php echo number_format($chargeAmount_amount, 2); ?>
                                        </div>
                                        <div class="box">
                                            $<?php echo number_format($customer_shipping_cost, 2); ?>
                                        </div>
                                        <div class="box">
                                            $<?php echo number_format($customer_shipping_cost_tax, 2);
                                             ?>
                                        </div>
                                        <div class="box negative">
                                            $<?php echo number_format($product_price, 2); ?>
                                        </div>
                                        <div class="box">
                                            $<?php echo number_format($taxAmount_amount, 2); ?>
                                        </div>
                                        <div class="box negative">
                                            $<?php 
                                            echo number_format($walmart_fees,2); ?>
                                        </div>
                                        <div class="box negative">
                                            $<?php echo number_format($total_shipping_cost, 2); ?>
                                        </div>
                                        
                                         <?php
                                        $profit_class = 'positive';
                                        if($profit<0){
                                            $profit_class = 'negative';
                                        }
                                        ?>
                                        <div class="box <?php echo $profit_class; ?>">
                                            $<?php
                                            echo number_format($profit,2);
                                            ?>
                                        </div>
                                    </div>
                                    <?php



                                } } ?>
                            
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                if(isset($_GET['set_date'])){
                                    $other_link = '&set_date='.$_GET['set_date'];
                                }

                                if(isset($_GET['sku'])){
                                    $other_link.= '&sku='.$_GET['sku'];
                                }
                                if(isset($_GET['product_name'])){
                                    $other_link.= '&product_name='.$_GET['product_name'];
                                }
                                if(isset($_GET['status'])){
                                    $other_link.= '&status='.$_GET['status'];
                                }


                                if($pagenopro>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="./dashboard_admin.php?pageno=<?php echo $pagenopro-1; ?>&show_tab=products&<?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pagenopro==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pagenopro-2;
                                    $next_2nbr = $pagenopro+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="./dashboard_admin.php?pagenopro=<?php echo $i; ?>&show_tab=products&<?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pagenopro<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="./dashboard_admin.php?pagenopro=<?php echo $pagenopro+1; ?>&show_tab=products&<?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }


                                ?>
                            </ul>
                        </div>
                    </div>
                </div>

<script>
    $(window).scroll(function() {
       if($(this).scrollTop() > 700) {
         $(".table_list_box.table_list_heading").addClass("fixed");
       }else{
        $(".table_list_box.table_list_heading").removeClass("fixed");
       }
    });
</script>
<?php

include('dashboard_footer.php');

?>