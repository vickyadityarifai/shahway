<?php
include('database_connection.php');
?>
<?php
$active_page = 'orders';
?>
<?php
include('dashboard_header.php');
$order_id = $_GET['order_id'];
$get_orders = "SELECT * FROM orders WHERE id=".$order_id;
$get_orders_query = mysqli_query($conn, $get_orders);
$order = $get_orders_query->fetch_assoc();
if(!empty($order)){
?>
<style>
    .fulfilled_by {
  display: inline-block;
}
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}

.product_info_top {
  display: inline-block;
  width: 98%;
  padding: 15px 1%;
  background-color: #f9f9f9;
  border-bottom: 1px solid #e2e2e2;
}
.product_info_top h3 {
  display: block;
  margin: 0 0 10px 0;
}
</style>
                <form method="post">
                    <input type="hidden" name="update_order" value="<?php echo $order_id; ?>">
                <div class="recently_view category_section add_product_page add_product_form">
                    <h2><a href="/dashboard_orders_admin.php"><i class="fas fa-arrow-left"></i></a> Order # <?php echo $order_id; ?> <div class="fulfilled_by <?php echo $order['shipNode']; ?>"><span><?php echo $order['shipNode']; ?></span></div></h2>
                    <div class="category_section_inner">
                        <div class="category_section_inner_box">
                            <div class="input_box">
                                <label>Customer Order ID</label>
                                <input type="text" placeholder="Customer Order ID" name="customer_order_id" value="<?php echo $order['customerOrderId']; ?>">
                            </div>
                        </div>
                        <div class="category_section_inner_box">
                            <div class="input_box">
                                <label>Date</label>
                                <?php
                                $mil = $order['orderDate'];
                                $seconds = ceil($mil / 1000);
                                
                                ?>
                                <input type="text" placeholder="Date" name="date" value="<?php echo date("d-m-Y", $seconds); ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="recently_view product_info add_product_page">
                    <h2>Product Info</h2>
                    <?php
                    $get_orderlines = "SELECT * FROM orderlines WHERE order_id=".$order_id;
                    $get_orderlines_query = mysqli_query($conn, $get_orderlines);
                    if(mysqli_num_rows($get_orderlines_query) > 0){
                    $kk=0;
                    while($orderline = $get_orderlines_query->fetch_assoc()) {
                        $kk++;
                        $product_price = 0;
                        $product_prices  = "SELECT * FROM product_prices WHERE sku='".$orderline['sku']."'";
                        $product_prices = mysqli_query($conn, $product_prices);
                        $row_product_prices = $product_prices->fetch_assoc();
                        if(!empty($row_product_prices)){
                            $product_price = floatval($row_product_prices['cost']);
                        }

                        $total_shipping_cost = 0;
                        $shipping_cost   = "SELECT * FROM shipping_cost  WHERE tracking_number='".$orderline['trackingNumber']."'";
                        $shipping_cost  = mysqli_query($conn, $shipping_cost );
                        $row_shipping_cost  = $shipping_cost ->fetch_assoc();
                        if(!empty($row_shipping_cost)){
                            $total_shipping_cost = floatval($row_shipping_cost['cost']);
                        }

                    ?>
                    <div class="product_info_top">
                        <h3>Item #<?php echo $kk; ?></h3>
                        <div class="product_info_top_left">
                            <div class="input_box">
                                <label>Product Name</label>
                                <input type="text" placeholder="Product Name" name="product_name[]" value="<?php echo $orderline['productName']; ?>">
                            </div>
                            <div class="input_box p_description">
                                <label>SKU</label>
                                <input type="text" placeholder="SKU" name="sku[]" value="<?php echo $orderline['sku']; ?>">
                            </div>
                            <div class="input_box">
                                <label>Product Price</label>
                                <span class="price_sign">$</span>
                                <input type="text" placeholder="Product Price" name="product_price[]" value="<?php echo $product_price ?>">
                            </div>
                        </div>
                        <div class="product_info_top_right">
                            <div class="input_box">
                                <label>Quantity</label>
                                <input type="text" placeholder="Quantity" name="quantity[]" value="<?php echo $orderline['orderLineQuantity_amount']; ?>">
                            </div>
                            <div class="input_box">
                                <label>Charge Amount</label>
                                <input type="text" placeholder="Charge Amount" name="charge_amount[]" value="<?php echo $orderline['chargeAmount_amount']; ?>">
                            </div>
                            <div class="input_box p_description">
                                <label>Tracking Number</label>
                                <input type="text" placeholder="Tracking Number" name="tracking_number[]" value="<?php echo $orderline['trackingNumber']; ?>">
                            </div>
                            <div class="input_box">
                                <label>Shipping Cost</label>
                                <span class="price_sign">$</span>
                                <input type="text" placeholder="Shipping Cost" name="shipping_cost[]" value="<?php echo $total_shipping_cost; ?>">
                            </div>
                        </div>
                        
                    </div>

                    <?php
                                    } }
                                    ?>
                    
                </div>
                
                <div class="recently_view submit_button">
                    <button>Submit</button>
                </div>
            </form>

<?php
}
include('dashboard_footer.php');

?>