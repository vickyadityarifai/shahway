<?php 
include('database_connection.php');
?>
<?php
$active_page = 'notifications';
?>
<?php
include('dashboard_header.php');
?>
<style>
    .fulfilled_by span {
  background-color: #0C9;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
  margin: 8px 0 0 0;
}
.fulfilled_by.WFSFulfilled span {
  background-color: #0070ff;
  border-radius: 3px;
  color: #fff;
  font-size: 10px;
  padding: 2px 5px;
  display: inline-block;
  font-weight: 700;
}
.order_item_box {
  border-bottom: 1px solid #ccc;
  padding: 10px 10px 10px 10px;
  background: #f9f9f9;
  text-align: left;
  font-size: 12px;
  line-height: 16px;
}
.order_item_box p {
  margin: 0 0 5px 0;
}
.table_list_box{
    padding: 10px 0;
}
</style>
<?php
                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                $no_of_records_per_page = 100;
                $offset = ($pageno-1) * $no_of_records_per_page; 
                ?>
                
                <div class="recently_view">
                    <div class="orders_list">
                    <?php
                    $user_id = $_SESSION['user_id'];

                    $check_users  = "SELECT * FROM users WHERE id=".$user_id."";
                    $users = mysqli_query($conn, $check_users);
                    $row_users = $users->fetch_assoc();
                    if($row_users && $row_users['type']==1){
                      ?>
                      <form class="filter_orders">
                            <!-- <div class="input_box">
                                <label>Product Name</label>
                                <input type="text" name="ProductName" placeholder="Product Name" value="">
                            </div> -->
                            <div class="input_box">
                                <label>User</label>
                                <select name="user_id">
                                    <option value="">--Select User--</option>
                                <?php
                                $suser_id = 0;
                                if(isset($_GET['user_id'])){
                                    $suser_id = $_GET['user_id'];
                                }
                                $get_orders = "SELECT * FROM users  ORDER BY id desc"; 

                                $get_orders_query = mysqli_query($conn, $get_orders);

                                if(mysqli_num_rows($get_orders_query) > 0){
                                    $k=0;
                                    while($order = $get_orders_query->fetch_assoc()) {
                                        $selected = '';
                                        if($suser_id==$order['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option value="<?php echo $order['id']; ?>"><?php echo $order['fullname']; ?></option>
                                        <?php
                                    }
                                }


                                ?>
                                </select>
                                
                            </div>
                            <div class="input_box">
                                <button>Apply</button>
                            </div>
                        </form>
                      <?php
                    }
                    ?> 
                        
                        <div class="table_list_outer orders_list">
                            <div class="table_list_box table_list_heading">
                                <div class="box">
                                    #
                                </div>
                                <div class="box">
                                    Message
                                </div>
                                <div class="box">
                                    Date Created
                                </div>
                            </div>

                            
                            <?php


                            $filter_code = '';


                            if($row_users && $row_users['type']==1){
                                if(isset($_GET['user_id'])){
                                    $suser_id = $_GET['user_id'];
                                    if(!empty($suser_id)){
                                        $filter_code.= " WHERE user_id=".$suser_id;
                                    }
                                }
                            }else{
                                $filter_code.= " WHERE user_id=".$user_id;
                            }


                            

                            $get_orders_count = "SELECT * FROM notifications $filter_code ORDER BY id desc";
                            $get_orders_count_query = mysqli_query($conn, $get_orders_count);
                            $total_pages = ceil(mysqli_num_rows($get_orders_count_query)/$no_of_records_per_page);


                            

                            $get_orders = "SELECT * FROM notifications $filter_code ORDER BY id desc LIMIT $offset, $no_of_records_per_page;"; 

                            $get_orders_query = mysqli_query($conn, $get_orders);

                            if(mysqli_num_rows($get_orders_query) > 0){
                                $k=0;
                                while($order = $get_orders_query->fetch_assoc()) {
                                    $k++;
                                    $order_id = $order['id'];
                            ?>
                            <div class="table_list_box">
                                <div class="box">
                                    <?php echo $k+($no_of_records_per_page*($pageno-1)); ?>
                                </div>
                                <div class="box">
                                    <?php echo $order['message']; ?>
                                    
                                </div>
                                <div class="box">
                                    <?php echo date("m-d-Y H:i", $order['date_created']);; ?>
                                    
                                </div>
                                
                            </div>
                            <?php  } } ?>
                            
                        </div>
                        <div class="pagination">
                            <ul>
                                <?php
                                $other_link = '';
                                

                                if($pageno>1){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/notifications.php?pageno=<?php echo $pageno-1; ?><?php echo $other_link; ?>">Prev</a></li>
                                    <?php
                                }

                                for($i=1; $i<=$total_pages; $i++){
                                    $active = '';
                                    if($pageno==$i){
                                        $active = 'active';
                                    }

                                    $prev_2nbr = $pageno-2;
                                    $next_2nbr = $pageno+2;

                                    if($i<$prev_2nbr || $i>$next_2nbr){
                                        continue;
                                    }


                                    
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/notifications.php?pageno=<?php echo $i; ?><?php echo $other_link; ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }

                                if($pageno<$total_pages){
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="/notifications.php?pageno=<?php echo $pageno+1; ?><?php echo $other_link; ?>">Next</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <script>
    $(window).scroll(function() {
       if($(this).scrollTop() > 240) {
         $(".table_list_box.table_list_heading").addClass("fixed");
       }else{
        $(".table_list_box.table_list_heading").removeClass("fixed");
       }
    });
</script>
<?php
include('dashboard_footer.php');
?>